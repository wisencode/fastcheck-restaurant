<?php 
	require_once('../datastation/config.php');
	require_once('../datastation/bones/Helper.php');
	Helper::loadBones(['Order', 'Restaurant']);

	$conn->dieOnError = FALSE;
	$hash = Helper::decrypt($_REQUEST['token']);
	// echo Helper::encrypt('1||133||FC5GAMI0095F1||fcp');
	// exit;
	$doFurtherCheck = true;
	$pageTitle = 'FC Card Payment';

	if (empty($hash)) {
		$doFurtherCheck = false;
	} else {
		$hashWith = explode('||', $hash);
		if (count($hashWith) < 4) {
			$doFurtherCheck = false;
		}
	}

	if ($doFurtherCheck) {
		$restaurant_id = $hashWith[0];
		$order_id = $hashWith[1];
		$fc_eCard_no = $hashWith[2];
		$linkSuffix = $hashWith[3];
		if (empty($restaurant_id) || empty($order_id) || empty($fc_eCard_no) || !is_numeric($restaurant_id) || !is_numeric($order_id) || empty($linkSuffix) || $linkSuffix != 'fcp') {
			$response = [
				'status' => false,
				'message' => MESSAGES['BROKEN_LINK'],
				'description' => ''
			];
		} else {
			$order = new Order();
			$orderWith = $order->statusByOrderId($restaurant_id, $order_id);
			if (!empty($orderWith) && $orderWith['order_status'] == 'pending') {
				$sql = "SELECT `ecard_no` FROM `".TABLES['fc_ecard']."` WHERE `ecard_no` = '".$fc_eCard_no."' AND `restaurant_id` = ".$restaurant_id;
				if ($conn->query($sql)->numRows() > 0) {
					$response = [
						'status' => true,
						'order' => $order->orderInfo($orderWith['id']),
						'restaurant' => (new Restaurant())->get($restaurant_id)
					];
				} else {
					$response = [
						'status' => false,
						'message' => MESSAGES['UNAUTHORISED_ACCESS'],
						'description' => 'Try again with your link and if you still have any concern then you can contact to us.',
					];
				}
			} else {
				$response = [
					'status' => false,
					'message' => 'This link is already expired',
					'description' => 'Possible Reason: Order associated with this link has been already completed or cancelled'
				];
			}
		}
	} else {
		$response = [
			'status' => false,
			'message' => MESSAGES['BROKEN_LINK'],
			'description' => ''
		];
	}

	if ($response['status'] == true) {
		$content = Helper::loadTemplate('payments', 'fc-card-pay-proceed.php', [
			'order' => $response['order'],
			'restaurant' => $response['restaurant'],
			'fc_eCard_no' => $fc_eCard_no,
			'pageTitle' => 'FC ECard Payment Request'
		]);
		echo $content;
	} else {
		$content = Helper::loadTemplate('payments', 'fc-card-pay-link-error.php', [
			'error' => $response['message'],
			'description' => $response['description'] ?? MESSAGES['OOPS'],
			'pageTitle' => 'FC ECard Payment Error'
		]);
		echo $content;
	}

	$conn->dieOnError = TRUE;
?>