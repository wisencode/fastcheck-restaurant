<?php

	include_once(__DIR__."/../datastation/config.php");
	require_once(__DIR__."/../datastation/".BONES_DIR."Helper.php");

	$webhook_data = file_get_contents('php://input');
	$response = json_decode($webhook_data);
	Helper::log($response, "RZPAY-PAYMENT-LINK");

	if (!empty($response->contains) && in_array('payment_link', $response->contains) && $response->event == 'payment_link.paid') {
		$rzpPaymentLinkId = $response->payload->payment_link->entity->id;
		$rzpPaymentLinkOrderId = $response->payload->payment_link->entity->order_id;
		if (!empty($rzpPaymentLinkId) && $response->payload->payment_link->entity->status == 'paid') {
			$customer_email = $response->payload->payment_link->entity->customer->email;
			$customer_contact = $response->payload->payment_link->entity->customer->contact;
			$sql = "UPDATE `orders` SET `order_status` = 'approved', `rzpOrder_id` = '".$rzpPaymentLinkOrderId."' WHERE `rzpPaymentLink_Id` = '".$rzpPaymentLinkId."'";
			Helper::log($sql, "RZPAY-PAYMENT-LINK-QUERY");
			$conn->query($sql);
			$sql = "SELECT `id`, `restaurant_id` FROM `".TABLES['order']."` WHERE `rzpPaymentLink_Id` = '".$rzpPaymentLinkId."' LIMIT 1";
			$restaurant = $conn->query($sql)->fetchArray();
			if(!empty($restaurant)) {
				addCustomerToRestaurant([
					'order_id' => $restaurant['id'],
					'restaurant_id' => $restaurant['restaurant_id'],
					'customer_email' => $customer_email,
					'customer_contact' => $customer_contact,
				]);
			}
		}
	}

	function addCustomerToRestaurant($customer) {
		global $conn;
		$sql = "SELECT `id` FROM `".TABLES['customer']."` WHERE `email` = '".$customer['customer_email']."' OR `phone_number` = '".$customer['customer_contact']."' LIMIT 1";
		$existsCustomer = $conn->query($sql)->fetchArray();
		if(!empty($existsCustomer)) {
			$sql = "UPDATE `".TABLES['customer']."` SET `email` = '".$customer['customer_email']."', `phone_number` = '".$customer['customer_contact']."' WHERE `id` = ".$existsCustomer['id'];
			$conn->query($sql);
			setCustomerToOrder($customer['order_id'], $existsCustomer['id']);
		} else {
			$sql = "INSERT INTO `".TABLES['customer']."`(`email`, `phone_number`, `restaurant_id`) VALUES('".$customer['customer_email']."', '".$customer['customer_contact']."', '".$customer['restaurant_id']."')";
			$conn->query($sql);
			setCustomerToOrder($customer['order_id'], $conn->lastInsertID());
		}
	}

	function setCustomerToOrder($orderId, $customerId) {
		global $conn;
		$sql = "UPDATE `".TABLES['order']."` SET `customer_id` = ".$customerId." WHERE `id` = ".$orderId;
		$conn->query($sql);	
	}