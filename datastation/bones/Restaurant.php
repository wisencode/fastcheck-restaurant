<?php

	class Restaurant {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['restaurant'];
			$this->loginTable = TABLES['restaurant_login'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function login($data) {
			Helper::log($data, 'login-attempt');
			$data['username'] = $this->conn->realEscape($data['username']);
			$data['password'] = $this->conn->realEscape($data['password']);
			$sql = 'SELECT `restaurant_id`, `role`, `rl`.`status` as res_login_status, `res`.`status` as res_status, `res`.`credentials` FROM '.$this->loginTable.' rl LEFT JOIN `'.$this->table.'` res ON `res`.id = `rl`.restaurant_id WHERE `username` = "'.$data['username'].'" AND `password` = "'.$data['password'].'" LIMIT 1';
			$result = $this->conn->query($sql)->fetchArray();
			if(!empty($result)) {
				if ($result['res_status'] == 1) {
					if ($result['res_login_status'] == 1) {
						$_SESSION['restaurant'] = Helper::encrypt($result['restaurant_id']);
						$_SESSION['accessor'] = Helper::encrypt("restaurant-" . $result['restaurant_id']);
						$_SESSION['accessor_role'] = $result['role'];
						$credentials = $result['credentials'];
						$pusherApiKey = null;
						$fcEchoSecret = null;
						if (!empty($credentials)) {
							$credentials = json_decode($credentials);
							foreach($credentials as $as => $credentialFor) {
								$_SESSION[$as] = [];
								foreach($credentialFor as $key => $value) {
									if ($as == 'pusher' && $key == 'api_key') {
										$pusherApiKey = $value;
									}
									if ($as == 'fc_echo' && $key == 'secret') {
										$fcEchoSecret = $value;
									}
									$_SESSION[$as][$key] = Helper::encrypt($value);
								}
							}
						}
						return [
							'status' => true,
							'restaurant_id' => $result['restaurant_id'],
							'pusher_api_key' => (EVENT_HANDLER == 'PUSHER') ? $pusherApiKey : null,
							'fc_echo_secret' => (EVENT_HANDLER == 'FC-ECHO') ? md5($fcEchoSecret) : null,
							'role' => $result['role'],
							'event_handler' => EVENT_HANDLER
						];
					} else {
						return [
							'status' => false,
							'message' => 'Your account has been deactivated by the system'
						];
					}
				} else {
					return [
						'status' => false,
						'message' => 'Your restaurant account has been deactivated by us. Kindly contact us on '.APP_POWERED_BY['CONTACT_NUMBER'].' or email us at '.APP_POWERED_BY['EMAIL']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => 'Invalid username/password provided'
				];
			}
		}

		public function get($restaurant_id) {
			$restaurant = $this->conn->query('SELECT restaurant.*, currency.symbol as currency_symbol, currency.code as currency_code FROM '.$this->table.' restaurant LEFT JOIN `'.TABLES["currency"].'` currency ON currency.id = restaurant.currency_id WHERE `restaurant`.`id` = '.$restaurant_id)->fetchArray();
			$restaurant['payment_methods'] = $this->paymentMethods($restaurant_id);
			return $restaurant;
		}

		public function getSystemAccounts($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			if (!Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['NO_ACCESS']
				];
			}
			Helper::log($request, 'get-system-accounts');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$sql = "SELECT `id`, `username`, `role`, `password` FROM `".$this->loginTable."` WHERE `restaurant_id` = ".$restaurant_id;
			$accounts = $this->conn->query($sql)->fetchAll();
			foreach ($accounts as &$account) {
				$account['id'] = Helper::encrypt('sys-account-'.$account['id']);
			}
			return [
				'status' => true,
				'accounts' => $accounts
			];
		}

		function changeSystemAccountPassword($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			if (!Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['NO_ACCESS']
				];
			}
			Helper::log($request, 'change-system-account-password');
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$sysAccountHash = $this->conn->realEscape($request['sysAccountHash']);
			$newPassword = $this->conn->realEscape($request['newPassword']);
			$sysAccountId = str_replace("sys-account-", "", Helper::decrypt($sysAccountHash));

			if (empty($newPassword) || strlen($newPassword) < 8) {
				return [
					'status' => false,
					'message' => 'Password can not be empty and must have minimum 8 charcters'
				];
			} 

			$sql = "UPDATE `".$this->loginTable."` SET `password` = '".$newPassword."' WHERE `id` = ".$sysAccountId." AND `restaurant_id` = ". $restaurant_id;
			if ($this->conn->query($sql)) {
				return [
					'status' => true,
					'message' => "Account password changed successfully"
				];
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['OOPS']
				];
			}
		}

		public function deleteSystemAccount($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			if (!Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['NO_ACCESS']
				];
			}
			Helper::log($request, 'deelete-system-account');
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$sysAccountHash = $this->conn->realEscape($request['sysAccountHash']);
			$sysAccountId = str_replace("sys-account-", "", Helper::decrypt($sysAccountHash));
			$sql = "DELETE FROM `".$this->loginTable."` WHERE `id` = ".$sysAccountId." AND `restaurant_id` = ".$restaurant_id;
			if ($this->conn->query($sql)) {
				return [
					'status' => true,
					'message' => "Account deleted successfully from the system",
					"id" => $sysAccountHash
				];
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['OOPS']
				];
			}
		}

		public function getMenu($request) {
			$hash = $this->conn->realEscape($request['hash']);
			$area = (!empty($request['area'])) ? $this->conn->realEscape($request['area']) : '';
			if (empty($hash)) {
				return [
					'status' => false,
					'message' => 'Provide valid restaurant hash to access the menu'
				];
			}
			$restaurant_id = str_replace("fastcheck-rest-", "", Helper::decrypt($hash));
			if (empty($restaurant_id) || !is_numeric($restaurant_id) || $restaurant_id <= 0) {
				return [
					'status' => false,
					'message' => 'Provide valid restaurant hash to access the menu'
				];	
			}
			$sql = "SELECT `id`, `name` FROM `".TABLES['area_category']."` WHERE `restaurant_id` = ".$restaurant_id." AND `status` = 'active'";
			if (!empty($area)) {
				$area_id = $this->conn->realEscape(Helper::decrypt($area));
				if (is_numeric($area_id) && $area_id > 0) {
					$sql.= " AND `id` = ".$area_id;
				}
			}
			return [
				'status' => true,
				'restaurant' => $this->get($restaurant_id),
				'areas' => $this->conn->query($sql)->fetchAll(),
				'categories' => (new Category())->allWithBaseInfo($restaurant_id),
				'items' => (new Item())->allWithBaseInfo($restaurant_id)
			];
		}

		public function paymentMethods($restaurant_id) {
			$sql = "SELECT `rpm`.`payment_method_id` as `id`, `pm`.`payment_method` FROM `".TABLES['restaurant_payment_methods']."` rpm LEFT JOIN `".TABLES['payment_methods']."` pm ON pm.id = rpm.`payment_method_id` WHERE `rpm`.`status` = '1' AND `pm`.`status` = '1' AND rpm.`restaurant_id`=".$restaurant_id;
			return $this->conn->query($sql)->fetchAll();
		}

		public function generateFCECard($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'generate-fc-ecard');

			$valid_for = $this->conn->realEscape($request['valid_for']);
			$display_name = $this->conn->realEscape($request['display_name']);
			$linked_phone_number = $this->conn->realEscape($request['linked_phone_number']);
			$linked_email = $this->conn->realEscape($request['linked_email']);
			$initial_recharge = $this->conn->realEscape($request['initial_recharge']);
			$has_payment_verification = $this->conn->realEscape($request['has_payment_verification']);
			$has_payment_verification = ($has_payment_verification == true) ? 1 : 0;
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);

			if (!empty($display_name) && strlen($display_name) > 8) {
				return [
					'status' => false,
					'message' => 'Name on the card can not be longer than 8 characters'
				];
			}

			if (empty($linked_phone_number) && empty($linked_email)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			if (!empty($linked_email) && !Helper::isValidEmail($linked_email)) {
				return [
					'status' => false,
					'message' => 'Invalid email address provided'
				];	
			}
			if (empty($initial_recharge) || !is_numeric($initial_recharge)) {
				return [
					'status' => false,
					'message' => 'Card can not be filled with empty amount'
				];
			}

			$this->conn->beginTransaction();
			try {
				$sql = "INSERT INTO `".TABLES['fc_ecard']."`(`restaurant_id`, `current_balance`, `status`, `valid_for`, `linked_phone_number`, `linked_email`, `display_name`, `has_payment_verification`) VALUES('".$restaurant_id."', ".$initial_recharge.", 'active', '".$valid_for."', '".$linked_phone_number."', '".$linked_email."', '".$display_name."', ".$has_payment_verification.")";
				if($this->conn->query($sql)) {
					$eCardID = $this->conn->lastInsertID();
					if($eCardID > 0) {
						$fc_eCard_no = $this->generateFCECardNumber($eCardID, $restaurant_id);
						$sql = "UPDATE `".TABLES['fc_ecard']."` SET `ecard_no` = '".$fc_eCard_no."' WHERE `id` = ".$eCardID;
						if($this->conn->query($sql)) {
							$sql = "INSERT INTO `".TABLES['fc_ecards_transactions_log']."`(`fc_ecard_id`, `transaction_type`, `transaction_amount`, `prev_balance`, `restaurant_id`) VALUES(".$eCardID.", 'INTRCHRG', ".$initial_recharge.", 0, ".$restaurant_id.")";
							$this->conn->query($sql);
							$this->conn->commit();
							$sql = "SELECT `ecard_no`, `current_balance`, `restaurant_id`, `valid_for`, `created_at`, `display_name` FROM `".TABLES['fc_ecard']."` WHERE `id` = ".$eCardID." LIMIT 1";
							$fc_eCard = $this->conn->query($sql)->fetchArray();
							$fc_eCard['created_at'] = date('d F Y', strtotime($fc_eCard['created_at']));
							return [
								'status' => true,
								'FCECard' => $fc_eCard
							];
						} else {
							return [
								'status' => false,
								'message' => 'FC eCard number '.$fc_eCard_no.' is not valid'
							];	
						}
					} else {
						return [
							'status' => false,
							'message' => 'FC eCardID is not valid'
						];
					}
				} else {
					return [
						'status' => false,
						'message' => 'FC eCard details are not valid'
					];
				}
			} catch(Exception $exception) {
				Helper::log(array_merge($request, ['err' => $exception->getMessage()]), "generate-fc-ecard-err");
				$this->conn->rollback();
				return [
					'status' => false,
					'message' => MESSAGES['OOPS']
				];
			}
		}

		public function checkFCECardPaymentVerification($data) {
			if (!Helper::isRequestValidForRestaurant($data) || empty($data['FCECardNumber'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($data, 'check-fc-ecard-payment-verification');
			$data['FCECardNumber'] = $this->conn->realEscape($data['FCECardNumber']);
			$sql = "SELECT `status`, `has_payment_verification` FROM `".TABLES['fc_ecard']."` WHERE `ecard_no` = '".$data['FCECardNumber']."' LIMIT 1";
			$fc_eCard = $this->conn->query($sql);
			if($fc_eCard->numRows() == 0) {
				return [
					'status' => false
				];
			} else {
				$fc_eCard = $fc_eCard->fetchArray();
				if ($fc_eCard['status'] == 'active') {
					if ($fc_eCard['has_payment_verification'] == 1) {
						return [
							'status' => true
						];
					} else {
						return [
							'status' => false
						];
					}
				} else {
					return [
						'status' => false
					];	
				}
			}
		}

		public function payWithFCECard($data) {
			if (!Helper::isRequestValidForRestaurant($data) || empty($data['order_id'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($data, 'pay-with-fc-ecard');

			$order_id = $this->conn->realEscape($data['order_id']);
			$data['FCECardNumber'] = $this->conn->realEscape($data['FCECardNumber']);
			$pay_link = (!empty($data['pay_link'])) ? $this->conn->realEscape($data['pay_link']) : false;
			$sql = "SELECT `id`, `restaurant_id`, `current_balance`, `valid_for`, `created_at`, `status`, `has_payment_verification` FROM `".TABLES['fc_ecard']."` WHERE `ecard_no` = '".$data['FCECardNumber']."' LIMIT 1";
			$fc_eCard = $this->conn->query($sql);
			if($fc_eCard->numRows() > 0) {
				$fc_eCard = $fc_eCard->fetchArray();
				if($fc_eCard['restaurant_id'] != $data['restaurant_id']) {
					if (!$pay_link) {
						$this->deleteTempOrder($order_id);
					}
					return [
						'status' => false,
						'message' => 'Payment Error: This FCECardNumber does not belong to this restaurant' 
					];
				} else {
					$checkFCECardEligible = $this->checkFCECardEligible($fc_eCard, $data['orderAmount'], $data['currency']);
					if($checkFCECardEligible['status'] == true) {
						$updated_balance = $fc_eCard['current_balance'] - $data['orderAmount'];
						$this->conn->beginTransaction();
						try {
							$sql = "UPDATE `".TABLES['fc_ecard']."` SET `current_balance` = '".$updated_balance."' WHERE `id` = ".$fc_eCard['id'];
							if($this->conn->query($sql)) {
								$payment_method = (new PaymentMethod())->getWhere(['name' => 'fcECard']);
								if (!empty($payment_method)) {
									$payment_method_info = [
										'id' => $payment_method['id'],
										'payment_method' => $payment_method['payment_method'],
										'fcECardId' => $fc_eCard['id'],
										'fcECardNumber' => $data['FCECardNumber'],
									];
									$sql = "UPDATE `".TABLES['order']."` SET `payment_method_info` = '".json_encode($payment_method_info)."', `order_status` = 'approved' WHERE `id` = ".$order_id;
									$this->conn->query($sql);
									$sql = "INSERT INTO `".TABLES['fc_ecards_transactions_log']."`(`fc_ecard_id`, `transaction_type`, `order_id`, `transaction_amount`, `prev_balance`, `restaurant_id`) VALUES(".$fc_eCard['id'].", 'ORDER', ".$order_id.", ".$data['orderAmount'].", ".$fc_eCard['current_balance'].", ".$fc_eCard['restaurant_id'].")";
									if ($this->conn->query($sql)) {
										$fcECardTransId = $this->conn->lastInsertID();
									}
									$this->conn->commit();
									return [
										'status' => true,
										'message' => 'Payment successfully done from FCECard. Your current balance is '.number_format($updated_balance, 2).' '.$data['currency'],
										'fcECardNumber' => $data['FCECardNumber'],
										'fcECardTransId' => $fcECardTransId
									];
								} else {
									if (!$pay_link) {
										$this->deleteTempOrder($order_id);
									}
									return [
										'status' => false,
										'message' => 'Payment Error: payment method not supported for now'
									];
								}
							} else {
								if (!$pay_link) {
									$this->deleteTempOrder($order_id);
								}
								return [
									'status' => false,
									'message' => 'Payment Error: Something went wrong!'
								];
							}
						} catch (Exception $exception) {
							Helper::log($exception->getMessage(), "pay-with-fc-ecard-".$fc_eCard['ecard_no']."-err");
							$this->conn->rollback();
							return [
								'status' => false,
								'message' => 'Payment Error: Something went wrong!'
							];
						}
					} else {
						if (!$pay_link) {
							$this->deleteTempOrder($order_id);
						}
						return $checkFCECardEligible;
					}
				}
			} else {
				$this->deleteTempOrder($order_id);
				return [
					'status' => false,
					'message' => 'Payment Error: FCECardNumber does not exists' 
				];
			}
		}

		function deleteTempOrder($order_id) {
			Helper::log($order_id, 'delete-temp-order');
			$sql = "DELETE FROM `".TABLES['order']."` WHERE `id` = ".$order_id;
			$this->conn->query($sql);
		}

		function generateFCECardNumber($fc_eCardId, $restaurant_id) {
			$length = 5;
		    $FCECardNumber = 'FC';
		    $chunks = array_merge(range(0, 9), range('a', 'z'));
		    for ($i = 0; $i < $length; $i++) {
		        $FCECardNumber.= strtoupper($chunks[array_rand($chunks)]);
		    }
		    $FCECardNumber.= str_pad($fc_eCardId, 4, '0', STR_PAD_LEFT);
		    $FCECardNumber.= 'F'.$restaurant_id;
		    return $FCECardNumber;
		}

		function checkFCECardEligible($fc_eCard, $orderAmount, $currency = 'INR') {
			if($fc_eCard['status'] == 'expired') {
				return [
					'status' => false,
					'message' => 'Payment Error: FCECardNumber already expired'
				];
			} else if($fc_eCard['status'] == 'inactive') {
				return [
					'status' => false,
					'message' => 'Payment Error: FCECardNumber is currently inactive'
				];
			} else if($fc_eCard['status'] == 'active') {
				$now = date('Y-m-d H:i:s');
				$created = $fc_eCard['created_at'];
				$needToCheckValidFor = false;
				if(!$needToCheckValidFor) {
					if($fc_eCard['current_balance'] < $orderAmount) {
						return [
							'status' => false,
							'message' => 'Payment Error: FCECardNumber has only '.$fc_eCard['current_balance'].' '.$currency.' as balance'
						];
					} else {
						return [
							'status' => true
						];
					}
				} else {
					$days = $this->diffDays($created, $now);
					if($days > ($fc_eCard['valid_for'] * 31)) {
						return [
							'status' => false,
							'message' => 'Payment Error: FCECardNumber already expired'
						];
					} else {
						if($fc_eCard['current_balance'] < $orderAmount) {
							return [
								'status' => false,
								'message' => 'Payment Error: FCECardNumber has only '.$fc_eCard['current_balance'].' '.$currency.' as balance'
							];
						} else {
							return [
								'status' => true
							];
						}
					}
				}
			}
		}

		function saveProfile($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-restaurant-profile');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$settings = json_decode($request['settings']);
			$errors = [];
			if (trim($settings->name) == '') {
				$errors[] = "Restaurant name can not be blank";
			}
			if (trim($settings->currency_id) == '') {
				$errors[] = "Restaurant currency can not be blank";
			}
			if (!is_numeric(trim($settings->currency_id))) {
				$errors[] = "Invalid currency for restaurant";
			}
			if (empty($settings->order_book_type) || !in_array($settings->order_book_type, ['FNCLY', 'CLNDRY'])) {
				$errors[] = "Restaurant must have valid order book type";
			}
			if (empty($settings->restaurant_food_types)) {
				$errors[] = "Atleast one food type must be set for the restaurant";
			}
			if (!empty($settings->invoice_bottom_line) && strlen($settings->invoice_bottom_line) > 80) {
				$errors[] = "Maximum 80 characters are allowed for invoice bottom line";
			}
			if (count($errors) > 0) {
				return [
					'status' => false,
					'message' => implode(',', $errors)
				];
			} else {
				$restaurant_name = $this->conn->realEscape($settings->name);
				$restaurant_currency_id = $this->conn->realEscape($settings->currency_id);
				$restaurant_contact_number = $this->conn->realEscape($settings->contact_number);
				$restaurant_address = $this->conn->realEscape($settings->address);
				$order_book_type = $this->conn->realEscape($settings->order_book_type);
				$restaurant_food_types = $this->conn->realEscape($settings->restaurant_food_types);
				$invoice_bottom_line = $this->conn->realEscape($settings->invoice_bottom_line);
				$gst_number = $this->conn->realEscape($settings->gst_number);

				$this->conn->beginTransaction();
				try {
					$sql = "UPDATE `".$this->table."` SET `name` = '".$restaurant_name."', `currency_id` = ".$restaurant_currency_id.", `contact_number` = '".$restaurant_contact_number."', `address` = '".$restaurant_address."', `order_book_type` = '".$order_book_type."', `food_type` = '".$restaurant_food_types."', `gst_number` = '".$gst_number."', `invoice_bottom_line` = '".$invoice_bottom_line."' WHERE `id` = ".$restaurant_id;
					if ($this->conn->query($sql)) {
						$event = [
							'restaurant_id' => $restaurant_id
						];
						if (!empty($this->eventHandler)) {
							$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-RESTAURANT'], $event);
						}
						$this->conn->commit();
						return [
							'status' => true,
							'message' => 'Restaurant profile settings saved successfully',
						];
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];
					}
				} catch (Exception $exception) {
					Helper::log(array_merge($request, ["err" => $exception->getMessage]), "save-restaurant-profile-err");
					$this->conn->rollback();
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			}
		}

		function uploadLogo($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log(array_merge($request, $_FILES), "upload-rest-logo");
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$tmpFilePath = $_FILES['profile_restaurant_logo']['tmp_name'];
			$extension = pathinfo($_FILES['profile_restaurant_logo']['name'], PATHINFO_EXTENSION);
			$allowed_file_types = ['png', 'jpg', 'jpeg', 'gif', 'bmp'];
			if (!in_array($extension, $allowed_file_types)) {
				return [
					'status' => false,
					'message' => 'Restaurant Logo: Only these formats are allowed : '.implode(', ', $allowed_file_types)
				];
			}
			$logoName = $this->conn->realEscape($_FILES['profile_restaurant_logo']['name']);

			if ($tmpFilePath != "") {
				$logoFolder = __DIR__ . "/../../assets/img/uploads/" . $restaurant_id;
				if (!file_exists($logoFolder)) {
					mkdir($logoFolder);
				}
				$logoFolder.= '/logo/';
				if (!file_exists($logoFolder)) {
					mkdir($logoFolder);
				}
				$currentLogos = glob($logoFolder . "/*");
				foreach($currentLogos as $logo) {
				  if(is_file($logo)) {
				    unlink($logo);
				  }
				}
				$newFilePath = $logoFolder . $logoName;
				if (move_uploaded_file($tmpFilePath, $newFilePath)) {
					$this->conn->beginTransaction();
					try {
						$sql = "UPDATE `".$this->table."` SET `logo` = '".$logoName."' WHERE `id` = ". $restaurant_id;
						if ($this->conn->query($sql)) {
							$event = [
								'restaurant_id' => $restaurant_id,
								'logo' => $logoName
							];
							if (!empty($this->eventHandler)) {
								$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-LOGO'], $event);
							}
							$this->conn->commit();
							return [
								'status' => true,
								'message' => 'Logo updated successfully',
								'logo' => $logoName
							];
						} else {
							return [
								'status' => false,
								'message' => MESSAGES['OOPS']
							];	
						}
					} catch (Exception $exception) {
						Helper::log($exception->getMessage(), "upload-rest-logo-err");
						$this->conn->rollback();
						return [
							'status' => false,
							'message' => 'Error occured while uploading the logo'
						];
					}
				} else {
					return [
						'status' => false,
						'message' => 'Error occured while uploading the logo'
					];
				}
			} else {
				return [
					'status' => false,
					'message' => 'Please upload valid file for logo'
				];
			}
		}

		function removeRestaurantLogo($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			if (!Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['NO_ACCESS']
				];
			}
			Helper::log($request, "remove-rest-logo");
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$logoFolder = __DIR__ . "/../../assets/img/uploads/" . $restaurant_id . "/logo/";
			$currentLogos = glob($logoFolder . "/*");
			if (!file_exists($logoFolder) || count($currentLogos) == 0) {
				return [
					'status' => false,
					'message' => 'Logo does not exists'
				];	
			}
			foreach($currentLogos as $logo) {
			  if(is_file($logo)) {
			    unlink($logo);
			  }
			}
			$this->conn->beginTransaction();
			try {
				$sql = "UPDATE `".$this->table."` SET `logo` = NULL WHERE `id` = ". $restaurant_id;
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
						'logo' => null
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-LOGO'], $event);
					}
					$this->conn->commit();
					return [
						'status' => true,
						'message' => 'Logo removed successfully',
						'logo' => null
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];	
				}
			} catch (Exception $exception) {
				Helper::log($exception->getMessage(), "remove-rest-logo-err");
				$this->conn->rollback();
				return [
					'status' => false,
					'message' => 'Error occured while removing the logo'
				];
			}
		}

		function getFCECardList($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$options = json_decode($request['options']);
			$sql = "SELECT *, date_format(date(`created_at`), '%D %M %Y') as created_at FROM `".TABLES['fc_ecard']."`";
			$where = " WHERE `restaurant_id` = ".$restaurant_id;
			if (!empty($options)) {
				if (!empty($options->FCECardNumber)) {
					$where.= " AND `ecard_no` LIKE '%".$this->conn->realEscape($options->FCECardNumber)."%'";
				}
				if (!empty($options->FCECardName)) {
					$where.= " AND `display_name` LIKE '".$this->conn->realEscape($options->FCECardName)."%'";
				}
				if (!empty($options->FCECardLinkedPhoneNo)) {
					$where.= " AND `linked_phone_number` = '".$this->conn->realEscape($options->FCECardLinkedPhoneNo)."'";
				}
				if (!empty($options->FCECardLinkedEmail)) {
					$where.= " AND `linked_email` = '".$this->conn->realEscape($options->FCECardLinkedEmail)."'";
				}
				if (!empty($options->FCECardCreatedOnFrom)) {
					$where.= " AND date(`created_at`) >= '".$this->conn->realEscape($options->FCECardCreatedOnFrom)."'";
				}
				if (!empty($options->FCECardCreatedOnTo)) {
					$where.= " AND date(`created_at`) <= '".$this->conn->realEscape($options->FCECardCreatedOnTo)."'";
				}
			}
			$sql.= $where;
			$sql.= " ORDER BY `id` DESC";
			
			return $this->conn->query($sql)->fetchAll();
		}

		function getFCECardTransactionsReport($request) {
			if (!Helper::isRequestValidForRestaurant($request) && empty($request['fcECardNumber'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$fcECardNumber = $this->conn->realEscape($request['fcECardNumber']);
			// $matchAs = '%"fcECardNumber":"'.$fcECardNumber.'"}';
			// $sql = "SELECT `o`.`order_no`, `o`.`order_amount`, date_format(date(`o`.`created_at`), '%D %M %Y') as created_at, `t`.`table_no` FROM `".TABLES['order']."` o LEFT JOIN `".TABLES['table']."` t ON `t`.`id` = `o`.`table_id` WHERE `o`.`restaurant_id` = ".$restaurant_id." AND `o`.`payment_method_info` LIKE '".$matchAs."'";
			$sql = "SELECT `id` FROM `".TABLES['fc_ecard']."` WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."' LIMIT 1";
			$fc_eCardInfo = $this->conn->query($sql)->fetchArray();
			$sql = "SELECT `transaction_type`, `transaction_amount`, `prev_balance`, date_format(date(`tr`.`created_at`), '%D %M %Y') as created_at, `o`.`order_no` FROM `".TABLES['fc_ecards_transactions_log']."` tr LEFT JOIN `".TABLES['fc_ecard']."` fc ON `fc`.`id` = `tr`.`fc_ecard_id` LEFT JOIN `".TABLES['order']."` o ON `o`.`id` = `tr`.`order_id` WHERE `tr`.`fc_ecard_id` = '".$fc_eCardInfo['id']."' AND `tr`.`restaurant_id` = ".$restaurant_id." ORDER BY `tr`.`id` DESC";
			return $this->conn->query($sql)->fetchAll();
		}

		function getFCECardRechargeReport($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$options = json_decode($request['options']);
			$where = "WHERE `tr`.`restaurant_id` = ".$restaurant_id." AND `tr`.`transaction_type` = 'RCHRG'";
			if (!empty($options)) {
				if (!empty($options->todayOnly) && $options->todayOnly == true) {
					$where.= " AND date(`tr`.`created_at`) = '".date('Y-m-d')."'";
				}
			}
			$sql = "SELECT `transaction_amount`, date_format(date(`tr`.`created_at`), '%D %M %Y') as created_at, `fc`.`ecard_no` FROM `".TABLES['fc_ecards_transactions_log']."` tr LEFT JOIN `".TABLES['fc_ecard']."` fc ON `fc`.`id` = `tr`.`fc_ecard_id` ".$where." ORDER BY `tr`.`id` DESC";
			return $this->conn->query($sql)->fetchAll();
		}

		function rechargeFCECard($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['fcECardNumber']) || empty($request['fcECardRechargeAmount'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'recharge-fc-ecard');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$fcECardNumber = $this->conn->realEscape($request['fcECardNumber']);
			$fcECardRechargeAmount = $this->conn->realEscape($request['fcECardRechargeAmount']);
			$sql = "SELECT `id`, `current_balance` FROM `".TABLES['fc_ecard']."` WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."' LIMIT 1";
			$result = $this->conn->query($sql);
			if ($result->numRows() > 0) {
				$fceCardInfo = $result->fetchArray();
				$this->conn->beginTransaction();
				try {
					$sql = "UPDATE `".TABLES['fc_ecard']."` SET `current_balance` = (`current_balance` + ".$fcECardRechargeAmount."), `status` = 'active' WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."'";
					if ($this->conn->query($sql)) {
						$sql = "INSERT INTO `".TABLES['fc_ecards_transactions_log']."`(`fc_ecard_id`, `transaction_type`, `transaction_amount`, `prev_balance`, `restaurant_id`) VALUES(".$fceCardInfo['id'].", 'RCHRG', ".$fcECardRechargeAmount.", ".$fceCardInfo['current_balance'].", ".$restaurant_id.")";
						if ($this->conn->query($sql)) {
							$fcECardTransId = $this->conn->lastInsertID();
						}
						$this->conn->commit();
						return [
							'status' => true,
							'fcECardTransId' => $fcECardTransId,
							'message' => 'Recharge of '.number_format($fcECardRechargeAmount, 2).' to FC eCard #'.$fcECardNumber." is successfully done"
						];
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];
					}	
				} catch(Exception $exception) {
					Helper::log($exception->getMessage(), "recharge-fc-ecard".$fcECardNumber."-err");
					$this->conn->rollback();
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
				// if ($this->conn->query($sql)) {
				// 	$sql = "INSERT INTO `".TABLES['fc_ecards_transactions_log']."`(`fc_ecard_id`, `transaction_type`, `transaction_amount`, `prev_balance`, `restaurant_id`) VALUES(".$result['id'].", 'RCHRG', ".$fcECardRechargeAmount.", ".$result['current_balance'].", ".$restaurant_id.")";
				// 	$this->conn->query($sql);
				// 	return [
				// 		'status' => true,
				// 		'message' => 'Recharge of '.number_format($fcECardRechargeAmount, 2).' to FC eCard #'.$fcECardNumber." is successfully done"
				// 	];
				// } else {
				// 	return [
				// 		'status' => false,
				// 		'message' => MESSAGES['OOPS']
				// 	];
				// }
			} else {
				return [
					'status' => false,
					'message' => 'FC eCard #'.$fcECardNumber.' does not exist in your system'
				];
			}
		}

		function getFCECardInfo($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['fcECardNumber'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$fcECardNumber = $this->conn->realEscape($request['fcECardNumber']);
			$sql = "SELECT `id`, `current_balance`, `ecard_no`, `linked_email`, `linked_phone_number`, `status` FROM `".TABLES['fc_ecard']."` WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."' LIMIT 1";
			$fcECard = $this->conn->query($sql)->fetchArray();
			if (!empty($fcECard) && !empty($fcECard['id'])) {
				return [
					'status' => true,
					'fcECard' => $fcECard
				];
			} else {
				return [
					'status' => false,
					'message' => 'FC eCard #'.$fcECardNumber.' does not exist in your system'
				];	
			}
		}

		function setFCECardStatus($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['fcECardNumber'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'set-fc-ecard-status');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$fcECardNumber = $this->conn->realEscape($request['fcECardNumber']);
			$fcECardStatus = $this->conn->realEscape($request['status']);
			$sql = "SELECT `id` FROM `".TABLES['fc_ecard']."` WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."' LIMIT 1";
			$result = $this->conn->query($sql);
			if ($result->numRows() > 0 && ($fcECardStatus == "active" || $fcECardStatus == "inactive")) {
				$this->conn->beginTransaction();
				try {
					$sql = "UPDATE `".TABLES['fc_ecard']."` SET `status` = '".$fcECardStatus."' WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."'";
					if ($this->conn->query($sql)) {
						$statusAck = ($fcECardStatus == "active") ? 'activated' : 'deactivated';
						$this->conn->commit();
						return [
							'status' => true,
							'message' => 'FC eCard #'.$fcECardNumber.' is successfully '.$statusAck
						];
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];
					}
				} catch (Exception $exception) {
					Helper::log($exception->getMessage(), "set-fc-ecard-status-".$fcECardNumber."-err");
					$this->conn->rollback();
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => 'FC eCard #'.$fcECardNumber.' does not exist in your system'
				];
			}
		}

		function setFCECardPaymentVerificationFlag($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['fcECardNumber'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'set-fc-ecard-payment-verification-flag');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$fcECardNumber = $this->conn->realEscape($request['fcECardNumber']);
			$fcECardPVFlag = $this->conn->realEscape($request['pvFlag']);
			$sql = "SELECT `id` FROM `".TABLES['fc_ecard']."` WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."' LIMIT 1";
			$result = $this->conn->query($sql);
			if ($result->numRows() > 0 && ($fcECardPVFlag == "0" || $fcECardPVFlag == "1")) {
				$this->conn->beginTransaction();
				try {
					$sql = "UPDATE `".TABLES['fc_ecard']."` SET `has_payment_verification` = ".$fcECardPVFlag." WHERE `restaurant_id` = ".$restaurant_id." AND `ecard_no` = '".$fcECardNumber."'";
					if ($this->conn->query($sql)) {
						$statusAck = ($fcECardPVFlag == "1") ? 'added' : 'removed';
						$this->conn->commit();
						return [
							'status' => true,
							'message' => 'Payment verification layer is '.$statusAck.' for FC eCard #'.$fcECardNumber
						];
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];
					}
				} catch (Exception $exception) {
					Helper::log($exception->getMessage(), "set-fc-ecard-payment-verification-flag-".$fcECardNumber."-err");
					$this->conn->rollback();
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => 'FC eCard #'.$fcECardNumber.' does not exist in your system'
				];
			}
		}

		function generateMenuQR($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['options'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'generate-restaurant-menu-qr');
			
			$options = json_decode($request['options']);
			$queryParams = 'hash='.Helper::encrypt('fastcheck-rest-'.$request["restaurant_id"]);
			if (!empty($options) && !empty($options->fieldRestMenuQRFor) && $options->fieldRestMenuQRFor != 'all' && is_numeric($options->fieldRestMenuQRFor) && $options->fieldRestMenuQRFor > 0) {
				$queryParams.= "&area=".Helper::encrypt($options->fieldRestMenuQRFor);
			}
			return [
				'status' => true,
				'message' => 'Menu is available for your restaurant',
				'menu_url' => Helper::protocol().Helper::host()."/services/menu.html?".$queryParams
			];
		}

		function diffDays($created) {
			$now = time();
			$your_date = strtotime($created);
			$datediff = $now - $your_date;
			return round($datediff / (60 * 60 * 24));
		}


	}