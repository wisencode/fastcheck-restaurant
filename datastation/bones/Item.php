<?php

	class Item {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['item'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function get($item_id) {
			$item = $this->conn->query('SELECT `id`, `name`, `picture`, `category_id`, `item_code`, `description`, `item_price_info` FROM '.$this->table. ' WHERE id = '.$item_id)->fetchArray();
			return $item;
		}

		public function all($restaurant_id) {
			$sql = 'SELECT `item`.`id`, `item`.`name`, `item`.`picture`, `item`.`category_id`, `item`.`item_code`, `item`.`description`, `item`.`item_price_info` FROM `'.TABLES['item'].'` item LEFT JOIN `'.TABLES["category"].'` category ON category.id = item.category_id WHERE `item`.`status` = 1 AND `category`.`restaurant_id` = '.$restaurant_id.' AND `category`.`status` = 1 ORDER BY `item`.`id` ASC';
			return $this->conn->query($sql)->fetchAll();
		}

		public function allWithBaseInfo($restaurant_id) {
			$sql = 'SELECT `item`.`name`, `item`.`item_price_info`, `item`.`category_id` FROM `'.TABLES['item'].'` item LEFT JOIN `'.TABLES["category"].'` category ON category.id = item.category_id WHERE `item`.`status` = 1 AND `category`.`restaurant_id` = '.$restaurant_id.' AND `category`.`status` = 1 ORDER BY `item`.`id` ASC';
			return $this->conn->query($sql)->fetchAll();
		}

		public function save($request) {
			if (empty($request['restaurant_id']) || empty($request['item'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-item');

			$item = json_decode($request['item']);
			$itemName = $this->conn->realEscape($item->name);
			$itemCode = $this->conn->realEscape($item->code);
			$itemDesc = $this->conn->realEscape($item->description);
			$category_id = $this->conn->realEscape($item->catId);
			$restaurant_id = $request['restaurant_id'];

			if (!empty($itemCode)) {
				$sql = "SELECT `item_code` FROM `items` WHERE `item_code` = '".$itemCode."' LIMIT 1";
				$itemCodes = $this->conn->query($sql)->numRows();
				if ($itemCodes > 0) {
					return [
						'status' => false,
						'message' => "Item code #".$itemCode.' already added in the system'
					];		
				}
			}

			$sql = "INSERT INTO `".$this->table."`(`name`, `description`, `category_id`, `item_code`, `status`) VALUES('".$itemName."', '".$itemDesc."', ".$category_id.", '".$itemCode."', 1)";
			$result = $this->conn->query($sql);

			if ($result) {
				$event = [
					'restaurant_id' => $restaurant_id,
					'category_id' => $category_id
				];
				if (!empty($this->eventHandler)) {
					$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ITEMS'], $event);
				}
				return [
					'status' => true,
					'message' => 'Item saved successfully',
					'item' => $this->get($this->conn->lastInsertID())
				];
			} else {
				return [
					'status' => false,
					'message' => 'Failed: Item was not saved successfully'
				];
			}
		}

		public function saveForOffice($request) {
			if (empty($request['restaurant_id']) || empty($request['item']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-item-from-office');

			$item = json_decode($request['item']);
			$itemName = $this->conn->realEscape($item->name);
			$itemCode = $this->conn->realEscape($item->code);
			$itemDesc = $this->conn->realEscape($item->description);
			$category_id = $this->conn->realEscape($item->catId);
			$restaurant_id = $request['restaurant_id'];

			$isUpdate = false;
			if ($item->id == 0) {
				if (!empty($itemCode)) {
					$sql = "SELECT `item_code` FROM `items` WHERE `item_code` = '".$itemCode."' LIMIT 1";
					$itemCodes = $this->conn->query($sql)->numRows();
					if ($itemCodes > 0) {
						return [
							'status' => false,
							'message' => "Item code #".$itemCode.' already added in the system'
						];		
					}
				}
				$sql = "INSERT INTO `".$this->table."`(`name`, `description`, `category_id`, `item_code`, `status`) VALUES('".$itemName."', '".$itemDesc."', ".$category_id.", '".$itemCode."', 1)";
			} else {
				if (!empty($itemCode)) {
					$sql = "SELECT `item_code` FROM `items` WHERE `item_code` = '".$itemCode."' AND `id` != ".$item->id." LIMIT 1";
					$itemCodes = $this->conn->query($sql)->numRows();
					if ($itemCodes > 0) {
						return [
							'status' => false,
							'message' => "Item code #".$itemCode.' already added in the system'
						];		
					}
				}
				$sql = "UPDATE `".$this->table."` SET `name` = '".$itemName."', `description` = '".$itemDesc."', `category_id` = ".$category_id.", `item_code` = '".$itemCode."' WHERE `id` = ".$item->id;
				$isUpdate = true;
			}
			$result = $this->conn->query($sql);

			if ($result) {
				$event = [
					'restaurant_id' => $restaurant_id,
					'category_id' => $category_id
				];
				if (!empty($this->eventHandler)) {
					$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ITEMS'], $event);
				}
				return [
					'status' => true,
					'message' => 'Item saved successfully',
					'item' => $this->get(($isUpdate) ? $item->id : $this->conn->lastInsertID())
				];
			} else {
				return [
					'status' => false,
					'message' => 'Failed: Item was not saved successfully'
				];
			}
		}

		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['item_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'delete-item');

			$item_id = $this->conn->realEscape($request['item_id']);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($item_id) && is_numeric($item_id) && $item_id > 0) {
				$sql = "UPDATE `".$this->table."` SET `status` = 0 WHERE `id` = ".$item_id;
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ITEMS'], $event);
					}
					return [
						'status' => true,
						'message' => 'Item deleted successfully',
						'items' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => 'Oops! Please try after some time'
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		public function savePriceInfo($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['item_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-item-price-info');
			$item_id = $this->conn->realEscape($request['item_id']);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$price_info = json_decode($request['item_price_info']);
			if (!empty($price_info)) {
				$errors = [];
				foreach ($price_info as $key => &$itemPrice) {
					if (empty($itemPrice->acatId) || !is_numeric($itemPrice->acatId) || $itemPrice->acatId < 0) {
						$itemPrice->acatId = $this->conn->realEscape($itemPrice->acatId);
						$errors[] = "Area category does not exists";
					}
					if (empty($itemPrice->price) || !is_numeric($itemPrice->price) || $itemPrice->price < 0) {
						$errors[] = "Invalid price info requested";
						$itemPrice->price = $this->conn->realEscape($itemPrice->price);
					}
				}
				if (count($errors) > 0) {
					return [
						'status' => false,
						'message' => implode(', ', $errors)
					];
				} else {
					$price_info = stripslashes(json_encode($price_info));
					$sql = "UPDATE `".$this->table."` SET `item_price_info` = '".$price_info."' WHERE `id` = ".$item_id;
					if ($this->conn->query($sql)) {
						$event = [
							'restaurant_id' => $restaurant_id,
						];
						if (!empty($this->eventHandler)) {
							$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ITEMS'], $event);
						}
						return [
							'status' => true,
							'message' => 'Item info has been set for this item',
							'item_id' => $item_id,
							'item_price_info' => $price_info
						];
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];		
					}
				}
			} else {
				return [
					'status' => false,
					'message' => 'No price information passed to save'
				];	
			}
		}

	}