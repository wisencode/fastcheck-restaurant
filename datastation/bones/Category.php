<?php

	class Category {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['category'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function all($restaurant_id) {
			$categories = $this->conn->query('SELECT `id`, `name`, `picture`, `description` FROM '.$this->table.' WHERE restaurant_id = '.$restaurant_id." AND `status` = 1 ORDER BY `id` ASC")->fetchAll();
			return $categories;
		}

		public function allWithBaseInfo($restaurant_id) {
			$categories = $this->conn->query('SELECT `id`, `name` FROM '.$this->table.' WHERE restaurant_id = '.$restaurant_id." AND `status` = 1 ORDER BY `id` ASC")->fetchAll();
			return $categories;
		}

		public function get($category_id) {
			$category = $this->conn->query('SELECT `id`, `name`, `picture`, `description` FROM '.$this->table. ' WHERE id = '.$category_id)->fetchArray();
			return $category;	
		}

		public function items($category_id) {
			$items = $this->conn->query('SELECT `id`, `name`, `picture`, `category_id`, `item_code`, `description` FROM '.TABLES['item']. ' WHERE `status` = 1 AND category_id = '.$category_id." ORDER BY `id` ASC")->fetchAll();
			return $items;	
		}

		public function save($request) {
			if (empty($request['restaurant_id']) || empty($request['category'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-category');

			$category = json_decode($request['category']);
			$categoryName = $this->conn->realEscape($category->name);
			$categoryDesc = $this->conn->realEscape($category->description);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$sql = "INSERT INTO `".$this->table."`(`name`, `description`, `restaurant_id`) VALUES('".$categoryName."', '".$categoryDesc."', ".$restaurant_id.")";
			$result = $this->conn->query($sql);
			if ($result) {
				$event = [
					'restaurant_id' => $restaurant_id
				];
				if (!empty($this->eventHandler)) {
					$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-CATEGORIES'], $event);
				}
				return [
					'status' => true,
					'message' => 'Category saved successfully',
					'category' => $this->get($this->conn->lastInsertID())
				];
			} else {
				return [
					'status' => false,
					'message' => 'Failed: Category was not saved successfully'
				];
			}
		}

		public function saveForOffice($request) {
			if (empty($request['restaurant_id']) || empty($request['category']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-category-from-office');

			$category = json_decode($request['category']);
			$categoryName = $this->conn->realEscape($category->name);
			$categoryDesc = $this->conn->realEscape($category->description);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($categoryName)) {
				if ($category->id == 0) {
					$sql = "INSERT INTO `".$this->table."`(`name`, `description`, `restaurant_id`) VALUES('".$categoryName."', '".$categoryDesc."', ".$restaurant_id.")";
				} else {
					$sql = "UPDATE `".$this->table."` SET `name` = '".$categoryName."', `description` = '".$categoryDesc."' WHERE `id` = ".$category->id." AND `restaurant_id` = ".$restaurant_id;
				}
				$result = $this->conn->query($sql);

				if ($result) {
					$event = [
						'restaurant_id' => $restaurant_id
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-CATEGORIES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Category saved successfully',
						'categories' => $this->all($restaurant_id),
						'category' => (!empty($category->id)) ? $this->get($category->id) : $this->get($this->conn->lastInsertID())
					];
				} else {
					return [
						'status' => false,
						'message' => 'Failed: Category was not saved successfully'
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['category_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'delete-category');

			$category_id = $request['category_id'];
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($category_id) && is_numeric($category_id) && $category_id > 0) {
				$sql = "UPDATE `".$this->table."` SET `status` = 0 WHERE `id` = ".$category_id." AND `restaurant_id` = ".$restaurant_id;
				try {
					$this->conn->beginTransaction();
					if ($this->conn->query($sql)) {
						$catItems = $this->items($category_id);
						$sql = "UPDATE `".TABLES['item']."` SET `status` = 0 WHERE `category_id` = ".$category_id;
						if ($this->conn->query($sql)) {
							$event = [
							'restaurant_id' => $restaurant_id,
							];
							$this->conn->commit();
							if (!empty($this->eventHandler)) {
								$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-CATEGORIES'], $event);
							}
							return [
								'status' => true,
								'message' => 'Category deleted successfully',
								'categories' => $this->all($restaurant_id)
							];
						} else {
							return [
								'status' => false,
								'message' => MESSAGES['OOPS']
							];	
						}
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];
					}
				} catch(Exception $exception) {
					$this->conn->rollback();
					return [
						'status' => false,
						'message' => $exception->getMessage()
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

	}