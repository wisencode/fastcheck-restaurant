<?php

	class AreaCategory {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['area_category'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function all($restaurant_id) {
			$areaCategories = $this->conn->query('SELECT `id`, `restaurant_id`, `name`, `is_parcel` FROM '.$this->table.' WHERE `status` = "active" AND `restaurant_id` = '.$restaurant_id)->fetchAll();
			return $areaCategories;
		}

		public function save($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['area_category']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-area-category');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$area_category = json_decode($request['area_category']);
			$area_category->name = $this->conn->realEscape($area_category->name);
			$area_category->id = $this->conn->realEscape($area_category->id);
			if (!empty($area_category->name)) {
				if ($area_category->id == 0) {
					$sql = "INSERT INTO `".$this->table."`(`name`, `restaurant_id`, `status`) VALUES('".$area_category->name."', '".$restaurant_id."', 'active')";
				} else {
					$sql = "UPDATE `".$this->table."` SET `name` = '".$area_category->name."', `status` = 'active' WHERE `id` = ".$area_category->id." AND `restaurant_id` = ".$restaurant_id;
				}
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-AREA-CATEGORIES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Area category settings saved successfully'
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];	
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['area_category_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'delete-area-category');

			$area_category_id = $request['area_category_id'];
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($area_category_id) && is_numeric($area_category_id) && $area_category_id > 0) {
				$sql = "UPDATE `".$this->table."` SET `status` = 'inactive' WHERE `id` = ".$area_category_id." AND `restaurant_id` = ".$restaurant_id;
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-AREA-CATEGORIES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Area categories settings saved successfully'
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

	}