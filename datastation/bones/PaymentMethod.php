<?php

	class PaymentMethod {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['payment_methods'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function all() {
			$paymentMethods = $this->conn->query('SELECT `id`, `payment_method` FROM '.$this->table.' WHERE `status` = "1" AND `deleted_at` IS NULL')->fetchAll();
			return $paymentMethods;
		}

		public function getWhere($params = []) {
			if (empty($params)) {
				return [];
			} else {
				$sql = "SELECT `id`, `payment_method` FROM `".$this->table."` WHERE 1 = 1";
				if (!empty($params['name'])) {
					$sql.= " AND `payment_method` = '".$params['name']."'";
				}
				if (!empty($params['id'])) {
					$sql.= " AND `id` = ".$params['id'];
				}
				$sql.= " LIMIT 1";
				return $this->conn->query($sql)->fetchArray();
			}
		}

		public function save($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['paymentMethod']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-payment-method');

			$paymentMethod = json_decode($request['paymentMethod']);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($paymentMethod)) {
				$id = $paymentMethod->id;
				$status = $paymentMethod->status;
				if (is_numeric($id) && $id > 0 && ($status == '0' || $status == '1')) {
					$sql = "SELECT `id` FROM `".TABLES['restaurant_payment_methods']."` WHERE `payment_method_id` = '".$id."' AND `restaurant_id` = ".$restaurant_id;
					if ($this->conn->query($sql)->numRows() > 0) {
						$sql = "UPDATE `".TABLES['restaurant_payment_methods']."` SET `status` = '".$status."' WHERE `payment_method_id` = '".$id."' AND `restaurant_id` = ".$restaurant_id;
					} else {
						$sql = "INSERT INTO `".TABLES['restaurant_payment_methods']."`(`restaurant_id`, `payment_method_id`, `status`) VALUES('".$restaurant_id."', '".$id."', '".$status."')";
					}
					if ($this->conn->query($sql)) {
						$event = [
							'restaurant_id' => $restaurant_id,
						];
						if (!empty($this->eventHandler)) {
							$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-RESTAURANT'], $event);
						}
						$sql = "SELECT `rpm`.`payment_method_id` as `id`, `pm`.`payment_method` FROM `".TABLES['restaurant_payment_methods']."` rpm LEFT JOIN `".TABLES['payment_methods']."` pm ON pm.id = rpm.`payment_method_id` WHERE `rpm`.`status` = '1' AND `pm`.`status` = '1' AND rpm.`restaurant_id`=".$restaurant_id;
						return [
							'status' => true,
							'message' => 'Payment method settings saved successfully',
							'payment_methods' => $this->conn->query($sql)->fetchAll()
						];
					} else {
						return [
							'status' => false,
							'message' => MESSAGES['OOPS']
						];
					}
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['INVALID_REQUEST']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];	
			}

		}

	}