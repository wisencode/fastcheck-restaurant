<?php
	
	use Dompdf\Dompdf;

	class Order {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['order'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		// Returns orders
		public function all($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$options = json_decode($request['options']);
			$where = "WHERE `o`.`restaurant_id` = ".$restaurant_id." AND `o`.`order_status` = 'approved'";
			if (!empty($options)) {
				if (!empty($options->overall) && !$options->overall) {
					$where.= " AND `o`.`deleted_at` IS NULL";
				}
				if (!empty($options->todayOnly) && $options->todayOnly) {
					$where.= " AND date(`o`.`created_at`) = '".date('Y-m-d')."'";
				}
				if (!empty($options->order_no)) {
					$options->order_no = $this->conn->realEscape($options->order_no);
					$where.= " AND `o`.`order_no` LIKE '".$options->order_no."%'";
				}
				if (!empty($options->table_id)) {
					$options->table_id = $this->conn->realEscape($options->table_id);
					$where.= " AND `o`.`table_id` = '".$options->table_id."'";
				}
				if (!empty($options->amount_from) && is_numeric($options->amount_from)) {
					$options->amount_from = $this->conn->realEscape($options->amount_from);
					$where.= " AND `o`.`order_amount` >= '".$options->amount_from."'";
				}
				if (!empty($options->amount_to) && is_numeric($options->amount_to)) {
					$options->amount_to = $this->conn->realEscape($options->amount_to);
					$where.= " AND `o`.`order_amount` <= '".$options->amount_to."'";
				}
				if (!empty($options->date_from) && $options->date_from) {
					$options->date_from = $this->conn->realEscape($options->date_from);
					$where.= " AND date(`o`.`created_at`) >= '".$options->date_from."'";
				}
				if (!empty($options->date_to) && $options->date_to) {
					$options->date_to = $this->conn->realEscape($options->date_to);
					$where.= " AND date(`o`.`created_at`) <= '".$options->date_to."'";
				}
				if (!empty($options->payment_method)) {
					$options->payment_method = $this->conn->realEscape($options->payment_method);
					$where.= " AND `o`.`payment_method` = '".$options->payment_method."'";
				}
			}
			$sql = "SELECT `o`.`id`, `order_no`, `order_amount`, `o`.`created_at`, `table_id`, `payment_method`, `payment_method_info`, `order_info`, `tax_info`, `discount_info`, `is_bill_given`, `t`.`table_no`, `o`.`is_empty` FROM `".$this->table."` o LEFT JOIN `".TABLES['table']."` t ON `t`.`id` = `o`.`table_id` ".$where." ORDER BY `created_at` ASC";
			return $this->conn->query($sql)->fetchAll();
		}

		// Returns month(daily) tax info
		public function getOrderData($restaurant_id, $options) {
			if (!empty($restaurant_id) && is_numeric($restaurant_id) && !empty($options)) {
				$sql = "SELECT `order_info`, `tax_info`, `discount_info`, day(`created_at`) as o_date, `order_amount` FROM `".$this->table."` WHERE `restaurant_id` = ".$restaurant_id." AND `order_status` = 'approved'";
				if (!empty($options['month'])) {
					$sql.= " AND month(`created_at`) = ".$options['month'];
				}
				if (!empty($options['year'])) {
					$sql.= " AND year(`created_at`) = ".$options['year'];
				}
				// $sql.= " LIMIT 1";
				return $this->conn->query($sql)->fetchAll();
			} else {
				return [];
			}
			return $options;
		}

		// Returns month(daily) report
		public function monthReport($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['monthReportFor']) || empty($request['yearReportFor'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$monthReportFor = $this->conn->realEscape($request['monthReportFor']);
			$yearReportFor = $this->conn->realEscape($request['yearReportFor']);
			if (is_numeric($monthReportFor) && is_numeric($yearReportFor) && ($monthReportFor >=1 && $monthReportFor <= 12)) {
				$sql = "SELECT date_format(date(`created_at`), '%D %M %Y') as `date`, sum(`order_amount`) as total_payment, min(`order_no`) as bill_no_from, max(`order_no`) as bill_no_to, day(`created_at`) as o_date FROM `orders` WHERE `restaurant_id` = ".$restaurant_id." AND month(`created_at`) = ".$monthReportFor." AND year(`created_at`) = ".$yearReportFor." AND `order_status` = 'approved' group by date(`created_at`)";
				$reportLines = $this->conn->query($sql)->fetchAll();
				return [
					'status' => true,
					'reportLines' => $reportLines
				];
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];		
			}
		}

		// Returns year(monthly) report
		public function yearReport($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['yearReportFor'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$yearReportFor = $this->conn->realEscape($request['yearReportFor']);
			if (is_numeric($yearReportFor)) {
				$sql = "SELECT monthname(`created_at`) as `month`, sum(`order_amount`) as total_payment FROM `orders` WHERE `restaurant_id` = ".$restaurant_id." AND `order_book_year` = ".$yearReportFor." AND `order_status` = 'approved' group by month(`created_at`)";
				$reportLines = $this->conn->query($sql)->fetchAll();
				return [
					'status' => true,
					'reportLines' => $reportLines
				];
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];		
			}
		}

		// Returns order head summary information
		public function orderHeadSummary($order_id) {
			$order_id = $this->conn->realEscape($order_id);
			$sql = 'SELECT `id`, `order_no`, `order_amount`, `created_at`, `table_id` FROM '.$this->table.' WHERE `id` = '.$order_id;
			return $this->conn->query($sql)->fetchArray();
		}

		// Returns order summary information
		public function orderInfoSummary($order_id) {
			$order_id = $this->conn->realEscape($order_id);
			$sql = 'SELECT `id`, `order_no`, `order_amount`, `created_at`, `table_id`, `payment_method`, `payment_method_info`, `order_info`, `tax_info`, `discount_info`, `is_bill_given`, `has_office_lock` FROM '.$this->table.' WHERE `id` = '.$order_id;
			return $this->conn->query($sql)->fetchArray();
		}

		// Returns order all information
		public function orderInfo($order_id) {
			$order_id = $this->conn->realEscape($order_id);
			$sql = 'SELECT *, date_format(date(`created_at`), "%D %M %Y") as created_at  FROM '.$this->table.' WHERE `id` = '.$order_id." LIMIT 1";
			return $this->conn->query($sql)->fetchArray();
		}

		public function orderBaseInfo($request) {
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($request['by_order_no']) && $request['by_order_no'] == true) {
				$order_no = $this->conn->realEscape($request['order_no']);
				$order_book_year = (!empty($request['order_book_year'])) ? $this->conn->realEscape($request['order_book_year']) : date('Y');
				$where = 'WHERE `o`.`order_book_year` = "'.$order_book_year.'" AND `o`.`order_no` = '.$order_no.' AND `o`.`restaurant_id` = '.$restaurant_id;
			} else {
				$order_id = $this->conn->realEscape($request['order_id']);
				$where = 'WHERE `o`.`id` = '.$order_id;
			}
			$sql = 'SELECT `o`.`id`, `order_no`, `order_amount`, date_format(date(`o`.`created_at`), "%D %M %Y") as created_at, `table_id`, `payment_method`, `payment_method_info`, `order_info`, `tax_info`, `discount_info`, `is_bill_given`, `extra_information`, `t`.`table_no` FROM '.$this->table.' o LEFT JOIN `'.TABLES['table'].'` t ON `t`.`id` = `o`.`table_id` '.$where.' LIMIT 1';
			return $this->conn->query($sql)->fetchArray();
		}

		// Returns order extra information
		public function orderExtraInfo($request) {
			$order_id = $this->conn->realEscape($request['order_id']);
			$sql = 'SELECT `o`.`id`, `order_no`, `extra_information`, `customer`.`name`, `customer`.`email`, `customer`.`phone_number` FROM '.$this->table.' o LEFT JOIN `'.TABLES['customer'].'` customer ON `customer`.`id` = `o`.`customer_id` WHERE `o`.`id` = '.$order_id.' LIMIT 1';
			return $this->conn->query($sql)->fetchArray();
		}

		// Returns order status
		public function statusByRZPOrderId($rzpOrder_id) {
			$rzpOrder_id = $this->conn->realEscape($rzpOrder_id);
			$sql = 'SELECT `order_status`, `order_no` FROM '.$this->table.' WHERE `rzpOrder_id` = "'.$rzpOrder_id.'"';
			return $this->conn->query($sql)->fetchArray();
		}

		// Returns order status
		public function statusByRZPPaymentLinkId($rzpPaymentLink_Id) {
			$rzpPaymentLink_Id = $this->conn->realEscape($rzpPaymentLink_Id);
			$sql = 'SELECT `order_status`, `order_no` FROM '.$this->table.' WHERE `rzpPaymentLink_Id` = "'.$rzpPaymentLink_Id.'"';
			return $this->conn->query($sql)->fetchArray();
		}

		// Returns order status
		public function statusByOrderId($restaurant_id, $order_id) {
			$restaurant_id = $this->conn->realEscape($restaurant_id);
			$order_id = $this->conn->realEscape($order_id);
			$sql = 'SELECT `order_status`, `order_no`, `id`, date_format(date(`created_at`), "%D %M %Y") as created_at FROM '.$this->table.' WHERE `id` = "'.$order_id.'" AND `restaurant_id` = '.$restaurant_id;
			return $this->conn->query($sql)->fetchArray();
		}

		// Delete temp order
		public function remove($rzpOrder_id) {
			Helper::log($rzpOrder_id, 'remove-by-rzporderid');
			$rzpOrder_id = $this->conn->realEscape($rzpOrder_id);
			$sql = 'DELETE FROM '.$this->table.' WHERE `rzpOrder_id` = "'.$rzpOrder_id.'"';
			if ($this->conn->query($sql)) {
				return [
					'status' => 'DELETED'
				];
			} else {
				return [
					'status' => false
				];
			}
		}

		// Delete/Empty order
		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['order_id'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'order-delete');
			$order_id = $this->conn->realEscape($request['order_id']);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$orderInfo = $this->orderInfoSummary($order_id);
			if ($orderInfo['is_bill_given'] == 1) {
				return [
					'status' => false,
					'message' => 'Technical Error: Order can not be changed'
				];
			}
			if ($orderInfo['has_office_lock'] == 1) {
				return [
					'status' => false,
					'message' => 'Technical Error: Order is locked by the office'
				];
			}
			$adjustedOrdersResponse = $this->adjustOrderNos($restaurant_id, $orderInfo['order_no']);
			if (!$adjustedOrdersResponse['status']) {
				return $adjustedOrdersResponse;
			} else {
				$sql = 'DELETE FROM `'.$this->table.'` WHERE `id` = '.$order_id.' AND `is_bill_given` = 0';
				if ($this->conn->query($sql)) {
					$data = [
						'restaurant_id' => $restaurant_id,
						'order_id' => $request['order_id']
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ORDER-DELETED'], $data);
					}
					return [
						'status' => true,
						'message' => 'Order successfully marked as deleted'
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['INVALID_REQUEST']
					];
				}
			}
		}

		// Restore order
		public function restore($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['order_id'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'order-restore');
			$order_id = $this->conn->realEscape($request['order_id']);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$sql = 'UPDATE '.$this->table.' SET `is_empty` = 0 WHERE `id` = '.$order_id.' AND `is_bill_given` = 0';
			if ($this->conn->query($sql)) {
				$order = [
					'restaurant_id' => $restaurant_id,
					'order' => $this->orderInfoSummary($order_id)
				];
				if (!empty($this->eventHandler)) {
					$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ORDER-UPDATED'], $order);
				}
				return [
					'status' => true,
					'message' => 'Order successfully restored'
				];
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		// Save order with options given
		public function save($orderInfo) {
			if (!Helper::isRequestValidForRestaurant($orderInfo)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}

			Helper::log($orderInfo, 'order');

			$rzpOrderId = 'NULL';
			$rzpPaymentLinkId = 'NULL';

			foreach($orderInfo as $key => $value) {
				if(!is_array($value) && $key != 'cart_info' && $key != 'tax_info' && $key != 'discount_info') {
					$orderInfo[$key] = $this->conn->realEscape($value);
				}
			}

			$cart_info = json_decode($orderInfo['cart_info']);
			$conn = $this->conn;
			foreach($cart_info as $key => $item) {
				array_walk($item, function($value, $ikey) use ($item, $key, $conn) {
					$item->$ikey = $conn->realEscape($value);
				});
			}
			$cart_info = stripslashes(json_encode($cart_info));
			$cart_info = json_decode($cart_info);
			foreach($cart_info as $key => &$log) {
				$log->specifications = json_decode($log->specifications);
			}
			$cart_info = json_encode($cart_info);

			$tax_info = json_decode($orderInfo['tax_info']);
			foreach($tax_info as $key => $tax) {
				array_walk($tax, function($value, $ikey) use ($tax, $key, $conn) {
					$tax->$ikey = $conn->realEscape($value);
				});
			}
			$tax_info = stripslashes(json_encode($tax_info));

			$discount_info = json_decode($orderInfo['discount_info']);
			foreach($discount_info as $key => $discount) {
				array_walk($discount, function($value, $ikey) use ($discount, $key, $conn) {
					$discount->$ikey = $conn->realEscape($value);
				});
			}
			$discount_info = stripslashes(json_encode($discount_info));

			$billExtraInformation = json_decode(stripslashes($orderInfo['extra_information']));
			foreach($billExtraInformation as $key => $info) {
				if (is_object($info)) {
					array_walk($info, function($value, $ikey) use ($info, $key, $conn) {
						if (!is_object($value)) {
							$info->$ikey = $conn->realEscape($value);
						}
					});
				}
			}
			$billExtraInformation = stripslashes(json_encode($billExtraInformation));

			$payment_method_info = json_decode(stripslashes($orderInfo['payment_method_info']));
			foreach($payment_method_info as $key => $payment_method_name) {
				$payment_method_info->$key = $conn->realEscape($payment_method_name);
			}
			$payment_method_info = stripslashes(json_encode($payment_method_info));

			if ($orderInfo['payment_method'] == 'online_payment') {
				$rzpOrder = $this->generateOrderOnRZP($orderInfo);
				if (!empty($rzpOrder->error)) {
					return [
						'status' => false,
						'message' => $rzpOrder->error->code.': '.$rzpOrder->error->description,
					];
				} else {
					$rzpOrderId = $rzpOrder->id;
				}
			}
			else if ($orderInfo['payment_method'] == 'payment_link') {
				$rzpPamentLink = $this->sendPaymentLink($orderInfo);
				if (!empty($rzpPamentLink->error)) {
					return [
						'status' => false,
						'message' => $rzpPamentLink->error->code.': '.$rzpPamentLink->error->description,
					];
				} else {
					$rzpPaymentLinkId = $rzpPamentLink->id;
				}
			}

			$restaurant_id = $this->conn->realEscape($orderInfo['restaurant_id']);
			$cart_info = $cart_info;
			$tax_info = $tax_info;
			$discount_info = $discount_info;
			$order_amount = $this->conn->realEscape($orderInfo['order_amount']);
			// $order_no = $this->conn->realEscape($orderInfo['order_no']);
			$payment_method = $this->conn->realEscape($orderInfo['payment_method']);
			$order_status = ($payment_method == 'cash' || $payment_method == 'upi') ? 'approved' : $this->conn->realEscape($orderInfo['order_status']);
			$payment_method_info = $this->conn->realEscape($orderInfo['payment_method_info']);
			$table_id = $this->conn->realEscape($orderInfo['table_id']);
			// $billExtraInformation = $this->conn->realEscape($orderInfo['extra_information']);
			$isBillGiven = $this->conn->realEscape($orderInfo['isBillGiven']);

			// If date is first day of financial year (1st of April) then RESET ORDER NO
			$fnclYearDate = '04/01'; // month/date
			$clndrYearDate = '01/01'; // month/date
			if (date('m/d') == $fnclYearDate || date('m/d') == $clndrYearDate) {
				$newOrderBookNeeded = false;
				if ($orderInfo['restaurant_ob_type'] == 'FNCLY' && date('m/d') == $fnclYearDate) {
					$dateOfNewOrderBook = date('Y').'/'.$fnclYearDate;
					$newOrderBookNeeded = true;
				} else if ($orderInfo['restaurant_ob_type'] == 'CLNDRY' && date('m/d') == $clndrYearDate) {
					$dateOfNewOrderBook = date('Y').'/'.$clndrYearDate;
					$newOrderBookNeeded = true;
				}
				if ($newOrderBookNeeded) {
					$noOfOrders = $this->getTotalOrders([
						'date' => $dateOfNewOrderBook,
						'restaurant_id' => $restaurant_id
					]);
					if ($noOfOrders == 0) {
						$order_no = 1;
					} else {
						$order_no = ($this->getLastOrderNo(date('Y'), $restaurant_id, $isBillGiven) + 1);	
					}
				} else {
					$order_no = ($this->getLastOrderNo(date('Y'), $restaurant_id, $isBillGiven) + 1);	
				}
			} else {
				$order_no = ($this->getLastOrderNo(date('Y'), $restaurant_id, $isBillGiven) + 1);
			}
			$order_book_year = date('Y');

			$orderPayload = [
				'order_info' => $cart_info,
				'tax_info' => $tax_info,
				'discount_info' => $discount_info,
				'order_amount' => $order_amount,
				'order_no' => $order_no,
				'order_status' => $order_status,
				'payment_method' => $payment_method,
				'payment_method_info' => $payment_method_info,
				'restaurant_id' => $restaurant_id,
				'table_id' => $table_id,
				'rzpOrder_id' => $rzpOrderId,
				'rzpPaymentLink_Id' => $rzpPaymentLinkId,
				'extra_information' => $billExtraInformation,
				'is_bill_given' => $isBillGiven,
				'is_empty' => 0,
				'created_at' => date('Y-m-d H:i:s'),
				'order_book_year' => $order_book_year
			];

			$sql = "SELECT `id` FROM `".$this->table."` WHERE `restaurant_id` = ".$restaurant_id." AND `is_empty` = 1 LIMIT 1";
			$emptySlot = $this->conn->query($sql)->fetchArray();
			$isEmptySlotAvailable = false;
			if ($isBillGiven == 0 && !empty($emptySlot) && !empty($emptySlot['id']) && $emptySlot['id'] > 0) {
				$isEmptySlotAvailable = true;
				$sql = "UPDATE `".$this->table."` SET ";
				$columnCount = count($orderPayload);
				foreach($orderPayload as $column => $value) {
					$columnCount--;
					if ($column == "order_no" || $column == "created_at" || $column == "order_book_year") {
						continue;
					}
					$sql.= "`".$column."` = '".$value."'";
					if ($columnCount > 2) {
						$sql.= ", ";
					}
				}
				$sql.= " WHERE `restaurant_id` = ".$restaurant_id." AND `id` = ".$emptySlot['id'];
			} else {
				$orderColumnNames = "`".implode('`,`', array_keys($orderPayload))."`";
				$orderColumnValues = "'".implode("', '", array_values($orderPayload))."'";
				$sql = "INSERT INTO `".$this->table."`(".$orderColumnNames.") VALUES (";
				$values = $orderColumnValues;
				$sql.= $values.")";
			}

			$this->conn->beginTransaction();
			try {
				if ($this->conn->query($sql)) {
					$lastOrderId = ($isEmptySlotAvailable) ? $emptySlot['id'] : $this->conn->lastInsertID();
					$orderInfo = $this->orderInfo($lastOrderId);
					$order = [
						'restaurant_id' => $restaurant_id,
						'order' => $this->orderHeadSummary($lastOrderId)
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ORDER-RECEIVED'], $order);
					}
					$this->conn->commit();
					return [
						'status' => true,
						'order' => $orderInfo
					];
				} else {
					return [
						'status' => false,
						'message' => 'Error while saving the order',
					];
				}
			} catch (Exception $exception) {
				Helper::log(array_merge($orderInfo, ['err' => $exception->getMessage()]), "save-order-err");
				$this->conn->rollback();
				return [
					'status' => false,
					'message' => 'Error while saving the order',
				];
			}
		}

		// Update order stats with options given
		public function updateStats($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['order_id']) || empty($request['order_amount']) || !is_numeric($request['order_amount']) || empty($request['order_table_id']) || !is_numeric($request['order_table_id']) || empty($request['order_payment_method'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			if (!Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['NO_ACCESS']
				];
			}
			$sql = 'SELECT `is_bill_given`, `has_office_lock` FROM '.$this->table.' WHERE `id` = '.$request['order_id'];
			$order_info = $this->conn->query($sql)->fetchArray();
			if (!empty($order_info)) {
				if ($order_info['is_bill_given'] == 1) {
					return [
						'status' => false,
						'message' => 'Technical Error: Order can not be updated'
					];
				}
				if ($order_info['has_office_lock'] == 1) {
					return [
						'status' => false,
						'message' => 'Technical Error: Order is locked by the office'
					];
				}
			}

			Helper::log($request, 'order-update');
			$order_items = json_decode($request['order_items']);
			foreach($order_items as $key => &$item) {
				$item->name = $this->conn->realEscape($item->name);
				$item->price = $this->conn->realEscape($item->price);
				$item->qty = $this->conn->realEscape($item->qty);
				$item->itemId = $this->conn->realEscape($item->itemId);
			}

			$order_items = stripslashes(json_encode($order_items));

			$order_amount =  $this->conn->realEscape($request['order_amount']);
			$order_id =  $this->conn->realEscape($request['order_id']);
			$order_table_id =  $this->conn->realEscape($request['order_table_id']);
			$order_payment_method =  $this->conn->realEscape($request['order_payment_method']);
			$payment_method_info = (new PaymentMethod())->getWhere(['id' => $order_payment_method]);
			$restaurant_id =  $this->conn->realEscape($request['restaurant_id']);

			$this->conn->beginTransaction();
			try {
				$sql = "UPDATE `".$this->table."` SET `order_info` = '".$order_items."', `order_amount` = ".$order_amount.", `table_id` = ".$order_table_id.", `payment_method` = '".$payment_method_info['payment_method']."' WHERE `id` = ".$order_id." AND `restaurant_id` = ".$restaurant_id;

				if ($this->conn->query($sql)) {
					$order = [
						'restaurant_id' => $restaurant_id,
						'order' => $this->orderInfoSummary($order_id)
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-ORDER-UPDATED'], $order);
					}
					$this->conn->commit();
					return [
						'status' => true,
						'message' => 'Order saved successfully'
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['INVALID_REQUEST']
					];
				}
			} catch (Exception $exception) {
				Helper::log(array_merge($orderInfo, ['err' => $exception->getMessage()]), "update-order-stats-err");
				$this->conn->rollback();
				return [
					'status' => false,
					'message' => 'Error while updating order'
				];
			}
		}

		// Generate order on RZ and return response
		public function generateOrderOnRZP($orderInfo) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, RZP_API_ENDPOINT. '/orders');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$payload = [
				'amount' => ($orderInfo['order_amount'] * 100),
				'currency' => $orderInfo['order_amount_currency'],
				'receipt' => $orderInfo['order_no'],
			];
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
			curl_setopt($ch, CURLOPT_USERPWD, RZP_API_KEY_ID . ':' . RZP_API_KEY_SECRET);
			$headers = array();
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$response = curl_exec($ch);
			if (curl_errno($ch)) {
				return [
					'status' => false,
					'message' => curl_error($ch)
				];
			}
			curl_close($ch);
			return json_decode($response);
		}

		// Send RZ Payment link to customer
		public function sendPaymentLink($orderInfo) {
			$orderDescription = $orderInfo['restaurant_name'].'- Bill for table #'.$orderInfo['table_id']." :: ";
			$cart_info = json_decode($orderInfo['cart_info']);
			foreach($cart_info as $item) {
				$orderDescription.= $item->qty.' '.$item->name." [".($item->price * $item->qty)." ".$orderInfo['order_amount_currency']."]";
				if(next($cart_info)) {
					$orderDescription.= ' | ';
				}
			}
			if(strlen($orderDescription) >= 2048) {
				$orderDescription = substr($orderDescription, 0, 2045)."...";
			}
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, RZP_API_ENDPOINT. '/payment_links/');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$payload = [
				'amount' => ($orderInfo['order_amount'] * 100),
				'currency' => $orderInfo['order_amount_currency'],
				'accept_partial' => false,
				'reference_id' => $orderInfo['order_no'],
				'description' => $orderDescription,
				'customer' => [
					'name' => (!empty($orderInfo['customer']['name'])) ? $orderInfo['customer']['name'] : '',
					'contact' => (!empty($orderInfo['customer']['phone'])) ? $orderInfo['customer']['phone'] : '',
					'email' => (!empty($orderInfo['customer']['email'])) ? $orderInfo['customer']['email'] : '',
				],
				'notify' => [
					'sms' => (!empty($orderInfo['customer']['phone'])) ? true : false,
					'email' => (!empty($orderInfo['customer']['email'])) ? true : false,
				],
				// 'upi_link' => true,
			];
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
			curl_setopt($ch, CURLOPT_USERPWD, RZP_API_KEY_ID . ':' . RZP_API_KEY_SECRET);
			$headers = array();
			$headers[] = 'Content-Type: application/json';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$response = curl_exec($ch);
			if (curl_errno($ch)) {
				return [
					'status' => false,
					'message' => curl_error($ch)
				];
			}
			curl_close($ch);
			return json_decode($response);
		}

		// Fetch order statistics with options given
		public function stats($request) {
			if (empty($request['restaurant_id'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}

			if (!Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => 'You have no access to this resource'
				];
			}

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$report_date = (!empty($request['report_date'])) ? $request['report_date'] : date('Y-m-d');
			$options = (!empty($request['options'])) ? $request['options'] : [];
			$where = "WHERE `restaurant_id` = ".$restaurant_id." AND `order_status` = 'approved'";
			if (!empty($options)) {
				if (!empty($options->overall) && !$options->overall) {
					$where.= " AND `deleted_at` IS NULL";
				}
				if (!empty($options->todayOnly) && $options->todayOnly) {
					$where.= " AND date(`created_at`) = '".date('Y-m-d')."'";
				}
				if (!empty($options->order_no)) {
					$options->order_no = $this->conn->realEscape($options->order_no);
					$where.= " AND `order_no` LIKE '".$options->order_no."%'";
				}
				if (!empty($options->table_id)) {
					$options->table_id = $this->conn->realEscape($options->table_id);
					$where.= " AND `table_id` = '".$options->table_id."'";
				}
				if (!empty($options->amount_from) && is_numeric($options->amount_from)) {
					$options->amount_from = $this->conn->realEscape($options->amount_from);
					$where.= " AND `order_amount` >= '".$options->amount_from."'";
				}
				if (!empty($options->amount_to) && is_numeric($options->amount_to)) {
					$options->amount_to = $this->conn->realEscape($options->amount_to);
					$where.= " AND `order_amount` <= '".$options->amount_to."'";
				}
				if (!empty($options->date_from) && $options->date_from) {
					$options->date_from = $this->conn->realEscape($options->date_from);
					$where.= " AND date(`created_at`) >= '".$options->date_from."'";
				}
				if (!empty($options->date_to) && $options->date_to) {
					$options->date_to = $this->conn->realEscape($options->date_to);
					$where.= " AND date(`created_at`) <= '".$options->date_to."'";
				}
				if (!empty($options->payment_method)) {
					$options->payment_method = $this->conn->realEscape($options->payment_method);
					$where.= " AND `payment_method` = '".$options->payment_method."'";
				}
			} else {
				$where.= "AND date(`created_at`) = '".$report_date."'";
			}
			$sql = "SELECT sum(`order_amount`) as order_amount, count(`id`) as total_orders FROM `".$this->table."` ".$where;
			$total_orders_info = $this->conn->query($sql)->fetchArray();
			if ($total_orders_info['order_amount'] == null) {
				$total_orders_info['order_amount'] = 0;
			}

			$sql = "SELECT sum(`order_amount`) as order_amount, count(`id`) as total_orders FROM `".$this->table."` WHERE `order_status` = 'approved' AND `payment_method` = 'cash' AND date(`created_at`) = '".$report_date."' AND `restaurant_id` = ".$restaurant_id;
			$total_cash_orders_info = $this->conn->query($sql)->fetchArray();

			$sql = "SELECT sum(`transaction_amount`) as total_cash_recharge_amount FROM `".TABLES['fc_ecards_transactions_log']."` WHERE `transaction_via` = 'cash' AND `transaction_type` = 'RCHRG' AND date(`created_at`) = '".$report_date."' AND `restaurant_id` = ".$restaurant_id;
			$total_cash_recharge_info = $this->conn->query($sql)->fetchArray();

			$sql = "SELECT sum(`transaction_amount`) as total_cash_init_recharge_amount FROM `".TABLES['fc_ecards_transactions_log']."` WHERE `transaction_via` = 'cash' AND `transaction_type` = 'INTRCHRG' AND date(`created_at`) = '".$report_date."' AND `restaurant_id` = ".$restaurant_id;
			$total_cash_new_fc_ecards_info = $this->conn->query($sql)->fetchArray();

			$sql = "SELECT count(`payment_method`) as total_orders, `payment_method` FROM `".$this->table."` WHERE `restaurant_id` = ".$restaurant_id." AND `order_status` = 'approved' AND date(`created_at`) = '".$report_date."' GROUP BY `payment_method`";
			$total_payment_methods_info = $this->conn->query($sql)->fetchAll();
			$sql = "SELECT `order_no`, `payment_method`, `order_amount` FROM `".$this->table."` WHERE `restaurant_id` = ".$restaurant_id." AND `order_status` = 'approved' AND date(`created_at`) = '".$report_date."' ORDER BY `id` DESC LIMIT 1";
			$last_order_info = $this->conn->query($sql)->fetchArray();

			return [
				'status' => true,
				'stats' => [
					'total_orders_info' => $total_orders_info,
					'total_cash_orders_info' => $total_cash_orders_info,
					'total_cash_recharge_info' => $total_cash_recharge_info,
					'total_cash_new_fc_ecards_info' => $total_cash_new_fc_ecards_info,
					'total_payment_methods_info' => $total_payment_methods_info,
					'last_order_info' => $last_order_info,
					'report_date' => date('d F Y', strtotime($report_date)),
				]
			];

		}

		// Print order report with options given
		public function printList($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$options = json_decode($request['options']);
			if (!empty($options->todayOnly) && $options->todayOnly == true) {
				$orders = $this->all([
					'restaurant_id' => $restaurant_id,
					'options' => json_encode([
						'todayOnly' => true,
						'overall' => false
					])
				]);
				$pdfReport = $this->generateOrderReport($orders, $restaurant_id, $options);
				if ($pdfReport['status'] == true) {
					return [
						'status' => true,
						'message' => 'Orders printed',
						'reportFileUrl' => $pdfReport['url']
					];
				} else {
					return [
						'status' => false,
						'message' => 'FileIO Failed: Something went wrong while generating report',
					];
				}	
			} else if (!empty($options->monthReport) && $options->monthReport == true && !empty($options->monthReportFor) && !empty($options->yearReportFor) && is_numeric($options->monthReportFor) && is_numeric($options->yearReportFor) && ($options->monthReportFor >=1 && $options->monthReportFor <=12)) {
				$reportLines = $this->monthReport([
					'restaurant_id' => $restaurant_id,
					'monthReportFor' => $options->monthReportFor,
					'yearReportFor' => $options->yearReportFor,
				])['reportLines'];
				$pdfReport = $this->generateOrderMonthReport($reportLines, $restaurant_id, $options);
				if ($pdfReport['status'] == true) {
					return [
						'status' => true,
						'message' => 'Orders printed',
						'reportFileUrl' => $pdfReport['url']
					];
				} else {
					return [
						'status' => false,
						'message' => 'FileIO Failed: Something went wrong while generating report',
					];
				}
			} else if (!empty($options->yearReport) && $options->yearReport == true && !empty($options->yearReportFor) && is_numeric($options->yearReportFor)) {
				$reportLines = $this->yearReport([
					'restaurant_id' => $restaurant_id,
					'yearReportFor' => $options->yearReportFor
				])['reportLines'];
				$pdfReport = $this->generateOrderYearReport($reportLines, $restaurant_id, $options);
				if ($pdfReport['status'] == true) {
					return [
						'status' => true,
						'message' => 'Orders printed',
						'reportFileUrl' => $pdfReport['url']
					];
				} else {
					return [
						'status' => false,
						'message' => 'FileIO Failed: Something went wrong while generating report',
					];
				}
			} else if (!empty($options)) {
				$orders = $this->all([
					'restaurant_id' => $restaurant_id,
					'options' => json_encode($options)
				]);
				$pdfReport = $this->generateOrderReport($orders, $restaurant_id, $options);
				if ($pdfReport['status'] == true) {
					return [
						'status' => true,
						'message' => 'Orders printed',
						'reportFileUrl' => $pdfReport['url']
					];
				} else {
					return [
						'status' => false,
						'message' => 'FileIO Failed: Something went wrong while generating report',
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];	
			}
		}

		// Custom order report
		public function generateOrderReport($orders, $restaurant_id, $options = []) {
			$dompdf = new Dompdf();
			$dompdf->set_option('isRemoteEnabled', TRUE);
			$dompdf->loadHtml(Helper::loadView(__DIR__ . '/../../assets/templates/orders/list.php', [
				'orders' => $orders,
				'options' => $options,
				'stats' => $this->stats([
					'restaurant_id' => $restaurant_id,
					'options' => $options
				])['stats'],
				'restaurant' => (new Restaurant())->get($restaurant_id)
			]));
			$dompdf->setPaper('A4', 'landscape');
			$dompdf->render();
			$output = $dompdf->output();

			$reportFolder = __DIR__ . "/../../assets/uploads/" . $restaurant_id;
			if (!file_exists($reportFolder)) {
				mkdir($reportFolder);
			}
			$reportFolder.= "/reports/";
			if (!file_exists($reportFolder)) {
				mkdir($reportFolder);
			}
			$reportFileName = date('dFY').'.pdf';
			if (!empty($options)) {
				$reportFileName = "Orders-Report-Res-".substr($restaurant_id, 0, 3)."-".date('Ymd').'.pdf';
			}
			$pdfReportFile = $reportFolder . $reportFileName;
			if (file_exists($pdfReportFile)) {
				unlink($pdfReportFile);
			}
			file_put_contents($pdfReportFile, $output);
			$uploadResponse = Helper::uploadToStorage('restaurant/reports/'.$restaurant_id, $pdfReportFile, $reportFileName);
			if ($uploadResponse['status']) {
				unlink($pdfReportFile);
				return [
					'status' => true,
					'url' => $uploadResponse['uploadedFileUrl'],
					'name' => $reportFileName
				];	
			} else {
				return [
					'status' => false,
					'message' => 'FileIO Failed: Something went wrong while generating report'
				];
			}
		}

		// Month (Daily) order report
		public function generateOrderMonthReport($reportLines, $restaurant_id, $options = []) {
			$dompdf = new Dompdf();
			$dompdf->set_option('isRemoteEnabled', TRUE);
			$dateObj   = DateTime::createFromFormat('!m', $options->monthReportFor);
			$reportMonth = $dateObj->format('F');
			$dompdf->loadHtml(Helper::loadView(__DIR__ . '/../../assets/templates/orders/month-report.php', [
				'reportLines' => $reportLines,
				'reportName' => $reportMonth." ".$options->yearReportFor,
				'reportMonth' => $options->monthReportFor,
				'reportYear' => $options->yearReportFor,
				'restaurant' => (new Restaurant())->get($restaurant_id)
			]));
			$dompdf->setPaper('A4', 'landscape');
			$dompdf->render();
			$output = $dompdf->output();

			$reportFolder = __DIR__ . "/../../assets/uploads/" . $restaurant_id;
			if (!file_exists($reportFolder)) {
				mkdir($reportFolder);
			}
			$reportFolder.= "/reports/";
			if (!file_exists($reportFolder)) {
				mkdir($reportFolder);
			}
			$reportFileName = date('dFY').'.pdf';
			if (!empty($options)) {
				$reportFileName = "Orders-Month-Report-".$reportMonth.'.pdf';
			}
			$pdfReportFile = $reportFolder . $reportFileName;
			if (file_exists($pdfReportFile)) {
				unlink($pdfReportFile);
			}
			file_put_contents($pdfReportFile, $output);
			$uploadResponse = Helper::uploadToStorage('restaurant/reports/'.$restaurant_id, $pdfReportFile, $reportFileName);
			if ($uploadResponse['status']) {
				unlink($pdfReportFile);
				return [
					'status' => true,
					'url' => $uploadResponse['uploadedFileUrl'],
					'name' => $reportFileName
				];	
			} else {
				return [
					'status' => false,
					'message' => 'FileIO Failed: Something went wrong while generating report'
				];
			}
			return [
				'url' => $pdfReportFile,
				'name' => $reportFileName
			];
		}

		// Year (Monthly) order report
		public function generateOrderYearReport($reportLines, $restaurant_id, $options = []) {
			$dompdf = new Dompdf();
			$dompdf->set_option('isRemoteEnabled', TRUE);
			$reportYear = $options->yearReportFor;
			$dompdf->loadHtml(Helper::loadView(__DIR__ . '/../../assets/templates/orders/year-report.php', [
				'reportLines' => $reportLines,
				'reportYear' => $reportYear,
				'restaurant' => (new Restaurant())->get($restaurant_id)
			]));
			$dompdf->setPaper('A4', 'landscape');
			$dompdf->render();
			$output = $dompdf->output();

			$reportFolder = __DIR__ . "/../../assets/uploads/" . $restaurant_id;
			if (!file_exists($reportFolder)) {
				mkdir($reportFolder);
			}
			$reportFolder.= "/reports/";
			if (!file_exists($reportFolder)) {
				mkdir($reportFolder);
			}
			$reportFileName = date('dFY').'.pdf';
			if (!empty($options)) {
				$reportFileName = "Orders-Year-Report-".$reportYear.'.pdf';
			}
			$pdfReportFile = $reportFolder . $reportFileName;
			if (file_exists($pdfReportFile)) {
				unlink($pdfReportFile);
			}
			file_put_contents($pdfReportFile, $output);
			$uploadResponse = Helper::uploadToStorage('restaurant/reports/'.$restaurant_id, $pdfReportFile, $reportFileName);
			if ($uploadResponse['status']) {
				unlink($pdfReportFile);
				return [
					'status' => true,
					'url' => $uploadResponse['uploadedFileUrl'],
					'name' => $reportFileName
				];	
			} else {
				return [
					'status' => false,
					'message' => 'FileIO Failed: Something went wrong while generating report'
				];
			}
		}

		// Get last order no with options given
		public function getLastOrderNo($orderBookYear = '', $restaurant_id = 0, $isBillGiven = 0) {
			if ($restaurant_id == 0 || !is_numeric($restaurant_id) || empty($orderBookYear) || !is_numeric($orderBookYear)) {
				return 0;
			}

			// Get last order no in given order book year
			$sql = "SELECT `order_no` FROM `".$this->table."` WHERE `restaurant_id` = ".$restaurant_id." AND `order_book_year` = '".$orderBookYear."' ORDER BY `id` DESC LIMIT 1";
			$orderInfo = $this->conn->query($sql)->fetchArray();
			if (!empty($orderInfo) && !empty($orderInfo['order_no']) && $orderInfo['order_no'] > 0) {
				return $orderInfo['order_no'];
			}

			return 0;
		}

		// Get total number of orders with options if given
		public function getTotalOrders($options = []) {
			if ($options['restaurant_id'] == 0 || !is_numeric($options['restaurant_id'])) {
				return 0;
			}

			$total_orders = 0;
			$sql = "SELECT count(`id`) as total_orders FROM `".$this->table."`";
			$where = " WHERE `restaurant_id` = ".$options['restaurant_id'];
			if (!empty($options['date'])) {
				$where.= " AND date(`created_at`) = '".$options['date']."'";
			}
			$sql.= $where;
			if ($result = $this->conn->query($sql)) {
				$total_orders = $result->fetchArray()['total_orders'];
			}

			return $total_orders;
		}

		// Adjust order nos
		public function adjustOrderNos($restaurant_id, $order_no) {
			if ($restaurant_id == 0 || !is_numeric($order_no)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$sql = "SELECT `id`, `order_no`, `is_bill_given` FROM `".$this->table."` WHERE `restaurant_id` = ".$restaurant_id." AND `order_no` >= ".$order_no;
			$orders = $this->conn->query($sql)->fetchAll();
			$orderToDelete = [
				'order_no' => $order_no
			];
			$adjsutedOrderNos = Helper::adjustOrderNos($orders, $orderToDelete);
			if ($adjsutedOrderNos['status'] == false) {
				return $adjsutedOrderNos;
			} else {
				if (count($adjsutedOrderNos['orders']) > 1) {
					$this->conn->beginTransaction();
					$adjustOrderNosSQL = "UPDATE `".$this->table."` SET ";
					$adjustOrderNosIds = [];
					foreach ($adjsutedOrderNos['orders'] as $key => $adjustedOrderInfo) {
						if ($adjustedOrderInfo['order_no'] != $orderToDelete['order_no']) {
							$adjustOrderNosSQL.= "`order_no` = IF(`id` = ".$adjustedOrderInfo['id'].", '".$adjustedOrderInfo['new_order_no']."', `order_no`), ";
							$adjustOrderNosIds[] = $adjustedOrderInfo['id'];
						}
					}
					$adjustOrderNosSQL = rtrim($adjustOrderNosSQL, ", ");
					$adjustOrderNosSQL.= " WHERE `id` IN (".implode(',', $adjustOrderNosIds).")";
					try {
						$this->conn->query($adjustOrderNosSQL);
						$this->conn->commit();
					} catch(Exception $exception) {
						Helper::log($exception->getMessage(), "adjust-order-nos");
						$this->conn->rollback();
					}
					return $adjsutedOrderNos;
				} else {
					return [
						'status' => true
					];
				}
			}
		}

		// Set office lock on order
		public function setLock($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['order_id']) || !is_numeric($request['order_id'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'set-order-office-lock');
			$order_id = $this->conn->realEscape($request['order_id']);
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);

			$this->conn->beginTransaction();
			try {
				$sql = "UPDATE `".$this->table."` SET `has_office_lock` = IF(`has_office_lock` = 0, 1, 0) WHERE `id` = ".$order_id." AND `restaurant_id` = ".$restaurant_id;
				if ($this->conn->query($sql)) {
					$this->conn->commit();
					return [
						'status' => true,
						'message' => 'Order access level has been updated'
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['INVALID_REQUEST']
					];	
				}
			} catch (Exception $exception) {
				Helper::log($request, 'set-order-office-lock');
				$this->conn->rollback();
				return [
					'status' => false,
					'message' => 'Error while setting order access level'
				];
			}
		}

	}