<?php

	class Table {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['table'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function all($restaurant_id) {
			$tables = $this->conn->query('SELECT `tables`.`id`, `tables`.`restaurant_id`, `tables`.`table_no`, `tables`.`table_alias`, `tables`.`status`, `tables`.`area_category_id` FROM '.$this->table.' LEFT JOIN `area_categories` ON `area_categories`.`id` = `tables`.`area_category_id` WHERE `area_categories`.status = "active" AND `tables`.`is_active` = 1 AND `tables`.`restaurant_id` = '.$restaurant_id)->fetchAll();
			return $tables;
		}

		public function tableInfo($restaurant_id, $table_id) {
			$sql = 'SELECT `id`, `restaurant_id`, `table_no`, `table_alias`, `status`, `area_category_id` FROM '.$this->table.' WHERE `restaurant_id` = '.$restaurant_id." AND `id` = ".$table_id;
			return $this->conn->query($sql)->fetchArray();
		}

		public function baseInfo($restaurant_id, $table_id) {
			$sql = 'SELECT `tables`.`table_no`, `area`.`name` as area FROM '.$this->table.' LEFT JOIN `'.TABLES["area_category"].'` area ON `area`.`id` = `tables`.`area_category_id` WHERE `tables`.`restaurant_id` = '.$restaurant_id." AND `tables`.`id` = ".$table_id;
			return $this->conn->query($sql)->fetchArray();
		}

		public function setStatus($table_data) {
			if (!Helper::isRequestValidForRestaurant($table_data)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$event['table'] = $table_data;
			if (!empty($this->eventHandler)) {
				$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $table_data['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['STATUS-UPDATED'], $event);
			}
			$sql = "UPDATE `".$this->table."` SET `status` = '".$table_data['table_status']."' WHERE `restaurant_id` = '".$table_data['restaurant_id']."' AND `id` = '".$table_data['table_id']."'";
			$result = $this->conn->query($sql);
			if($result) {
				return $this->tableInfo($table_data['restaurant_id'], $table_data['table_id']);
			}
		}

		public function cartUpdated($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			// $payloadSize = strlen(json_encode($request, JSON_NUMERIC_CHECK));
			// $needToChunk = false;
			// $maxPayloadSize = 7000;
			// if ($payloadSize >= $maxPayloadSize) {
			// 	$needToChunk = true;
			// }

			// if ($request['cartInfo'] != '[]' && !$needToChunk) {
			// 	$response = [
			// 		'restaurant_id' => $request['restaurant_id'],
			// 		'tableId' => $request['tableId'],
			// 	];
			// 	$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['CART-UPDATED'], $response);
			// } else {
			// 	$requestChunks = str_split(json_encode($request), $maxPayloadSize);
			// 	$chunkId = uniqid();
			// 	foreach ($requestChunks as $chunkCount => $chunk) {
			// 		$chunkBlock = [
			// 			'chunkId' => $chunkId,
			// 			'isChunk' => true,
			// 			'isFinalChunk' => ($chunkCount == (count($requestChunks) - 1)) ? true : false,
			// 			'chunkBlock' => $chunk
			// 		];
			// 		$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['CART-UPDATED'], $chunkBlock);
			// 	}
			// }

			$response = [
				'restaurant_id' => $request['restaurant_id'],
				'tableId' => $request['tableId'],
			];

			$this->saveCartItems($request);
			if ($request['cartInfo'] != '[]') {
				if (!empty($this->eventHandler)) {
					$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['CART-UPDATED'], $response);
				}
			} else if ($request['cartInfo'] == '[]') {
				if (!empty($this->eventHandler)) {
					$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['CART-CLEARED'], $response);
				}
				$this->deleteKitchenLog($request['restaurant_id'], $request['tableId']);
				$this->deleteTableCartItemsLog($request['restaurant_id'], $request['tableId']);
			}
			return [
				'status' => true,
				'message' => 'Cart updated successfully',
				'restaurant_id' => $request['restaurant_id'],
				'tableId' => $request['tableId'],
			];
		}

		public function cartEmpty($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$response = [
				'restaurant_id' => $request['restaurant_id'],
				'tableId' => $request['tableId'],
			];
			if (!empty($this->eventHandler)) {
				$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['CART-CLEARED'], $response);
			}
			$this->deleteKitchenLog($request['restaurant_id'], $request['tableId']);
			$this->deleteTableCartItemsLog($request['restaurant_id'], $request['tableId']);
			return [
				'status' => true,
				'message' => 'Cart cleared successfully',
				'restaurant_id' => $request['restaurant_id'],
				'tableId' => $request['tableId'],
			];
		}

		public function saveCartItems($cartItemsInfo) {
			$tableCartItems = $this->getTableCartItems($cartItemsInfo['restaurant_id'], $cartItemsInfo['tableId']);
			$itemsLog = json_decode($cartItemsInfo['cartInfo']);
			foreach($itemsLog as $key => &$log) {
				$log->specifications = json_decode($log->specifications);
			}
			$itemsLog = json_encode($itemsLog);

			if(!empty($tableCartItems) && $tableCartItems['id'] > 0) {
				$sql = "UPDATE `".TABLES['table_cart_items']."` SET `restaurant_id` = ".$cartItemsInfo['restaurant_id'].", `table_id` = ".$cartItemsInfo['tableId'].", `items_log` = '".$itemsLog."' WHERE `id` = ".$tableCartItems['id'];
			} else {
				$sql = "INSERT INTO `".TABLES['table_cart_items']."`(`restaurant_id`, `table_id`, `items_log`) VALUES('".$cartItemsInfo['restaurant_id']."', '".$cartItemsInfo['tableId']."', '".$itemsLog."')";
			}
			$this->conn->query($sql);
		}

		public function pushItemsToKitchen($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			// $this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['PUSH-TO-KITCHEN'], $request);
			$kotData = [
				'restaurant_id' => $request['restaurant_id'],
				'table_id' => $request['tableId']
			];
			if (!empty($this->eventHandler)) {
				$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['NEW-KOT'], $kotData);
			}
			$kitchenLog = $this->getKitchenLog($request['restaurant_id'], $request['tableId']);
			$itemsLog = json_decode($request['items']);
			foreach($itemsLog as $key => &$log) {
				$log->specifications = json_decode($log->specifications);
			}
			$itemsLog = json_encode($itemsLog);

			if(!empty($kitchenLog) && $kitchenLog['id'] > 0) {
				$sql = "UPDATE `".TABLES['kitchen_log']."` SET `restaurant_id` = ".$request['restaurant_id'].", `table_id` = ".$request['tableId'].", `items_log` = '".$itemsLog."' WHERE `id` = ".$kitchenLog['id'];
			} else {
				$sql = "INSERT INTO `".TABLES['kitchen_log']."`(`restaurant_id`, `table_id`, `items_log`) VALUES('".$request['restaurant_id']."', '".$request['tableId']."', '".$itemsLog."')";
			}
			$this->conn->query($sql);
			return $request;
		}

		public function markItemsAsServed($request) {
			if (!Helper::isRequestValidForRestaurant($request)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$tableCartItemsLog = $this->getTableCartItems($request['restaurant_id'], $request['tableId']);
			$tableCartItems = $tableCartItemsLog['items_log'];
			$servedItemsInfo = json_decode($request['itemsInfo']);
			if (!empty($tableCartItems)) {
				$tableCartItems = json_decode($tableCartItems);
				if (count($tableCartItems) > 0) {
					foreach ($tableCartItems as &$itemInfo) {
						if ($itemInfo->itemId == $servedItemsInfo->itemId) {
							$itemSpecifications = $itemInfo->specifications;
							if (!empty($itemSpecifications)) {
								foreach ($itemSpecifications as &$itemSpecification) {
									if ($itemSpecification->taste == $request['taste'] && $itemSpecification->extraSpecification == $request['extraSpecification']) {
										$itemSpecification->servedQty = $itemSpecification->qty;
									}
								}
							}
						}
					}
				}
			}

			$itemsLog = $tableCartItems;
			foreach($itemsLog as $key => &$log) {
				$log->specifications = json_encode($log->specifications);
			}
			$itemsLog = json_encode($itemsLog);

			$updatedCartItemsData = [
				'restaurant_id' => $request['restaurant_id'],
				'tableId' => $request['tableId'],
				'cartInfo' => $itemsLog,
			];
			$this->saveCartItems($updatedCartItemsData);
			
			if (!empty($this->eventHandler)) {
				$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['TABLE']['NAME'], $request['restaurant_id']), EVENT_CHANNELS['TABLE']['EVENTS']['MARK-ITEMS-AS-SERVED'], $request);
			}
			return $request;
		}

		public function getKitchenLog($restaurant_id, $table_id = null) {
			if ($table_id != null && is_numeric($table_id) && $table_id > 0) {
				$sql = "SELECT id FROM `".TABLES['kitchen_log']."` WHERE `restaurant_id` = ".$restaurant_id." AND `table_id` = ".$table_id." LIMIT 1";
				return $this->conn->query($sql)->fetchArray();
			} else {
				$sql = "SELECT `table_id`, `items_log` FROM `".TABLES['kitchen_log']."` WHERE `restaurant_id` = ".$restaurant_id;
				$conn = $this->conn->connection;
				$result = $conn->query($sql);
				$logs = [];
				while($log = $result->fetch_assoc()) {
				    $logs[] = $log;    
				}
				$conn->close();
				return $logs;
			}
		}

		public function getTableCartItems($restaurant_id, $table_id) {
			if (empty($restaurant_id) || empty($table_id)) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$restaurant_id = $this->conn->realEscape($restaurant_id);
			$table_id = $this->conn->realEscape($table_id);
			$sql = "SELECT `id`, `table_id`, `items_log` FROM `".TABLES['table_cart_items']."` WHERE `restaurant_id` = ".$restaurant_id." AND `table_id` = ".$table_id;
			$conn = $this->conn->connection;
			$result = $conn->query($sql);
			$logs = [];
			while($log = $result->fetch_assoc()) {
			    $logs[] = $log;    
			}
			return (!empty($logs) && !empty($logs[0])) ? $logs[0] : [];
		}

		public function deleteKitchenLog($restaurant_id, $table_id = null) {
			if ($table_id != null && is_numeric($table_id) && $table_id > 0) {
				$sql = "DELETE FROM `".TABLES['kitchen_log']."` WHERE `restaurant_id` = ".$restaurant_id." AND `table_id` = ".$table_id;
				return $this->conn->query($sql);
			}
		}

		public function deleteTableCartItemsLog($restaurant_id, $table_id = null) {
			if ($table_id != null && is_numeric($table_id) && $table_id > 0) {
				$sql = "DELETE FROM `".TABLES['table_cart_items']."` WHERE `restaurant_id` = ".$restaurant_id." AND `table_id` = ".$table_id;
				return $this->conn->query($sql);
			}
		}

		public function save($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['table']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-table');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$table = json_decode($request['table']);
			$table->table_no = $this->conn->realEscape($table->table_no);
			$table->table_alias = $this->conn->realEscape($table->table_alias);
			$table->area_category_id = $this->conn->realEscape($table->area_category_id);
			$table->id = $this->conn->realEscape($table->id);
			if (!empty($table->table_no)) {
				if ($table->id == 0) {
					$sql = "INSERT INTO `".$this->table."`(`area_category_id`, `table_no`, `table_alias`, `is_active`, `restaurant_id`, `status`) VALUES('".$table->area_category_id."', '".$table->table_no."', '".$table->table_alias."', 1, ".$restaurant_id.", 'A')";
				} else {
					$sql = "UPDATE `".$this->table."` SET `area_category_id` = '".$table->area_category_id."', `table_no` = '".$table->table_no."', `table_alias` = '".$table->table_alias."', `is_active` = 1, `status` = 'A' WHERE `id` = ".$table->id." AND `restaurant_id` = ".$restaurant_id;
				}
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-TABLES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Table settings saved successfully',
						'tables' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];	
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['table_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'delete-table');

			$table_id = $request['table_id'];
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($table_id) && is_numeric($table_id) && $table_id > 0) {
				$sql = "UPDATE `".$this->table."` SET `is_active` = 0 WHERE `id` = ".$table_id." AND `restaurant_id` = ".$restaurant_id;
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-TABLES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Table settings saved successfully',
						'tables' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

	}