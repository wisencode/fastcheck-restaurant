<?php

	class Notify {

		protected $conn;
		public function __construct() {
			global $conn;
			$this->conn = $conn;
		}

		public function invokeFCECard($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['FCECardNumber'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$isNotified = false;
			Helper::log($request, 'notify-customer-at-invoke-fc-ecard');
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$FCECardNumber = $this->conn->realEscape($request['FCECardNumber']);
			$sql = "SELECT * FROM `".TABLES['fc_ecard']."` WHERE `ecard_no` = '".$FCECardNumber."' AND `restaurant_id` = ".$restaurant_id." LIMIT 1";
			$fc_eCard = $this->conn->query($sql)->fetchArray();
			if(!empty($fc_eCard)) {
				$restaurant = (new Restaurant())->get($restaurant_id);
				if (!empty($fc_eCard['linked_email']) && Helper::isValidEmail($fc_eCard['linked_email'])) {
					(new Mail())->send([
						'template' => 'mail/send-fc-card.php',
						'to' => $fc_eCard['linked_email'],
						'subject' => $restaurant['name']."- FC-ECard #".$fc_eCard['ecard_no']." is generated",
						'restaurant' => $restaurant,
						'FCECard' => $fc_eCard
					]);
					$isNotified = true;
				}
			}
			return [
				'status' => true,
				'isNotified' => $isNotified,
				'message' => ($isNotified) ? 'Notification sent' : 'Nothing to notify'
			];
		}

		public function fcECardTransaction($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['fcECardTransId'])) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			$isNotified = false;
			Helper::log($request, 'notify-customer-for-fc-ecard-transaction');
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$fcECardTransId = $this->conn->realEscape($request['fcECardTransId']);
			$sql = "SELECT `transaction_type`, `order_id`, `transaction_amount`, `prev_balance`, `fcECardLog`.`created_at` trans_date, `fcECard`.`ecard_no`, `fcECard`.`linked_email`, `fcECard`.`linked_phone_number` FROM `".TABLES['fc_ecards_transactions_log']."` fcECardLog LEFT JOIN `".TABLES['fc_ecard']."` fcECard ON `fcECard`.`id` = `fcECardLog`.`fc_ecard_id` LEFT JOIN `".TABLES['order']."` orders ON `orders`.`id` = `fcECardLog`.`order_id` WHERE `fcECardLog`.`id` = ".$fcECardTransId." AND `fcECardLog`.`restaurant_id` = ".$restaurant_id." LIMIT 1";
			$fcECardTransaction = $this->conn->query($sql)->fetchArray();
			if(!empty($fcECardTransaction)) {
				$restaurant = (new Restaurant())->get($restaurant_id);
				if (!empty($fcECardTransaction['linked_email']) && Helper::isValidEmail($fcECardTransaction['linked_email'])) {
					(new Mail())->send([
						'template' => 'mail/send-fc-ecard-trans-single-log.php',
						'to' => $fcECardTransaction['linked_email'],
						'subject' => $restaurant['name']."- New transaction for FC-ECard #".$fcECardTransaction['ecard_no'],
						'restaurant' => $restaurant,
						'fcECardTransaction' => $fcECardTransaction
					]);
					$isNotified = true;
				}
			}
			return [
				'status' => true,
				'isNotified' => $isNotified,
				'fcECardTransaction' => $fcECardTransaction,
				'message' => ($isNotified) ? 'Notification sent' : 'Nothing to notify'
			];
		}

		public function sendFCECardPaymentLink($request) {
			$isNotified = false;
			Helper::log($request, 'send-payment-link-for-fc-card-payment');

			$fcECardPaymentHashWith = [];
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$order_id = $this->conn->realEscape($request['order_id']);
			$FCECardNumber = $this->conn->realEscape($request['FCECardNumber']);

			if (!empty($restaurant_id) && !empty($order_id) && !empty($FCECardNumber) && is_numeric($restaurant_id) && is_numeric($order_id)) {
				$sql = "SELECT * FROM `".TABLES['fc_ecard']."` WHERE `ecard_no` = '".$FCECardNumber."' AND `restaurant_id` = ".$restaurant_id." LIMIT 1";
				$fc_eCard = $this->conn->query($sql)->fetchArray();
				if(!empty($fc_eCard)) {
					$payLink = Helper::protocol().Helper::host().'/services/pay-with-fc-card.php?token=';
					$fcECardPaymentHashWith[] = $restaurant_id;
					$fcECardPaymentHashWith[] = $order_id;
					$fcECardPaymentHashWith[] = $FCECardNumber;
					$fcECardPaymentHashWith[] = 'fcp';
					$token = Helper::encrypt(implode('||', $fcECardPaymentHashWith));
					$payLink.= urlencode($token);
					$restaurant = (new Restaurant())->get($restaurant_id);
					if (!empty($fc_eCard['linked_email']) && Helper::isValidEmail($fc_eCard['linked_email'])) {
						(new Mail())->send([
							'template' => 'mail/send-fc-card-payment-link.php',
							'to' => $fc_eCard['linked_email'],
							'subject' => $restaurant['name']."- FC-ECard payment request for ".number_format($request['orderAmount'], 2)." ".$restaurant['currency_code'],
							'pay_link' => $payLink,
							'restaurant' => $restaurant,
							'FCECardNumber' => $FCECardNumber,
							'orderAmount' => $request['orderAmount'],
						]);
						$isNotified = true;
					}
				}
				if (!$isNotified) {
					(new Order())->deleteTempOrder($request['order_id']);
				}
				return [
					'status' => $isNotified,
					'isNotified' => $isNotified,
					'message' => ($isNotified) ? 'Notification sent' : 'Nothing to notify'
				];
			} else {
				(new Order())->deleteTempOrder($request['order_id']);
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			print_r($request);exit;
		}

	}