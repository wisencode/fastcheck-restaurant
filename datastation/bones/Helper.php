<?php
	use Aws\S3\S3Client;
	use Aws\S3\Exception\S3Exception;

	class Helper {

		public static function verifyLogin() {
			if (empty($_SESSION['accessor'])) {
				return false;
			}
			return true;
		}

		public static function isAccessorAsOffice() {
			if (empty($_SESSION['accessor_role']) || $_SESSION['accessor_role'] != 'office') {
				return false;
			}
			return true;
		}

		public static function logout($request) {
			Helper::log($request, 'logout');
			$_SESSION['accessor'] = '';
			unset($_SESSION['accessor']);
			session_destroy();
		}

		public static function isRequestValidForRestaurant($request) {
			if (empty($request['restaurant_id']) || !is_numeric($request['restaurant_id'])) {
				return false;
			} 
			return true;
		}

		public function decryptRequestData($request) {
			foreach ($request as $param => $value) {
				if (!empty($param) && !empty($value) && $param != 'action') {
					$request[$param] = Helper::decrypt($value);
				}
			}
			return $request;
		}
		
		public static function adjustChannelPrefix($channelName, $restaurant_id = 'default') {
			return str_replace(CHANNEL_PREFIX, CHANNEL_PREFIX.'rest-'.$restaurant_id.'-', $channelName);
		}

		public static function checkPostRequest($method) {
			if ($method != 'POST') {
				echo json_encode([
					'status' => false,
					'message' => MESSAGES['UNAUTHORISED_ACCESS']
				]);
				exit;
			}
		}

		public static function host() {
			// if (strpos('localhost', $_SERVER['SERVER_NAME']) !== FALSE) {
			// 	return $_SERVER['SERVER_NAME'].SUB_DIR;
			// }
			return $_SERVER['SERVER_NAME'].SUB_DIR;
		}

		public static function protocol() {
			return isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https://" : "http://";
		}

		public static function loadBones($bones = []) {
			if (!empty($bones)) {
				foreach ($bones as $bone) {
					$boneFile = __DIR__ . '/../../datastation/bones/'.$bone.'.php';
					if (file_exists($boneFile)) {
						require_once($boneFile);
					} else {
						echo "FC-SERVICE-ERROR-RUNTIME: ".$bone." does not exists in the system";
					}
				}
			}
		}

		public static function loadView($templatePath = '', $data = []) {
			if (!empty($templatePath) && file_exists($templatePath)) {
				ob_start();
				extract($data);
				include_once($templatePath);
				$view = ob_get_contents();
				ob_end_clean();
				return $view;
			}
			return "No need to show case our own mistakes";
		}

		public static function loadTemplate($dir, $path = '', $data = []) {
			$templatePath =  __DIR__ . '/../../assets/templates/'.$dir.'/'.$path;
			$templateDir = __DIR__ . '/../../assets/templates/'.$dir;
			if (!empty($templatePath) && file_exists($templatePath)) {
				ob_start();
				extract($data);
				$templateHeader =  $templateDir.'/header.php';
				if (file_exists($templateHeader)) {
					include_once($templateHeader);
				}
				include_once($templatePath);
				$templateFooter =  $templateDir.'/footer.php';
				if (file_exists($templateFooter)) {
					include_once($templateFooter);
				}
				$view = ob_get_contents();
				ob_end_clean();
				return $view;
			}
			return '';
		}

		public static function isValidEmail($email) { 
		    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
		}

		public static function log($data = '', $module = 'general') {
			if (!empty($data) && KEEP_LOG) {
				$logDir = __DIR__ . '/../../datastation/logs';
				if (!file_exists($logDir)) {
			        mkdir($logDir, 0777, true);
			    }
			    $logFile = $logDir.'/FCLOG-' . strtoupper(date('dMY')) . '.log';
			    if (is_array($data) || is_object($data)) {
			    	$logContent = json_encode($data);
			    } else {
			    	$logContent = $data;
			    }
			    if (!empty($module)) {
			    	$moduleBlock = "";
			    	$moduleCharCount = strlen($module) + 2;
			    	for ($count = 0; $count <= $moduleCharCount; $count++) {
			    		$moduleBlock.= "*";
			    	}
			    	$moduleBlock.= PHP_EOL. '|'.strtoupper($module).'|' .PHP_EOL;
			    	for ($count = 0; $count <= $moduleCharCount; $count++) {
			    		$moduleBlock.= "*";
			    	}
			    	$moduleBlock.= PHP_EOL;
			    	file_put_contents($logFile, $moduleBlock, FILE_APPEND);
			    }
			    file_put_contents($logFile, $logContent.PHP_EOL, FILE_APPEND);
			    file_put_contents($logFile, '|FC-LOG-LINE-OVER|'. PHP_EOL.PHP_EOL, FILE_APPEND);
			}
		}

		public static function uploadToStorage($toDir, $sourceFile, $sourceFileAs) {
			$client = S3Client::factory([
			    'region' => AWS_REGION,
			    'version' => '2006-03-01',
			    'credentials' => [
			    	'key' => AWS_ACCESS_KEY,
			    	'secret' => AWS_ACCESS_SECRET
			    ]
			]);
			try {
				$response = $client->putObject([
					'Bucket' => AWS_BUCKET,
					'Key' => $toDir.'/'.$sourceFileAs,
					'SourceFile' => $sourceFile,
					'StorageClass' => 'REDUCED_REDUNDANCY',
					'ACL' => 'public-read'
				]);
				if ($response) {
					$uploadedUrl = $client->getObjectUrl(AWS_BUCKET, $toDir.'/'.$sourceFileAs);
					if (!empty($uploadedUrl)) {
						return [
							'status' => true,
							'uploadedFileUrl' => $uploadedUrl
						];
					} else {
						return [
							'status' => false
						];
					}
				} else {
					return [
						'status' => false
					];
				}
			} catch (S3Exception $e) {
				Helper::log($e->getMessage(), "upload-resource-to-storage");
				return [
					'status' => false
				];
			}
		}

		public static function isNetworkConnected() {
			return 'CONNECTED';
		}

		public static function encrypt($data = '') {
			if (empty($data)) {
				return '';
			}
			$result = '';
			for ($i = 0, $k = strlen($data); $i<$k; $i++) {
				$char = substr($data, $i, 1);
				$keychar = substr(ENCRYPTION_SALT, ($i % strlen(ENCRYPTION_SALT))-1, 1);
				$char = chr(ord($char) + ord($keychar));
				$result.= $char;
			}
			return base64_encode($result);
		}

		public static function decrypt($data = '') {
			$result = '';
			$data = base64_decode($data);
			for($i = 0,$k = strlen($data); $i < $k ; $i++) {
				$char = substr($data, $i, 1);
				$keychar = substr(ENCRYPTION_SALT, ($i % strlen(ENCRYPTION_SALT)) - 1, 1);
				$char = chr(ord($char) - ord($keychar));
				$result.= $char;
			}
			return $result;
		}

		public static function adjustOrderNos($orders = [], $orderToDelete = []) {
			if (empty($orders) || empty($orderToDelete)) {
				return [
					'status' => true,
					'orders' => $orders
				];
			}
			$last_locked_order = [];
			$last_order = [];
			foreach ($orders as $key => $order) {
					if (empty($last_locked_order)) {
						$last_order = $order;
					}
					if (empty($last_locked_order)) {
						if ($order['is_bill_given'])
							$last_locked_order = $order;
					} else if ($last_locked_order['order_no'] < $order['order_no']) {
						if ($order['is_bill_given'])
							$last_locked_order = $order;
					}
					if ($last_order['order_no'] < $order['order_no'])
						$last_order = $order;
			}

			if ($last_order['is_bill_given'] == true) {
				return [
					'status' => false,
					'message' => "Orders are locked until order #".$last_order['order_no']
				];
			} 
			if (!empty($last_locked_order) && $orderToDelete['order_no'] <= $last_locked_order['order_no']) {
				return [
					'status' => false,
					'message' => "Orders are locked until order #".$last_locked_order['order_no']
				];
			}

			$adjusted_orders = [];
			$needToAdjust = false;
			foreach ($orders as $key => $order) {
				if ($order['order_no'] == $orderToDelete['order_no']) {
					$orders[$key]['new_order_no'] = 'DELETED';
					$needToAdjust = true;
				}
				if ($needToAdjust) {
					$orderToTransfer = $order['order_no'];
				}
				if (!$needToAdjust || $order['is_bill_given']) {
					$orders[$key]['new_order_no'] = $order['order_no'];
				} else {
					if ($needToAdjust) {
						if ($order['order_no'] == $orderToDelete['order_no']) {
							$orders[$key]['new_order_no'] = 'DELETED';
						} else {
							$orders[$key]['new_order_no'] = $orders[$key-1]['order_no'];
						}
					} else {
						$orders[$key]['new_order_no'] = $order['order_no'];
					}
				}
				unset($orders[$key]['is_bill_given']);
			}

			return [
				'status' => true,
				'orders' => $orders
			];

		}

		public function transfer($restaurant_id, $order_id) {
			// $sql = "SELECT `is_bill_given` FROM `".TABLES['order']."` WHERE `id` = ".$order_id." LIMIT 1";
			// $orderInfo = $this->conn->query($sql)->fetchArray();
			// if (!empty($orderInfo) && !empty($orderInfo['is_bill_given']) && $orderInfo['is_bill_given'] == '1') {
			// 	return [
			// 		'status' => false,
			// 		'message' => 'This order can not be deleted'
			// 	];
			// }

			// $sql = 'UPDATE '.$this->table.' SET `is_empty` = 1 WHERE `id` = '.$order_id.' AND `is_bill_given` = 0';
			// if ($this->conn->query($sql)) {
			// 	return [
			// 		'status' => true,
			// 		'message' => 'Order successfully transferred'
			// 	];
			// } else {
			// 	return [
			// 		'status' => false,
			// 		'message' => 'Invalid request'
			// 	];
			// }
		}

		// Get last order no for an empty order in given order book year
		public function getLastEmptyOrderNo($restaurant_id, $orderBookYear) {
			// $sql = "SELECT `order_no` FROM `".TABLES['order']."` WHERE `restaurant_id` = ".$restaurant_id." AND `is_empty` = 1 AND `order_book_year` = '".$orderBookYear."' LIMIT 1";
			// $emptySlot = $this->conn->query($sql)->fetchArray();
			// $isEmptySlotAvailable = false;
			// if ($isBillGiven == 0 && !empty($emptySlot) && !empty($emptySlot['order_no']) && $emptySlot['order_no'] > 0) {
			// 	return ($emptySlot['order_no'] - 1);
			// }
		}

	}