<?php

	class Discount {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['discount'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function all($restaurant_id) {
			$taxes = $this->conn->query('SELECT `id`, `minimum_amount`, `type`, `amount`, `description` FROM '.$this->table.' WHERE `status` = 1 AND `restaurant_id` = '.$restaurant_id)->fetchAll();
			return $taxes;
		}

		public function save($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['discount']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-discount');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$discount = json_decode($request['discount']);
			$discount->type = $this->conn->realEscape($discount->type);
			$discount->description = $this->conn->realEscape($discount->description);
			$discount->minimum_amount = $this->conn->realEscape($discount->minimum_amount);
			$discount->minimum_amount = (!is_numeric($discount->minimum_amount) || intval($discount->minimum_amount) <= 0) ? 'NULL' : $discount->minimum_amount;
			if (!empty($discount->amount)) {
				if ($discount->id == 0) {
					$sql = "INSERT INTO `".$this->table."`(`type`, `description`, `amount`, `minimum_amount`, `restaurant_id`, `status`) VALUES('".$discount->type."', '".$discount->description."', '".$discount->amount."', ".$discount->minimum_amount.", ".$restaurant_id.", 1)";
				} else {
					$sql = "UPDATE `".$this->table."` SET `type` = '".$discount->type."', `description` = '".$discount->description."', `amount` = '".$discount->amount."', `minimum_amount` = '".$discount->minimum_amount."', `status` = 1 WHERE `id` = ".$discount->id." AND `restaurant_id` = ".$restaurant_id;
				}
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-DISCOUNTS'], $event);
					}
					return [
						'status' => true,
						'message' => 'Discount settings saved successfully',
						'discounts' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];	
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['discount_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'delete-discount');

			$discount_id = $request['discount_id'];
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($discount_id) && is_numeric($discount_id) && $discount_id > 0) {
				$sql = "UPDATE `".$this->table."` SET `status` = 0 WHERE `id` = ".$discount_id." AND `restaurant_id` = ".$restaurant_id;
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-DISCOUNTS'], $event);
					}
					return [
						'status' => true,
						'message' => 'Discount settings saved successfully',
						'discounts' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

	}