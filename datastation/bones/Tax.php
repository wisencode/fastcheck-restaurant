<?php

	class Tax {

		protected $table;
		protected $conn;
		protected $eventHandler;

		public function __construct() {
			global $conn, $eventHandler;
			$this->table = TABLES['tax'];
			$this->conn = $conn;
			$this->eventHandler = $eventHandler;
		}

		public function all($restaurant_id) {
			$taxes = $this->conn->query('SELECT `id`, `name`, `type`, `amount`, `tax_type` FROM '.$this->table.' WHERE `status` = 1 AND `restaurant_id` = '.$restaurant_id)->fetchAll();
			return $taxes;
		}

		public function save($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['tax']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'save-tax');

			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			$tax = json_decode($request['tax']);
			$tax->type = $this->conn->realEscape($tax->type);
			$tax->name = $this->conn->realEscape($tax->name);
			$tax->tax_type = $this->conn->realEscape($tax->tax_type);
			if (!empty($tax->amount)) {
				if ($tax->id == 0) {
					$sql = "INSERT INTO `".$this->table."`(`type`, `name`, `amount`, `restaurant_id`, `status`, `tax_type`) VALUES('".$tax->type."', '".$tax->name."', '".$tax->amount."', ".$restaurant_id.", 1, '".$tax->tax_type."')";
				} else {
					$sql = "UPDATE `".$this->table."` SET `type` = '".$tax->type."', `name` = '".$tax->name."', `amount` = '".$tax->amount."', `status` = 1, `tax_type` = '".$tax->tax_type."' WHERE `id` = ".$tax->id." AND `restaurant_id` = ".$restaurant_id;
				}
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-TAXES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Tax settings saved successfully',
						'taxes' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];	
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

		public function delete($request) {
			if (!Helper::isRequestValidForRestaurant($request) || empty($request['tax_id']) || !Helper::isAccessorAsOffice()) {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
			Helper::log($request, 'delete-tax');

			$tax_id = $request['tax_id'];
			$restaurant_id = $this->conn->realEscape($request['restaurant_id']);
			if (!empty($tax_id) && is_numeric($tax_id) && $tax_id > 0) {
				$sql = "UPDATE `".$this->table."` SET `status` = 0 WHERE `id` = ".$tax_id." AND `restaurant_id` = ".$restaurant_id;
				if ($this->conn->query($sql)) {
					$event = [
						'restaurant_id' => $restaurant_id,
					];
					if (!empty($this->eventHandler)) {
						$this->eventHandler->trigger(Helper::adjustChannelPrefix(EVENT_CHANNELS['COMMANDS']['NAME'], $restaurant_id), EVENT_CHANNELS['COMMANDS']['EVENTS']['SYNC-TAXES'], $event);
					}
					return [
						'status' => true,
						'message' => 'Tax settings saved successfully',
						'taxes' => $this->all($restaurant_id)
					];
				} else {
					return [
						'status' => false,
						'message' => MESSAGES['OOPS']
					];
				}
			} else {
				return [
					'status' => false,
					'message' => MESSAGES['INVALID_REQUEST']
				];
			}
		}

	}