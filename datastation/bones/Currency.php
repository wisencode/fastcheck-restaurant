<?php

	class Currency {

		protected $table;
		protected $conn;

		public function __construct() {
			global $conn;
			$this->table = TABLES['currency'];
			$this->conn = $conn;
		}

		public function all() {
			$currencies = $this->conn->query('SELECT `id`, `code`, `symbol` FROM '.$this->table)->fetchAll();
			return $currencies;
		}

	}