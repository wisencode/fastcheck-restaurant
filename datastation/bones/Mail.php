<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    class Mail {
        public function send($options = []) {
            $to = $options['to'];
            $from = (!empty($options['from'])) ? $options['from'] : MAIL['FROM'];
            $subject = $options['subject'];
            $message = Helper::loadView(__DIR__ . '/../../assets/templates/'.$options['template'], $options);
            $mail = new PHPMailer(true);
            try {
                $mail->isSMTP();
                $mail->SMTPDebug = 0;
                $mail->setFrom($from['EMAIL'], $from['NAME']);
                $mail->Username   = MAIL['SMTP']['USERNAME'];
                $mail->Password   = MAIL['SMTP']['PASSWORD'];
                $mail->Host       = MAIL['SMTP']['HOST'];
                $mail->Port       = MAIL['SMTP']['PORT'];
                $mail->SMTPAuth   = true;
                $mail->SMTPSecure = MAIL['SMTP']['ENCRYPTION'];
                $mail->addAddress($to);
                $mail->isHTML(true);
                $mail->Subject    = $subject;
                $mail->Body       = $message;
                $mail->AltBody    = (!empty($options['plainMessage'])) ? $options['plainMessage'] : 'Your email client does not have HTML support. Kindly upgrade/update to latest version of it.\r\nRegards,\r\n'.APP_NAME." Services";
                $mail->Send();
            } catch (phpmailerException $e) {
                Helper::log(array_merge([$e->errorMessage()], $options), $subject);
            } catch (Exception $e) {
                Helper::log(array_merge([$mail->ErrorInfo], $options), $subject);
            }
            return true;
        }
    }