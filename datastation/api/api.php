<?php

	header('Content-type: application/json');

	include_once('../config.php');

	foreach(glob('../'.BONES_DIR.'/*.php') as $file) {
		require_once('../'.BONES_DIR.'/'.$file);
	}

	if(!empty($_REQUEST['action'])) {
		if (!in_array($_REQUEST['action'], ['check-network', 'process-login', 'check-login', 'get-restaurant-menu', 'customer-pay-with-fc-ecard'])) {
			if (!Helper::verifyLogin()) {
				echo json_encode([
					'status' => false,
					'message' => 'You can not perform this action without login'
				]);
				exit;
			}
		}
		switch($_REQUEST['action']) {
			case 'check-network':
				if (Helper::isNetworkConnected() == "CONNECTED") {
					echo json_encode([
						'status' => true
					]);
				} else {
					echo json_encode([
						'status' => false
					]);
				}
				break;
			case 'process-login':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->login($_REQUEST);
				echo json_encode($response);
				break;
			case 'check-login':
				if (Helper::verifyLogin()) {
					echo json_encode([
						'status' => true
					]);
				} else {
					echo json_encode([
						'status' => false
					]);
				}
				break;
			case 'get-categories':
				$category = new Category();
				$categories = $category->all($_REQUEST['restaurant_id']);
				echo json_encode($categories);
				break;
			case 'get-items':
				$item = new Item();
				$items = $item->all($_REQUEST['restaurant_id']);
				echo json_encode($items);
				break;
			case 'category-items':
				$category_id = (!empty($_REQUEST['category_id'])) ? $_REQUEST['category_id'] : 0;
				$category = new Category();
				$items = $category->items($category_id);
				echo json_encode($items);
				break;
			case 'get-restaurant-info':
				$restaurant = new Restaurant();
				$restaurant_info = $restaurant->get($_REQUEST['restaurant_id']);
				echo json_encode($restaurant_info);
				break;
			case 'get-restaurant-menu':
				$restaurant = new Restaurant();
				$restaurant_menu = $restaurant->getMenu($_REQUEST);
				echo json_encode($restaurant_menu);
				break;
			case 'get-taxes':
				$tax = new Tax();
				$taxes_info = $tax->all($_REQUEST['restaurant_id']);
				echo json_encode($taxes_info);
				break;
			case 'get-discounts':
				$discount = new Discount();
				$discounts_info = $discount->all($_REQUEST['restaurant_id']);
				echo json_encode($discounts_info);
				break;
			case 'get-area-categories':
				$areaCategory = new AreaCategory();
				$areaCategories = $areaCategory->all($_REQUEST['restaurant_id']);
				echo json_encode($areaCategories);
				break;
			case 'get-tables':
				$table = new Table();
				$tables_info = $table->all($_REQUEST['restaurant_id']);
				echo json_encode($tables_info);
				break;
			case 'set-table-status':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$table = new Table();
				$table_data = [
					'restaurant_id' => $_REQUEST['restaurant_id'],
					'table_id' => $_REQUEST['table_id'],
					'table_status' => (!empty($_REQUEST['status']) && ($_REQUEST['status'] == 'A' || $_REQUEST['status'] == 'B')) ? $_REQUEST['status'] : 'A',
				];
				$tables_info = $table->setStatus($table_data);
				echo json_encode($tables_info);
				break;
			case 'save-order':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->save($_REQUEST['orderInfo']);
				echo json_encode($response);
				break;
			case 'update-order-stats':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->updateStats($_REQUEST);
				echo json_encode($response);
				break;
			case 'order-status':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				if(!empty($_REQUEST['rzpOrder_id'])) {
					$response = $order->statusByRZPOrderId($_REQUEST['rzpOrder_id']);
				} else if(!empty($_REQUEST['rzpPaymentLink_Id'])) {
					$response = $order->statusByRZPPaymentLinkId($_REQUEST['rzpPaymentLink_Id']);
				} else if(!empty($_REQUEST['order_id'])) {
					$response = $order->statusByOrderId($_REQUEST['restaurant_id'], $_REQUEST['order_id']);
				}
				echo json_encode($response);
				break;
			case 'remove-order':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->remove($_REQUEST['orderId']);
				echo json_encode($response);
				break;
			case 'delete-order':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'restore-order':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->restore($_REQUEST);
				echo json_encode($response);
				break;
			case 'set-order-lock-access':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->setLock($_REQUEST);
				echo json_encode($response);
				break;
			case 'generate-fc-ecard':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->generateFCECard($_REQUEST);
				echo json_encode($response);
				break;
			case 'notify-customer-at-invoke-fc-ecard':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$notifyFor = new Notify();
				$response = $notifyFor->invokeFCECard($_REQUEST);
				echo json_encode($response);
				break;
			case 'pay-with-fc-ecard':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->checkFCECardPaymentVerification($_REQUEST);
				if ($response['status'] == true) {
					$notify = (new Notify())->sendFCECardPaymentLink($_REQUEST);
					if (!empty($notify) && $notify['status'] == true) {
						echo json_encode([
							'status' => false,
							'message' => MESSAGES['FC_CARD_VERIFICATION_ENABLED'],
							'WFR' => true
						]);
					} else {
						echo json_encode([
							'status' => false,
							'message' => 'Payment Error : FC ECard payment can not be sent to this card',
						]);
					}
				} else {
					$response = $restaurant->payWithFCECard($_REQUEST);
					echo json_encode($response);
				}
				break;
			case 'customer-pay-with-fc-ecard':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$request = Helper::decryptRequestData($_REQUEST);
				$response = $restaurant->payWithFCECard($request);
				echo json_encode($response);
				break;
			case 'notify-customer-for-fc-ecard-transaction':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$notifyFor = new Notify();
				$response = $notifyFor->fcECardTransaction($_REQUEST);
				echo json_encode($response);
				break;
			case 'trigger-cart-updated':
				$table = new Table();
				$response = $table->cartUpdated($_REQUEST);
				echo json_encode($response);
				break;
			case 'trigger-cart-empty':
				$table = new Table();
				$response = $table->cartEmpty($_REQUEST);
				echo json_encode($response);
				break;
			case 'trigger-kitchen-for-items':
				$table = new Table();
				$response = $table->pushItemsToKitchen($_REQUEST);
				echo json_encode($response);
				break;
			case 'trigger-mark-items-as-served':
				$table = new Table();
				$response = $table->markItemsAsServed($_REQUEST);
				echo json_encode($response);
				break;
			case 'trigger-sync-categories':
				$category = new Category();
				$response = $table->triggerSyncCommand($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-category':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$category = new Category();
				$response = $category->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-category':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$category = new Category();
				$response = $category->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-category-for-office':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$category = new Category();
				$response = $category->saveForOffice($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-item':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$item = new Item();
				$response = $item->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-item':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$item = new Item();
				$response = $item->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-item-for-office':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$item = new Item();
				$response = $item->saveForOffice($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-item-price-info':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$item = new Item();
				$response = $item->savePriceInfo($_REQUEST);
				echo json_encode($response);
				break;	
			case 'get-stats':
				$order = new Order();
				$response = $order->stats($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-kitchen-log':
				$table = new Table();
				$response = $table->getKitchenLog($_REQUEST['restaurant_id']);
				echo json_encode($response);
				break;
			case 'get-table-cart-items':
				$table = new Table();
				$response = $table->getTableCartItems($_REQUEST['restaurant_id'], $_REQUEST['table_id']);
				echo json_encode($response);
				break;
			case 'get-orders':
				$order = new Order();
				$response = $order->all($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-orders-month-report':
				$order = new Order();
				$response = $order->monthReport($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-orders-year-report':
				$order = new Order();
				$response = $order->yearReport($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-order-base-info':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->orderBaseInfo($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-order-extra-info':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->orderExtraInfo($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-payment-methods':
				$paymentMethod = new PaymentMethod();
				$response = $paymentMethod->all();
				echo json_encode($response);
				break;
			case 'save-payment-method':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$paymentMethod = new PaymentMethod();
				$response = $paymentMethod->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-discount':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$discount = new Discount();
				$response = $discount->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-discount':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$discount = new Discount();
				$response = $discount->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-tax':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$tax = new Tax();
				$response = $tax->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-tax':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$tax = new Tax();
				$response = $tax->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-table':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$table = new Table();
				$response = $table->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-table':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$table = new Table();
				$response = $table->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-area-category':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$areaCategory = new AreaCategory();
				$response = $areaCategory->delete($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-area-category':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$areaCategory = new AreaCategory();
				$response = $areaCategory->save($_REQUEST);
				echo json_encode($response);
				break;
			case 'print-orders':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$order = new Order();
				$response = $order->printList($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-all-currencies':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$currency = new Currency();
				$response = $currency->all();
				echo json_encode($response);
				break;
			case 'update-restaurant-logo':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->uploadLogo($_REQUEST);
				echo json_encode($response);
				break;
			case 'save-restaurant-settings':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->saveProfile($_REQUEST);
				echo json_encode($response);
				break;
			case 'recharge-fc-ecard':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->rechargeFCECard($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-fc-ecard-info':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->getFCECardInfo($_REQUEST);
				echo json_encode($response);
				break;
			case 'set-fc-ecard-status':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->setFCECardStatus($_REQUEST);
				echo json_encode($response);
				break;
			case 'set-fc-ecard-payment-verification-flag':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->setFCECardPaymentVerificationFlag($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-fc-ecard-list':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->getFCECardList($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-fc-ecard-transactions':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->getFCECardTransactionsReport($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-fc-ecard-recharges':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->getFCECardRechargeReport($_REQUEST);
				echo json_encode($response);
				break;
			case 'generate-restaurant-menu-qr':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->generateMenuQR($_REQUEST);
				echo json_encode($response);
				break;
			case 'get-system-accounts':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->getSystemAccounts($_REQUEST);
				echo json_encode($response);
				break;
			case 'change-system-account-password':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->changeSystemAccountPassword($_REQUEST);
				echo json_encode($response);
				break;
			case 'delete-system-account':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->deleteSystemAccount($_REQUEST);
				echo json_encode($response);
				break;
			case 'remove-restaurant-logo':
				Helper::checkPostRequest($_SERVER['REQUEST_METHOD']);
				$restaurant = new Restaurant();
				$response = $restaurant->removeRestaurantLogo($_REQUEST);
				echo json_encode($response);
				break;
			case 'logout':
				Helper::logout($_REQUEST);
				break;
			default:
				echo json_encode([
					'status' => false,
					'message' => 'Invalid request'
				]);
				break;
		}
	} else {
		echo json_encode([
			'status' => false,
			'message' => 'Unauthorised access'
		]);
	}

	exit;