// Fetch latest orders
function fetchOrders() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$("table.pagination").remove();
			$("table.table-orders-office").show();
			var orders = LocalDB.orders();
			if (orders.length > 0) {
				if (orders.length >= NUMBER_OF_ITEM_PER_PAGE) {
					$("table.table-orders-office td span.orders-count").html(orders.length);
					var pagination = new Pagination(orders);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml("orders-office", "loadOrdersPerPage");
						$(paginationHtml).insertAfter("table.table-orders-office");
					}
					loadOrdersPerPage();
				} else {
					loadOrdersPerPage();
				}
			} else {
				$("table.table-orders-office tr.no-data-holder").remove();
				$("table.table-orders-office").append(generateNoDataFoundBlock(6));
			}
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
    } else {
        showLoginLayout(true);
    }
}

// Load area categories for an office
function loadAreaCategoriesForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$(".table-area-categories-office").show();
			$("table.pagination").remove();
			var areaCategories = LocalDB.areaCategories();
			if (areaCategories.length > 0) {
				if (areaCategories.length >= NUMBER_OF_ITEM_PER_PAGE) {
					var pagination = new Pagination(areaCategories);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml("area-categories-office", "loadAreaCategoriesPerPage");
						$(paginationHtml).insertAfter("table.table-area-categories-office");
					}
					loadAreaCategoriesPerPage();
				} else {
					loadAreaCategoriesPerPage();
				}
			} else {
				$("table.table-area-categories-office tr.no-data-holder").remove();
				$("table.table-area-categories-office").append(generateNoDataFoundBlock(4));
			}
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
        showLoginLayout(true);
    }
}

// Load tables for an office
function loadTablesForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$(".table-tables-office").show();
			$("table.pagination").remove();
			var tables = LocalDB.tables();
			if (tables.length > 0) {
				if (tables.length >= NUMBER_OF_ITEM_PER_PAGE) {
					var pagination = new Pagination(tables);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml("tables-office", "loadTablesPerPage");
						$(paginationHtml).insertAfter("table.table-tables-office");
					}
					loadTablesPerPage();
				} else {
					loadTablesPerPage();
				}
			} else {
				$("table.table-tables-office tr.no-data-holder").remove();
				$("table.table-tables-office").append(generateNoDataFoundBlock(5));
			}
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
        showLoginLayout(true);
    }
}

// Load items for an office
function loadItemsForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			var tablePrefix = 'table.table-';
			var tableTitle = 'items-office';
			var table = tablePrefix + tableTitle;
			$(table).show();
			$("table.pagination").remove();
			var items = LocalDB.items('all');
			if (items.length > 0) {
				if (items.length >= NUMBER_OF_ITEM_PER_PAGE) {
					var pagination = new Pagination(items);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml(tableTitle, "loadItemsPerPage");
						$(paginationHtml).insertAfter(table);
					}
					loadItemsPerPage();
				} else {
					loadItemsPerPage();
				}
			} else {
				$(table + " tr.no-data-holder").remove();
				$(table).append(generateNoDataFoundBlock(6));
			}
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
        showLoginLayout(true);
    }
}

// Load categories for an office
function loadCategoriesForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$(".table-categories-office").show();
			$("table.pagination").remove();
			var categories = LocalDB.categories();
			if (categories.length > 0) {
				if (categories.length >= NUMBER_OF_ITEM_PER_PAGE) {
					var pagination = new Pagination(categories);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml("categories-office", "loadCategoriesPerPage");
						$(paginationHtml).insertAfter("table.table-categories-office");
					}
					loadCategoriesPerPage();
				} else {
					loadCategoriesPerPage();
				}
			} else {
				$("table.table-categories-office tr.no-data-holder").remove();
				$("table.table-categories-office").append(generateNoDataFoundBlock(5));
			}
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
        showLoginLayout(true);
    }
}

// Load taxes for an office
function loadTaxesForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$(".table-taxes-office").show();
			$("table.table-taxes-office tr:not(.header)").remove();
			$("table.pagination").remove();
			var taxes = LocalDB.taxes();
			if (taxes.length > 0) {
				if (taxes.length >= NUMBER_OF_ITEM_PER_PAGE) {
					var pagination = new Pagination(taxes);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml("taxes-office", "loadTaxesPerPage");
						$(paginationHtml).insertAfter("table.table-taxes-office");
					}
					loadTaxesPerPage();
				} else {
					loadTaxesPerPage();
				}
			} else {
				$("table.table-taxes-office tr.no-data-holder").remove();
				$("table.table-taxes-office").append(generateNoDataFoundBlock(6));
			}
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
        showLoginLayout(true);
    }
}

// Load discounts for an office
function loadDiscountsForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$(".table-discounts-office").show();
			$("table.table-discounts-office tr:not(.header)").remove();
			$("table.pagination").remove();
			var discounts = LocalDB.discounts();
			if (discounts.length > 0) {
				if (discounts.length >= NUMBER_OF_ITEM_PER_PAGE) {
					var pagination = new Pagination(discounts);
					if (pagination.numberOfPages > 1) {
						var paginationHtml = pagination.getPagesHtml("discounts-office", "loadDiscountsPerPage");
						$(paginationHtml).insertAfter("table.table-discounts-office");
					}
					loadDiscountsPerPage();
				} else {
					loadDiscountsPerPage();
				}
			} else {
				$("table.table-discounts-office tr.no-data-holder").remove();
				$("table.table-discounts-office").append(generateNoDataFoundBlock(6));
			}
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
        showLoginLayout(true);
    }
}

// Store all payment methods supported by the system
function storePaymentMethods() {
	if (checkLogin()) {
		if (withAccess("office")) {
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-payment-methods',
				},
				function(paymentMethods) {
					LocalDB.set(LocalDB.paymentMethodsInfo, JSON.stringify(paymentMethods));
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    }
	}
}

// Store all currencies supported by the systen
function storeAllCurrencies() {
	if (checkLogin()) {
		if (withAccess("office")) {
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-all-currencies',
				},
				function(currencies) {
					LocalDB.set(LocalDB.currencyInfo, JSON.stringify(currencies));
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    }
	}	
}

// Set base info about order
function setOrderBaseInfo(orderId) {
	$("div.order-info-holder").find("span.progressing-msg").show();
	$("div.order-info-holder").find("table.order-items-list").empty();
	if (checkLogin()) {
		if (withAccess("office")) {
			var orderInfo = LocalDB.getOrderInfo(orderId);
			setLayoutForOrderBaseInfo(orderInfo);
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
	} else {
		showLoginLayout(true);
	}
}

// Find order from cloud
function findOrderByNumber(orderBookYear, orderNo) {
	if (checkLogin()) {
		if (withAccess("office") || withTableAccess()) {
			toggleWaitingLoader('show');
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-order-base-info',
					restaurant_id : RESTAURANT.ID,
					order_no : orderNo,
					order_book_year : orderBookYear,
					by_order_no : true
				},
				function(orderInfo) {
					toggleWaitingLoader('hide');
					if (typeof orderInfo.id != "undefined" && orderInfo.id > 0) {
						viewOrderInvoice(orderInfo);
					} else {
						showNotification("No order found for #" + orderNo, "error");
					}
				},
				function(error) {
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}
}

// Set base info about live order
function setLiveOrderBaseInfo(orderId) {
	$("div.order-info-holder").find("span.progressing-msg").show();
	$("div.order-info-holder").find("table.order-items-list").empty();
	if (checkLogin()) {
		if (withAccess("office")) {
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-order-base-info',
					restaurant_id : RESTAURANT.ID,
					order_id : orderId
				},
				function(orderInfo) {
					$("table.order-header-info span.manage-order-id").html(orderId);
			        $("table.order-header-info span.manage-order-number").html(orderInfo.order_no);
			        let tableInfo = LocalDB.getTableInfo(orderInfo.table_id);
			        let table_no = (typeof tableInfo != 'undefined') ? tableInfo.table_no : orderInfo.table_id;
			        let table_area_category = (typeof LocalDB.getAreaCategoryInfo(tableInfo.area_category_id) != 'undefined') ? ' (' + LocalDB.getAreaCategoryInfo(tableInfo.area_category_id).name + ')' : '';
			        $("table.order-header-info span.manage-order-table-no").html(table_no + table_area_category);
			        $("table.order-header-info span.manage-order-payment-method").html(orderInfo.payment_method);
			        $("table.order-header-info span.manage-order-date").html(orderInfo.created_at);
			        $("table.order-header-info").find("input[type='hidden'][name='g2c']").val(orderInfo.is_bill_given);
					setLayoutForOrderBaseInfo(orderInfo, true);
				},
				function(error) {
					showNotification(OOPS_MSG, "error");
				}
			);
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
	} else {
		showLoginLayout(true);
	}
}

// Set extra info for an order
function setOrderExtraInfo(orderId) {
	if (checkLogin()) {
		$("div.order-extra-info-holder").find("span.progressing-msg").show();
		$("div.order-extra-info-holder").find("table.order-extra-info tr.extra-fields").remove();
		if (withAccess("office")) {
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-order-extra-info',
					restaurant_id : RESTAURANT.ID,
					order_id : orderId
				},
				function(orderExtraInfo) {
					$("div.order-extra-info-holder").find("span.progressing-msg").hide();
					$("table.order-extra-info").show();
					if (typeof orderExtraInfo.id != 'undefined' && !isNaN(orderExtraInfo.id)) {
						$("table.order-extra-info").append(getExtraInfoBlock('Order No #', orderExtraInfo.order_no));
						var orderExtraInfo = JSON.parse(orderExtraInfo.extra_information);
						var noExtraInfo = true;
						if (typeof orderExtraInfo.customer_name != 'undefined') {
							if (orderExtraInfo.customer_name != '') {
								$("table.order-extra-info").append(getExtraInfoBlock('Customer Name', orderExtraInfo.customer_name));
								noExtraInfo = false;
							}
						}
						if (typeof orderExtraInfo.extra_fields != 'undefined') {
							if (orderExtraInfo.extra_fields.length > 0) {
								$(orderExtraInfo.extra_fields).each(function(ikey, info) {
									$("table.order-extra-info").append(getExtraInfoBlock(info.field, info.value));
								});
								noExtraInfo = false;
							}
						}
						if (noExtraInfo) {
							$("table.order-extra-info").append(getExtraInfoBlock('No extra information available for this order', '', 2));
						}
					} else {
						showNotification("No information available for this order", "error");
					}
				},
				function(error) {
					showNotification(OOPS_MSG, "error");
				}
			);
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
	} else {
		showLoginLayout(true);
	}
}

function getExtraInfoBlock(title, value, colspan = '1') {
	var extraInfoLine = "<tr class='extra-fields'>";
	extraInfoLine+= "<td colspan='"+colspan+"' class='bg-theme dotted-border-sm text-sm text-bold text-left'>";
	extraInfoLine+= title;
	extraInfoLine+= "</td>";
	if (colspan == '1') {
		extraInfoLine+= "<td class='bg-theme dotted-border-sm text-sm text-bold text-left'>";
		extraInfoLine+= value;
		extraInfoLine+= "</td>";
	}
	extraInfoLine+= "</tr>";
	return extraInfoLine;
}

// Add new item line for order manage popup
function addNewOrderItemLineForOffice(tableId = 0) {
	var oikey = $("tr.manage-order-item-line").length;
	var orderItemLine = '<tr class="manage-order-item-line">';
	orderItemLine+= '<input type="hidden" name="orderItems[id][' + oikey + ']" value="0" />';
	orderItemLine+= '<td class="dotted-border-sm text-left text-sm">';
	orderItemLine+= '<select class="order-set-new-item text-sm p-2-f" data-for="'+ oikey +'" data-ordertable="'+ tableId +'">';
	orderItemLine+= '<option value="">Select Item</option>';
	$(LocalDB.items('all')).each(function(ikey, item) {
		orderItemLine+= '<option value="'+ item.id +'">'+ item.name +'</option>';
	});
	orderItemLine+= '</select>';
	orderItemLine+= '<span class="hide" id="orderItemNameAt'+ oikey +'"></span>';
	orderItemLine+= '</td>';
	orderItemLine+= '<td class="dotted-border-sm text-center field">';
	orderItemLine+= '<input type="text" class="w-auto-f field-sm decimal" name="orderItems[price][' + oikey + ']" value="0" size="10" />';
	orderItemLine+= '</td>';
	orderItemLine+= '<td class="dotted-border-sm text-center field">';
	orderItemLine+= '<input type="text" class="w-auto-f field-sm flat" name="orderItems[qty][' + oikey + ']" value="0" size="10" />';
	orderItemLine+= '</td>';
	orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
	orderItemLine+= '<span id="orderItemsTotalFor' + oikey + '">0</span>';
	orderItemLine+= '</td>';
	orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
	orderItemLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-order-item mr-1" title="Delete Item" data-itemid="0"><img class="w-auto-f h-auto-f" src="assets/img/icons/icon-delete.png" /></button>';
	orderItemLine+= '</td>';
	orderItemLine+= '</tr>';
	$(orderItemLine).insertAfter($("div.order-info-holder").find("table.order-items-list tr.manage-order-item-line:last"));
}

// Set layout for order base info
function setLayoutForOrderBaseInfo(orderInfo, isLive = false) {
	$("div.order-info-holder").find("span.progressing-msg").hide();
	if (orderInfo.is_bill_given == 1) {
		$("td.bill-given-tag").show();
	} else {
		$("td.bill-given-tag").hide();
	}
	$("td.bill-given-tag").remove();
	var orderItems = $.parseJSON(orderInfo.order_info);
	var orderDiscounts = $.parseJSON(orderInfo.discount_info);
	var orderTaxes = $.parseJSON(orderInfo.tax_info);
	var orderBaseComponentsLine = '<tr>';
	orderBaseComponentsLine+= '<td class="text-left dotted-border-sm">';
	orderBaseComponentsLine+= '<label class="d-block text-sm" for="orderTableId">Table</label>';
	orderBaseComponentsLine+= '<select name="orderTableId" class="p-2-f">';
	orderBaseComponentsLine+= getTableSelectOptions(false, orderInfo.table_id);
	orderBaseComponentsLine+= '</select>';
	orderBaseComponentsLine+= '<td class="text-left dotted-border-sm">';
	orderBaseComponentsLine+= '<label class="d-block text-sm" for="orderPaymentMethod">Payment Method</label>';
	orderBaseComponentsLine+= '<select name="orderPaymentMethod" class="p-2-f">';
	orderBaseComponentsLine+= getPaymentMethodsSelectOptions(false, orderInfo.payment_method);
	orderBaseComponentsLine+= '</select>';
	$("div.order-info-holder").find("table.order-items-list").append(orderBaseComponentsLine);
	orderBaseComponentsLine+= '</td>';
	orderBaseComponentsLine+= '</tr>';
	var orderItemLineAddBtn = '<tr>';
	orderItemLineAddBtn+= "<td colspan='5' class='text-right'>";
	orderItemLineAddBtn+= '<button class="btn-solid btn-sm btn-info btn-add-order-item text-white text-sm" data-ordertable="'+ orderInfo.table_id +'">Add Item</button>';
	orderItemLineAddBtn+= '</td>';
	orderItemLineAddBtn+= '</tr>';
	$("div.order-info-holder").find("table.order-items-list").append(orderItemLineAddBtn);
	if (orderItems.length > 0) {
		var orderTotal = 0;
		var orderItemLine = '<tr>';
		orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
		orderItemLine+= 'Item';
		orderItemLine+= '</td>';
		orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
		orderItemLine+= 'Rate (' + RESTAURANT.CURRENCY + ')';
		orderItemLine+= '</td>';
		orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
		orderItemLine+= 'Qty';
		orderItemLine+= '</td>';
		orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
		orderItemLine+= 'Amount (' + RESTAURANT.CURRENCY + ')';
		orderItemLine+= '</td>';
		orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
		orderItemLine+= 'Action';
		orderItemLine+= '</td>';
		orderItemLine+= '</tr>';
		$("div.order-info-holder").find("table.order-items-list").append(orderItemLine);
		$(orderItems).each(function(oikey, orderItem) {
			var orderItemLine = '<tr class="manage-order-item-line item-holder-for-' + orderItem.itemId + '">';
			orderItemLine+= '<input type="hidden" name="orderItems[id][' + oikey + ']" value="' + toNumeric(orderItem.itemId) + '" />';
			orderItemLine+= '<td class="dotted-border-sm text-left text-sm">';
			orderItemLine+= '<span id="orderItemNameAt' + oikey + '">' + orderItem.name + '</span>';
			orderItemLine+= '</td>';
			orderItemLine+= '<td class="dotted-border-sm text-center field">';
			orderItemLine+= '<input type="text" class="w-auto-f field-sm decimal" name="orderItems[price][' + oikey + ']" value="' + toNumeric(orderItem.price).toFixed(2) + '" size="10" />';
			orderItemLine+= '</td>';
			orderItemLine+= '<td class="dotted-border-sm text-center field">';
			orderItemLine+= '<input type="text" class="w-auto-f field-sm flat" name="orderItems[qty][' + oikey + ']" value="' + toNumeric(orderItem.qty) + '" size="10" />';
			orderItemLine+= '</td>';
			orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
			var itemTotal = (toNumeric(orderItem.price) * toNumeric(orderItem.qty));
			orderItemLine+= '<span id="orderItemsTotalFor' + oikey + '">'+(itemTotal).toFixed(2) + '</span>';
			orderItemLine+= '</td>';
			orderItemLine+= '<td class="dotted-border-sm text-center text-sm">';
			orderItemLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-order-item mr-1" title="Delete Item" data-itemid="'+ orderItem.itemId +'"><img class="w-auto-f h-auto-f" src="assets/img/icons/icon-delete.png" /></button>';
			orderItemLine+= '</td>';
			orderItemLine+= '</tr>';
			orderTotal+= itemTotal;
			$("div.order-info-holder").find("table.order-items-list").append(orderItemLine);
		});
		var subTotal = orderTotal;
		var orderSubTotalLine = '<tr>';
		orderSubTotalLine+= '<td colspan="3" class="dotted-border-sm text-right text-sm subtotal">';
		orderSubTotalLine+= 'Subtotal';
		orderSubTotalLine+= '</td>';
		orderSubTotalLine+= '<td class="dotted-border-sm text-center text-sm subtotal">';
		orderSubTotalLine+= '<span id="subTotalAmount">' + orderTotal.toFixed(2) + '</span>';
		orderSubTotalLine+= '</td>';
		orderSubTotalLine+= '</tr>';
		$("div.order-info-holder").find("table.order-items-list").append(orderSubTotalLine);

		if (orderDiscounts.length > 0) {
			var discountCounter = 0;
			orderDiscounts = LocalDB.sequencedDiscounts(orderDiscounts);
			$(orderDiscounts).each(function(odkey, orderDiscount) {
				orderDiscountAmount = 0;
				var hasDiscount = false;
				if (orderDiscount.minimum_amount != null && subTotal >= toNumeric(orderDiscount.minimum_amount)) {
					if (orderDiscount.type == "percentage") {
						var percentage = orderDiscount.amount;
						orderDiscountAmount = ((orderTotal * percentage) / 100).toRound();
					} else {
						orderDiscountAmount = orderDiscount.amount.toRound();
					}
					orderTotal-= toNumeric(orderDiscountAmount);
					hasDiscount = true;
				} else if (orderDiscount.minimum_amount == null) {
					if (orderDiscount.type == "percentage") {
						var percentage = orderDiscount.amount;
						orderDiscountAmount = ((orderTotal * percentage) / 100).toRound();
					} else {
						orderDiscountAmount = orderDiscount.amount.toRound();
					}
					orderTotal-= toNumeric(orderDiscountAmount);
					hasDiscount = true;
				}
				if (hasDiscount) {
					var orderDiscountLine = '<tr>';
					orderDiscountLine+= '<td class="dotted-border-sm text-sm text-right" colspan="2">';
					orderDiscountLine+= 'Discount';
					orderDiscountLine+= '</td>';
					orderDiscountLine+= '<td class="dotted-border-sm text-sm">';
					let orderDiscountLabel = ' ('+ orderDiscount.amount;
					orderDiscountLabel+= (orderDiscount.type == 'percentage') ? '%' : RESTAURANT.CURRENCY;
					orderDiscountLabel+= ')';
					orderDiscountLine+= '<span id="orderDiscountAt' + odkey + '">' + orderDiscount.description + orderDiscountLabel + '</span>';
					orderDiscountLine+= '</td>';
					orderDiscountLine+= '<td class="dotted-border-sm text-sm field">';
					orderDiscountLine+= '<input type="text" readonly="" class="w-auto-f field-sm decimal text-center summary-field" name="orderDiscounts[' + discountCounter + ']" data-type="'+orderDiscount.type+'" data-amount="'+orderDiscount.amount+'" data-minamount="'+orderDiscount.minimum_amount+'" value="' + toNumeric(orderDiscountAmount).toFixed(2) + '" size="10" />';
					orderDiscountLine+= '</td>';
					orderDiscountLine+= '</tr>';
					discountCounter++;
					$("div.order-info-holder").find("table.order-items-list").append(orderDiscountLine);
				}
			});
		}

		if (orderTaxes.length > 0) {
			var orderTotalExclTaxLine = '<tr>';
			orderTotalExclTaxLine+= '<td colspan="3" class="dotted-border-sm text-right subtotal-excl-tax text-sm summary-field">';
			orderTotalExclTaxLine+= 'Subtotal (Excl. Tax)';
			orderTotalExclTaxLine+= '</td>';
			orderTotalExclTaxLine+= '<td class="dotted-border-sm text-sm text-center subtotal-excl-tax summary-field">';
			orderTotalExclTaxLine+= '<span id="subTotalExclTaxAmount">' + orderTotal.toFixed(2) + '</span>';
			orderTotalExclTaxLine+= '</td>';
			orderTotalExclTaxLine+= '</tr>';
			$("div.order-info-holder").find("table.order-items-list").append(orderTotalExclTaxLine);
		}

		var orderTotalAfterDiscounts = orderTotal;
		if (orderTaxes.length > 0) {
			$(orderTaxes).each(function(otkey, orderTax) {
				var orderTaxLine = '<tr>';
				orderTaxLine+= '<td class="dotted-border-sm text-sm text-right" colspan="2">';
				orderTaxLine+= 'Tax';
				orderTaxLine+= '</td>';
				orderTaxLine+= '<td class="dotted-border-sm text-sm">';
				let orderTaxLabel = ' ('+ orderTax.amount;
				orderTaxLabel+= (orderTax.type == 'percentage') ? '%' : RESTAURANT.CURRENCY;
				orderTaxLabel+= ')';
				orderTaxLine+= '<span id="orderTaxAt' + otkey + '">' + orderTax.name + orderTaxLabel +'</span>';
				orderTaxLine+= '</td>';
				orderTaxAmount = 0;
				if (orderTax.type == "percentage") {
					var percentage = toNumeric(orderTax.amount);
					orderTaxAmount = toNumeric((orderTotalAfterDiscounts * percentage) / 100).toRound();
				} else {
					orderTaxAmount = orderTax.amount.toRound();
				}
				orderTotal+= toNumeric(orderTaxAmount);
				orderTaxLine+= '<td class="dotted-border-sm text-sm field">';
				orderTaxLine+= '<input type="text" readonly="" class="w-auto-f field-sm decimal text-center summary-field" name="orderTaxes[' + otkey + ']" value="' + toNumeric(orderTaxAmount).toFixed(2) + '" data-type="'+orderTax.type+'" data-amount="'+orderTax.amount+'" data-taxtype="'+orderTax.tax_type+'" size="10" />';
				orderTaxLine+= '</td>';
				orderTaxLine+= '</tr>';
				$("div.order-info-holder").find("table.order-items-list").append(orderTaxLine);
			});
		}
		
		let totalRoundedBy = Math.round(orderTotal) - orderTotal;
		var orderTotalRoundLine = '<tr class="grandtotal-round-row">';
		orderTotalRoundLine+= '<td colspan="3" class="dotted-border-sm text-right grandtotal-round">';
		orderTotalRoundLine+= 'Rounded By';
		orderTotalRoundLine+= '</td>';
		orderTotalRoundLine+= '<td class="dotted-border-sm text-center grandtotal-round">';
		orderTotalRoundLine+= "<span id='grandtotalRoundAmount'>"+ totalRoundedBy.toFixed(2) +"</span>";
		orderTotalRoundLine+= '</td>';
		orderTotalRoundLine+= '</tr>';
		$("div.order-info-holder").find("table.order-items-list").append(orderTotalRoundLine);
		if (totalRoundedBy == 0) {
			$("tr.grandtotal-round-row").hide();
		} else {
			$("tr.grandtotal-round-row").show();
		}

		var orderTotalLine = '<tr>';
		orderTotalLine+= '<td colspan="3" class="dotted-border-sm text-right grandtotal">';
		orderTotalLine+= 'Grand Total';
		orderTotalLine+= '</td>';
		orderTotalLine+= '<td class="dotted-border-sm text-center grandtotal">';
		orderTotalLine+= "<span id='grandtotalAmount'>"+ Math.round(orderTotal).toFixed(2) +"</span>";
		orderTotalLine+= '</td>';
		orderTotalLine+= '</tr>';
		$("div.order-info-holder").find("table.order-items-list").append(orderTotalLine);
		var orderSaveButtonLine = '<tr>';
		orderSaveButtonLine+= '<td colspan="5" class="p-5-f">';
		orderSaveButtonLine+= '&nbsp;';
		orderSaveButtonLine+= '</td>';
		orderSaveButtonLine+= '</tr>';
		orderSaveButtonLine+= '<tr>';
		orderSaveButtonLine+= '<td colspan="5" class="text-center">';
		orderSaveButtonLine+= '<button class="btn-solid btn-save-office-order mt-2-f" data-orderid="' + toNumeric(orderInfo.id) + '" data-islive="' + isLive + '">Save Order</button>';
		orderSaveButtonLine+= '</td>';
		orderSaveButtonLine+= '</tr>';
		$("div.order-info-holder").find("table.order-items-list").append(orderSaveButtonLine);
	} else {
		var noItemLine = '<tr>';
		noItemLine+= '<td colspan="4" class="dotted-border-sm text-center">';
		noItemLine+= "No items found";
		noItemLine+= '</td>';
		noItemLine+= '</tr>';
		$("div.order-info-holder").find("table.order-items-list").append(noItemLine);
	}
}

// Load payment methods for office
function loadPaymentMethodsForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'get-payment-methods',
				},
				function(paymentMethods) {
					LocalDB.set(LocalDB.paymentMethodsInfo, JSON.stringify(paymentMethods));
					toggleWaitingLoader("hide");
					$(".table-payment-methods-office").show();
					$("table.table-payment-methods-office tr:not(.header)").remove();
					$("table.pagination").remove();
					var enabledPaymentMethods = RESTAURANT.PAYMENT_METHODS;
					if ($(paymentMethods).length > 0) {
						$(paymentMethods).each(function(pmkey, paymentMethod) {
							var paymentMethodLine = '<tr>';
							paymentMethodLine+= '<td class="dotted-border-sm office-line-item">';
							paymentMethodLine+= paymentMethod.payment_method;
							paymentMethodLine+= '</td>';
							paymentMethodLine+='<td class="dotted-border-sm">';
							var paymentMethodEnabled = false;
							$(enabledPaymentMethods).each(function(ek, epm) {
								if (!paymentMethodEnabled && epm.id == paymentMethod.id) {
									paymentMethodEnabled = true;
								}
							});
							if (paymentMethodEnabled) {
								paymentMethodLine+= '<button class="btn-solid btn-change-pm-status btn-deactivate-payment-method" data-id="'+paymentMethod.id+'">De-Activate</button>';
							} else {
								paymentMethodLine+= '<button class="btn-solid btn-change-pm-status btn-activate-payment-method" data-id="'+paymentMethod.id+'">Activate</button>';
							}
							paymentMethodLine+= '</td>';
							paymentMethodLine+= '</tr>';
							$("table.table-payment-methods-office").append(paymentMethodLine);
						});
					} else {
						$("table.table-payment-methods-office tr.no-data-holder").remove();
						$("table.table-payment-methods-office").append(generateNoDataFoundBlock(2));
					}
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    } else {
	    	showNotification(NO_ACCESS_MSG, 'error');
	    }
	} else {
        showLoginLayout(true);
    }
}

// Update payment method info
function updatePaymentMethod(paymentMethod) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'save-payment-method',
					restaurant_id : RESTAURANT.ID,
					paymentMethod : JSON.stringify(paymentMethod)
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						var restaurantInfo = LocalDB.restaurant();
						restaurantInfo.payment_methods = response.payment_methods;
						LocalDB.set(LocalDB.restaurantInfo, JSON.stringify(restaurantInfo));
						RESTAURANT.PAYMENT_METHODS = restaurantInfo.payment_methods;
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='paymentmethods']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Load profile settings/configuration
function loadProfileSettingsForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$("table.pagination").remove();
			$(".table-profile-settings-office").show();
			var restaurant = LocalDB.restaurant();
			if (!isMobileScreen()) {
				$(".table-profile-settings-office").find("input[type='text']:first").trigger("focus");
			}
			$("input[name='profile_restaurant_name']").val(restaurant.name);
			$("select[name='profile_restaurant_currency']").html(getCurrencySelectOptions(true));
			$("select[name='profile_restaurant_currency']").val(restaurant.currency_id);
			$("input[name='profile_restaurant_contact_number']").val(restaurant.contact_number);
			$("textarea[name='profile_restaurant_address']").val(restaurant.address);
			$("input[name='profile_restaurant_gst_number']").val(restaurant.gst_number);
			$("select[name='profile_restaurant_ob_type']").val(restaurant.order_book_type);
			var restaurantFoodTypes = LocalDB.getRestaurantFoodTypes();
			$("input[type='checkbox'].restaurant-food-type").each(function() {
	            if ($.inArray($(this).val(), restaurantFoodTypes) >= 0) {
	                $(this).prop("checked", true);
	            }
	        });
	        $("textarea[name='profile_restaurant_invoice_bottom_line']").val(restaurant.invoice_bottom_line);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Load order report section
function loadOrderReportForOffice() {
	if (checkLogin()) {
		if (withAccess("office")) {
			$("table.pagination").remove();
			$("button.btn-print-order-report").hide();
			$(".table-orders-report-office").show();
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);	
	}
}

// Save profile settings/configuration
function saveRestaurantProfile(restaurantProfile) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'save-restaurant-settings',
					restaurant_id : RESTAURANT.ID,
					settings : JSON.stringify(restaurantProfile)
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						showNotification(response.message, "success");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}
}

// Delete discount from cloud
function deleteDiscountToCloud(discountId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'delete-discount',
					restaurant_id : RESTAURANT.ID,
					discount_id : discountId
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						LocalDB.set(LocalDB.discountInfo, JSON.stringify(response.discounts));
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='discounts']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Save discount to cloud
function saveDiscountToCloud(discount) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'save-discount',
					restaurant_id : RESTAURANT.ID,
					discount : JSON.stringify(discount),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						LocalDB.set(LocalDB.discountInfo, JSON.stringify(response.discounts));
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='discounts']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);	
	}
}

// Delete tax from cloud
function deleteTaxToCloud(taxId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'delete-tax',
					restaurant_id : RESTAURANT.ID,
					tax_id : taxId
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						LocalDB.set(LocalDB.taxInfo, JSON.stringify(response.taxes));
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='taxes']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Save tax to cloud
function saveTaxToCloud(tax) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'save-tax',
					restaurant_id : RESTAURANT.ID,
					tax : JSON.stringify(tax),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						LocalDB.set(LocalDB.taxInfo, JSON.stringify(response.taxes));
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='taxes']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);	
	}
}

// Delete category from cloud
function deleteCategoryToCloud(categoryId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			var itemsInCategory = LocalDB.items(toNumeric(categoryId)).length;
			if (itemsInCategory > 0) {
				if (!confirm('There are ' + itemsInCategory + " items in this category. Are you sure want to delete those items as well?")) {
					return false;
				}
			}
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'delete-category',
					restaurant_id : RESTAURANT.ID,
					category_id : categoryId
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						LocalDB.set(LocalDB.categoriesInfo, JSON.stringify(response.categories));
						showNotification(response.message, "info");
						var categoryRow = $("tr.office-list-category-row[data-id='" + categoryId + "']");
						if ($(categoryRow).length > 0) {
							$(categoryRow).remove();
						}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Save category to cloud
function saveCategoryToCloudForOffice(category) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'save-category-for-office',
					restaurant_id : RESTAURANT.ID,
					category : JSON.stringify(category),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						LocalDB.set(LocalDB.categoriesInfo, JSON.stringify(response.categories));
						showNotification(response.message, "info");
						var categoryRow = $("tr.office-list-category-row[data-id='" + response.category.id + "']");
						if ($(categoryRow).length == 0) {
							if ($("button.btn-office-action[data-action='categories']").hasClass("active")) {
	                            $("button.btn-office-action[data-action='categories']").trigger("click");
	                        }
						} else {
							$(categoryRow).find("td.category-name").html(response.category.name);
							$(categoryRow).find("td.category-description").html(response.category.description);
						}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);	
	}
}

// Save item to cloud
function saveItemToCloudForOffice(item) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'save-item-for-office',
					restaurant_id : RESTAURANT.ID,
					item : JSON.stringify(item),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						LocalDB.saveItem(response.item, response.item.category_id, true);
						showNotification(response.message, "info");
						var itemRow = $("tr.office-list-item-row[data-id='" + response.item.id + "']");
						if ($(itemRow).length == 0) {
							if ($("button.btn-office-action[data-action='items']").hasClass("active")) {
	                            let page = Math.ceil(LocalDB.items('all').length / NUMBER_OF_ITEM_PER_PAGE);
	                            // var items = LocalDB.items('all');
	                            // var pagination = new Pagination(items);
	                            // loadItemsPerPage(pagination.inbounds(page).start, pagination.inbounds(page).end, page, items);
	                            if ($("table.pagination-for-items-office td[data-page="+ page +"]").length > 0) {
	                            	$("table.pagination-for-items-office td[data-page="+ page +"]").trigger("click");
	                            } else {
	                            	$("button.btn-office-action[data-action='items']").trigger("click");
	                            }
	                        }
						} else {
							$(itemRow).find("td.item-code").html(response.item.item_code);
							$(itemRow).find("td.item-name").html(response.item.name);
							$(itemRow).find("td.item-category").html(LocalDB.getCategoryInfo(response.item.category_id).name);
							$(itemRow).find("td.item-price").html(response.item.price);
						}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);	
	}
}

// Delete item from cloud
function deleteItemToCloud(itemId, categoryId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'delete-item',
					restaurant_id : RESTAURANT.ID,
					item_id : itemId,
					category_id : categoryId
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						LocalDB.set(LocalDB.itemsInfo, JSON.stringify(response.items));
						showNotification(response.message, "info");
						var itemRow = $("tr.office-list-item-row[data-id='" + itemId + "']");
						if ($(itemRow).length > 0) {
							$(itemRow).remove();
						}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Save item price info to cloud
function setItemPriceInfoToCloud(itemId, itemPriceInfo) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'save-item-price-info',
					restaurant_id : RESTAURANT.ID,
					item_id : itemId,
					item_price_info : JSON.stringify(itemPriceInfo)
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						showNotification(response.message, "success");
						LocalDB.setItemPriceInfo(toNumeric(response.item_id), response.item_price_info);
						hideModal();
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}
}

// Save category to cloud
function saveTableToCloud(table) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'save-table',
					restaurant_id : RESTAURANT.ID,
					table : JSON.stringify(table),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						LocalDB.set(LocalDB.tableInfo, JSON.stringify(response.tables));
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='tables']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);	
	}
}

// Delete table from cloud
function deleteTableToCloud(tableId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'delete-table',
					restaurant_id : RESTAURANT.ID,
					table_id : tableId,
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						LocalDB.set(LocalDB.tableInfo, JSON.stringify(response.tables));
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='tables']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Save area category to cloud
function saveAreaCategoryToCloud(areaCategory) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'save-area-category',
					restaurant_id : RESTAURANT.ID,
					area_category : JSON.stringify(areaCategory),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='areacategories']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);	
	}
}

// Delete area category from cloud
function deleteAreaCategoryToCloud(areaCategoryId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'delete-area-category',
					restaurant_id : RESTAURANT.ID,
					area_category_id : areaCategoryId,
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						showNotification(response.message, "info");
						$("button.btn-office-action[data-action='areacategories']").trigger("click");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

function loadOrdersPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection = null) {
	var orders = (collection != null) ? collection : LocalDB.orders();
	orders = orders.reverse();
	clearTableCommonBlocks('orders');
	var pageSummary = Pagination.getPageSummary(start, end, page, orders.length);
	$("table.table-orders-office tr.summary td:first").prepend(pageSummary);
	var totalOrderAmount = 0;
	var totalOrderAmountWithoutEmpty = 0;
	$(orders).each(function(okey, order) {
		if (okey >= start && okey <= end) {
			var is_bill_given_class = "";
			var is_order_deleted = "";
			if (order.is_bill_given == 1) {
				is_bill_given_class = "bill_given";
			}
			if (order.is_empty == 1) {
				is_order_deleted = "order-deleted";
			}
			var orderLine = '<tr class="today-order-data ' + is_order_deleted + '" data-id="' + order.id + '">';
			orderLine+= '<td class="dotted-border-sm office-line-order ' + is_bill_given_class + '">';
			orderLine+= (okey + 1);
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm cursor-pointer office-order-link ' + is_bill_given_class + '" data-id="' + order.id + '">';
			orderLine+= order.order_no;
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm order-amount ' + is_bill_given_class + '">';
			orderLine+= toNumeric(order.order_amount).toFixed(2).toNumberFormat();
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= order.payment_method.toString().toUpperCase();
			if (order.payment_method == "fcECard") {
				var extraPaymentInfo = "";
				var paymentMethodSummary = JSON.parse(order.payment_method_info);
				extraPaymentInfo = paymentMethodSummary.fcECardNumber;
				orderLine+= '<span class="d-block payment-info-extra text-sm badge-fc-ecard">' + extraPaymentInfo + '</span>';
			}
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= (typeof order.table_no != 'undefined') ? order.table_no : LocalDB.getTableInfo(order.table_id).table_no;
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= '<button class="btn-solid btn-sm btn-view-order-extra-info btn-info mr-1" title="View extra information" data-order="'+ order.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-eye.png" /></button>';
			orderLine+= '</td>';
			$("table.table-orders-office").append(orderLine);
		}
		totalOrderAmount+= toNumeric(order.order_amount);
		if (order.is_empty == 1) {
			totalOrderAmountWithoutEmpty+= toNumeric(order.order_amount);
		}
	});
	var totalAmountToDisplay = totalOrderAmount - totalOrderAmountWithoutEmpty;
	var orderSummary = '<span class="orders-summary ml-px5 text-sm text-bold bg-highlighted-f">';
	orderSummary+= 'Total Amount - ' + toNumeric(totalAmountToDisplay).toFixed(2).toNumberFormat() + " " + RESTAURANT.CURRENCY;
	orderSummary+= '</span>';
	$("table.table-orders-office tr.summary td:nth-child(2)").append(orderSummary);
	Pagination.managePagination(page, "orders-office");
}

function loadOrdersPerPageForReport(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection = null) {
	var orders = (collection != null) ? collection : [];
	clearTableCommonBlocks('orders-report');
	var pageSummary = Pagination.getPageSummary(start, end, page, orders.length);
	$("table.table-orders-report-office tr.summary td:first").prepend(pageSummary);
	var totalOrderAmount = 0;
	var totalOrderAmountWithoutEmpty = 0;
	$(orders).each(function(okey, order) {
		if (okey >= start && okey <= end) {
			var is_bill_given_class = "";
			var is_order_deleted = "";
			if (order.is_bill_given == 1) {
				is_bill_given_class = "bill_given";
			}
			if (order.is_empty == 1) {
				// is_order_deleted = "order-deleted";
			}
			var orderLine = '<tr class="report-data ' + is_order_deleted + '" data-id="' + order.id + '">';
			orderLine+= '<td class="dotted-border-sm office-line-order-report ' + is_bill_given_class + '">';
			orderLine+= (okey + 1);
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm cursor-pointer office-order-report-link ' + is_bill_given_class + '" data-id="' + order.id + '">';
			orderLine+= order.order_no;
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm order-amount ' + is_bill_given_class + '">';
			orderLine+= toNumeric(order.order_amount).toFixed(2).toNumberFormat();
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= order.payment_method.toString().toUpperCase();
			if (order.payment_method == "fcECard") {
				var extraPaymentInfo = "";
				var paymentMethodSummary = JSON.parse(order.payment_method_info);
				extraPaymentInfo = paymentMethodSummary.fcECardNumber;
				orderLine+= '<span class="d-block payment-info-extra text-sm badge-fc-ecard">' + extraPaymentInfo + '</span>';
			}
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= (typeof order.table_no != 'undefined') ? order.table_no : LocalDB.getTableInfo(order.table_id).table_no;
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= order.created_at;
			orderLine+= '</td>';
			orderLine+= '<td class="dotted-border-sm ' + is_bill_given_class + '">';
			orderLine+= '<button class="btn-solid btn-sm btn-view-order-extra-info btn-info mr-1" title="View extra information" data-order="'+ order.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-eye.png" /></button>';
			orderLine+= '</td>';
			$("table.table-orders-report-office").append(orderLine);
		}
		totalOrderAmount+= toNumeric(order.order_amount);
		if (order.is_empty == 1) {
			// totalOrderAmountWithoutEmpty+= toNumeric(order.order_amount);
		}
	});
	$("table.table-orders-report-office tr.summary td span.orders-report-summary").remove();
	var totalAmountToDisplay = totalOrderAmount - totalOrderAmountWithoutEmpty;
	var orderSummary = '<span class="orders-report-summary ml-px5 text-bold float-right bg-highlighted-f">';
	orderSummary+= 'Total Amount - ' + toNumeric(totalAmountToDisplay).toFixed(2).toNumberFormat() + " " + RESTAURANT.CURRENCY;
	orderSummary+= '</span>';
	$("table.table-orders-report-office tr.summary td:nth-child(2)").append(orderSummary);
}

function loadCategoriesPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection = null) {
	var categories = (collection != null) ? collection : LocalDB.categories();
	clearTableCommonBlocks('categories');
	var pageSummary = Pagination.getPageSummary(start, end, page, categories.length);
	$("table.table-categories-office tr.summary td:first").prepend(pageSummary);
	$(categories).each(function(ckey, category) {
		if (ckey >= start && ckey <= end) {
			var categoryLine = '<tr class="office-list-category-row" data-id="' + category.id + '">';
			categoryLine+= '<td class="dotted-border-sm text-center office-line-item">';
			categoryLine+= (ckey + 1);
			categoryLine+= '</td>';
			categoryLine+= '<td class="dotted-border-sm category-name">';
			categoryLine+= category.name;
			categoryLine+= '</td>';
			categoryLine+= '<td class="dotted-border-sm category-description">';
			categoryLine+= category.description;
			categoryLine+= '</td>';
			categoryLine+='<td class="dotted-border-sm">';
			categoryLine+= LocalDB.items(category.id).length;
			categoryLine+= '</td>';
			categoryLine+= '<td class="dotted-border-sm">';
			categoryLine+= '<button class="btn-solid btn-sm btn-edit-category btn-info mr-1" title="Edit Category" data-categoryid="'+ category.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-pencil.png" /></button>';
			categoryLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-category mr-1" title="Delete Category" data-categoryid="'+ category.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png" /></button>';
			categoryLine+= '</td>';
			categoryLine+= '</tr>';
			$("table.table-categories-office").append(categoryLine);
		}
	});
	Pagination.managePagination(page, "categories-office");
}

function loadItemsPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection) {
	var items = (collection != null) ? collection : LocalDB.items('all');
	clearTableCommonBlocks('items');
	var pageSummary = Pagination.getPageSummary(start, end, page, items.length);
	var table = 'table.table-items-office';
	$(table + " tr.summary td:first").prepend(pageSummary);
	$(items).each(function(ikey, item) {
		var itemCategory = LocalDB.getCategoryInfo(item.category_id);
		if (typeof itemCategory != "undefined" && itemCategory.id > 0) {
			if (ikey >= start && ikey <= end) {
					var itemLine = '<tr class="office-list-item-row" data-id="' + item.id + '">';
					itemLine+= '<td class="dotted-border-sm text-center office-line-item srno">';
					itemLine+= (ikey + 1);
					itemLine+= '</td>';
					itemLine+= '<td class="dotted-border-sm text-center office-line-item item-code">';
					itemLine+= item.item_code;
					itemLine+= '</td>';
					itemLine+= '<td class="dotted-border-sm item-name">';
					itemLine+= item.name;
					itemLine+= '</td>';
					itemLine+='<td class="dotted-border-sm item-category">';
					itemLine+= (typeof itemCategory != 'undefined') ? itemCategory.name : '-';
					itemLine+= '</td>';
					itemLine+= '<td class="dotted-border-sm">';
					itemLine+= '<button class="btn-solid btn-sm btn-office-set-item-price-info btn-warning mr-1" title="Edit item price info" data-itemid="'+ item.id +'"><span class="text-white text-bold font-20-f">&nbsp;'+ RESTAURANT.CURRENCY +'&nbsp;</span></button>';
					itemLine+= '<button class="btn-solid btn-sm btn-edit-item btn-info mr-1" title="Edit Item" data-itemid="'+ item.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-pencil.png" /></button>';
					itemLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-item mr-1" title="Delete Item" data-itemid="'+ item.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png" /></button>';
					itemLine+= '</td>';
					itemLine+= '</tr>';
					$(table).append(itemLine);
			}
		}
	});
	Pagination.managePagination(page, "items-office");
}

function loadTablesPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection = null) {
	var tables = (collection != null) ? collection : LocalDB.tables();
	clearTableCommonBlocks('tables');
	var pageSummary = Pagination.getPageSummary(start, end, page, tables.length);
	$("table.table-tables-office tr.summary td:first").prepend(pageSummary);
	$(tables).each(function(tkey, table) {
		if (tkey >= start && tkey <= end) {
			var areaCategory = LocalDB.getAreaCategoryInfo(table.area_category_id);
			if (typeof areaCategory != "undefined") {
				var tableLine = '<tr>';
				tableLine+= '<td class="dotted-border-sm text-center office-line-item">';
				tableLine+= (tkey + 1);
				tableLine+= '</td>';
				tableLine+= '<td class="dotted-border-sm text-center">';
				tableLine+= table.table_no;
				tableLine+= '</td>';
				tableLine+='<td class="dotted-border-sm">';
				tableLine+= areaCategory.name;
				tableLine+= '</td>';
				tableLine+='<td class="dotted-border-sm">';
				tableLine+= (typeof table.table_alias == 'undefined' || table.table_alias == null) ? '-' : table.table_alias;
				tableLine+= '</td>';
				tableLine+= '<td class="dotted-border-sm">';
				tableLine+= '<button class="btn-solid btn-sm btn-edit-table btn-info mr-1" title="Edit Table" data-tableid="'+ table.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-pencil.png" /></button>';
				if (areaCategory.is_parcel == 0) {
					tableLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-table mr-1" title="Delete Table" data-tableid="'+ table.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png" /></button>';
				}
				tableLine+= '</td>';
				tableLine+= '</tr>';
				$("table.table-tables-office").append(tableLine);
			}
		}
	});
	Pagination.managePagination(page, "tables-office");
}

function loadAreaCategoriesPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection = null) {
	var areaCategories = (collection != null) ? collection : LocalDB.areaCategories();
	clearTableCommonBlocks('area-categories');
	var pageSummary = Pagination.getPageSummary(start, end, page, areaCategories.length);
	$("table.table-area-categories-office tr.summary td:first").prepend(pageSummary);
	$(areaCategories).each(function(akey, areaCategory) {
		if (akey >= start && akey <= end) {
			var noOfTables = LocalDB.tables(areaCategory.id).length;
			var areaCategoryLine = '<tr class="office-list-area-category-row" data-id="' + areaCategory.id + '">';
			areaCategoryLine+= '<td class="text-center dotted-border-sm office-line-item srno">';
			areaCategoryLine+= (akey + 1);
			areaCategoryLine+= '</td>';
			areaCategoryLine+= '<td class="dotted-border-sm ac-name">';
			areaCategoryLine+= areaCategory.name;
			areaCategoryLine+= '</td>';
			areaCategoryLine+='<td class="dotted-border-sm">';
			areaCategoryLine+= noOfTables;
			areaCategoryLine+= '</td>';
			areaCategoryLine+= '<td class="dotted-border-sm">';
			areaCategoryLine+= '<button class="btn-solid btn-sm btn-edit-area-category btn-info mr-1" title="Edit Area Category" data-areacategoryid="'+ areaCategory.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-pencil.png" /></button>';
			if (areaCategory.is_parcel == 0) {
				areaCategoryLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-area-category mr-1" title="Delete Area Category" data-areacategoryid="'+ areaCategory.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png" /></button>';
			}
			areaCategoryLine+= '</td>';
			areaCategoryLine+= '</tr>';
			$("table.table-area-categories-office").append(areaCategoryLine);
		}
	});
	Pagination.managePagination(page, "area-categories-office");
}

function loadDiscountsPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1) {
	var discounts = LocalDB.discounts();
	clearTableCommonBlocks('discounts');
	var pageSummary = Pagination.getPageSummary(start, end, page, discounts.length);
	$("table.table-discounts-office tr.summary td:first").prepend(pageSummary);
	$(discounts).each(function(dkey, discount) {
		if (dkey >= start && dkey <= end) {
			var discountLine = '<tr>';
			discountLine+= '<td class="dotted-border-sm text-center office-line-item">';
			discountLine+= (dkey + 1);
			discountLine+= '</td>';
			discountLine+= '<td class="dotted-border-sm office-line-item">';
			discountLine+= discount.description;
			discountLine+= '</td>';
			discountLine+='<td class="dotted-border-sm">';
			discountLine+= discount.amount;
			discountLine+= '</td>';
			discountLine+= '<td class="dotted-border-sm">';
			discountLine+= discount.type;
			discountLine+= '</td>';
			discountLine+= '<td class="dotted-border-sm">';
			discountLine+= discount.minimum_amount;
			discountLine+= '</td>';
			discountLine+= '<td class="dotted-border-sm">';
			discountLine+= '<button class="btn-solid btn-sm btn-edit-discount btn-info mr-1" title="Edit Discount" data-discountid="'+ discount.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-pencil.png" /></button>';
			discountLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-discount mr-1" title="Delete Discount" data-discountid="'+ discount.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png" /></button>';
			discountLine+= '</td>';
			discountLine+= '</tr>';
			$("table.table-discounts-office").append(discountLine);	
		}
	});
	Pagination.managePagination(page, "discounts-office");
}

function loadTaxesPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1) {
	var taxes = LocalDB.taxes();
	clearTableCommonBlocks('taxes');
	var pageSummary = Pagination.getPageSummary(start, end, page, taxes.length);
	$("table.table-taxes-office tr.summary td:first").prepend(pageSummary);
	$(taxes).each(function(tkey, tax) {
		if (tkey >= start && tkey <= end) {
			var taxLine = '<tr>';
			taxLine+= '<td class="dotted-border-sm text-center office-line-item">';
			taxLine+= (tkey + 1);
			taxLine+= '</td>';
			taxLine+= '<td class="dotted-border-sm office-line-item">';
			taxLine+= tax.name;
			taxLine+= '</td>';
			taxLine+='<td class="dotted-border-sm">';
			taxLine+= tax.amount;
			taxLine+= '</td>';
			taxLine+= '<td class="dotted-border-sm">';
			taxLine+= tax.type;
			taxLine+= '</td>';
			taxLine+= '<td class="dotted-border-sm">';
			taxLine+= tax.tax_type;
			taxLine+= '</td>';
			taxLine+= '<td class="dotted-border-sm">';
			taxLine+= '<button class="btn-solid btn-sm btn-edit-tax btn-info mr-1" title="Edit Tax" data-taxid="'+ tax.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-pencil.png" /></button>';
			taxLine+= '<button class="btn-solid btn-sm btn-danger btn-confirm btn-delete-tax mr-1" title="Delete Tax" data-taxid="'+ tax.id +'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png" /></button>';
			taxLine+= '</td>';
			taxLine+= '</tr>';
			$("table.table-taxes-office").append(taxLine);
		}
	});
	Pagination.managePagination(page, "taxes-office");
}

function loadFCECardReportPerPage(start = 0, end = (NUMBER_OF_ITEM_PER_PAGE-1), page = 1, collection) {
	var fcECards = collection;
	clearTableCommonBlocks('fc-ecard-report');
	var pageSummary = Pagination.getPageSummary(start, end, page, fcECards.length);
	$("table.table-fc-ecard-report-office tr.summary td:first").prepend(pageSummary);
	$(fcECards).each(function(fckey, fcECard) {
		if (fckey >= start && fckey <= end) {
			var fcECardLine = '<tr>';
			fcECardLine+= '<td class="dotted-border-sm text-center">';
			fcECardLine+= (fckey + 1);
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm fc-ecard-no-block">';
			let secureBadgeClass = (fcECard.has_payment_verification == '0') ? 'hide' : '';
			fcECardLine+= '<span class="badge badge-success '+secureBadgeClass+'">Secured</span>';
			fcECardLine+= fcECard.ecard_no;
			fcECardLine+= '</td>';
			fcECardLine+='<td class="dotted-border-sm balance">';
			fcECardLine+= toNumeric(fcECard.current_balance).toFixed(2).toNumberFormat();
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm">';
			fcECardLine+= (typeof fcECard.display_name == 'undefined' || fcECard.display_name == null || fcECard.display_name == "") ? "-" : fcECard.display_name;
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm">';
			fcECardLine+= (typeof fcECard.linked_phone_number == 'undefined' || fcECard.linked_phone_number == null || fcECard.linked_phone_number == "") ? "-" : fcECard.linked_phone_number;
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm">';
			fcECardLine+= (typeof fcECard.linked_email == 'undefined' || fcECard.linked_email == null || fcECard.linked_email == "") ? "-" : fcECard.linked_email;
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm status">';
			fcECardLine+= fcECard.created_at;
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm status">';
			fcECardLine+= fcECard.status;
			fcECardLine+= '</td>';
			fcECardLine+= '<td class="dotted-border-sm">';
			fcECardLine+= '<button class="btn-solid btn-sm btn-proceed-recharge-fc-eCard btn-warning mr-1" title="Recharge" data-fc-number="'+ fcECard.ecard_no +'"><img class="icon-xsmall" src="assets/img/icons/icon-recharge.png" /></button>';
			fcECardLine+= '<button class="btn-solid btn-sm btn-view-fc-ecard-transactions btn-info mr-1" title="View Transactions" data-fc-number="'+ fcECard.ecard_no +'"><img class="icon-xsmall" src="assets/img/icons/icon-transactions.png" /></button>';
			if (fcECard.status == 'active') {
				fcECardLine+= '<button class="btn-solid btn-sm btn-change-fc-ecard-status btn-deactivate-fc-ecard" data-fc-ecard-number="' + fcECard.ecard_no + '" mr-1" title="Deactivate Card" data-cardid="'+ fcECard.id +'" data-status="inactive"><img class="icon-xsmall" src="assets/img/icons/icon-ban.png" /></button>';
			} else {
				fcECardLine+= '<button class="btn-solid btn-sm btn-change-fc-ecard-status btn-activate-fc-ecard" data-fc-ecard-number="' + fcECard.ecard_no + '" mr-1" title="Activate Card" data-cardid="'+ fcECard.id +'" data-status="active"><img class="icon-xsmall" src="assets/img/icons/icon-activate.png" /></button>';
			}
			if (fcECard.has_payment_verification == '1') {
				fcECardLine+= '<button class="ml-px5 btn-solid btn-sm btn-change-fc-ecard-payment-verification-flag btn-remove-payment-verification-fc-ecard" data-fc-ecard-number="' + fcECard.ecard_no + '" mr-1" title="Remove Payment Verification" data-cardid="'+ fcECard.id +'" data-status="0"><img class="icon-xsmall" src="assets/img/icons/icon-unlock.png" /></button>';
			} else {
				fcECardLine+= '<button class="ml-px5 btn-solid btn-sm btn-change-fc-ecard-payment-verification-flag btn-set-payment-verification-fc-ecard" data-fc-ecard-number="' + fcECard.ecard_no + '" mr-1" title="Set Payment Verification" data-cardid="'+ fcECard.id +'" data-status="1"><img class="icon-xsmall" src="assets/img/icons/icon-lock.png" /></button>';
			}
			fcECardLine+= '</td>';
			fcECardLine+= '</tr>';
			$("table.table-fc-ecard-report-office").append(fcECardLine);
		}
	});
}

// Load FC ECard report
function loadFCECardReportForOffice(options = {}) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-fc-ecard-list',
					restaurant_id : RESTAURANT.ID,
					options : JSON.stringify(options)
				},
				function(fcECards) {
					toggleWaitingLoader("hide");
					var fcECardReportTableHolder = 'table-fc-ecard-report-office';
					$("." + fcECardReportTableHolder).show();
					$("table.pagination").remove();
					clearTableCommonBlocks('fc-ecard-report');
					if (fcECards.length > 0) {
						$("table." + fcECardReportTableHolder + " tr:not(.header)").remove();
						loadFCECardReportPerPage(0, fcECards.length, 1, fcECards);
					} else {
						$("table."+ fcECardReportTableHolder +" tr.no-data-holder").remove();
						$("table."+ fcECardReportTableHolder).append(generateNoDataFoundBlock(9));
					}
					if (typeof options != "undefined" && typeof options.inBg != "undefined" && options.inBg == true) {
						// Do process in background if any
					} else {
						hideModal();
					}
				},
				function(error) {
					hideModal();
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}
}

// Get fc ecard transactions report based on card number
function getFCECardTransactions(fcECardNumber) {
	if (checkLogin()) {
		if (withAccess("office")) {
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-fc-ecard-transactions',
					restaurant_id : RESTAURANT.ID,
					fcECardNumber : fcECardNumber
				},
				function(transactions) {
					$("table.fc-ecard-transactions").empty();
					$("table.fc-ecard-transactions").show();
					$(".fc-ecard-transactions-holder span.progressing-msg").show();
					if (transactions.length > 0) {
						$(".fc-ecard-transactions-holder span.progressing-msg").hide();
						$("table.fc-ecard-transactions").append("<caption class='text-left font-14'>Transactions for "+ fcECardNumber +"</caption>");
						var transactionLine = "<tr>";
						transactionLine+= "<th class='dotted-border-sm'>";
						transactionLine+= "Type";
						transactionLine+= "</th>";
						transactionLine+= "<th class='dotted-border-sm'>";
						transactionLine+= "Amount";
						transactionLine+= "</th>";
						transactionLine+= "<th class='dotted-border-sm'>";
						transactionLine+= "Prev. Balance";
						transactionLine+= "</th>";
						transactionLine+= "<th class='dotted-border-sm'>";
						transactionLine+= "Trans. Date";
						transactionLine+= "</th>";
						transactionLine+= "</tr>";
						$("table.fc-ecard-transactions").append(transactionLine);
						$ (transactions).each(function(tkey, transaction) {
							var transactionLine = "<tr>";
							let trLineClass = '';
							let trTypeText = "N/A";
							let amountInOutBadge = "badge-default";
							let amountInOutText = "~";
							if (typeof transaction.transaction_type != 'undefined') {
								if (transaction.transaction_type == 'ORDER') {
									trLineClass = "order-no-view-link badge-half-success";
									amountInOutBadge = "badge-danger";
									amountInOutText = "-";
									trTypeText = "ORDER #" + transaction.order_no;
								} else if (transaction.transaction_type == 'RCHRG') {
									trLineClass = "";
									amountInOutBadge = "badge-success";
									amountInOutText = "+";
									trTypeText = "RECHARGE";
								} else if (transaction.transaction_type == 'INTRCHRG') {
									trLineClass = "";
									amountInOutBadge = "badge-success";
									amountInOutText = "+";
									trTypeText = "INITIAL RECHARGE";
								}
							}
							transactionLine+= "<td class='dotted-border-sm font-14 "+ trLineClass +"' data-orderno='"+ transaction.order_no +"'>";
							transactionLine+= trTypeText;
							transactionLine+= "</td>";
							transactionLine+= "<td class='dotted-border-sm font-14'>";
							transactionLine+= "<span class='text-bold d-inline-block font-14 badge "+ amountInOutBadge +"'>"+ amountInOutText +"</span>";
							transactionLine+= transaction.transaction_amount +" "+ RESTAURANT.CURRENCY;
							transactionLine+= "</td>";
							transactionLine+= "<td class='dotted-border-sm font-14'>";
							transactionLine+= transaction.prev_balance +" "+ RESTAURANT.CURRENCY;
							transactionLine+= "</td>";
							transactionLine+= "<td class='dotted-border-sm font-14'>";
							transactionLine+= transaction.created_at;
							transactionLine+= "</td>";
							transactionLine+= "</tr>";
							$("table.fc-ecard-transactions").append(transactionLine);
						});
						showNotification(transactions.length + " transaction(s) found for card #" + fcECardNumber, "success");
					} else {
						hideModal();
						$(".fc-ecard-transactions-holder span.progressing-msg").show();
						$("table.fc-ecard-transactions").hide();
						showNotification("No transactions found for card #" + fcECardNumber, "error");
					}
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
	} else {
		showLoginLayout(true);
	}
}

// Get fc ecard recharge report
function getFCECardRecharges(options) {
	if (checkLogin()) {
		if (withAccess("office")) {
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-fc-ecard-recharges',
					restaurant_id : RESTAURANT.ID,
					options : JSON.stringify(options)
				},
				function(fcECardRecharges) {
					$("table.fc-ecard-recharges").empty();
					$("table.fc-ecard-recharges").show();
					$(".fc-ecard-recharge-holder span.progressing-msg").show();
					if (fcECardRecharges.length > 0) {
						$(".fc-ecard-recharge-holder span.progressing-msg").hide();
						$("table.fc-ecard-recharges").append("<caption class='text-center font-14 text-bold'>Total Recharge of </caption>");
						var fcECardRechargeLine = "<tr>";
						fcECardRechargeLine+= "<th class='dotted-border-sm'>";
						fcECardRechargeLine+= "FC ECard No.";
						fcECardRechargeLine+= "</th>";
						fcECardRechargeLine+= "<th class='dotted-border-sm'>";
						fcECardRechargeLine+= "Amount";
						fcECardRechargeLine+= "</th>";
						fcECardRechargeLine+= "<th class='dotted-border-sm'>";
						fcECardRechargeLine+= "Trans. Date";
						fcECardRechargeLine+= "</th>";
						fcECardRechargeLine+= "</tr>";
						$("table.fc-ecard-recharges").append(fcECardRechargeLine);
						var totalFCECardRechargeAmount = 0;
						$ (fcECardRecharges).each(function(tkey, recharge) {
							totalFCECardRechargeAmount+= toNumeric(recharge.transaction_amount);
							var fcECardRechargeLine = "<tr>";
							fcECardRechargeLine+= "<td class='dotted-border-sm font-14'>";
							fcECardRechargeLine+= recharge.ecard_no;
							fcECardRechargeLine+= "</td>";
							fcECardRechargeLine+= "<td class='dotted-border-sm font-14'>";
							fcECardRechargeLine+= recharge.transaction_amount +" "+ RESTAURANT.CURRENCY;
							fcECardRechargeLine+= "</td>";
							fcECardRechargeLine+= "<td class='dotted-border-sm font-14'>";
							fcECardRechargeLine+= recharge.created_at;
							fcECardRechargeLine+= "</td>";
							fcECardRechargeLine+= "</tr>";
							$("table.fc-ecard-recharges").append(fcECardRechargeLine);
						});
						$("table.fc-ecard-recharges").find("caption").append(totalFCECardRechargeAmount.toFixed(2) +" "+ RESTAURANT.CURRENCY);
						showNotification(fcECardRecharges.length + " Recharge(s) found", "success");
					} else {
						hideModal();
						$(".fc-ecard-recharge-holder span.progressing-msg").show();
						$("table.fc-ecard-recharges").hide();
						showNotification("No fc ECard Recharges found", "error");
					}
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
	} else {
		showLoginLayout(true);
	}
}

// Submit event of order search form
function submitOrderSearchForm() {
	var order_no = $("input[name='field_search_order_orderno']").val();
	var table_id = $("select[name='field_search_order_tableno']").val();
	var amount_from = $("input[name='field_search_order_amountfrom']").val();
	var amount_to = $("input[name='field_search_order_amountto']").val();
	var payment_method = $("select[name='field_search_order_payment_method']").val();
	if (order_no == '' && table_id == '' && amount_from == '' && amount_to == '' && payment_method == '') {
		fetchOrders();
		return false;
	}
	var orderParams = { 
		'order_no' : order_no,
		'table_id' : table_id,
		'amount_from' : amount_from,
		'amount_to' : amount_to,
		'payment_method' : (payment_method != '') ? LocalDB.getPaymentMethodInfo(toNumeric(payment_method)).payment_method : '',
	};
	var orders = LocalDB.filterOrders(orderParams);
	if (orders.length > 0) {
		loadOrdersPerPage(0, (orders.length - 1), 1, orders);
	} else {
		clearTableCommonBlocks('orders');
		$("table.table-orders-office tr.no-data-holder").remove();
		$("table.table-orders-office").append(generateNoDataFoundBlock(6));
	}
}

function setAssetsForOfficeView() {
	$("#modal-manage-item select#field_office_item_category").html(getCategoriesSelectOptions());
	$("select[name='field_search_order_tableno']").html(getTableSelectOptions(true));
	$("select[name='field_search_order_tableno_for_report']").html(getTableSelectOptions(true));
    $("select[name='field_search_order_payment_method']").html(getPaymentMethodsSelectOptions(true));
    $("select[name='field_search_order_payment_method_for_report']").html(getPaymentMethodsSelectOptions(true));
    $("select[name='report-search-options-month']").html(getMonthsSelectOptions(true, getCurrentDate('month')));
    $("select[name='report-search-options-month-year']").html(getYearsSelectOptions(true, getCurrentDate('year')));
    $("select[name='report-search-options-year']").html(getYearsSelectOptions(true, getCurrentDate('year')));
    bindSpecificEvents();
}

function clearTableCommonBlocks(table = 'common') {
	$("table.table-"+table+"-office tr").each(function() {
		if (!$(this).hasClass("header")) {
			$(this).remove();
		}
		$(this).find("td span.pagination-page-summary").remove();
		$(this).find("td span."+table+"-summary").remove();
	});
}

function renderOrderStatsForOffice() {
	var orderTotal = 0;
	var subTotal = 0;
	var totalAfterDiscount = 0;
	var subTotalIncludingTax = 0;
	var subTotalExclusiveTax = 0;
	$("table.order-items-list input[name^='orderItems[id][']").each(function(k, v) {
		var index = $(this).attr("name").toString().split("[id][")[1].toString().replace("]", "");
		var itemQty = $("table.order-items-list input[name='orderItems[qty][" + index + "]']").val();
		var itemPrice = $("table.order-items-list input[name='orderItems[price][" + index + "]']").val();
		var itemTotal = itemQty * itemPrice;
		$("table.order-items-list span#orderItemsTotalFor" + index + "").html(toNumeric(itemTotal).toFixed(2));
		orderTotal+= toNumeric(itemTotal);
	});
	subTotal = orderTotal;
	$("table.order-items-list input[name^='orderDiscounts[']").each(function(k, v) {
		var discountAmount = 0; 
		var discountType = $(this).attr("data-type");
		var discountValue = $(this).attr("data-amount");
		var discountMinAmount = $(this).attr("data-minamount");
		if (discountMinAmount != null && subTotal >= toNumeric(discountMinAmount)) {
			if (discountType == "percentage") {
				var percentage = discountValue;
				discountAmount = ((orderTotal * percentage) / 100).toRound();
			} else {
				discountAmount = discountValue.toRound();
			}
			orderTotal-= toNumeric(discountAmount);
		} else if (discountMinAmount == null && toNumeric(discountValue) > 0) {
			if (discountType == "percentage") {
				var percentage = discountValue;
				discountAmount = ((orderTotal * percentage) / 100).toRound();
			} else {
				discountAmount = discountValue.toRound();
			}
			orderTotal-= toNumeric(discountAmount);
		}
		$("table.order-items-list input[name='orderDiscounts[" + k + "]']").val(discountAmount);
	});
	subTotalExclusiveTax = orderTotal;
	$("table.order-items-list input[name^='orderTaxes[']").each(function(k, v) { 
		var taxAmount = 0;
		var taxType = $(this).attr("data-type");
		var taxValue = $(this).attr("data-amount");
		if (taxType == "percentage") {
			var percentage = taxValue;
			taxAmount = ((subTotalExclusiveTax * percentage) / 100).toRound();
		} else {
			taxAmount = taxValue.toRound();
		}
		$("table.order-items-list input[name='orderTaxes[" + k + "]']").val(taxAmount);
		orderTotal+= toNumeric(taxAmount);
	});
	subTotalIncludingTax = orderTotal;
	$("table.order-items-list span#subTotalAmount").html(toNumeric(subTotal).toRound());
	$("table.order-items-list span#subTotalExclTaxAmount").html(toNumeric(subTotalExclusiveTax).toRound());
	var grandtotalRoundAmount = Math.round(toNumeric(orderTotal)) - toNumeric(orderTotal);
	if (grandtotalRoundAmount == 0) {
		$("table.order-items-list tr.grandtotal-round-row").hide();
	} else {
		$("table.order-items-list tr.grandtotal-round-row").show();
	}
	$("table.order-items-list span#grandtotalRoundAmount").html(grandtotalRoundAmount.toFixed(2));
	$("table.order-items-list span#grandtotalAmount").html(Math.round(toNumeric(orderTotal)).toFixed(2));
}

function updateOrderStatsToCloud(orderId, orderFrom = 'local') {
	var orderItems = [];
	var orderDiscounts = [];
	var orderTaxes = [];
	var orderTotal = 0;
	var orderTableId = $("table.order-items-list select[name='orderTableId']").val();
	var orderPaymentMethod = $("table.order-items-list select[name='orderPaymentMethod']").val();
	$("table.order-items-list input[name^='orderItems[id][']").each(function(k, v) {
		var itemId = $(this).val();
		var index = $(this).attr("name").toString().split("[id][")[1].toString().replace("]", "");
		var itemQty = $("table.order-items-list input[name='orderItems[qty][" + index + "]']").val();
		var itemPrice = $("table.order-items-list input[name='orderItems[price][" + index + "]']").val();
		var itemName = $("table.order-items-list span#orderItemNameAt" + index + "").text();
		var orderItem = {
			'itemId' : itemId,
			'qty' : itemQty,
			'price' : itemPrice,
			'name' : itemName
		};
		orderItems.push(orderItem);
		orderTotal+= toNumeric(itemQty * itemPrice);
	});
	$("table.order-items-list input[name^='orderDiscounts[']").each(function(k, v) {
		orderTotal-= toNumeric(toNumeric($(this).val()));
	});
	$("table.order-items-list input[name^='orderTaxes[']").each(function(k, v) {
		orderTotal+= toNumeric(toNumeric($(this).val()));
	});
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'update-order-stats',
					restaurant_id : RESTAURANT.ID,
					order_id : orderId,
					order_table_id : orderTableId,
					order_payment_method : orderPaymentMethod,
					order_items : JSON.stringify(orderItems),
					order_amount : Math.round(toNumeric(orderTotal)).toFixed(2)
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						if (orderFrom == "local") {
							let orderTable = LocalDB.getTableInfo(toNumeric(orderTableId));
							let orderTableNo = orderTable.table_no;
							let orderTableInfo = orderTable.table_no + " (" + LocalDB.getAreaCategoryInfo(toNumeric(orderTable.area_category_id)).name + ")";
							let paymentMethodTitle = LocalDB.getPaymentMethodInfo(toNumeric(orderPaymentMethod)).payment_method;
							LocalDB.updateOrderInfo(orderId, {
								'order_info' : JSON.stringify(orderItems),
								'order_amount' : Math.round(toNumeric(orderTotal)).toFixed(2),
								'table_id' : orderTableId,
								'table_no' : orderTableNo,
								'payment_method' : paymentMethodTitle
							});
							$("tr.today-order-data[data-id=" + orderId + "]").find("td.order-amount").html(toNumeric(orderTotal).toFixed(2).toNumberFormat());
							$("table.order-header-info span.manage-order-table-no").html(orderTableInfo);
			        		$("table.order-header-info span.manage-order-payment-method").html(paymentMethodTitle);
						} else if (orderFrom == "live") {
							$("tr.report-data[data-id=" + orderId + "]").find("td.order-amount").html(toNumeric(orderTotal).toFixed(2).toNumberFormat());
						}
						showNotification(response.message, "info");
						if ($("button.btn-office-action[data-action='todayorders']").hasClass("active")) {
	                        fetchOrders();
                    	}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Delete order from cloud
function deleteOrderFromCloud(orderId) {
	if (isNaN(orderId)) {
		return false;
	}
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'delete-order',
					restaurant_id : RESTAURANT.ID,
					order_id : toNumeric(orderId),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						$("tr.report-data[data-id=" + orderId + "]").removeClass("order-deleted").addClass("order-deleted");
						$("tr.today-order-data[data-id=" + orderId + "]").removeClass("order-deleted").addClass("order-deleted");
						if ($("button.btn-office-action[data-action='todayorders']").hasClass("active")) {
                            syncTodayOrders(RESTAURANT.ID);
                        }
                        if ($("button.btn-office-action[data-action='orderreport']").hasClass("active")) {
                        	$("button.btn-office-search-for-report").trigger("click");
                        }
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Restore order to cloud
function restoreOrderToCloud(orderId) {
	if (isNaN(orderId)) {
		return false;
	}
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'restore-order',
					restaurant_id : RESTAURANT.ID,
					order_id : toNumeric(orderId),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						$("tr.report-data[data-id=" + orderId + "]").removeClass("order-deleted");
						$("tr.today-order-data[data-id=" + orderId + "]").removeClass("order-deleted");
						showNotification(response.message, "info");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Toggle order office lock
function toggleOrderOfficeLock(orderId) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'set-order-lock-access',
					restaurant_id : RESTAURANT.ID,
					order_id : toNumeric(orderId),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						showNotification(response.message, "info");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');
		}
	} else {
		showLoginLayout(true);
	}
}

// Print orders for reports
function printOrders(options) {
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'print-orders',
					restaurant_id : RESTAURANT.ID,
					options : JSON.stringify(options),
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true && typeof response.reportFileUrl != 'undefined' && response.reportFileUrl != '') {
						var link = document.createElement('a');
						link.href = response.reportFileUrl;
						link.target = '_blank';
						document.body.appendChild(link);
						link.click();
						showNotification(response.message, "info");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');	
		}
	} else {
		showLoginLayout(true);	
	}
}

// Submit event of order custom search form for report
function submitOrderSearchFormForCustomReport() {
	var order_no = $("input[name='field_search_order_orderno_for_report']").val();
	var table_id = $("select[name='field_search_order_tableno_for_report']").val();
	var amount_from = $("input[name='field_search_order_amountfrom_for_report']").val();
	var amount_to = $("input[name='field_search_order_amountto_for_report']").val();
	var date_from = $("input[name='field_search_order_datefrom_for_report']").val();
	var date_to = $("input[name='field_search_order_dateto_for_report']").val();
	var payment_method = $("select[name='field_search_order_payment_method_for_report']").val();
	if (order_no == '' && table_id == '' && amount_from == '' && amount_to == '' && date_from == '' && date_to == '' && payment_method == '') {
		return false;
	}
	var orderParams = { 
		'order_no' : order_no,
		'table_id' : table_id,
		'amount_from' : amount_from,
		'amount_to' : amount_to,
		'date_from' : date_from,
		'date_to' : date_to,
		'payment_method' : (payment_method != '') ? LocalDB.getPaymentMethodInfo(toNumeric(payment_method)).payment_method : '',
	};
	if (checkLogin()) {
		if (withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-orders',
					restaurant_id : RESTAURANT.ID,
					options : JSON.stringify(orderParams),
				},
				function(orders) {
					toggleWaitingLoader("hide");
					hideModal();
					$("table.table-office").hide();
					$("table.table-orders-report-office").show();
					$("table.table-orders-report-office tr.header.summary").show();
					$("table.table-orders-report-office tr.report").show();
					$("table.table-orders-report-office tr.report-data").remove();
					if (orders.length > 0) {
						$("button.btn-print-order-report").show();
						$("button.btn-print-order-report").attr("data-report", "custom");
						loadOrdersPerPageForReport(0, orders.length, 1, orders);
					} else {
						$("button.btn-print-order-report").hide();
						$("button.btn-print-order-report").attr("data-report", "");
						$("table.table-orders-report-office tr.summary").hide();
						$("table.table-orders-report-office tr.no-data-holder").remove();
						$("table.table-orders-report-office").append(generateNoDataFoundBlock(6));
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');	
		}
	} else {
		showLoginLayout(true);	
	}
}

// Submit event of orders month report
function submitOrderSearchFormForMonthReport(monthReportFor, yearReportFor) {
	if (checkLogin()) {
		if (withAccess("office")) {
			var monthName = CALENDER_MONTHS[(monthReportFor - 1)];
			var reportName = monthName + ' ' + yearReportFor;
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-orders-month-report',
					restaurant_id : RESTAURANT.ID,
					monthReportFor : monthReportFor,
					yearReportFor : yearReportFor
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						$("table.table-office").hide();
						$("table.table-orders-report-office").show();
						$("table.table-orders-report-office tr:not(.header)").hide();
						$("table.table-orders-report-office tr").each(function(rkey, row) {
							var hideRow = true;
							if ($(this).hasClass("header") && !$(this).hasClass("summary") && rkey == 0) {
								hideRow = false;
							}
							if (hideRow) {
								$(this).hide();
							}
						});
						$("table.table-orders-month-report-office").show();
						$("table.table-orders-month-report-office tr.month-report").show();
						$("table.table-orders-month-report-office tr.summary").find("span.order-month-report-name").html(reportName);
						$("table.table-orders-month-report-office tr.summary").show();
						$("table.table-orders-month-report-office tr.month-report-line").remove();
						$("table.table-orders-month-report-office tr:not(.header)").remove();
						$("button.btn-print-order-report").hide();
						if (response.reportLines.length > 0) {
							showNotification(response.reportLines.length + " report line(s) found for " + monthName, "info");
							var monthReport = response.reportLines;
							$("button.btn-print-order-report").show();
							$("button.btn-print-order-report").attr("data-report", "month");
							var totalMonthAmount = 0;
							$(monthReport).each(function(mrKey, mrLine) {
								var monthReportLine = "<tr class='month-report-line'>";
								monthReportLine+= "<td class='dotted-border-sm'>";
								monthReportLine+= (mrKey + 1);
								monthReportLine+= "</td>";
								monthReportLine+= "<td class='dotted-border-sm'>";
								monthReportLine+= mrLine.bill_no_from + "-" + mrLine.bill_no_to;
								monthReportLine+= "</td>";
								monthReportLine+= "<td class='dotted-border-sm'>";
								monthReportLine+= mrLine.date;
								monthReportLine+= "</td>";
								monthReportLine+= "<td class='dotted-border-sm'>";
								monthReportLine+= toNumeric(mrLine.total_payment).toFixed(2).toNumberFormat();
								monthReportLine+= "</td>";
								monthReportLine+= "</tr>";
								totalMonthAmount+= toNumeric(mrLine.total_payment);
								$("table.table-orders-month-report-office").append(monthReportLine);
							});
							$("table.table-orders-month-report-office tr.summary").find("span.order-month-report-total-payment").html(totalMonthAmount.toFixed(2).toNumberFormat() + " " + RESTAURANT.CURRENCY);
						} else {
							$("table.table-orders-month-report-office tr.summary").find("span.order-month-report-total-payment").html("0.00 " + RESTAURANT.CURRENCY);							
							$("button.btn-print-order-report").attr("data-report", "");
							showNotification("No order found for " + monthName, "error");
							$("table.table-orders-month-report-office tr.no-data-holder").remove();
							$("table.table-orders-month-report-office").append(generateNoDataFoundBlock(4));
						}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');	
		}
	} else {
		showLoginLayout(true);	
	}
}

// Submit event of orders year report
function submitOrderSearchFormForYearReport(yearReportFor) {
	if (checkLogin()) {
		if (withAccess("office")) {
			var yearName = yearReportFor;
			var reportName = yearName;
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-orders-year-report',
					restaurant_id : RESTAURANT.ID,
					yearReportFor : yearReportFor,
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						$("table.table-office").hide();
						$("table.table-orders-report-office").show();
						$("table.table-orders-report-office tr:not(.header)").hide();
						$("table.table-orders-report-office tr").each(function(rkey, row) {
							var hideRow = true;
							if ($(this).hasClass("header") && !$(this).hasClass("summary") && rkey == 0) {
								hideRow = false;
							}
							if (hideRow) {
								$(this).hide();
							}
						});
						$("table.table-orders-year-report-office").show();
						$("table.table-orders-year-report-office tr.year-report").show();
						$("table.table-orders-year-report-office tr.summary").find("span.order-year-report-name").html(reportName);
						$("table.table-orders-year-report-office tr.summary").show();
						$("table.table-orders-year-report-office tr.year-report-line").remove();
						$("table.table-orders-year-report-office tr:not(.header)").remove();
						$("button.btn-print-order-report").hide();
						if (response.reportLines.length > 0) {
							showNotification(response.reportLines.length + " report line(s) found for " + yearName, "info");
							var yearReport = response.reportLines;
							$("button.btn-print-order-report").show();
							$("button.btn-print-order-report").attr("data-report", "year");
							var totalyearAmount = 0;
							$(yearReport).each(function(yrKey, yrLine) {
								var yearReportLine = "<tr class='year-report-line'>";
								yearReportLine+= "<td class='dotted-border-sm'>";
								yearReportLine+= (yrKey + 1);
								yearReportLine+= "</td>";
								yearReportLine+= "<td class='dotted-border-sm'>";
								yearReportLine+= yrLine.month;
								yearReportLine+= "</td>";
								yearReportLine+= "<td class='dotted-border-sm'>";
								yearReportLine+= toNumeric(yrLine.total_payment).toFixed(2).toNumberFormat();
								yearReportLine+= "</td>";
								yearReportLine+= "</tr>";
								totalyearAmount+= toNumeric(yrLine.total_payment);
								$("table.table-orders-year-report-office").append(yearReportLine);
							});
							$("table.table-orders-year-report-office tr.summary").find("span.order-year-report-total-payment").html(totalyearAmount.toFixed(2).toNumberFormat() + " " + RESTAURANT.CURRENCY);
						} else {
							$("table.table-orders-year-report-office tr.summary").find("span.order-year-report-total-payment").html("0.00 " + RESTAURANT.CURRENCY);
							$("button.btn-print-order-report").attr("data-report", "");
							showNotification("No order found for " + yearName, "error");
							$("table.table-orders-year-report-office tr.no-data-holder").remove();
							$("table.table-orders-year-report-office").append(generateNoDataFoundBlock(3));
						}
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, 'error');	
		}
	} else {
		showLoginLayout(true);	
	}
}

// Recharge FC eCard
function rechargeFCECard(fcECardNumber, fcECardRechargeAmount) {
	if (checkLogin()) {
		if (withAccess("office") || withAccess("counter")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'recharge-fc-ecard',
					restaurant_id : RESTAURANT.ID,
					fcECardNumber : fcECardNumber,
					fcECardRechargeAmount : fcECardRechargeAmount,
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						hideModal();
						if ($("table.table-fc-ecard-report-office").css("display") != "none") {
							var btnFCECardStatus = $("button.btn-change-fc-ecard-status[data-fc-ecard-number="+ fcECardNumber +"]");
							var FCECardCurBalance = toNumeric($(btnFCECardStatus).parent().parent().find("td.balance").html().toString());
							let updatedBalance = (FCECardCurBalance + toNumeric(fcECardRechargeAmount)).toFixed(2).toNumberFormat();
							$(btnFCECardStatus).parent().parent().find("td.balance").html(updatedBalance);
						} else {
							$("#modal-recharge-fc-eCard input[type='text'][name='field_fcECardNumber']").val("");
							$("#modal-recharge-fc-eCard input[type='text'][name='field_fcECardRechargeAmount']").val("");
						}
						showNotification(response.message, "info");
						EventHandler.notifyCustomerForFCCardTransaction(response);
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}
}

function showInfoForFCECard(fcECardNumber) {
	if (checkLogin()) {
		if (withAccess("office") || withAccess("counter")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'get-fc-ecard-info',
					restaurant_id : RESTAURANT.ID,
					fcECardNumber : fcECardNumber
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						$("table.table-fcECard-for-view").show();
						$("table.table-fcECard-for-view span#fcECard-number-for-view").html(response.fcECard.ecard_no);
						$("table.table-fcECard-for-view span#fcECard-current-balance-for-view").html(response.fcECard.current_balance + " " + RESTAURANT.CURRENCY);
						$("table.table-fcECard-for-view span#fcECard-status-for-view").html(response.fcECard.status.toString().toUpperCase());
						$("table.table-fcECard-for-view button.btn-change-fc-ecard-status").attr("data-fc-ecard-number", response.fcECard.ecard_no);
						if (response.fcECard.status == "inactive") {
							$("table.table-fcECard-for-view button.btn-activate-fc-ecard").show();
							$("table.table-fcECard-for-view button.btn-deactivate-fc-ecard").hide();
						} else {
							$("table.table-fcECard-for-view button.btn-activate-fc-ecard").hide();
							$("table.table-fcECard-for-view button.btn-deactivate-fc-ecard").show();
						}
						$("table.table-fcECard-for-view span#fcECard-linked-email-for-view").html((response.fcECard.linked_email != "") ? response.fcECard.linked_email : "N/A");
						$("table.table-fcECard-for-view span#fcECard-linked-contact-number-for-view").html((response.fcECard.linked_phone_number != "") ? response.fcECard.linked_phone_number : "N/A");
					} else {
						$("table.table-fcECard-for-view").hide();
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					$("table.table-fcECard-for-view").hide();
					showNotification(OOPS_MSG, "error");
				}
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}
}

// Set status for fc eCard
function setFCECardStatus(fcECardNumber, status = "") {
	if (checkLogin()) {
		if (withAccess("office") || withAccess("counter")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'set-fc-ecard-status',
					restaurant_id : RESTAURANT.ID,
					fcECardNumber : fcECardNumber,
					status : status
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						if ($("table.table-fc-ecard-report-office").css("display") != "none") {
							var btnFCECardStatus = $("button.btn-change-fc-ecard-status[data-fc-ecard-number="+ fcECardNumber +"]");
							if (status == "active") {
								$(btnFCECardStatus).removeClass("btn-activate-fc-ecard").removeClass("btn-deactivate-fc-ecard").addClass("btn-deactivate-fc-ecard");
								$(btnFCECardStatus).find("img:first").attr("src", "assets/img/icons/icon-ban.png");
								$(btnFCECardStatus).attr("title", "Deactivate Card");
								$(btnFCECardStatus).attr("data-status", "inactive");
							} else {
								$(btnFCECardStatus).removeClass("btn-deactivate-fc-ecard").removeClass("btn-activate-fc-ecard").addClass("btn-activate-fc-ecard");
								$(btnFCECardStatus).find("img:first").attr("src", "assets/img/icons/icon-activate.png");
								$(btnFCECardStatus).attr("title", "Activate Card");
								$(btnFCECardStatus).attr("data-status", "active");
							}
							$(btnFCECardStatus).parent().parent().find("td.status").html(status);
						} else {
							$("table.table-fcECard-for-view").hide();
							$("button.btn-view-fc-eCard").trigger("click");
						}
						showNotification(response.message, (status == "active") ? "info" : "error");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				},
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}

}

// Set payment verification flag for fc eCard
function setFCECardPaymentVerificationStatus(fcECardNumber, pvFlag = "") {
	if (checkLogin()) {
		if (withAccess("office") || withAccess("office")) {
			toggleWaitingLoader("show");
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'set-fc-ecard-payment-verification-flag',
					restaurant_id : RESTAURANT.ID,
					fcECardNumber : fcECardNumber,
					pvFlag : pvFlag
				},
				function(response) {
					toggleWaitingLoader("hide");
					if (response.status == true) {
						if ($("table.table-fc-ecard-report-office").length > 0 && $("table.table-fc-ecard-report-office").css("display") != "none") {
							var btnFCECardStatus = $("button.btn-change-fc-ecard-payment-verification-flag[data-fc-ecard-number="+ fcECardNumber +"]");
							if (pvFlag == "1") {
								$(btnFCECardStatus).removeClass("btn-set-payment-verification-fc-ecard").removeClass("btn-remove-payment-verification-fc-ecard").addClass("btn-remove-payment-verification-fc-ecard");
								$(btnFCECardStatus).find("img:first").attr("src", "assets/img/icons/icon-lock.png");
								$(btnFCECardStatus).attr("title", "Remove Payment Verification");
								$(btnFCECardStatus).attr("data-status", "0");
								$(btnFCECardStatus).parent().parent().find('td.fc-ecard-no-block').find('span.badge-success').show();
							} else {
								$(btnFCECardStatus).parent().parent().find('td.fc-ecard-no-block').find('span.badge-success').hide();
								$(btnFCECardStatus).removeClass("btn-remove-payment-verification-fc-ecard").removeClass("btn-set-payment-verification-fc-ecard").addClass("btn-set-payment-verification-fc-ecard");
								$(btnFCECardStatus).find("img:first").attr("src", "assets/img/icons/icon-unlock.png");
								$(btnFCECardStatus).attr("title", "Set Payment Verification");
								$(btnFCECardStatus).attr("data-status", "1");
							}
						} else {
							$("table.table-fcECard-for-view").hide();
							$("button.btn-view-fc-eCard").trigger("click");
						}
						showNotification(response.message, (pvFlag == "1") ? "info" : "error");
					} else {
						showNotification(response.message, "error");
					}
				},
				function(error) {
					toggleWaitingLoader("hide");
					showNotification(OOPS_MSG, "error");
				},
			);
		} else {
			showNotification(NO_ACCESS_MSG, "error");
		}
	} else {
		showLoginLayout(true);
	}

}

// Bind specific events
function bindSpecificEvents() {
	// Bind restaurant logo upload update event
	$("div#profile_restaurant_logo_uploader").uploadFile({
		url : APP_API_ENDPOINT + 'api.php',
		fileName : "profile_restaurant_logo",
		multiple : false,
		dragDrop : true,
		acceptFiles : "image/*",
		maxFileSize : 5000 * 1024, // Max filesize 5 MB
		returnType : "json",
		dynamicFormData: function()
		{
			var data = { 
				restaurant_id : RESTAURANT.ID,
				action : 'update-restaurant-logo'
			}
			return data;
		},
		onSuccess : function(files, data, xhr, pd) {
			$(".ajax-file-upload-container").remove();
			console.log(data)
			if (typeof data != "undefined") {
                if (data.status == true) {
                    showNotification("Logo updated successfully!", "success");
                } else {
                    showNotification(data.message, "error");
                }
            }
		},
		onError: function(files, status, errMsg, pd)
		{
			showNotification(errMsg, "error");
		},
	});
}

// View order info in invoice format
function viewOrderInvoice(orderInfo) {
    if (checkLogin()) {
    	if (withAccess("office") || withTableAccess()) {
    		var cartInfo = JSON.parse(orderInfo.order_info);
	        var subTotal = 0;
	        var grandTotal = 0;
	        var inclTaxLabel = '';
	        $("tr.invoice-line").each(function() {
	            $(this).remove();
	        });
	        $("tr.invoice-header-line").each(function() {
	            if ($(this).hasClass("bill-extra-field")) {
	                $(this).remove();
	            }
	        });
	        var billExtraInformation = JSON.parse(orderInfo.extra_information);
	        if (typeof billExtraInformation != 'undefined') {
	            if (billExtraInformation.customer_name != '') {
	                var invoiceExtraInfoLine = '<tr class="invoice-header-line bill-extra-field">';
	                invoiceExtraInfoLine+= '<td class="text-left text-sm p-2-f dotted-border-dark" colspan="5">';
	                invoiceExtraInfoLine+= '<span class="text-bold">CUSTOMER : ';
	                invoiceExtraInfoLine+= billExtraInformation.customer_name;
	                invoiceExtraInfoLine+= '</span>';
	                invoiceExtraInfoLine+= '</td>';
	                invoiceExtraInfoLine+= '</tr>';
	                $(invoiceExtraInfoLine).insertBefore($("tr.invoice-header-line:first"));
	            }
	            if (billExtraInformation.extra_fields.length > 0) {
	                $(billExtraInformation.extra_fields).each(function(befiKey, billExtraFieldInfo) {
	                    var invoiceExtraInfoLine = '<tr class="invoice-header-line bill-extra-field">';
	                    invoiceExtraInfoLine+= '<td class="text-left text-sm p-2-f dotted-border-dark" colspan="5">';
	                    invoiceExtraInfoLine+= '<span class="text-bold">' + billExtraFieldInfo.field + ' : ';
	                    invoiceExtraInfoLine+= billExtraFieldInfo.value;
	                    invoiceExtraInfoLine+= '</span>';
	                    invoiceExtraInfoLine+= '</td>';
	                    invoiceExtraInfoLine+= '</tr>';
	                    if ($("tr.invoice-header-line.bill-extra-field:last").length == 0) {
	                        $(invoiceExtraInfoLine).insertBefore($("tr.invoice-header-line:first"));
	                    } else {
	                        $(invoiceExtraInfoLine).insertAfter($("tr.invoice-header-line.bill-extra-field:last"));
	                    }
	                });
	            }
	        }
	        $(cartInfo).each(function(k, v) {
	            var invoiceItemRowClone = $("tr.item-line:first").clone();
	            let itemPrice = v.price;
	            var total = toNumeric(v.qty * itemPrice);
	            $(invoiceItemRowClone).addClass('item-line-info').addClass('invoice-line');
	            $(invoiceItemRowClone).find('td:first').html(v.name);
	            $(invoiceItemRowClone).find('td:nth-child(2)').html(v.qty);
	            $(invoiceItemRowClone).find('td:nth-child(3)').html(toNumeric(itemPrice).toFixed(2));
	            $(invoiceItemRowClone).find('td:nth-child(4)').html(total.toFixed(2));
	            $(invoiceItemRowClone).insertAfter($("tr.item-line:last"));
	            subTotal+= total;
	        });
	        grandTotal+= subTotal;

	        var discounts = JSON.parse(orderInfo.discount_info);
	        var taxes = JSON.parse(orderInfo.tax_info);

	        if (discounts.length > 0 || taxes.length > 0) {
	        	var subTotalRow = '<tr class="invoice-line invoice-subtotal">';
		        subTotalRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Subtotal</td>';
		        subTotalRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + subTotal.toFixed(2) + '</td>';
		        subTotalRow+= '<td class="text-sm p-2-f"></td>';
		        subTotalRow+= '</tr>';
		        $(subTotalRow).insertAfter($("tr.item-line:last"));
	        }

	        var cartTotalWithoutDiscount = grandTotal;
	        
	        discounts = LocalDB.sequencedDiscounts(discounts);
	        var hasAtleastOneDiscount = false;
	        if (discounts.length > 0) {
	            var subTotalIncludingTax = grandTotal;
	            $(discounts).each(function(k, v) {
	                if (v.tableId == null || v.tableId == '' || v.tableId == orderInfo.table_id) {
	                    if (!isNaN(v.amount) && v.amount > 0) {
	                        var hasDiscount = false;
	                        if (v.minimum_amount != null && subTotalIncludingTax >= toNumeric(v.minimum_amount)) {
	                            if (v.type == "percentage") {
	                                var percentage = v.amount;
	                                var discountAmount = ((grandTotal * percentage) / 100).toRound();
	                            } else {
	                                var discountAmount = v.amount.toRound();
	                            }
	                            grandTotal-= toNumeric(discountAmount);
	                            hasDiscount = true;
	                            if (!hasAtleastOneDiscount) {
		                        	hasAtleastOneDiscount = true;
		                        }
	                        } else if (v.minimum_amount == null) {
	                            if (v.type == "percentage") {
	                                var percentage = v.amount;
	                                var discountAmount = ((grandTotal * percentage) / 100).toRound();
	                            } else {
	                                var discountAmount = v.amount.toRound();
	                            }
	                            grandTotal-= toNumeric(discountAmount);
	                            hasDiscount = true;
	                            if (!hasAtleastOneDiscount) {
		                        	hasAtleastOneDiscount = true;
		                        }
	                        }
	                        if (hasDiscount) {
	                            var discountRow = '<tr class="invoice-line invoice-discount-line">';
	                            var discountDesc = '';
	                            if (v.description != null && v.description != '') {
	                                discountDesc = v.description + ' ';
	                            }
	                            if (v.type == "percentage") {
	                                var discountBase = v.amount + "%";
	                            } else {
	                                var discountBase = "Flat";
	                            }
	                            discountRow+= '<td colspan="3" class="text-sm p-2-f dotted-border-dark text-right">' + discountDesc + '[' + discountBase + '] </td>';
	                            discountRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + toNumeric(discountAmount).toFixed(2) + '</td>';
	                            subTotalRow+= '<td class="text-sm p-2-f"></td>';
	                            discountRow+= '</tr>';
	                            $(discountRow).insertAfter($("tr.invoice-line:last"));
	                        }
	                    }
	                }
	            });
	        }

	        var cartTotalAfterDiscount = grandTotal;
	        if (taxes.length > 0) {
	        	if (hasAtleastOneDiscount) {
	        		var subTotalRow = '<tr class="invoice-line invoice-total-after-discount">';
			        subTotalRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Total (Excl. Tax)</td>';
			        subTotalRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + cartTotalAfterDiscount.toRound() + '</td>';
			        subTotalRow+= '<td class="text-sm p-2-f"></td>';
			        subTotalRow+= '</tr>';
			        $(subTotalRow).insertAfter($("tr.invoice-line:last"));
	        	}

	            inclTaxLabel = ' (Incl. Tax)';
	            $(taxes).each(function(k, v) {
	                if (v.tableId == null || v.tableId == '' || v.tableId == orderInfo.table_id) {
	                    if (!isNaN(v.amount) && v.amount > 0) {
	                        if (v.type == "percentage") {
	                            var percentage = v.amount;
	                            var taxAmount = ((cartTotalAfterDiscount * percentage) / 100).toRound();
	                        } else {
	                            var taxAmount = v.amount.toRound();
	                        }
	                        grandTotal+= toNumeric(taxAmount);
	                        var taxBase = "";
	                        if (v.type != null && v.type == "percentage") {
	                            taxBase = ' [' + v.amount + '%]';
	                        }
	                        var taxRow = '<tr class="invoice-line invoice-tax-line">';
	                        taxRow+= '<td colspan="3" class="text-sm p-2-f dotted-border-dark text-right"> ' + v.name + taxBase + ' </td>';
	                        taxRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + toNumeric(taxAmount).toFixed(2) + '</td>';
	                        subTotalRow+= '<td class="text-sm p-2-f"></td>';
	                        taxRow+= '</tr>';
	                        $(taxRow).insertAfter($("tr.invoice-line:last"));
	                    }
	                }
	            });
	        }	        

	        let totalRoundedBy = (Math.round(toNumeric(grandTotal)) - toNumeric(grandTotal)).toFixed(2);
	        if (totalRoundedBy != 0) {
	        	var grandTotalRoundRow = '<tr class="invoice-line invoice-grand-total-round">';
		        grandTotalRoundRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Rounded By</td>';
		        grandTotalRoundRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + (Math.round(toNumeric(grandTotal)) - toNumeric(grandTotal)).toFixed(2) + '</td>';
		        grandTotalRoundRow+= '<td class="text-sm p-2-f"></td>';
		        grandTotalRoundRow+= '</tr>';
		        $(grandTotalRoundRow).insertAfter($("tr.invoice-line:last"));
	        }

	        var grandTotalRow = '<tr class="invoice-line invoice-grand-total">';
	        grandTotalRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Grand Total</td>';
	        grandTotalRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + Math.round(toNumeric(grandTotal)).toFixed(2) + '</td>';
	        grandTotalRow+= '<td class="text-sm p-2-f"></td>';
	        grandTotalRow+= '</tr>';
	        $(grandTotalRow).insertAfter($("tr.invoice-line:last"));

	        var invoiceTable = LocalDB.getTableInfo(orderInfo.table_id);
	        var tableInfo = "Table #"+invoiceTable.table_no;
	        if (invoiceTable.area_category_id != null && invoiceTable.area_category_id > 0) {
	            var invoiceTableAreaCategory = LocalDB.getAreaCategoryInfo(invoiceTable.area_category_id);
	            tableInfo+= " [" + invoiceTableAreaCategory.name + "]";
	        }
	        var invoiceOrderNo = orderInfo.order_no;
	        $("span.invoice-payment-method").html(orderInfo.payment_method.toUpperCase());
	        $("span.invoice-restaurant-name").html(RESTAURANT.NAME);
	        if (getRestaurantFoodTypesBadge() != '') {
	            $(".invoice-restaurant-food-types-badge").html(getRestaurantFoodTypesBadge());
	        }
	        $("span.invoice-table-no").html(tableInfo);
	        $("span.invoice-ref-no").html("Order #" + invoiceOrderNo);
	        $("span.invoice-date").html(orderInfo.created_at);
	        $(".btn-print-invoice").show();
	        $("img.invoice-logo").show();
	        showInvoice();
    	} else {
    		showNotification(NO_ACCESS_MSG, "error");
    	}
    } else {
        showLoginLayout(true);
    }
}

// Get system accounts for managing them
function getSystemAccountsInfo() {
	if (checkLogin()) {
    	if (withAccess("office")) {
    		toggleWaitingLoader("show");
    		$("table.table-system-accounts-info").empty();
    		callAJAX(APP_API_ENDPOINT + "api.php", "POST",
    			{
    				action : 'get-system-accounts',
    				restaurant_id : RESTAURANT.ID
    			},
    			function(response) {
    				toggleWaitingLoader("hide");
    				if (response.status == true) {
    					let accounts = response.accounts;
    					if (accounts.length > 0) {
	    					let accountSystemHeaderLine = '<tr>';
	    					accountSystemHeaderLine+= '<th>';
	    					accountSystemHeaderLine+= 'Username';
	    					accountSystemHeaderLine+= '</th>';
	    					accountSystemHeaderLine+= '<th>';
	    					accountSystemHeaderLine+= 'Role';
	    					accountSystemHeaderLine+= '</th>';
	    					accountSystemHeaderLine+= '<th>';
	    					accountSystemHeaderLine+= 'Password';
	    					accountSystemHeaderLine+= '</th>';
	    					accountSystemHeaderLine+= '<th>';
	    					accountSystemHeaderLine+= 'Action';
	    					accountSystemHeaderLine+= '</th>';
	    					accountSystemHeaderLine+= '</tr>';
	    					$("table.table-system-accounts-info").append(accountSystemHeaderLine);
	    					$(accounts).each(function(ak, account) {
	    						let accountSystemLine = '<tr>';
	    						accountSystemLine+= '<td class="dotted-border-sm text-center text-bold text-orange">';
		    					accountSystemLine+= account.role.toString().toUpperCase();
		    					accountSystemLine+= '</td>';
		    					accountSystemLine+= '<td class="dotted-border-sm text-center text-grey-dark">';
		    					accountSystemLine+= account.username;
		    					accountSystemLine+= '</td>';
		    					accountSystemLine+= '<td class="dotted-border-sm text-left text-grey-dark field">';
		    					accountSystemLine+= '<input type="text" class="account-password" value="'+account.password+'" placeholder="Set new password" />';
		    					accountSystemLine+= '</td>';
		    					accountSystemLine+= '<td class="dotted-border-sm text-left text-grey-dark field">';
		    					accountSystemLine+= '<button class="btn-solid btn-sm btn-success btn-save-account-password" data-accounthash="'+account.id+'" title="Set account password"><img class="icon-xsmall" src="assets/img/icons/icon-apply.png"></button>';
		    					accountSystemLine+= '<button class="btn-solid btn-sm ml-px5 btn-danger btn-delete-system-account" title="Delete this account" data-accounthash="'+account.id+'"><img class="icon-xsmall" src="assets/img/icons/icon-delete.png"></button>';
		    					accountSystemLine+= '</td>';
		    					accountSystemLine+= '</tr>';
		    					$("table.table-system-accounts-info").append(accountSystemLine);
	    					});
	    				} else {
	    					var noAccountSystemLine = '<tr>';
	    					noAccountSystemLine+= '<td class="dotted-border-sm">';
	    					noAccountSystemLine+= "There is no other account in the system";
	    					noAccountSystemLine+= '</td>';
	    					noAccountSystemLine+= '</tr>';
	    					$("table.table-system-accounts-info").append(noAccountSystemLine);
	    				}
    				} else {
    					hideModal();
    					showNotification(response.message, "error");
    				}
    			},
    			function(error) {
    				toggleWaitingLoader("hide");
    				showNotification(OOPS_MSG, "error");
    			}
    		);
    	} else {
    		showNotification(NO_ACCESS_MSG, "error");
    	}
    } else {
    	showLoginLayout(true);
    }
}

// Change system account password
function changeSystemAccountPassword(sysAccountHash, newPassword) {
	if (checkLogin()) {
    	if (withAccess("office")) {
    		toggleWaitingLoader("show");
    		callAJAX(APP_API_ENDPOINT + "api.php", "POST", 
	    		{
	    			action : 'change-system-account-password',
	    			restaurant_id : RESTAURANT.ID,
	    			sysAccountHash : sysAccountHash,
	    			newPassword : newPassword
	    		},
	    		function(response) {
	    			toggleWaitingLoader("hide");
	    			if (response.status == true) {
	    				showNotification(response.message, "info");
	    			} else {
	    				showNotification(response.message, "error");
	    			}
	    		},
	    		function(error) {
	    			toggleWaitingLoader("hide");
	    			showNotification(OOPS_MSG, "error");
	    		}
    		);
    	} else {
    		showNotification(NO_ACCESS_MSG, "error");
    	}
    } else {
    	showLoginLayout(true);
    }
}

// Delete system account
function deleteSystemAccount(sysAccountHash, sourceObj = null) {
	if (checkLogin()) {
    	if (withAccess("office")) {
    		toggleWaitingLoader("show");
    		callAJAX(APP_API_ENDPOINT + "api.php", "POST", 
	    		{
	    			action : 'delete-system-account',
	    			restaurant_id : RESTAURANT.ID,
	    			sysAccountHash : sysAccountHash
	    		},
	    		function(response) {
	    			toggleWaitingLoader("hide");
	    			if (response.status == true) {
	    				if (sourceObj != null) {
	    					$(sourceObj).remove();
	    				}
	    				showNotification(response.message, "info");
	    			} else {
	    				showNotification(response.message, "error");
	    			}
	    		},
	    		function(error) {
	    			toggleWaitingLoader("hide");
	    			showNotification(OOPS_MSG, "error");
	    		}
    		);
    	} else {
    		showNotification(NO_ACCESS_MSG, "error");
    	}
    } else {
    	showLoginLayout(true);
    }
}

// Remove logo for restaurant
function removeLogoForRestaurant() {
	if (checkLogin()) {
    	if (withAccess("office")) {
    		toggleWaitingLoader("show");
    		callAJAX(APP_API_ENDPOINT + "api.php", "POST", 
	    		{
	    			action : 'remove-restaurant-logo',
	    			restaurant_id : RESTAURANT.ID
	    		},
	    		function(response) {
	    			toggleWaitingLoader("hide");
	    			if (response.status == true) {
	    				showNotification(response.message, "info");
	    			} else {
	    				showNotification(response.message, "error");
	    			}
	    		},
	    		function(error) {
	    			toggleWaitingLoader("hide");
	    			showNotification(OOPS_MSG, "error");
	    		}
	    	);
    	} else {
    		showNotification(NO_ACCESS_MSG, "error");
    	}
    } else {
    	showLoginLayout(true);
    }
}