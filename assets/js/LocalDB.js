class LocalDB {

	static cartName = '__FCCartItems';
	static restaurantInfo = 'restaurantInfo';
	static categoriesInfo = 'categories';
	static itemsInfo = 'items';
	static taxInfo = 'tax';
	static discountInfo = 'discounts';
	static tableInfo = 'tables';
	static activeTableInfo = 'activeTable';
	static loginAs = 'loginAs';
	static areaCategoryInfo = 'areaCategories';
	static kitchenItemsInfo = 'itemsForKitchen';
	static ordersInfo = 'orders';
	static paymentMethodsInfo = 'paymentMethods';
	static currencyInfo = 'currencies';

	static isSet(item) {
		if (this.get(item) == null || this.get(item) == '[]' || this.get(item).length == 0 || this.get(item) == '' ) {
			return false;
		}
		return true;
	}

	static set(item, value) {
		localStorage.setItem(item, value);
	}
	static get(item) {
		if (localStorage.getItem(item) == null) {
			return JSON.stringify([]);
		}
		return localStorage.getItem(item);
	}
	static clearAll() {
		localStorage.clear();
	}

	static cartTotal(includeTax = false, includeDiscount = false) {
		var cartItems = this.cartItems();
		var cartTotal = 0;
		var dbObj = this;
		if (cartItems != null) {
			$(cartItems).each(function(k, v) {
				let tableId = dbObj.getActiveTable();
				var itemPrice = dbObj.getItemPriceByArea(v.itemId, dbObj.getTableInfo(tableId).area_category_id);
				if (v.itemId != null) {
					var item = dbObj.getItemInfo(v.itemId);
					if (!isNaN(v.qty) && !isNaN(itemPrice)) {
						cartTotal+= toNumeric(v.qty) * toNumeric(itemPrice);
					}
				}
			});
		}
		var cartTotalWithoutDiscount = cartTotal;
		if(includeDiscount) {
			if (this.discounts().length > 0) {
				var discounts = this.sequencedDiscounts();
				$(discounts).each(function(k, v) {
					if (v.tableId == null || v.tableId == LocalDB.getActiveTable()) {
						if (!isNaN(v.amount) && v.amount > 0) {
							if (v.minimum_amount != null && cartTotalWithoutDiscount >= toNumeric(v.minimum_amount)) {
								if (v.type == "percentage") {
	                                var percentage = v.amount;
	                                var discountAmount = ((cartTotal * percentage) / 100).toRound();
	                            } else {
	                                var discountAmount = v.amount.toRound();
	                            }
								cartTotal-= toNumeric(discountAmount);
							} else if (v.minimum_amount == null) {
								if (v.type == "percentage") {
	                                var percentage = v.amount;
	                                var discountAmount = ((cartTotal * percentage) / 100).toRound();
	                            } else {
	                                var discountAmount = v.amount.toRound();
	                            }
								cartTotal-= toNumeric(discountAmount);
							}
						}
					}
				});
			}	
		}
		var cartTotalAfterDiscount = cartTotal;
		if(includeTax) {
			if (this.taxes().length > 0) {
				var taxes = this.taxes();
				$(taxes).each(function(k, v) {
					if (v.tableId == null || v.tableId == LocalDB.getActiveTable()) {
						if (!isNaN(v.amount) && v.amount > 0) {
							if (v.type == "percentage") {
	                            var percentage = v.amount;
	                            var taxAmount = ((cartTotalAfterDiscount * percentage) / 100).toRound();
	                        } else {
	                            var taxAmount = v.amount.toRound();
	                        }
						}
						cartTotal+= toNumeric(taxAmount);
					}
				});
			}
		}
		return Math.round(toNumeric(cartTotal)).toFixed(2);
	}
	static cartItems(includeTax = false, tableId = 0) {
		if (this.get(this.getCartName(tableId)) == null || this.get(this.getCartName(tableId)) == '[]') {
			return [];
		}
		tableId = (tableId > 0) ? tableId : this.getActiveTable();
		return JSON.parse(this.get(this.getCartName(tableId)));
	}
	static cartItemInfo(tableId, itemId) {
		if (this.get(this.getCartName(tableId)) == null || this.get(this.getCartName(tableId)) == '[]') {
			return [];
		}
		tableId = (tableId > 0) ? tableId : this.getActiveTable();
		var cartItems = this.cartItems();
		var cartItemInfo = [];
		$(cartItems).each(function(k, item) {
			if (item.itemId == itemId) {
				cartItemInfo = item;
			}
		});
		return cartItemInfo;
	}
	static addItemToCart(itemId, quantity, itemObj, tableId = 0, specifications = []) {
		itemId = toNumeric(itemId);
		quantity = toNumeric(quantity);
		var cartItems = this.cartItems();
		var isNewItem = true;
		var item = this.getItemInfo(itemId);
		var itemPrice = this.getItemPriceByArea(itemId, this.getTableInfo(this.getActiveTable()).area_category_id);
		if (cartItems != null) {
			$(cartItems).each(function(k, v) {
				if(v.itemId == itemId) {
					var itemsSpecifications = [];
					var currentSpecifications = (cartItems[k].specifications != '') ? JSON.parse(cartItems[k].specifications) : [];
					if (currentSpecifications.length > 0) {
						$(currentSpecifications).each(function(k, currentSpecification) {
							itemsSpecifications.push(currentSpecification);
						});
					}
					itemsSpecifications.push(specifications);
					cartItems[k].qty = toNumeric(v.qty) + quantity;
					cartItems[k].specifications = JSON.stringify(itemsSpecifications);
					if(isNewItem != false)
						isNewItem = false;
				}
			});
			if (isNewItem) {
				cartItems.push({
					'itemId' : itemId,
					'qty' : toNumeric(quantity),
					'price' : toNumeric(itemPrice),
					'picture' : $(itemObj).find('.picture').find('img').attr('src'),
					'name' : $(itemObj).find('.info').find('span.name').html(),
					'categoryId' : item.category_id,
					'tableId' : (tableId > 0) ? tableId : this.getActiveTable(),
					'specifications' : JSON.stringify([specifications]),
					'isServed' : false,
					'servedQty' : 0,
				});
			}
		} else {
			cartItems = [];
			cartItems.push({
				'itemId' : itemId,
				'qty' : toNumeric(quantity),
				'price' : toNumeric(itemPrice),
				'picture' : $(itemObj).find('.picture').find('img').attr('src'),
				'name' : $(itemObj).find('.info').find('span.name').html(),
				'categoryId' : item.category_id,
				'tableId' : (tableId > 0) ? tableId : this.getActiveTable(),
				'specifications' : JSON.stringify([specifications]),
				'isServed' : false,
				'servedQty' : 0,
			});
		}
		this.set(this.getCartName(tableId), JSON.stringify(cartItems));
		this.adjustTableStatus();
	}
	static replaceItemQtyToCart(itemId, quantity, specifications = []) {
		var cartItems = this.cartItems();
		$(cartItems).each(function(k, v) {
			if (v.itemId == itemId) {
				var itemsSpecifications = [];
				var currentSpecifications = [];
				if (currentSpecifications.length > 0) {
					$(currentSpecifications).each(function(k, currentSpecification) {
						itemsSpecifications.push(currentSpecification);
					});
				}
				itemsSpecifications.push(specifications);
				cartItems[k].qty = toNumeric(quantity);
				cartItems[k].specifications = JSON.stringify(itemsSpecifications);
			}
		});
		this.set(this.getCartName(), JSON.stringify(cartItems));
	}
	static removeItemFromCart(itemId) {
		var cartItems = this.cartItems();
		$(cartItems).each(function(k, v) {
			if (v.itemId == itemId) {
				cartItems.splice(k, 1);
			}
		});
		this.set(this.getCartName(), JSON.stringify(cartItems));
		this.adjustTableStatus();
	}
	static clearCart() {
		this.set(this.getCartName(), JSON.stringify([]));
		this.adjustTableStatus();
	}
	static clearTableCart(tableId) {
		var tableCartName = this.cartName + '.table-' + tableId;
		this.set(tableCartName, JSON.stringify([]));
		this.adjustTableStatus(tableId);
		triggerCartUpdated(tableId);
	}

	static saveRestaurant(restaurant) {
		this.set(this.restaurantInfo, JSON.stringify(restaurant));
	}
	static restaurant() {
		if (this.get(this.restaurantInfo) == null || this.get(this.restaurantInfo) == '[]') {
			return [];
		}
		return JSON.parse(this.get(this.restaurantInfo));
	}
	static getRestaurantFoodTypes() {
		if (this.get(this.restaurantInfo) == null || this.get(this.restaurantInfo) == '[]') {
			return [];
		}
		var foodTypes = this.restaurant().food_type;
		if (typeof foodTypes != 'undefined' && foodTypes != '') {
			foodTypes = foodTypes.split(",");
			return foodTypes;
		} else {
			return [];
		}
	}

	static saveCategory(category) {
		var categories = this.categories();
		var categoryExists = false;
		$(categories).each(function(k, v) {
			if (!categoryExists && v.id === category.id) {
				categoryExists = true;
			}
		});
		if (!categoryExists) {
			categories.push(category);
		}
		this.set(this.categoriesInfo, JSON.stringify(categories));
	}
	static categories() {
		if (this.get(this.categoriesInfo) == null || this.get(this.categoriesInfo) == '[]') {
			return [];
		}
		return JSON.parse(this.get(this.categoriesInfo));
	}
	static getCategoryInfo(categoryId) {
		var categories = this.categories();
		let category = categories.find(category => category.id === categoryId);
		return category;
	}
	static searchCategories(categoryTitle = '') {
		if(categoryTitle == '') {
			return [];
		}
		var categories = this.categories();
		var categoriesBySearchPhase = [];
		$(categories).each(function(k, v) {
			if((v.name).toString().trim().toLowerCase().indexOf(categoryTitle.toLowerCase()) != -1) {
				categoriesBySearchPhase.push(v);
			}
		});
		return categoriesBySearchPhase;
	}

	static saveItem(item, category_id, isSync = false) {
		var items = this.items('all');
		var itemExists = false;
		item.colorCode = generateDarkColorHex();
		$(items).each(function(k, v) {
			if (!itemExists && v.id === item.id) {
				itemExists = true;
				if(isSync) {
					items[k] = item;
				}
			}
		});
		if (!itemExists) {
			items.push(item);
		}
		this.set(this.itemsInfo, JSON.stringify(items));
	}
	static items(category_id) {
		if (this.get(this.itemsInfo) == null || this.get(this.itemsInfo) == '[]' || (category_id != 'all' && isNaN(category_id))) {
			return [];
		}

		var items = JSON.parse(this.get(this.itemsInfo));
		if(category_id == 'all') {
			return items;
		}

		var categoryItems = [];
		$(items).each(function(k, v) {
			if(v.category_id == toNumeric(category_id)) {
				categoryItems.push(v);
			}
		});
		return categoryItems;
	}
	static itemInCart(itemId) {
		var itemInCart = false;
		var cartItems = this.cartItems();
		$(cartItems).each(function(k, v) {
			if(!itemInCart && v.itemId == itemId && v.tableId == this.getActiveTable()) {
				itemInCart = true;
			}
		});
		return itemInCart;
	}
	static getItemInfo(itemId) {
		var items = this.items('all');
		let item = items.find(item => item.id === itemId);
		return item;
	}
	static removeCategoryItems(category_id) {
		var items = this.items('all');
		var remainingItems = [];
		$(items).each(function(kitem, item) {
			if (item.category_id != category_id) {
				remainingItems.push(item);
			}
		});
		this.set(this.itemsInfo, JSON.stringify(remainingItems));
	}
	static searchItems(itemTitle = '') {
		if(itemTitle == '') {
			return [];
		}
		var items = this.items('all');
		var itemsBySearchPhase = [];
		$(items).each(function(k, v) {
			if((v.name).toString().trim().toLowerCase().indexOf(itemTitle.toLowerCase()) != -1 || (v.item_code).toString().trim().toLowerCase().indexOf(itemTitle.toLowerCase()) != -1) {
				itemsBySearchPhase.push(v);
			}
		});
		return itemsBySearchPhase;
	}
	static setItemPriceInfo(itemId, itemPriceInfo = '') {
		var items = this.items('all');
		$(items).each(function(ik, iv) {
			if (iv.id == itemId) {
				items[ik].item_price_info = itemPriceInfo;
			}
		});
		this.set(this.itemsInfo, JSON.stringify(items));
	}
	static getItemPriceByArea(itemId, areaCategoryId) {
		var items = this.items('all');
		var price = 0;
		$(items).each(function(ik, iv) {
			if (iv.id == itemId) {
				var itemPriceInfo = JSON.parse(iv.item_price_info);
				$(itemPriceInfo).each(function(pi, priceInfo) {
					if (priceInfo.acatId == areaCategoryId) {
						price = priceInfo.price;
					}
				});
			}
		});
		return price;
	}

	static saveTax(tax, tableId = null) {
		var taxes = this.taxes();
		var taxExist = false;
		tax.tableId = tableId;
		$(taxes).each(function(k, v) {
			if (!taxExist && v.id === tax.id) {
				taxExist = true;
			}
		});
		if (!taxExist) {
			taxes.push(tax);
		}
		this.set(this.taxInfo, JSON.stringify(taxes));
	}
	static taxes(tableId = 0) {
		var taxes = JSON.parse(this.get(this.taxInfo));
		var availableTaxes = [];
		if (this.get(this.taxInfo) == null || this.get(this.taxInfo) == '[]') {
			return [];
		}
		$(taxes).each(function(k, v) {
			if (tableId > 0) {
				if (v.tableId == null || v.tableId == tableId) {
					availableTaxes.push(v);
				}
			} else {
				availableTaxes.push(v);
			}
		});
		return availableTaxes;
	}
	static getTaxInfo(taxId) {
		var taxes = this.taxes();
		let tax = taxes.find(tax => tax.id === toNumeric(taxId));
		return tax;
	}
	static clearCustomTaxes(tableId = 0) {
		if (tableId > 0) {
			var taxes = JSON.parse(this.get(this.taxInfo));
			var availableTaxes = [];
			if (this.get(this.taxInfo) == null || this.get(this.taxInfo) == '[]') {
				return [];
			}
			$(taxes).each(function(k, v) {
				if (tableId != v.tableId) {
					availableTaxes.push(v);
				}
			});
			this.set(this.taxInfo, JSON.stringify(availableTaxes));
		}
	}
	static removeTableTax(taxId, tableId) {
		if (!isNaN(taxId) && !isNaN(tableId)) {
			var taxes = this.taxes();
			$(taxes).each(function(k, v) {
				if (v.id == taxId && v.tableId == tableId) {
					taxes.splice(k, 1);
				}
			});
			this.set(this.taxInfo, JSON.stringify(taxes));
		}
	}

	static saveDiscount(discount, tableId = null) {
		var discounts = this.discounts();
		var discountExist = false;
		discount.tableId = tableId;
		$(discounts).each(function(k, v) {
			if (!discountExist && v.id === discount.id) {
				discountExist = true;
			}
		});
		if (!discountExist) {
			discounts.push(discount);
		}
		this.set(this.discountInfo, JSON.stringify(discounts));
	}
	static discounts(tableId = 0) {
		var discounts = JSON.parse(this.get(this.discountInfo));
		var availableDiscounts = [];
		if (this.get(this.discountInfo) == null || this.get(this.discountInfo) == '[]') {
			return [];
		}
		$(discounts).each(function(k, v) {
			if (tableId > 0) {
				if (v.tableId == null || v.tableId == tableId) {
					availableDiscounts.push(v);
				}
			} else {
				availableDiscounts.push(v);
			}
		});
		return availableDiscounts;
	}
	static sequencedDiscounts(discounts = null) {
		var flatDicounts = [];
		var perDicounts = [];
		var discountsToAdjust = (discounts != null) ? discounts : this.discounts();
		$(discountsToAdjust).each(function(k, v) {
			if (v.type == "flat") {
				flatDicounts.push(v);
			} else {
				perDicounts.push(v);
			}
		});
		var discounts = [];
		$(flatDicounts).each(function(fdck, fdc) {
			discounts.push(fdc);
		});
		perDicounts.sort(function(a, b) {
		    return b.amount - a.amount;
		});
		$(perDicounts).each(function(pdck, pdc) {
			discounts.push(pdc);
		});
		return discounts;
	}
	static getDiscountInfo(discountId) {
		var discounts = this.discounts();
		let discount = discounts.find(discount => discount.id === toNumeric(discountId));
		return discount;
	}
	static clearCustomDiscounts(tableId = 0) {
		if (tableId > 0) {
			var discounts = JSON.parse(this.get(this.discountInfo));
			var availableDiscounts = [];
			if (this.get(this.discountInfo) == null || this.get(this.discountInfo) == '[]') {
				return [];
			}
			$(discounts).each(function(k, v) {
				if (tableId != v.tableId) {
					availableDiscounts.push(v);
				}
			});
			this.set(this.discountInfo, JSON.stringify(availableDiscounts));
		}
	}
	static removeTableDiscount(discountId, tableId) {
		if (!isNaN(discountId) && !isNaN(tableId)) {
			var discounts = this.discounts();
			$(discounts).each(function(k, v) {
				if (v.id == discountId && v.tableId == tableId) {
					discounts.splice(k, 1);
				}
			});
			this.set(this.discountInfo, JSON.stringify(discounts));	
		}
	}

	static setLastVisitedCategory(category_id) {
		this.set('lastVisitedCategory', category_id);
	}
	static getLastVisitedCategory() {
		return this.get('lastVisitedCategory');
	}

	static saveAreaCategory(areaCategory) {
		var areaCategories = this.areaCategories();
		var areaCategoryExist = false;
		$(areaCategories).each(function(k, v) {
			if (!areaCategoryExist && v.id === areaCategory.id) {
				areaCategoryExist = true;
			}
		});
		if (!areaCategoryExist) {
			areaCategories.push(areaCategory);
		}
		this.set(this.areaCategoryInfo, JSON.stringify(areaCategories));
	}
	static areaCategories() {
		if (this.get(this.areaCategoryInfo) == null || this.get(this.areaCategoryInfo) == '[]') {
			return [];
		}
		return JSON.parse(this.get(this.areaCategoryInfo));
	}
	static getAreaCategoryInfo(areaCategoryId) {
		var areaCategories = this.areaCategories();
		let areaCategory = areaCategories.find(areaCategory => areaCategory.id === areaCategoryId);
		return areaCategory;
	}
	static searchAreaCategories(areaCategoryTitle = '') {
		if(areaCategoryTitle == '') {
			return [];
		}
		var areaCategories = this.areaCategories();
		var areaCategoriesBySearchPhase = [];
		$(areaCategories).each(function(k, areaCategory) {
			if((areaCategory.name).toString().trim().toLowerCase().indexOf(areaCategoryTitle.toLowerCase()) != -1) {
				areaCategoriesBySearchPhase.push(areaCategory);
			}
		});
		return areaCategoriesBySearchPhase;
	}

	static saveTable(table) {
		var tables = this.tables();
		var tableExist = false;
		$(tables).each(function(k, v) {
			if (!tableExist && v.id === table.id) {
				tableExist = true;
			}
		});
		if (!tableExist) {
			tables.push(table);
		}
		this.set(this.tableInfo, JSON.stringify(tables));
	}
	static tables(areaCategoryId = 0) {
		if (this.get(this.tableInfo) == null || this.get(this.tableInfo) == '[]') {
			return [];
		}

		var tables = JSON.parse(this.get(this.tableInfo));
		var tablesWithFilter = [];

		if (areaCategoryId > 0) {
			$(tables).each(function(k, table) {
				if (table.area_category_id == areaCategoryId) {
					tablesWithFilter.push(table);
				}
			});
			return tablesWithFilter;
		} else {
			return tables;
		}
	}
	static setActiveTable(table_id) {
		this.set(this.activeTableInfo, table_id);
	}
	static getActiveTable() {
		return toNumeric(this.get(this.activeTableInfo));
	}
	static getCartName(tableId) {
		tableId = (tableId > 0) ? tableId : this.getActiveTable();
		return this.cartName + '.table-' + tableId;
	}
	static adjustTableStatus(tableId = 0) {
		tableId = (tableId > 0) ? tableId : this.getActiveTable();
		if (this.cartItems().length > 0) {
			this.setTableStatus(tableId, 'B');
		} else {
			this.setTableStatus(tableId, 'A');
		}
		loadTables(this.tables());
	}
	static setTableStatus(tableId, status = 'B') {
		var tables = this.tables();
		var updatedTables = [];
		$(tables).each(function(k, v) {
			if (v.id === tableId) {
				v.status = status;
			}
			updatedTables.push(v);
		});
		this.set(this.tableInfo, JSON.stringify(updatedTables));
	}
	static getTableInfo(tableId = 0) {
		var tables = this.tables();
		let table = tables.find(table => table.id === tableId);
		return (typeof table != 'undefined' && typeof table.table_no != 'undefined') ? table : {
			'id' : tableId,
			'table_no' : '#' + tableId + ' [DELETED]'
		};
	}
	static searchTables(tableTitle = '') {
		if(tableTitle == '') {
			return [];
		}
		var tables = this.tables();
		var tablesBySearchPhase = [];
		$(tables).each(function(k, table) {
			if((table.table_no).indexOf(tableTitle) != -1) {
				tablesBySearchPhase.push(table);
			}
		});
		return tablesBySearchPhase;
	}

	static generateOrder(paymentMethod = []) {
		if ($.inArray(paymentMethod.payment_method, ALLOWED_PAYMENT_METHODS) == -1) {
			return false;
		}
		var orderInfo = [];
		orderInfo['order_no'] = Math.floor(new Date().valueOf() * Math.random());
		orderInfo['cart_info'] = JSON.stringify(this.cartItems());
		orderInfo['tax_info'] = JSON.stringify(this.taxes(this.getActiveTable()));
		orderInfo['discount_info'] = JSON.stringify(this.discounts(this.getActiveTable()));
		orderInfo['order_status'] = 'pending';
		orderInfo['order_amount'] = LocalDB.cartTotal(true, true);
		orderInfo['payment_method'] = paymentMethod.payment_method;
		orderInfo['payment_method_info'] = JSON.stringify(paymentMethod);
		orderInfo['table_id'] = this.getActiveTable();
		orderInfo['restaurant_id'] = this.restaurant().id;
		return orderInfo;
	}
	static orders() {
		return JSON.parse(this.get(this.ordersInfo));
	}
	static searchOrders(orderNo = '') {
		if(orderNo == '') {
			return [];
		}
		var orders = this.orders();
		var ordersByOrderNo = [];
		$(orders).each(function(k, order) {
			if((order.order_no).toString().trim().toLowerCase().indexOf(orderNo.toLowerCase()) != -1) {
				ordersByOrderNo.push(order);
			}
		});
		return ordersByOrderNo;
	}
	static filterOrders(orderParams) {
		var orders = LocalDB.orders();
		var filteredOrders = [];
		$(orders).each(function(k, order) {
			var isMatching = true;
			if (orderParams.order_no != 'undefined' && orderParams.order_no != '') {
				if((order.order_no).toString().trim().toLowerCase().indexOf(orderParams.order_no.toLowerCase()) != -1) {
					isMatching = true; 
				} else {
					isMatching = false;
				}
			}
			if (orderParams.table_id != 'undefined' && orderParams.table_id != '') {
				if (isMatching && order.table_id == orderParams.table_id) {
					isMatching = true;
				} else {
					isMatching = false;
				}
			}
			if (orderParams.amount_from != 'undefined' && orderParams.amount_from != '' && !isNaN(orderParams.amount_from)) {
				if (isMatching && toNumeric(order.order_amount) >= toNumeric(orderParams.amount_from)) {
					isMatching = true;
				} else {
					isMatching = false;
				}
			}
			if (orderParams.amount_to != 'undefined' && orderParams.amount_to != '' && !isNaN(orderParams.amount_to)) {
				if (isMatching && toNumeric(order.order_amount) <= toNumeric(orderParams.amount_to)) {
					isMatching = true;
				} else {
					isMatching = false;
				}
			}
			if (orderParams.payment_method != 'undefined' && orderParams.payment_method != '') {
				if (isMatching && order.payment_method == orderParams.payment_method) {
					isMatching = true;
				} else {
					isMatching = false;
				}
			}
			if (isMatching) {
				filteredOrders.push(order);
			}
		});
		return filteredOrders;
	}
	static getOrderInfo(orderId = 0) {
		var orders = this.orders();
		let order = orders.find(order => order.id === orderId);
		return order;
	}
	static getOrderIndex(orderId = 0) {
		var orders = this.orders();
		var orderIndex = -1;
		$(orders).each(function(okey, order) {
			if (orderIndex == -1 && order.id == orderId) {
				orderIndex = okey;
			}
		});
		return orderIndex;
	}
	static updateOrderInfo(orderId = 0, orderInfo) {
		if (typeof orderId == 'undefined' || orderId <= 0) {
			return false;
		}
		var orders = this.orders();
		$(orders).each(function(okey, order) {
			if (order.id == orderId) {
				orders[okey].order_info = orderInfo.order_info;
				orders[okey].order_amount = orderInfo.order_amount;
				orders[okey].table_id = toNumeric(orderInfo.table_id);
				orders[okey].table_no = orderInfo.table_no;
				orders[okey].payment_method = orderInfo.payment_method;
			}
		});
		this.set(this.ordersInfo, JSON.stringify(orders));
	}

	static setLoginAs(loginAs) {
		this.set(this.loginAs, JSON.stringify(loginAs));
	}
	static loggedInAs() {
		return JSON.parse(this.get(this.loginAs));
	}

	static kitchenItems() {
		return JSON.parse(this.get(this.kitchenItemsInfo));
	}
	static kitchenItemsForTable(tableId) {
		var kitchenItems = this.kitchenItems();
		var kitchenItemsForTable = [];
		if (kitchenItems.length > 0) {
			$(kitchenItems).each(function(k, kitchenItem) {
				if (kitchenItem.tableId == tableId) {
					kitchenItemsForTable.push(kitchenItem);
				}
			});
		}
		return kitchenItemsForTable;
	}
	static itemExistsOnTableForKitchen(tableId, itemId) {
		var kitchenItems = this.kitchenItems();
		var itemExists = false;
		$(kitchenItems).each(function(k, v) {
			if (!itemExists && v.tableId == tableId && v.itemId == itemId) {
				itemExists = true;
			}
		});
		return itemExists;
	}
	static getItemQtyWithSameAttrs(tableId, itemId, taste) {
		var kitchenItems = this.kitchenItems();
		var itemExists = false;
		var qty = 0;
		$(kitchenItems).each(function(k, v) {
			var specifications = JSON.parse(v.specifications);
			if (!itemExists && v.tableId == tableId && v.itemId == itemId && specifications.taste == taste) {
				itemExists = true;
				qty = v.qty;
			}
		});
		return qty;
	}
	static saveItemForKitchen(item) {
		var kitchenItems = this.kitchenItems();
		var itemExistsInKitchenLog = false;
		var dbObj = this;
		$(kitchenItems).each(function(k, v) {
			if (!itemExistsInKitchenLog && v.tableId == item.tableId && v.itemId === item.itemId) {
				itemExistsInKitchenLog = true;
				kitchenItems[k].qty = toNumeric(item.qty);
				kitchenItems[k].specifications = item.specifications;
			} else if (v.tableId == item.tableId && v.itemId === item.itemId && savedSpecifications.taste == newItemSpecifications.taste) {
			}
		});
		if (!itemExistsInKitchenLog) {
			kitchenItems.push(item);
		}
		this.set(this.kitchenItemsInfo, JSON.stringify(kitchenItems));
	}
	static removeKitchenLog(logId) {
		var kitchenItems = this.kitchenItems();
		$(kitchenItems).each(function(k, v) {
			if(v.logId == logId) {
				markItemsAsServed(v, 'no', 'N/A', v.tableId);
				kitchenItems.splice(k, 1);
			}
		});
		this.set(this.kitchenItemsInfo, JSON.stringify(kitchenItems));
	}
	static removeTableKitchenLog(tableId) {
		var kitchenItems = this.kitchenItems();
		var remainingKitchens = [];
		$(kitchenItems).each(function(k, v) {
			if(v.tableId != tableId) {
				remainingKitchens.push(v);
			}
		});
		this.set(this.kitchenItemsInfo, JSON.stringify(remainingKitchens));
	}
	static removeKitchenItemLog(itemInfo, taste = 'no', servedItemExtraSpecification = 'N/A') {
		var kitchenItems = this.kitchenItems();
		var updatedSpecifications = [];
		$(kitchenItems).each(function(k, v) {
			if(v.logId == itemInfo.logId && v.tableId == itemInfo.tableId && v.itemId == itemInfo.itemId) {
				var specifications = JSON.parse(v.specifications);
				$(specifications).each(function(spkey, specification) {
					if (specification.taste == itemInfo.taste && specification.extraSpecification == itemInfo.extraSpecification) {
						// Avoid this specification and do further
						specification.servedQty = toNumeric(specification.qty);
						updatedSpecifications.push(specification);
					} else {
						updatedSpecifications.push(specification);
					}
				});
				kitchenItems[k].specifications = JSON.stringify(updatedSpecifications);
				// kitchenItems[k].qty = toNumeric(kitchenItems[k].qty) - toNumeric(itemInfo.qty);
				markItemsAsServed(kitchenItems[k], taste, servedItemExtraSpecification, kitchenItems[k].tableId);
				if (kitchenItems[k].qty <= 0) {
					// kitchenItems.splice(k, 1);
				}
			}
		});
		this.set(this.kitchenItemsInfo, JSON.stringify(kitchenItems));
	}
	static removeItemFromTableForKitchen(tableId, itemId) {
		var kitchenItems = this.kitchenItems();
		var remainingKitchenItems = [];
		$(kitchenItems).each(function(k, v) {
			if(v.tableId != tableId && v.itemId != itemId) {
				remainingKitchenItems.push(v);
			}
		});
		this.set(this.kitchenItemsInfo, JSON.stringify(remainingKitchenItems));
	}
	static getTablePositionForKitchen(logId) {
		var kitchenItems = this.kitchenItems();
		$(kitchenItems).each(function(k, v) {
			if(v.logId == logId) {
				return v.tableId;
			}
		});
	}
	static markItemAsServed(itemsInfo, servedItemTaste = 'no', servedItemExtraSpecification = 'N/A') {
		var cartItems = this.cartItems(false, itemsInfo.tableId);
		$(cartItems).each(function(k, item) {
			if (item.itemId === itemsInfo.itemId) {
				var itemSpecifications = JSON.parse(item.specifications);
				var updatedItemSpecifications = JSON.parse(itemsInfo.specifications);

				$(itemSpecifications).each(function(spkey, specification) {
					$(updatedItemSpecifications).each(function(uspkey, uspecification) {
						if (specification.taste == servedItemTaste && specification.extraSpecification == servedItemExtraSpecification) {
							itemSpecifications[spkey].servedQty = itemSpecifications[spkey].qty;
							// if (uspecification.taste == specification.taste && uspecification.extraSpecification == specification.extraSpecification) {
							// 	// Avoid for further use
							// 	itemSpecifications[spkey].servedQty = itemSpecifications[spkey].qty;
							// } else {
								
							// }
						}
					});
				});
				// cartItems[k].isServed = true;
				cartItems[k].specifications = JSON.stringify(itemSpecifications);
			}
		});
		this.set(this.getCartName(itemsInfo.tableId), JSON.stringify(cartItems));
		if (withTableAccess()) {
			pushCartItemsToKitchen(toNumeric(itemsInfo.tableId));
		}
	}

	static paymentMethods() {
		return JSON.parse(this.get(this.paymentMethodsInfo));
	}
	static getPaymentMethodInfo(paymentMethodId) {
		var paymentMethods = this.paymentMethods();
		let paymentMethod = paymentMethods.find(paymentMethod => paymentMethod.id === paymentMethodId);
		return paymentMethod;
	}

	static currencies() {
		return JSON.parse(this.get(this.currencyInfo));
	}
	static getCurrencyInfo(currencyId) {
		var currencies = this.currencies();
		let currency = currencies.find(currency => currency.id === currencyId);
		return currency;
	}

}