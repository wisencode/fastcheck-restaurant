window.APP_TITLE = 'Fastcheck';
window.APP_POWERED_BY = {
    'NAME': 'Wisencode Infotech',
    'EMAIL': 'info@wisencode.com',
    'WEBSITE': 'https://wisencode.com',
    'MOBILE_NUMBER': '+91 8238136154'
}
window.APP_DATASTATION = 'datastation/';
window.APP_API_ENDPOINT = APP_DATASTATION + 'api/';
window.RESTAURANT = {
    'ID': 0,
    'NAME': '',
    'CURRENCY': '',
    'CURRENCY_CODE': '',
    'LOGO': '',
    'PAYMENT_METHODS': []
};

window.NO_INTERNET_MSG = 'NO_INTERNET: You are not connected to the internet. Please check your network.';
window.NO_ACCESS_MSG = 'You have no any access for this action';
window.OOPS_MSG = "Something goes wrong! Please try after some time";
window.ALLOWED_PAYMENT_METHODS = [];
window.NOTIFICATION_CHUNKS = [];

window.ITEM_TASTE_ADDONS = ["regular", "spicy", "medium"];
window.ITEM_COMMUNITY_ADDONS = ["jain"];

window.PAYMENT_IN_PROCESS = false;
window.CHECK_ORDER_STATUS_INTERVAL_ID = null;
window.CHECK_PAYMENT_LINK_STATUS_INTERVAL_ID = null;
window.CHECK_FC_PAYMENT_LINK_STATUS_INTERVAL_ID = null;
window.RZ_PAY_API_KEY_ID = 'rzp_test_fIxSpBE7Jo0bRw';
window.PUSHER_API_KEY = null;
window.eventHandler = null;
window.NUMBER_OF_ITEM_PER_PAGE = 10;
const CALENDER_MONTHS = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
window.fcEchoSystemProtocol = 'wss';
window.fcEchoSystemEndPoint = 'fastcheck.io/echo/';
window.kitchenAlertSound = new Audio("assets/sounds/kitchenalert.mp3");

function callAJAX(url, type = 'GET', data, onSuccess = null, onError = null) {
    $.ajax({
        url : 'assets/img/defaults/pixel.png?v=' + Math.random(),
        type : 'GET',
        success : function(success) {
            $.ajax({
                url: url,
                type: type.toString().toUpperCase(),
                data: data,
                success: (onSuccess) ? onSuccess : function() {},
                error: (onError) ? onError : function(error) {
                    showNotification(error.responseText, "error");
                },
            });
        },
        error : function(error) {
            toggleWaitingLoader("hide");
            showNotification(NO_INTERNET_MSG, "error");
            setAssetsToInitState();
        }
    });
}

function initEvents() {

    // Bind login button
    $(document).on("click", ".btn-login", function() {
        processLogin();
    });

    // Bind enter key on login fields to submit on enter
    $(document).on("keyup", ".login-form-field", function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            processLogin();
        }
    });

    // Bind category click event
    $(document).on("click", "li.category", function(e) {
        e.preventDefault();
        hideCart();
        var categoryRow = this;
        if ($(categoryRow).hasClass("active")) {
            // return false;
        }
        var category_id = $(categoryRow).attr("data-id");
        LocalDB.setLastVisitedCategory(category_id);
        $("li.category").each(function(k, v) {
            $(this).removeClass("active");
        });
        $(categoryRow).addClass("active");
        var items = LocalDB.items(category_id);
        setItems(items, category_id);
    });

    // Bind item click event
    var touchtime = 0;
    $(document).on("click", ".item", function(e) {
        $(this).removeClass("searched");
        if (touchtime == 0) {
            touchtime = new Date().getTime();
            submitItemToCart($(this).attr('data-id'), 1);
        } else {
            if (((new Date().getTime()) - touchtime) < 800) {
                e.preventDefault();
                var itemRow = this;
                if ($(itemRow).hasClass("active")) {
                    return false;
                }
                var specifications = getItemSpecifications(-1, $(this).attr('data-id'));
                addItemToCart($(this).attr('data-id'), -1, specifications);
                askQuantityForItem($(this).attr('data-id'));
                touchtime = 0;
            } else {
                touchtime = new Date().getTime();
                submitItemToCart($(this).attr('data-id'), 1);
            }
        }
    });

    // Bind quantity field submit/plus/minus event on arrow up/down and enter key
    $(document).on("keydown", "input[name='item_qty']", function(e) {
        var key = e.charCode || e.keyCode || 0;
        if (key == 8 || 
            key == 9 ||
            key == 46 ||
            key == 110 ||
            key == 190 ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105)) {
            return;
        }

        if (e.which == 13) {
            hideModal();
            submitItemToCart($("#modal-ask-quantity").find("input[name='item_id']").val(), $(this).val());
        }
        if (e.keyCode == 38) {
            $(".btn-manage-qty[data-action=plus]").trigger("click");
            e.preventDefault();
        }
        if (e.keyCode == 40) {
            $(".btn-manage-qty[data-action=minus]").trigger("click");
            e.preventDefault();
        }
    });

    // Bind quantity field with allowing only numbers to be entered
    $(document).on("keyup", "input[name='item_qty']", function(e) {
        if (/\D/g.test(this.value)){
            this.value = this.value.replace(/\D/g, '');
        }
    });

    // Bind outside click event of modal to hide itself
    $(document).on("click", ".modal-container", function(e) {
        var targetClasses = e.target.className;
        if (typeof targetClasses != 'undefined' && targetClasses != '') {
            targetClasses = targetClasses.split(" ");
            if ($.inArray('modal-container', targetClasses) >= 0) {
                hideModal();
            }
        }
    });

    // Bind plus/minus qty button
    $(document).on("click", ".btn-manage-qty", function(e) {
        var qty = toNumeric($("input[name='item_qty']").val());
        if ($(this).attr('data-action') == 'plus') {
            qty++;
        } else {
            if ((qty - 1) >= 0)
                qty--;
        }
        $("input[name='item_qty']").val(qty);
        return;
    });

    // Bind cart icon hide/show event
    $(document).on("click", ".btn-shopping-cart, .btn-back-to-products-screen", function(e) {
        if (!isMobile()) {
            return false;
        }
        $(".panel-cart").toggleClass('show');
        if ($(".panel-cart").hasClass("show")) {
            $(".panel-categories").css('display', 'none');
            $(".panel-items").css('display', 'none');
        } else {
            $(".panel-items").css('display', 'table-cell');
            $(".panel-categories").css('display', 'table-cell');
        }
    });

    // Bind clear cart button
    $(document).on("click", ".btn-empty-cart", function() {
        if (LocalDB.cartItems().length > 0 && confirm('Are you sure?')) {
            LocalDB.clearCart();
            triggerCartUpdated(LocalDB.getActiveTable());
            renderCart();
        }
    });

    // Bind enter button to add item in cart
    $(document).on("click", ".btn-add-item", function() {
        submitItemToCart($("input[name='item_id']").val(), $("input[name='item_qty']").val());
        hideModal();
    });

    // Bind quick item quantity adding btns
    $(document).on("click", ".btn-qty-quick-add", function() {
        $("input[name='item_qty']").val(toNumeric($(this).attr('data-qty')));
        $("input[name='item_qty']").trigger('focus');
    });

    // Bind cart buttons
    $(document).on("click", ".btn-cart.add-qty", function() {
        var cartItemHolder = $(this).closest("li");
        var itemId = toNumeric((cartItemHolder).attr('data-itemid'));
        submitItemToCart(itemId, 1);
    });
    $(document).on("click", ".btn-cart.remove-qty", function() {
        var cartItemHolder = $(this).closest("li");
        var itemId = toNumeric((cartItemHolder).attr('data-itemid'));
        submitItemToCart(itemId, -1, true);
    });
    $(document).on("click", ".btn-cart.edit-qty", function() {
        var cartItemHolder = $(this).closest("li");
        var itemId = toNumeric((cartItemHolder).attr('data-itemid'));
        var itemQty = toNumeric((cartItemHolder).attr('data-qty'));
        askQuantityForItem(itemId, itemQty, true);
    });
    $(document).on("click", ".btn-cart.remove-item", function() {
        var cartItemHolder = $(this).closest("li");
        var itemId = toNumeric((cartItemHolder).attr('data-itemid'));
        LocalDB.removeItemFromCart(itemId);
        if (LocalDB.cartItems().length == 0) {
            triggerCartUpdated(LocalDB.getActiveTable());
        }
        renderCart();
    });

    // Bind office action button click event
    $(document).on("click", "button.btn-office-action", function() {
        if ($(this).attr("data-action") == "orderreport" && $(this).hasClass("active")) {
          return false;  
        }
        hideModal();
        $(".table-office").hide();
        $(".office-welcome-msg").hide();
        $(".btn-office-action").each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
        var action = $(this).attr("data-action");
        if (action == "todayorders") {
            fetchOrders();
        } else if (action == "areacategories") {
            loadAreaCategoriesForOffice();
        } else if (action == "tables") {
            loadTablesForOffice();
        } else if (action == "items") {
            loadItemsForOffice();
        } else if (action == "categories") {
            loadCategoriesForOffice();
        } else if (action == "taxes") {
            loadTaxesForOffice();
        } else if (action == "discounts") {
            loadDiscountsForOffice();
        } else if (action == "paymentmethods") {
            loadPaymentMethodsForOffice();
        } else if (action == "profile") {
            loadProfileSettingsForOffice();
        } else if (action == "fcecardreport") {
            loadFCECardReportForOffice();
        } else if (action == "orderreport") {
            loadOrderReportForOffice();
        }
    });

    // Bind deactivate payment method button click event
    $(document).on("click", "button.btn-deactivate-payment-method", function() {
        var paymentMethodId = $(this).attr("data-id");
        var paymentMethodInfo = {
            'id' : paymentMethodId,
            'status' : '0'
        };
        updatePaymentMethod(paymentMethodInfo);
    });

    // Bind activate payment method button click event
    $(document).on("click", "button.btn-activate-payment-method", function() {
        var paymentMethodId = $(this).attr("data-id");
        var paymentMethodInfo = {
            'id' : paymentMethodId,
            'status' : '1'
        };
        updatePaymentMethod(paymentMethodInfo);
    });

    // Bind area category click event
    $(document).on("click", "button.btn-table-type", function() {
        $(".btn-table-type").each(function() {
            $(this).removeClass("active");
        });
        var areaCategoryId = $(this).attr("data-areacategory");
        var areaTables = LocalDB.tables(areaCategoryId);
        $(this).addClass("active");
        loadTables(areaTables, areaCategoryId);
    });

    // Bind table click event
    $(document).on("click", ".table-block", function() {
        LocalDB.setActiveTable(toNumeric($(this).attr('data-tableid')));
        // Set table no for bill
        if (!isNaN(LocalDB.getActiveTable()) && LocalDB.getActiveTable() > 0) {
            $(".footer .actions").show();
            if ($(".table-no-badge").length > 0) {
                var activeTable = LocalDB.getTableInfo(LocalDB.getActiveTable());
                $(".table-no-badge").html(activeTable.table_no + '<span class="area-category d-block">' + LocalDB.getAreaCategoryInfo(activeTable.area_category_id).name+ '</span>');
            }
        }
        LocalDB.setLastVisitedCategory(null);
        $("li.category:first").trigger('click');
        showProductsLayout();        
        renderCart();
        hideCart();
    });

    // Bind table area click event
    $(document).on("click", ".table-no-area", function() {
        var activeTable = LocalDB.getActiveTable();
        if (typeof activeTable != 'undefined' && activeTable != '' && !isNaN(activeTable) && activeTable > 0) {
            activeTable = toNumeric(activeTable);
            let tableInfo = LocalDB.getTableInfo(activeTable);
            let areaCategory = LocalDB.getAreaCategoryInfo(toNumeric(tableInfo.area_category_id));
            if (typeof tableInfo != 'undefined' && typeof areaCategory != 'undefined') {
                let activeTableItems = LocalDB.cartItems(false, activeTable);
                if (activeTableItems.length <= 0) {
                    showNotification("Nothing to manage: Empty table", "error");
                    return false;
                }
                let areaCategoryName = (typeof areaCategory != 'undefined' && typeof areaCategory.name != 'undefined' && areaCategory.name != '') ? areaCategory.name : '-';
                $("#modal-manage-running-order-table span.manage-table-no").html(tableInfo.table_no + ' (' + areaCategoryName + ')');
                var areaCategoryTables = LocalDB.tables(toNumeric(areaCategory.id));
                var areaCategoryTableOptions = "";
                $(areaCategoryTables).each(function(tk, table) {
                    if (table.id != activeTable) {
                        areaCategoryTableOptions+= "<option value='"+ table.id +"'>Table "+ table.table_no +"</option>";
                    }
                });
                if (areaCategoryTableOptions == "") {
                    showNotification("Nothing to manage: There is only 1 table in " + areaCategoryName, "error");
                    return false;
                }
                $("#modal-manage-running-order-table select[name='transferRunningOrderTableTo']").html(areaCategoryTableOptions);
                popupModal("#modal-manage-running-order-table");
            } else {
                return false;
            }
        }
        return false;
    });

    // Bind transfer running order table save button click event
    $(document).on("click", "button.btn-transfer-running-order-table", function() {
        var transferTo = $("#modal-manage-running-order-table select[name='transferRunningOrderTableTo']").val();
        if (typeof transferTo == 'undefined' || transferTo == '' || isNaN(transferTo) || transferTo <= 0) {
            showNotification("Please select table to transfer this order", "error");
        }
        var transferToTableItems = LocalDB.cartItems(false, toNumeric(transferTo));
        if (transferToTableItems.length > 0) {
            let transferToTableNo = LocalDB.getTableInfo(toNumeric(transferTo)).table_no;
            if (!confirm('There is already order running on table ' + transferToTableNo + '. Do you want to merge with this table?')) {
                return false;
            }
        }
        transferTableItems(LocalDB.getActiveTable(), transferTo);
    });

    // Bind button app home click event
    $(document).on("click", ".btn-app-home", function() {
        if (withTableAccess()) {
            // Initialize table view process
            showTableLayout();
        } else if (withAccess("kitchen")) {
            // Initialize kitchen view process
            showKitchenLayout();
        } else if (withAccess("office")) {
            showOfficeLayout();
        }
    });

    // Bind button app settings click event
    $(document).on("click", ".btn-app-configuration", function() {
        showSettingsLayout();
    });

    // Bind button app help-tips click event
    $(document).on("click", ".btn-app-help-tips", function() {
        showHelpTipsOption();
    });

    // Bind sync buttons for app settings
    $(document).on("click", ".btn-sync", function() {
        var module = $(this).attr("data-module");
        $(this).find(".sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync-in-process.gif");
        switch(module) {
            case 'tables':
                checkOnlineStatus().then(function(isConnected) {
                    if(isConnected) {
                        LocalDB.set(LocalDB.areaCategoryInfo, JSON.stringify([]));
                        syncAreaCategories(toNumeric(LocalDB.restaurant().id));
                    } else {
                        showNotification(NO_INTERNET_MSG, 'error');
                    }
                });
                break;
            case 'categories':
                checkOnlineStatus().then(function(isConnected) {
                    if(isConnected) {
                        LocalDB.set(LocalDB.categoriesInfo, JSON.stringify([]));
                        syncCategories(toNumeric(LocalDB.restaurant().id));
                    } else {
                        showNotification(NO_INTERNET_MSG, 'error');
                    }
                });
                break;
            case 'taxes':
                checkOnlineStatus().then(function(isConnected) {
                    if(isConnected) {
                        LocalDB.set(LocalDB.taxInfo, JSON.stringify([]));
                        LocalDB.set(LocalDB.discountInfo, JSON.stringify([]));
                        syncTaxes(toNumeric(LocalDB.restaurant().id));
                        syncDiscounts(toNumeric(LocalDB.restaurant().id));
                    } else {
                        showNotification(NO_INTERNET_MSG, 'error');
                    }
                });
                break;
            case 'discounts':
                checkOnlineStatus().then(function(isConnected) {
                    if(isConnected) {
                        LocalDB.set(LocalDB.discountInfo, JSON.stringify([]));
                        syncDiscounts(toNumeric(LocalDB.restaurant().id));
                    } else {
                        showNotification(NO_INTERNET_MSG, 'error');
                    }
                });
                break;
            case 'restaurant':
                checkOnlineStatus().then(function(isConnected) {
                    if(isConnected) {
                        syncRestaurant(toNumeric(LocalDB.restaurant().id));
                    } else {
                        showNotification(NO_INTERNET_MSG, 'error');
                    }
                });
                break;
            default:
                break;
        }
    });

    // Bind refresh cart table span
    $(document).on("click", ".btn-refresh-cart-table", function(e) {
        e.preventDefault();
        let tableId = LocalDB.getActiveTable();
        if (isNaN(tableId) || tableId <= 0) {
            return false;
        }
        setLiveTableCartItems(tableId);
    });

    // Bind save cart button
    $(document).on("click", ".btn-save-cart", function(e) {
        e.preventDefault();
        triggerCartUpdated(LocalDB.getActiveTable());
        pushCartItemsToKitchen();
        showNotification("Cart details saved for table #" + LocalDB.getTableInfo(toNumeric(LocalDB.getActiveTable())).table_no, "info");
    });

    // Bind checkout button
    $(document).on("click", ".btn-checkout", function(e) {
        e.preventDefault();
        if (LocalDB.cartItems().length > 0) {
            askCheckoutOptions();
        }
    });

    // Bind cart adjustment button
    $(document).on("click", ".btn-cart-adjustment", function(e) {
        e.preventDefault();
        if (LocalDB.cartItems().length > 0) {
            showCartAdjustments();
            loadTableDiscounts();
            loadTableTaxes();
            $("input[type='checkbox'].cart_adjusment_options").each(function() {
                $(this).prop("checked", false);
            });
            $("input[type='checkbox'][value='discount'].cart_adjusment_options").prop("checked", true).trigger("change");
        }
    });

    // Bind cart adjustment checkbox change event
    $(document).on("change", "input[type='checkbox'].cart_adjusment_options", function() {
        var checked = $(this).prop('checked');
        $(".cart-adjustment-option").hide();
        if (checked) {
            $("input[type='checkbox'].cart_adjusment_options").each(function() {
                $(this).prop("checked", false);
            });
        }
        $(this).prop("checked", checked);
        if (checked) {
            if ($(this).val() == "discount") {
                $(".cart-adjustment-option.cart-adjustment-discount-holder").show();
            } else if ($(this).val() == "tax") {
                $(".cart-adjustment-option.cart-adjustment-tax-holder").show();
            }
        } else {
            $("input[type='checkbox'][value='discount'].cart_adjusment_options").prop("checked", true);
            $(".cart-adjustment-option.cart-adjustment-discount-holder").show();
        }
    });

    // Bind cart adjusment discount type checkbox
    $(document).on("change", "input[type='checkbox'].cart_adjusment_discount_type", function() {
        var checked = $(this).prop('checked');
        if (checked) {
            $("input[type='checkbox'].cart_adjusment_discount_type").each(function() {
                $(this).prop("checked", false);
            });
        }
        $(this).prop("checked", checked);
        if (checked) {
            if ($(this).val() == "percentage") {
                $("input#cart_adjusment_discount_value").attr("placeholder", "% Discount (Ex. - 10%)");
                $("label[for='cart_adjusment_discount_value']").find("span.discount_type").html(" %");
            } else if ($(this).val() == "flat") {
                $("label[for='cart_adjusment_discount_value']").find("span.discount_type").html(" Flat");
                $("input#cart_adjusment_discount_value").attr("placeholder", "Flat Discount (Ex. - 100)");
            }
        } else {
            $("input#cart_adjusment_discount_value").attr("placeholder", "% Discount (Ex. - 10%)");
            $("label[for='cart_adjusment_discount_value']").find("span.discount_type").html(" %");
            $("input[type='checkbox'][value='percentage'].cart_adjusment_discount_type").prop("checked", true);
        }
    });

    // Bind save table discount button
    $(document).on("click", "button.btn-save-discount", function() {
        var discountFor = $("input#cart_adjusment_discount_for").val();
        var discountType = $("input[type='checkbox'].cart_adjusment_discount_type:checked").val();
        var discountValue = $("input#cart_adjusment_discount_value").val();
        if (discountFor == "") {
            discountFor = "Special Discount";
        }
        if (typeof discountType == "undefined" || discountType == "" || ($.inArray(discountType, ["percentage", "flat"]) === -1)) {
            showNotification("Please select discount type", "error");
            return false;
        }
        if (discountValue == "" || isNaN(discountValue)) {
            showNotification("Please enter valid discount", "error");
            return false;
        }
        var discount = {
            'id' : LocalDB.discounts().length + 1,
            'description' : discountFor,
            'minimum_amount' : null,
            'amount' : discountValue,
            'type' : discountType,
        };
        LocalDB.saveDiscount(discount, LocalDB.getActiveTable());
        showNotification("Discount saved for Table #" + LocalDB.getTableInfo(LocalDB.getActiveTable()).table_no, "info");
        $("input#cart_adjusment_discount_for").val("");
        $("input#cart_adjusment_discount_value").val("");
        hideModal();
        renderCart();
    });

    // Bind remove table discount button
    $(document).on("click", "button.btn-remove-table-discount", function() {
        var discountId = $(this).attr("data-id");
        var tableId = $(this).attr("data-tableid");
        if (!isNaN(discountId) && !isNaN(tableId)) {
            LocalDB.removeTableDiscount(discountId, tableId);
            $(this).parent().parent().remove();
            showNotification("Discount removed for Table #" + LocalDB.getTableInfo(LocalDB.getActiveTable()).table_no, "info");
            renderCart();
        }
    });

    // Bind cart adjusment tax type checkbox
    $(document).on("change", "input[type='checkbox'].cart_adjusment_tax_type", function() {
        var checked = $(this).prop('checked');
        if (checked) {
            $("input[type='checkbox'].cart_adjusment_tax_type").each(function() {
                $(this).prop("checked", false);
            });
        }
        $(this).prop("checked", checked);
        if (checked) {
            if ($(this).val() == "percentage") {
                $("input#cart_adjusment_tax_value").attr("placeholder", "% Tax (Ex. - 10%)");
                $("label[for='cart_adjusment_tax_value']").find("span.tax_type").html(" %");
            } else if ($(this).val() == "flat") {
                $("label[for='cart_adjusment_tax_value']").find("span.tax_type").html(" Flat");
                $("input#cart_adjusment_tax_value").attr("placeholder", "Flat Tax (Ex. - 100)");
            }
        } else {
            $("input#cart_adjusment_tax_value").attr("placeholder", "% Tax (Ex. - 10%)");
            $("label[for='cart_adjusment_tax_value']").find("span.tax_type").html(" %");
            $("input[type='checkbox'][value='percentage'].cart_adjusment_tax_type").prop("checked", true);
        }
    });

    // Bind save table tax button
    $(document).on("click", "button.btn-save-tax", function() {
        var taxFor = $("input#cart_adjusment_tax_for").val();
        var taxType = $("input[type='checkbox'].cart_adjusment_tax_type:checked").val();
        var taxValue = $("input#cart_adjusment_tax_value").val();
        if (taxFor == "") {
            taxFor = "Service Tax";
        }
        if (typeof taxType == "undefined" || taxType == "" || ($.inArray(taxType, ["percentage", "flat"]) === -1)) {
            showNotification("Please select tax type", "error");
            return false;
        }
        if (taxValue == "" || isNaN(taxValue)) {
            showNotification("Please enter valid tax", "error");
            return false;
        }
        var tax = {
            'id' : LocalDB.taxes().length + 1,
            'name' : taxFor,
            'amount' : taxValue,
            'type' : taxType,
        };
        LocalDB.saveTax(tax, LocalDB.getActiveTable());
        showNotification("Tax saved for Table #" + LocalDB.getTableInfo(LocalDB.getActiveTable()).table_no, "info");
        $("input#cart_adjusment_tax_for").val("");
        $("input#cart_adjusment_tax_value").val("");
        hideModal();
        renderCart();
    });

    // Bind remove table tax button
    $(document).on("click", "button.btn-remove-table-tax", function() {
        var taxId = $(this).attr("data-id");
        var tableId = $(this).attr("data-tableid");
        if (!isNaN(taxId) && !isNaN(tableId)) {
            LocalDB.removeTableTax(taxId, tableId);
            $(this).parent().parent().remove();
            showNotification("Tax removed for Table #" + LocalDB.getTableInfo(LocalDB.getActiveTable()).table_no, "info");
            renderCart();
        }
    });

    // Bind checkout options
    $(document).on("change", "input[type='checkbox'].checkout_options", function(e) {
        var checked = $(this).prop("checked");
        $(".checkout_options").prop("checked", false);
        $(this).prop('checked', checked);
        $("tr.customer-info-holder").hide();
        $("span.info-scan-fccard").removeClass('d-block-f');
        $(".btn-process-payment").show();
        if (checked && $(this).val() == 'payment_link') {
            $("tr.customer-info-holder").show();
        }
        if (checked && $(this).val() == 'fcECard') {
            $(".btn-process-payment").hide();
            $("span.info-scan-fccard").addClass('d-block-f');
        }
    });

    // Bind have customer info checkbox
    $(document).on("change", "input[type='checkbox'][name='have_customer_info']", function(e) {
        if ($(this).prop("checked")) {
            $("tr.section_set_customer_info_for_invoice").show();
        } else {
            $("tr.section_set_customer_info_for_invoice").hide();
        }
    });

    // Bind add extra field for set customer info for bill section
    $(document).on("click", "button.btn-add-cust-info-for-bill", function() {
        var billCustInfoExtraFieldLine = $("table.table-set-customer-extra-field-info tr:last").clone();
        $(billCustInfoExtraFieldLine).find("input").val("");
        var removeLineBtn = '<button class="btn-solid btn-adjust-cust-info-for-bill btn-remove-cust-info-for-bill">-</button>';
        $(billCustInfoExtraFieldLine).find("td.actions").find(".btn-add-cust-info-for-bill").remove();
        $(billCustInfoExtraFieldLine).find("td.actions").find(".btn-remove-cust-info-for-bill").remove();
        $(billCustInfoExtraFieldLine).find("td.actions").append(removeLineBtn);
        $("table.table-set-customer-extra-field-info").append(billCustInfoExtraFieldLine);
    });

    // Bind remove extra field for set customer info for bill section
    $(document).on("click", "button.btn-remove-cust-info-for-bill", function() {
        $(this).closest('tr').remove();
    });

    // Bind shortcuts
    document.onkeyup = function(e) {
        e.preventDefault();

        if (e.which == 27) { // Hide popups on ESC key pressed
            hideModal();
        }
        else if (e.shiftKey && e.which == 70) { // Trigger search
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                $(".btn-search").trigger("click");
            }
        }
        else if (e.shiftKey && e.which == 67) { // Trigger clear cart
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                if ($(".checkout-layout").css("display") != "none" && LocalDB.getActiveTable() > 0) {
                    $(".btn-empty-cart").trigger("click");
                }
            }
        }
        else if (e.shiftKey && e.which == 72) { // Trigger checkout
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                if ($(".checkout-layout").css("display") != "none" && LocalDB.getActiveTable() > 0) {
                    $(".btn-checkout").trigger("click");
                }
            }
        }
        else if (e.shiftKey && e.which == 84) { // Load table view
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                if ($(".table-layout").css("display") != "block") {
                    $(".btn-app-home").trigger("click");
                }
            }
        }
        else if (e.shiftKey && e.which == 80) { // Print invoice
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                if (withTableAccess()) {
                    if ($("#modal-container").css("visibility") != "hidden" && $("#modal-invoice").css("display") != "none" && $("button.btn-print-invoice").css("display") != "none" && LocalDB.getActiveTable() > 0) {
                        $(".btn-print-invoice").trigger("click");
                    } else {
                        $("button.btn-print-any-bill").trigger("click");    
                    }
                } else if (withAccess("office")) {
                    $("button.btn-print-any-bill").trigger("click");
                }
            }
        }
        else if (e.shiftKey && e.which == 83) { // Load settings view
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                $(".btn-app-configuration").trigger("click");
            }
        }
        else if (e.shiftKey && e.which == 85) { // Trigger empty order 
            if (isFocusOnField((e.target)) && 1 == 2) {
                return false;
            } else {
                if (withAccess("office")) {
                    if ($("#modal-manage-order").css("display") != "none" && $("#modal-container").css("visibility") != "hidden") {
                        var orderId = toNumeric($("table.order-header-info span.manage-order-id").html());
                        var isLive = $("#modal-manage-order button.btn-save-office-order:first").attr("data-islive");
                        if (orderId > 0) {
                            var allowToDelete = false;
                            if (isLive == "false") {
                                var orderInfo = LocalDB.getOrderInfo(orderId);
                                if (typeof orderInfo != 'undefined' && typeof orderInfo.is_bill_given != 'undefined' && orderInfo.is_bill_given == '0') {
                                    allowToDelete = true;
                                } else {
                                    allowToDelete = false;
                                }
                            } else if (isLive == "true") {
                                // allowToDelete = false;
                                if ($("#modal-manage-order input[type='hidden'][name='g2c']").val() == 0) {
                                    allowToDelete = true;
                                } else {
                                    allowToDelete = false;
                                }
                            }
                            
                            if (allowToDelete) {
                                if (confirm("Are you sure want to perform this action?")) {
                                    deleteOrderFromCloud(orderId);
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (e.shiftKey && e.which == 90) { // Trigger restore order 
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                if (withAccess("office")) {
                    if ($("#modal-manage-order").css("display") != "none" && $("#modal-container").css("visibility") != "hidden") {
                        var orderId = toNumeric($("table.order-header-info span.manage-order-id").html());
                        var isLive = $("#modal-manage-order button.btn-save-office-order:first").attr("data-islive");
                        if (orderId > 0) {
                            // restoreOrderToCloud(orderId);
                        }
                    }
                }
            }
        }
        else if (e.shiftKey && e.which == 88) { // Trigger toggle office lock on order
            if (isFocusOnField((e.target)) && 1 == 2) {
                return false;
            } else {
                if (withAccess("office")) {
                    if ($("#modal-manage-order").css("display") != "none" && $("#modal-container").css("visibility") != "hidden") {
                        var orderId = toNumeric($("table.order-header-info span.manage-order-id").html());
                        var isLive = $("#modal-manage-order button.btn-save-office-order:first").attr("data-islive");
                        if (orderId > 0) {
                            toggleOrderOfficeLock(orderId);
                        }
                    }
                }
            }
        }
        else if (e.shiftKey && e.which == 73) { // Trigger show help/tips
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                $(".btn-app-help-tips").trigger("click");
            }
        }
        else if (e.shiftKey && e.which == 78) { // Trigger add new bone
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                $(".btn-add-bone").trigger("click");
            }
        }
        else if (e.shiftKey && e.which == 68) { // Trigger show statistics
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                $(".btn-show-stats").trigger("click");
            }
        }
        else if (e.shiftKey && e.which == 76) { // Trigger logout
            if (isFocusOnField((e.target))) {
                return false;
            } else {
                $(".btn-app-logout").trigger("click");
            }
        }
    };

    // Bind process payment button
    $(document).on("click", ".btn-process-payment", function() {
        var paymentMethodName = $("input[type='checkbox'].checkout_options:checked").val();
        var paymentMethod = RESTAURANT.PAYMENT_METHODS.find(payment_method => payment_method.payment_method === paymentMethodName.toString())
        if (paymentMethod.payment_method == '' || $.inArray(paymentMethod.payment_method, ALLOWED_PAYMENT_METHODS) == -1) {
            showNotification("Only "+ ALLOWED_PAYMENT_METHODS.join(', ') + " payment methods are allowed", "error");
            return false;
        }

        PAYMENT_IN_PROCESS = true;
        $(this).attr("disabled", "disabled");
        $(this).html(getLoaderImg());

        var orderInfo = LocalDB.generateOrder(paymentMethod);
        switch(paymentMethod.payment_method) {
            case 'cash':
                saveOrder(orderInfo);
                break;
            case 'upi':
                saveOrder(orderInfo, {
                    'upi' : true
                });
                break;
            case 'online_payment':
                saveOrder(orderInfo, {
                    'onlinePayment' : true
                });
                break;
            case 'payment_link':
                validateCustInfoAndSendPaymentLink(orderInfo);
                break;
            case 'fcECard':
                break;
            default:
                break;
        }
    });

    // Bind print invoice button
    $(document).on("click", ".btn-print-invoice", function() {
        printInvoice();
    });

    // Bind logout button
    $(document).on("click", ".btn-app-logout", function() {
        logout();
    });

    // Bind search button
    $(document).on("click", ".btn-search", function(e) {
        e.preventDefault();
        var allowSearch = false;
        if (withAccess("office")) {
            $("table.table-office").each(function() {
                if (!allowSearch && $(this).css("display") != "none" && !$(this).hasClass("table-orders-report-office")) {
                    allowSearch = true;
                }
            });
        } else if (withTableAccess()) {
            allowSearch = true;
        }
        if (allowSearch) {
            showSearchOption();
        }
    });

    // Bind search field
    $(document).on("input", "input[name='field-search']", function(e) {
        var searchPhase = $(this).val(); 
        showSearchSuggestions(searchPhase);
    });

    // Navigation on search results
    $(document).on("keydown", "input[name='field-search']", function(e) {
        switch(e.keyCode) {
            case 38:
                e.preventDefault();
                navigateSearchResult('up');
                break;
            case 40:
                e.preventDefault();
                navigateSearchResult('down');
                break;
            case 13:
                e.preventDefault();
                $(".search-result-line.active:first").trigger("click");
                break;
            default:
                break;
        }
    });

    // Bind search result area category line click
    $(document).on("click", ".search-result-line-area-category", function() {
        var areaCategoryId = $(this).attr("data-searchareacategoryid");
        showTableLayout();
        $("button.btn-table-type[data-areacategory=" + areaCategoryId + "]").trigger('click');
        hideModalByForce();
    });

    // Bind search result table line click
    $(document).on("click", ".search-result-line-table", function() {
        var tableId = $(this).attr("data-searchtableid");
        $("td.table-block[data-tableid=" + tableId + "]").trigger('click');
        hideModalByForce();
    });

    // Bind search result category line click
    $(document).on("click", ".search-result-line-category", function() {
        var categoryId = $(this).attr("data-searchcategoryid");
        $("li.category[data-id=" + categoryId + "]").trigger('click');
        hideModalByForce();
        showProductsLayout();
    });

    // Bind search result item line click
    $(document).on("click", ".search-result-line-item", function() {
        var categoryId = $(this).attr("data-searchitemcategoryid");
        var itemId = $(this).attr("data-searchitemid");
        $("li.category[data-id=" + categoryId + "]").trigger('click');
        hideModalByForce();
        askQuantityForItem(itemId);
        showProductsLayout();
        $(".btn-shopping-cart").trigger("click");
    });

    // Bind search result order line click
    $(document).on("click", ".search-result-line-order", function() {
        var orderId = $(this).attr("data-searchorderid");
        var order = LocalDB.getOrderInfo(toNumeric(orderId));
        if (typeof order != 'undefined') {
            var orders = LocalDB.searchOrders(order.order_no);
            loadOrdersPerPage(0, (NUMBER_OF_ITEM_PER_PAGE - 1), 1, orders);
            $("table.pagination").remove();
        }
        hideModalByForce();
    });

    // Bind search result office table line click
    $(document).on("click", ".search-result-line-office-table", function() {
        var tableId = $(this).attr("data-searchtableid");
        var table = LocalDB.getTableInfo(toNumeric(tableId));
        if (typeof table != 'undefined') {
            loadTablesPerPage(0, (NUMBER_OF_ITEM_PER_PAGE - 1), 1, [table]);
            $("table.pagination").remove();
        }
        hideModalByForce();
    });

    // Bind search result office item line click
    $(document).on("click", ".search-result-line-office-item", function() {
        var itemId = $(this).attr("data-searchitemid");
        var item = LocalDB.getItemInfo(toNumeric(itemId));
        if (typeof item != 'undefined') {
            loadItemsPerPage(0, (NUMBER_OF_ITEM_PER_PAGE - 1), 1, [item]);
            $("table.pagination").remove();
        }
        hideModalByForce();
    });

    // Bind search result office category line click
    $(document).on("click", ".search-result-line-office-category", function() {
        var categoryId = $(this).attr("data-searchcategoryid");
        var category = LocalDB.getCategoryInfo(toNumeric(categoryId));
        if (typeof category != 'undefined') {
            loadCategoriesPerPage(0, (NUMBER_OF_ITEM_PER_PAGE - 1), 1, [category]);
            $("table.pagination").remove();
        }
        hideModalByForce();
    });

    // Bind search result office area category line click
    $(document).on("click", ".search-result-line-office-area-category", function() {
        var areaCategoryId = $(this).attr("data-searchareacategoryid");
        var areaCategory = LocalDB.getAreaCategoryInfo(toNumeric(areaCategoryId));
        if (typeof areaCategory != 'undefined') {
            loadAreaCategoriesPerPage(0, (NUMBER_OF_ITEM_PER_PAGE - 1), 1, [areaCategory]);
            $("table.pagination").remove();
        }
        hideModalByForce();
    });

    // Bind search result line hover
    $(document).on("mouseenter", ".search-result-line", function() {
        $(".search-result-line").each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });

    // Bind print invoice/bill button click event
    $(document).on("click", "button.btn-print-any-bill", function() {
        askFieldsForPrintOrder();
    });

    // Bind print qr code for restaurant menu button click event
    $(document).on("click", "button.btn-proceed-menu-qr", function() {
        $("#modal-print-rest-menu-qr").css("min-width", "300px");
        var options = "<option value='all' selected>All</option>";
        $(LocalDB.areaCategories()).each(function(ack, areaCategory) {
            options+= "<option value='"+ areaCategory.id +"'>"+ areaCategory.name +"</option>";
        });
        $("select[name='fieldRestMenuQRFor']").html(options);
        $("tr.restMenuQRCodeFieldBlock").show();
        $("tr.restMenuQRCodeImgBlock").hide();
        popupModal("#modal-print-rest-menu-qr");
    });

    // Bind generate qr code for restaurant menu button click event
    $(document).on("click", "button.btn-generate-menu-qr", function() {
        let fieldRestMenuQRFor = $("#modal-print-rest-menu-qr select[name='fieldRestMenuQRFor']").val().toString().trim();
        let fieldRestMenuQRSize = $("#modal-print-rest-menu-qr select[name='fieldRestMenuQRSize']").val().toString().trim();
        let fieldRestMenuQRText = $("#modal-print-rest-menu-qr input[name='fieldRestMenuQRText']").val().toString().trim();
        if (fieldRestMenuQRText != '' && fieldRestMenuQRText.length > 18) {
            showNotification("QR Code custom text must not be greater than 18 characters", "error");
            return false;
        }
        generateMenuQR({
            'fieldRestMenuQRFor' : fieldRestMenuQRFor,
            'fieldRestMenuQRSize' : fieldRestMenuQRSize,
            'fieldRestMenuQRText' : fieldRestMenuQRText
        });
    });

    // Bind print restaurant qr code button click event
    $(document).on("click", "button.btn-print-rest-menu-qr", function() {
        printRestQR();
    });

    // Bind submit event of find order form
    $(document).on("click", "button.btn-print-this-order", function() {
        var field_order_book_year = $("table.table-print-any-order-fields select[name='field_order_book_year']").val();
        var field_order_number = $("table.table-print-any-order-fields input[name='field_order_number']").val();
        field_order_book_year = field_order_book_year.toString().trim();
        field_order_number = field_order_number.toString().trim();
        var isError = false;
        if (field_order_book_year == "") {
            showNotification("Please select order book year", "error");
            isError = true;
        }
        if (field_order_number == "") {
            showNotification("Please enter valid order number", "error");
            isError = true;
        }
        if (isError) {
            return false;
        }
        findOrderByNumber(field_order_book_year, field_order_number);
    });

    // Bind proceed fc eCard button
    $(document).on("click", ".btn-proceed-fc-eCard", function() {
        askFCECardFields();
    });

    // Bind generate fc eCard button
    $(document).on("click", ".btn-generate-fc-eCard", function() {
        var field_fcECardDisplayName = $("table.table-fcECard-form-fields input[name='field_fcECardDisplayName']").val();
        field_fcECardDisplayName = field_fcECardDisplayName.toString().split(' ').join('').toUpperCase();
        $("table.table-fcECard-form-fields input[name='field_fcECardDisplayName']").val(field_fcECardDisplayName);
        var field_fcECardPhoneNumber = $("table.table-fcECard-form-fields input[name='field_fcECardPhoneNumber']").val();
        var field_fcECardEmail = $("table.table-fcECard-form-fields input[name='field_fcECardEmail']").val();
        var field_fcECardInitRecharge = $("table.table-fcECard-form-fields input[name='field_fcECardInitRecharge']").val();
        var field_fcECardEnablePaymentVerification = $("table.table-fcECard-form-fields input[type='checkbox'][name='field_fcECardEnableVerification']").prop("checked");
        var isError = false;
        if (field_fcECardDisplayName != '' && field_fcECardDisplayName.length > 8) {
            showNotification("Display name on card can not be longer than 8 characters", "error");
            isError = true;
        }
        if (field_fcECardPhoneNumber.toString().trim() == '' && field_fcECardEmail.toString().trim() == '') {
            showNotification("Please add either phone number or email for FC eCard", "error");
            isError = true;
        }
        if (field_fcECardEmail.toString().trim() != '' && !isValidEmail(field_fcECardEmail)) {
            showNotification("Please enter valid email for FC eCard", "error");
            isError = true;
        }
        if (field_fcECardInitRecharge.toString() == '' || isNaN(field_fcECardInitRecharge) || field_fcECardInitRecharge <= 0) {
            showNotification("Please fill initial recharge for the card", "error");
            isError = true;   
        }
        if (isError) {
            return false;
        }
        generateAndshowFCeCard(field_fcECardDisplayName, field_fcECardPhoneNumber, field_fcECardEmail, field_fcECardInitRecharge, field_fcECardEnablePaymentVerification);
    });    

    // Bind print fc-eCard button
    $(document).on("click", ".btn-print-fc-eCard", function() {
        printFCeCard();
    });

    // Bind recharge fc eCard button
    $(document).on("click", ".btn-proceed-recharge-fc-eCard", function() {
        askFCECardFieldsForRecharge();
        if (typeof $(this).attr("data-fc-number") != "undefined" && $(this).attr("data-fc-number") != "") {
            $("#modal-recharge-fc-eCard input[name='field_fcECardNumber']").val($(this).attr("data-fc-number"));
        }
    });

    // Bind proceed for FC eCard recharge
    $(document).on("click", "button.btn-recharge-fc-eCard", function() {
        var field_fcECardNumber = $("#modal-recharge-fc-eCard input[type='text'][name='field_fcECardNumber']").val();
        var field_fcECardRechargeAmount = $("#modal-recharge-fc-eCard input[type='text'][name='field_fcECardRechargeAmount']").val();
        var isError = false;
        if (field_fcECardNumber.toString().trim() == '') {
            showNotification("FC eCard number can not be blank", "error");
            isError = true;
        }
        if (field_fcECardRechargeAmount.toString().trim() == '' || isNaN(field_fcECardRechargeAmount) || field_fcECardRechargeAmount <= 0) {
            showNotification("Please fill valid recharge amount for FC eCard", "error");
            isError = true;
        }
        if (isError) {
            return false;
        }
        rechargeFCECard(field_fcECardNumber, field_fcECardRechargeAmount);
    });

    // Bind proceed to view fc eCard button
    $(document).on("click", ".btn-proceed-view-fc-eCard", function() {
        askFCECardFieldsForView();
    });

    // Bind view fc eCard button click event
    $(document).on("click", "button.btn-view-fc-eCard", function() {
        var field_fcECardNumber = $("#modal-view-fc-eCard input[type='text'][name='field_fcECardNumber_forView']").val();
        var isError = false;
        if (field_fcECardNumber.toString().trim() == '') {
            showNotification("FC eCard number can not be blank", "error");
            isError = true;
        }
        if (isError) {
            return false;
        }
        showInfoForFCECard(field_fcECardNumber);
    });

    // Bind button change fc eCard status click event
    $(document).on("click", "button.btn-change-fc-ecard-status", function() {
        var field_fcECardNumber = $(this).attr("data-fc-ecard-number");
        var field_fcECardStatus = $(this).attr("data-status");
        var isError = false;
        if (field_fcECardNumber == "" || field_fcECardStatus == "" || (field_fcECardStatus != "active" && field_fcECardStatus != "inactive")) {
            return false;
        }
        setFCECardStatus(field_fcECardNumber, field_fcECardStatus);
    });

    // Bind button change fc eCard payment verification flag click event
    $(document).on("click", "button.btn-change-fc-ecard-payment-verification-flag", function() {
        var field_fcECardNumber = $(this).attr("data-fc-ecard-number");
        var field_fcECardPaymentVerificationStatus = $(this).attr("data-status");
        var isError = false;
        if (field_fcECardNumber == "" || field_fcECardPaymentVerificationStatus == "" || (field_fcECardPaymentVerificationStatus != "1" && field_fcECardPaymentVerificationStatus != "0")) {
            return false;
        }
        setFCECardPaymentVerificationStatus(field_fcECardNumber, field_fcECardPaymentVerificationStatus);
    });

    // Bind button search options for fc cards report for office
    $(document).on("click", "button.btn-filter-fc-ecard-report", function() {
        popupModal("#modal-search-fc-ecards");
    });

    // Bind button submit of search form for fc ecards report
    $(document).on("click", "button.btn-fc-ecard-search-report", function() {
        let modal = "#modal-search-fc-ecards";
        var FCECardNumber = $(modal + " input[name='search_fc_field_fcNumber']").val().toString().trim();
        var FCECardName = $(modal + " input[name='search_fc_field_fcECardName']").val().toString().trim();
        var FCECardLinkedPhoneNo = $(modal + " input[name='search_fc_field_fcECardLinkedPhone']").val().toString().trim();
        var FCECardLinkedEmail = $(modal + " input[name='search_fc_field_fcECardLinkedEmail']").val().toString().trim();
        var FCECardCreatedOnFrom = $(modal + " input[name='search_fc_field_fcECardCreatedOn_from']").val().toString().trim();
        var FCECardCreatedOnTo = $(modal + " input[name='search_fc_field_fcECardCreatedOn_to']").val().toString().trim();
        if (FCECardNumber == "" && FCECardName == "" && FCECardLinkedPhoneNo == "" && FCECardLinkedEmail == "" && FCECardCreatedOnFrom == "" && FCECardCreatedOnTo == "") {
            return false;
        }
        var fcSearchParams = {
            'FCECardNumber' : FCECardNumber,
            'FCECardName' : FCECardName,
            'FCECardLinkedPhoneNo' : FCECardLinkedPhoneNo,
            'FCECardLinkedEmail' : FCECardLinkedEmail,
            'FCECardCreatedOnFrom' : FCECardCreatedOnFrom,
            'FCECardCreatedOnTo' : FCECardCreatedOnTo,
        };
        loadFCECardReportForOffice(fcSearchParams);
    });

    // Bind click event for view transactions of fc ecart
    $(document).on("click", "button.btn-view-fc-ecard-transactions", function() {
        var fcECardNumber = $(this).attr("data-fc-number");
        if (fcECardNumber == "") {
            showNotification("Invalid FC ECard number", "error");
            return false;
        }
        popupModal("#modal-view-fc-ecard-transactions");
        getFCECardTransactions(fcECardNumber);
    });

    // Bind click event for view recharges of fc ecart
    $(document).on("click", "button.btn-report-today-fc-eCard-recharge", function() {
        popupModal("#modal-report-fc-ecard-recharge");
        getFCECardRecharges({
            'todayOnly' : true
        });
    });

    // Bind view order link by order no
    $(document).on("click", ".order-no-view-link", function() {
        let orderNo = $(this).attr("data-orderno");
        if (isNaN(orderNo) || orderNo == "") {
            return false;
        }
        findOrderByNumber(orderNo);
        $("#modal-view-fc-ecard-transactions").css("display", "block");
    });

    // Bind onscan process
    onScan.attachTo(document);
    document.addEventListener('scan', function(scanInfo) {
        if($("input[type='checkbox'].checkout_options:checked").val() == 'fcECard') {
            var FCECardNumber  = (typeof scanInfo != 'undefined' && typeof scanInfo.detail != 'undefined' && typeof scanInfo.detail.scanCode != 'undefined' && scanInfo.detail.scanCode != '') ? scanInfo.detail.scanCode : '';
            if (isFCECardNumberValid(FCECardNumber)) {
                var paymentMethod = RESTAURANT.PAYMENT_METHODS.find(payment_method => payment_method.payment_method === "fcECard");
                var orderInfo = LocalDB.generateOrder(paymentMethod);
                saveOrder(orderInfo, {
                    'FCECard' : true,
                    'FCECardNumber' : FCECardNumber
                });
            } else {
                showNotification("FC eCard is not valid", "error");
            }
        }
    });

    // Bind remove log button in kitchen view
    $(document).on("click", ".btn-kitchen-log-action.remove", function() {
        var logId = $(this).attr("data-logid");
        LocalDB.removeKitchenLog(logId);
        renderKitchenLog();
    });

    // Bind remove item/mark as served button in kitchen view
    $(document).on("click", ".btn-kitchen-log-action.remove-item", function() {
        var itemInfo = {
            logId : toNumeric($(this).attr("data-logid")),
            tableId : toNumeric($(this).attr("data-tableid")),
            itemId : toNumeric($(this).attr("data-itemid")),
            qty : toNumeric($(this).attr("data-qty")),
            taste : $(this).attr("data-taste"),
            extraSpecification : $(this).attr("data-extraspecification"),
        };
        LocalDB.removeKitchenItemLog(itemInfo, itemInfo.taste.toString(), itemInfo.extraSpecification.toString());
        renderKitchenLog();
    });

    // Bind specification checkbox for an item
    $(document).on("change", "input[type='checkbox'][name='has_specifications']", function() {
        if ($(this).prop('checked')) {
            $("tr.specification_options").show();
        } else {
            $("tr.specification_options").hide();
        }
    });

    // Bind taste specifications checkbox options for an item
    $(document).on("change", "input[type='checkbox'][name='spefications_taste']", function() {
        $("input[type='checkbox'].spefications_taste").each(function() {
            $(this).prop("checked", false);
        });
        $(this).prop("checked", true);
    });

    // Bind kitchen item block click event
    $(document).on("click", "td.block-kitchen-log-item", function() {
        var itemId = $(this).attr("data-itemid");
        var filterBy = {
            'itemId' : toNumeric(itemId),
        };
        renderKitchenLog(filterBy);
    });

    // Bind kitchen log show all button click event
    $(document).on("click", "button.btn-reset-kitchen-filters", function() {
        renderKitchenLog();
    });

    // Bind add new category/item button click event
    $(document).on("click", ".btn-add-bone", function() {
        showAddNewBoneOption();
        var activeCategory = 0;
        if ($("li.category.active:first").length > 0) {
            activeCategory = toNumeric($("li.category.active:first").attr("data-id"));
        }
        $(".form tr.field-item select#field-item-category").html(getCategoriesSelectOptions());
        if (activeCategory > 0) {
            $(".form tr.field-item select#field-item-category").val(activeCategory);
        }
    });

    // Bind add bone tab header click event
    $(document).on("click", "td.add-bone-tab", function() {
        var bone = $(this).attr("data-bone");
        if (bone != "") {
            $("td.add-bone-tab").removeClass("selected");
            $(this).addClass("selected");
            $("table.form").each(function() {
                if ($(this).hasClass("bone-tab")) {
                    $(this).hide();
                }
            });
            $("table.form.form-" + bone).show();
            $("table.form.form-" + bone + " input[type='text']:first").trigger("focus");
        }
    });

    // Bind save category button in add bone modal
    $(document).on("click", "button.btn-add-category-bone", function() {
        var categoryName = $(".form.form-category input[name='field-category-name']").val();
        var categoryDescription = $(".form.form-category textarea[name='field-category-description']").val();
        if (categoryName == "") {
            showNotification("Please provide category name!", "error");
            return false;
        }
        var category = {
            'name' : categoryName,
            'description' : categoryDescription
        };
        saveCategoryToCloud(category);
    });

    // Bind save item button in add bone modal
    $(document).on("click", "button.btn-add-item-bone", function() {
        var itemName = $(".form.form-item input[name='field-item-name']").val();
        var itemCode = $(".form.form-item input[name='field-item-code']").val();
        var itemCatId = $(".form.form-item select[name='field-item-category']").val();
        var itemDesc = $(".form.form-item textarea[name='field-item-description']").val();
        var isError = false;
        if (itemName == "") {
            showNotification("Please provide an item name!", "error");
            isError = true;
        }
        if (itemCatId == "") {
            showNotification("Please select an item category!", "error");
            isError = true;
        }
        if (isError) {
            return !isError;
        }
        var item = {
            'name' : itemName,
            'code' : itemCode,
            'catId' : itemCatId,
            'description' : itemDesc
        };
        saveItemToCloud(item);
    });

    // Bind show statistics button click event
    $(document).on("click", ".btn-show-stats", function() {
        if (withAccess("office")) {
            loadStatistics();
        }
    });

    // Bind modal close button click event
    $(document).on("click", "button.btn-close-modal", function() {
        hideModalByForce();
    });

    // Bind modal close button click event
    $(document).on("click", "button.btn-refresh-kitchen-log", function() {
        renderLiveKitchenLog();
    });

    // Bind confirm button click event
    $(document).on("click", "a.btn-confirm", function(e) {
        e.preventDefault();
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
    });

    // Submit on enter for modal-manage-office modal
    $(document).on("keydown", ".modal-manage-office input", function(e) {
        if (e.which == 13) {
            $(this).parent().parent().parent().find("button.btn-office-save:first").trigger("click");
            return false;
        }
    });

    // Submit on enter for custom search options modal
    $(document).on("keydown", "table.report-search-options.custom-search input[type='text']", function(e) {
        if (e.which == 13) {
            $("table.report-search-options.custom-search").find("button.btn-office-search-for-report").trigger("click");
            return false;
        } 
    })

    // Submit on enter for modal-search-office modal
    $(document).on("keydown", ".modal-search-office input", function(e) {
        if (e.which == 13) {
            $(this).parent().parent().parent().find("button.btn-office-search:first").trigger("click");
            return false;
        }
    });

    // Submit on enter for print any order/bill section
    $(document).on("keydown", "table.table-print-any-order-fields input[type='text']", function(e) {
        if (e.which == 13) {
            $("table.table-print-any-order-fields").find("button.btn-print-this-order").trigger("click");
            return false;
        }
    });

    // Submit on enter for profile setting save section
    $(document).on("keydown", "table.table-profile-settings-office input[type='text']", function(e) {
        if (e.which == 13) {
            $("table.table-profile-settings-office").find("button.btn-office-save-profile").trigger("click");
            return false;
        }
    });

    // Submit on enter for generate fc-ecard section
    $(document).on("keydown", "table.table-fcECard-form-fields input[type='text']", function(e) {
        if (e.which == 13) {
            $("table.table-fcECard-form-fields").find("button.btn-generate-fc-eCard").trigger("click");
            return false;
        }
    });

    // Submit on enter for recharge fc-ecard section
    $(document).on("keydown", "table.table-fcECard-recharge-form-fields input[type='text']", function(e) {
        if (e.which == 13) {
            $("table.table-fcECard-recharge-form-fields").find("button.btn-recharge-fc-eCard").trigger("click");
            return false;
        }
    });

    // Submit on enter for view fc-ecard section
    $(document).on("keydown", "table.table-fcECard-view-form-fields input[type='text']", function(e) {
        if (e.which == 13) {
            $("table.table-fcECard-view-form-fields").find("button.btn-view-fc-eCard").trigger("click");
            return false;
        }
    });

    // Bind add area category button click event
    $(document).on("click", ".btn-office-add-area-category", function(e) {
        $("#modal-manage-area-category input[name='field_area_category_name']").val('');
        $("#modal-manage-area-category input[name='field_area_category_id']").val(0);
        popupModal("#modal-manage-area-category");
    });

    // Bind edit area category button click event
    $(document).on("click", ".btn-edit-area-category", function(e) {
        var areaCategoryId = $(this).attr("data-areacategoryid");
        var areaCategoryInfo = LocalDB.getAreaCategoryInfo(toNumeric(areaCategoryId));
        $("#modal-manage-area-category input[name='field_area_category_name']").val(areaCategoryInfo.name);
        $("#modal-manage-area-category input[name='field_area_category_id']").val(areaCategoryId);
        popupModal("#modal-manage-area-category");
    });

    // Bind delete area category button click event
    $(document).on("click", ".btn-delete-area-category", function(e) {
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
        var areaCategoryId = $(this).attr("data-areacategoryid");
        var areaCategoryInfo = LocalDB.getAreaCategoryInfo(toNumeric(areaCategoryId));
        if (typeof areaCategoryInfo != 'undefined') {
            deleteAreaCategoryToCloud(areaCategoryInfo.id);
        }
    });

    // Bind save table button click event
    $(document).on("click", "button.btn-office-save-area-category", function() {
        var areaCategoryName = $("#modal-manage-area-category input[name='field_area_category_name']").val();
        var areaCategoryId = $("#modal-manage-area-category input[name='field_area_category_id']").val();
        var isError = false;
        if (areaCategoryName == "") {
            showNotification("Please provide area category name!", "error");
            isError = true;
        }
        if (isError) {
            return !isError;
        }
        var areaCategory = {
            'name' : areaCategoryName,
            'id' : areaCategoryId
        };
        saveAreaCategoryToCloud(areaCategory);
    });

    // Bind add table button click event
    $(document).on("click", ".btn-office-add-table", function(e) {
        $("#modal-manage-table input[name='field_table_no']").val('');
        $("#modal-manage-table input[name='field_table_alias']").val('');
        $("#modal-manage-table select#field_table_area_category_id").html(getAreaCategoriesSelectOptions());
        $("#modal-manage-table input[name='field_item_id']").val(0);
        popupModal("#modal-manage-table");
    });

    // Bind edit table button click event
    $(document).on("click", ".btn-edit-table", function(e) {
        var tableId = $(this).attr("data-tableid");
        var tableInfo = LocalDB.getTableInfo(toNumeric(tableId));
        var tableAreaCategory = LocalDB.getAreaCategoryInfo(toNumeric(tableInfo.area_category_id));
        $("#modal-manage-table input[name='field_table_no']").val(tableInfo.table_no);
        $("#modal-manage-table input[name='field_table_alias']").val(tableInfo.table_alias);
        $("#modal-manage-table select#field_table_area_category_id").html(getAreaCategoriesSelectOptions());
        $("#modal-manage-table select#field_table_area_category_id").val(tableInfo.area_category_id);
        $("#modal-manage-table input[name='field_table_id']").val(tableId);
        popupModal("#modal-manage-table");
    });

    // Bind delete table button click event
    $(document).on("click", ".btn-delete-table", function(e) {
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
        var tableId = $(this).attr("data-tableid");
        var tableInfo = LocalDB.getTableInfo(toNumeric(tableId));
        if (typeof tableInfo != 'undefined') {
            deleteTableToCloud(tableInfo.id);
        }
    });

    // Bind save table button click event
    $(document).on("click", "button.btn-office-save-table", function() {
        var tableNo = $("#modal-manage-table input[name='field_table_no']").val();
        var tableAlias = $("#modal-manage-table input[name='field_table_alias']").val();
        var tableAreaCategoryId = $("#modal-manage-table select#field_table_area_category_id").val();
        var tableId = $("#modal-manage-table input[name='field_table_id']").val();
        var isError = false;
        if (tableNo == "") {
            showNotification("Please provide table number!", "error");
            isError = true;
        }
        if (tableAreaCategoryId == "") {
            showNotification("Please select table area category!", "error");
            isError = true;
        }
        if (isError) {
            return !isError;
        }
        var table = {
            'table_no' : tableNo,
            'table_alias' : tableAlias,
            'area_category_id' : tableAreaCategoryId,
            'id' : tableId
        };
        saveTableToCloud(table);
    });

    // Bind add item button click event
    $(document).on("click", ".btn-office-add-item", function(e) {
        // $("#modal-manage-item select#field_office_item_category").html(getCategoriesSelectOptions());
        $("#modal-manage-item textarea[name='field_office_item_description']").val('');
        $("#modal-manage-item input[name='field_office_item_name']").val('');
        $("#modal-manage-item input[name='field_office_item_code']").val('');
        $("#modal-manage-item input[name='field_office_item_price']").val('');
        $("#modal-manage-item input[name='field_item_id']").val(0);
        popupModal("#modal-manage-item");
    });

    // Bind edit item button click event
    $(document).on("click", ".btn-edit-item", function(e) {
        var itemId = $(this).attr("data-itemid");
        var itemInfo = LocalDB.getItemInfo(toNumeric(itemId));
        var itemCategory = LocalDB.getCategoryInfo(itemInfo.category_id);
        let itemPrice = LocalDB.getItemPriceByArea(itemInfo.id, LocalDB.getTableInfo(LocalDB.getActiveTable()).area_category_id);
        $("#modal-manage-item select#field_office_item_category").html(getCategoriesSelectOptions());
        $("#modal-manage-item textarea[name='field_office_item_description']").val(itemInfo.description);
        $("#modal-manage-item input[name='field_office_item_name']").val(itemInfo.name);
        $("#modal-manage-item input[name='field_office_item_code']").val(itemInfo.item_code);
        $("#modal-manage-item input[name='field_office_item_price']").val(itemPrice);
        $("#modal-manage-item select#field_office_item_category").val(itemCategory.id);
        $("#modal-manage-item input[name='field_item_id']").val(itemId);
        popupModal("#modal-manage-item");
    });

    // Bind delete item button click event
    $(document).on("click", ".btn-delete-item", function(e) {
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
        var itemInfo = LocalDB.getItemInfo(toNumeric($(this).attr("data-itemid")));
        var categoryInfo = LocalDB.getCategoryInfo(toNumeric(itemInfo.category_id));
        if (typeof itemInfo != 'undefined' && typeof categoryInfo != 'undefined') {
            deleteItemToCloud(itemInfo.id, categoryInfo.id);
        }
    });

    $(document).on("click", "button.btn-office-save-item", function() {
        var itemDesc = $("#modal-manage-item textarea[name='field_office_item_description']").val();
        var itemName = $("#modal-manage-item input[name='field_office_item_name']").val();
        var itemCode = $("#modal-manage-item input[name='field_office_item_code']").val();
        var itemCatId = $("#modal-manage-item select#field_office_item_category").val();
        var itemId = $("#modal-manage-item input[name='field_item_id']").val();
        var isError = false;
        if (itemName == "") {
            showNotification("Please provide an item name!", "error");
            isError = true;
        }
        if (itemCatId == "") {
            showNotification("Please select an item category!", "error");
            isError = true;
        }
        if (isError) {
            return !isError;
        }
        var item = {
            'name' : itemName,
            'code' : itemCode,
            'catId' : itemCatId,
            'description' : itemDesc,
            'id' : itemId
        };
        saveItemToCloudForOffice(item);
    });

    // Bind set item price button click event
    $(document).on("click", ".btn-office-set-item-price-info", function() {
        var itemId = toNumeric($(this).attr("data-itemid"));
        $("#modal-manage-item-price-info input[name='field_item_id']").val(itemId);
        $("#modal-manage-item-price-info span.item-name").html(LocalDB.getItemInfo(itemId).name);
        $("table.form-item-prices tr.acat-price-line").remove();
        var itemPriceInfo = LocalDB.getItemInfo(itemId).item_price_info;
        if (typeof itemPriceInfo != 'undefined' && itemPriceInfo != null) {
            itemPriceInfo = JSON.parse(itemPriceInfo);
        } else {
            itemPriceInfo = [];
        }
        $(LocalDB.areaCategories()).each(function(ack, acat) {
            var itemPrice = 0;
            $(itemPriceInfo).each(function(ik, priceInfo) {
                if (priceInfo.acatId == acat.id) {
                    itemPrice = priceInfo.price;
                }
            });
            var acatItemPriceLine = "<tr class='acat-price-line'>";
            acatItemPriceLine+= "<td class='text-left text-sm field'>";
            acatItemPriceLine+= "<label class='d-block'>"+ acat.name.toString().toUpperCase() +" Price ("+ RESTAURANT.CURRENCY +")</label>";
            acatItemPriceLine+= "<input type='text' class='item_acat_price' size='40' placeholder='"+ acat.name.toString().toUpperCase() +" Price' data-acatid='"+ acat.id +"' data-itemid='"+ itemId +"' value='"+ itemPrice +"' />";
            acatItemPriceLine+= "</td>";
            acatItemPriceLine+= "</tr>";
            $("table.form-item-prices").append(acatItemPriceLine);
        });
        var acatItemPriceSaveBtn = "<tr class='acat-price-line'>";
        acatItemPriceSaveBtn+= "<td>";
        acatItemPriceSaveBtn+= "<button class='btn-solid btn-office-save btn-save-item-price-info' data-itemid="+itemId+">Save</button>";
        acatItemPriceSaveBtn+= "</td>";
        acatItemPriceSaveBtn+= "</tr>";
        $("table.form-item-prices").append(acatItemPriceSaveBtn);
        popupModal("#modal-manage-item-price-info");
        $("input[type='text'].item_acat_price:first").focusTextToEnd();
    });

    // Bind save item price button click event
    $(document).on("click", "button.btn-save-item-price-info", function() {
        var itemId = toNumeric($(this).attr("data-itemid"));
        var itemPriceInfo = [];
        var errors = [];
        $("input[type='text'].item_acat_price").each(function() {
            var acatId = toNumeric($(this).attr("data-acatid"));
            var itemPrice = toNumeric($(this).val());
            var acatName = LocalDB.getAreaCategoryInfo(acatId).name;
            if (isNaN(itemPrice) || itemPrice <= 0 || itemPrice == "") {
                errors.push("Please set valid price for " + acatName);
            }
            itemPriceInfo.push({
                acatId : toNumeric(acatId),
                price : toNumeric(itemPrice)
            });
        });
        if (errors.length > 0) {
            $(errors).each(function(ek, error) {
                showNotification(error, "error");
            });
            return false;
        } else {
            setItemPriceInfoToCloud(itemId, itemPriceInfo);
        }
    });

    // Bind add category button click event
    $(document).on("click", ".btn-office-add-category", function(e) {
        $("#modal-manage-category input[name='field_category_name']").val('');
        $("#modal-manage-category textarea[name='field_category_desc']").val('');
        $("#modal-manage-category input[name='field_category_id']").val(0);
        popupModal("#modal-manage-category");
    });

    // Bind edit category button click event
    $(document).on("click", ".btn-edit-category", function(e) {
        var categoryId = $(this).attr("data-categoryid");
        var categoryInfo = LocalDB.getCategoryInfo(toNumeric(categoryId));
        $("#modal-manage-category input[name='field_category_name']").val(categoryInfo.name);
        $("#modal-manage-category textarea[name='field_category_desc']").val(categoryInfo.description);
        $("#modal-manage-category input[name='field_category_id']").val(categoryId);
        popupModal("#modal-manage-category");
    });

    // Bind delete category button click event
    $(document).on("click", ".btn-delete-category", function(e) {
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
        var categoryId = $(this).attr("data-categoryid");
        deleteCategoryToCloud(categoryId);
    });

    // Bind save discount button click event
    $(document).on("click", "button.btn-office-save-category", function() {
        var categoryId = $("#modal-manage-category input[name='field_category_id']").val();
        var categoryName = $("#modal-manage-category input[name='field_category_name']").val();
        var categoryDesc = $("#modal-manage-category textarea[name='field_category_desc']").val();
        var isError = false;
        if (categoryName == '') {
            showNotification("Please provide category title", "error");
            isError = true;
        }
        if (isError) {
            return false;
        }
        var category = {
            'name' : categoryName,
            'description' : categoryDesc,
            'id' : toNumeric(categoryId),
        };
        saveCategoryToCloudForOffice(category);
    });

    // Bind add tax button click event
    $(document).on("click", ".btn-office-add-tax", function(e) {
        $("#modal-manage-tax input[name='field_tax_name']").val('');
        $("#modal-manage-tax input[name='field_tax_value']").val('');
        $("#modal-manage-tax select[name='field_tax_type']").val('percentage');
        $("#modal-manage-tax input[name='field_tax_id']").val(0);
        popupModal("#modal-manage-tax");
    });

    // Bind edit tax button click event
    $(document).on("click", ".btn-edit-tax", function(e) {
        var taxId = $(this).attr("data-taxid");
        var taxInfo = LocalDB.getTaxInfo(toNumeric(taxId));
        $("#modal-manage-tax input[name='field_tax_name']").val(taxInfo.name);
        $("#modal-manage-tax input[name='field_tax_value']").val(taxInfo.amount);
        $("#modal-manage-tax select[name='field_tax_type']").val(taxInfo.type);
        $("#modal-manage-tax select[name='field_tax_authority_type']").val(taxInfo.tax_type);
        $("#modal-manage-tax input[name='field_tax_id']").val(taxId);
        popupModal("#modal-manage-tax");
    });

    // Bind delete tax button click event
    $(document).on("click", ".btn-delete-tax", function(e) {
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
        var taxId = $(this).attr("data-taxid");
        deleteTaxToCloud(taxId);
    });

    // Bind save discount button click event
    $(document).on("click", "button.btn-office-save-tax", function() {
        var taxId = $("#modal-manage-tax input[name='field_tax_id']").val();
        var taxName = $("#modal-manage-tax input[name='field_tax_name']").val();
        var taxAmount = $("#modal-manage-tax input[name='field_tax_value']").val();
        var taxType = $("#modal-manage-tax select[name='field_tax_type']").val();
        var taxAuthorityType = $("#modal-manage-tax select[name='field_tax_authority_type']").val();
        var isError = false;
        if (taxName == '') {
            showNotification("Please provide tax info", "error");
            isError = true;
        }
        if (isNaN(taxAmount) || toNumeric(taxAmount) <=0) {
            showNotification("Please apply valid tax value", "error");
            isError = true;
        }
        if (isError) {
            return false;
        }
        if (taxAuthorityType.toString().trim() == '') {
            taxAuthorityType = 'CUSTOM';
        }
        var tax = {
            'amount' : taxAmount,
            'type' : taxType,
            'name' : taxName,
            'id' : toNumeric(taxId),
            'tax_type' : taxAuthorityType,
        };
        saveTaxToCloud(tax);
    });

    // Bind delete discount button click event
    $(document).on("click", ".btn-delete-discount", function(e) {
        if (!confirm("Are you sure want to perform this action?")) {
            return false;
        }
        var discountId = $(this).attr("data-discountid");
        deleteDiscountToCloud(discountId);
    });

    // Bind add discount button click event
    $(document).on("click", ".btn-office-add-discount", function(e) {
        $("#modal-manage-discount input[name='field_discount_desc']").val('');
        $("#modal-manage-discount input[name='field_discount_value']").val('');
        $("#modal-manage-discount select[name='field_discount_type']").val('percentage');
        $("#modal-manage-discount input[name='field_discount_minimum_amount']").val('');
        $("#modal-manage-discount input[name='field_discount_id']").val(0);
        popupModal("#modal-manage-discount");
    });

    // Bind edit discount button click event
    $(document).on("click", ".btn-edit-discount", function(e) {
        var discountId = $(this).attr("data-discountid");
        var discountInfo = LocalDB.getDiscountInfo(discountId);
        $("#modal-manage-discount input[name='field_discount_desc']").val(discountInfo.description);
        $("#modal-manage-discount input[name='field_discount_value']").val(discountInfo.amount);
        $("#modal-manage-discount select[name='field_discount_type']").val(discountInfo.type);
        $("#modal-manage-discount input[name='field_discount_minimum_amount']").val(discountInfo.minimum_amount);
        $("#modal-manage-discount input[name='field_discount_id']").val(discountId);
        popupModal("#modal-manage-discount");
    });

    // Bind save discount button click event
    $(document).on("click", "button.btn-office-save-discount", function() {
        var discountId = $("#modal-manage-discount input[name='field_discount_id']").val();
        var discountDesc = $("#modal-manage-discount input[name='field_discount_desc']").val();
        var discountAmount = $("#modal-manage-discount input[name='field_discount_value']").val();
        var discountType = $("#modal-manage-discount select[name='field_discount_type']").val();
        var discountMinAmount = $("#modal-manage-discount input[name='field_discount_minimum_amount']").val();
        var isError = false;
        if (discountDesc == '') {
            showNotification("Please provide discount info", "error");
            isError = true;
        }
        if (isNaN(discountAmount) || toNumeric(discountAmount) <=0) {
            showNotification("Please apply valid discount value", "error");
            isError = true;
        }
        if (discountMinAmount != '' && isNaN(discountMinAmount)) {
            showNotification("Please provide valid minimum_amount value", "error");
            isError = true;
        }
        if (isError) {
            return false;
        }
        var discount = {
            'amount' : discountAmount,
            'type' : discountType,
            'description' : discountDesc,
            'minimum_amount' : discountMinAmount,
            'id' : toNumeric(discountId),
        };
        saveDiscountToCloud(discount);
    });

    // Set page number active on click for pagination buttons
    $(document).on("click", "table.pagination td", function() {
        if ($(this).hasClass("navs")) {
            $("table.pagination td:not('navs')[data-page='"+$(this).attr("data-navpage")+"']").trigger("click");
        } else {
            var page = $(this).attr("data-page");
            $("table.pagination td").each(function() {
                $(this).removeClass("active");
            });
            $(this).addClass("active");
        }
    });

    // Bind search/filter orders button click event
    $(document).on("click", "button.btn-filter-orders", function() {
        popupModal("#modal-search-orders");
    });

    // Bind search/filter for order report button click event
    $(document).on("click", "button.btn-filter-orders-for-report", function() {
        popupModal("#modal-search-orders-for-report");
    });

    // Bind print today orders button click event
    $(document).on("click", "button.btn-print-today-orders", function() {
        printOrders({
            'todayOnly' : true
        });
    });

    // Bind search button submit/click event
    $(document).on("click", "button.btn-office-search", function() {
        var searchModule = $(this).attr("data-module");
        $("table.pagination").remove();
        hideModal();
        switch(searchModule) {
            case 'orders':
                submitOrderSearchForm();
                break;
            default:
                break;
        }
    });

    // Bind order manage click event on order link to manage order
    $(document).on("click", "td.office-order-link", function() {
        var orderId = $(this).attr("data-id");
        var orderInfo = LocalDB.getOrderInfo(toNumeric(orderId));
        $("table.order-header-info span.manage-order-id").html(orderId);
        $("table.order-header-info span.manage-order-number").html(orderInfo.order_no);
        let tableInfo = LocalDB.getTableInfo(orderInfo.table_id);
        let table_no = (typeof tableInfo != 'undefined') ? tableInfo.table_no : orderInfo.table_id;
        let table_area_category = (typeof LocalDB.getAreaCategoryInfo(tableInfo.area_category_id) != 'undefined') ? ' (' + LocalDB.getAreaCategoryInfo(tableInfo.area_category_id).name + ')' : '';
        $("table.order-header-info span.manage-order-table-no").html(table_no + table_area_category);
        $("table.order-header-info span.manage-order-payment-method").html(orderInfo.payment_method);
        $("table.order-header-info span.manage-order-date").html(orderInfo.created_at);
        setOrderBaseInfo(orderInfo.id, $(this).hasClass("bill_given"));
        popupModal("#modal-manage-order");
    });

    // Bind hit enter click event to submit the form for manage order popup
    $(document).on("keydown", "#modal-manage-order input[type='text']", function(e) {
        if (e.which == "13") {
            $("#modal-manage-order").find("button.btn-save-office-order").trigger("click");
        }
    });

    // Bind order manage click event on order link to manage live order
    $(document).on("click", "td.office-order-report-link", function() {
        var orderId = $(this).attr("data-id");
        setLiveOrderBaseInfo(orderId);
        popupModal("#modal-manage-order");
    });

    // Bind view extra information button click event
    $(document).on("click", "button.btn-view-order-extra-info", function() {
        var orderId = $(this).attr("data-order");
        popupModal("#modal-view-order-extra-info");
        setOrderExtraInfo(orderId);
    });

    // Bind flat type input change in manage order popup for office view
    $(document).on("input", "table.order-items-list input[type='text'].flat", function() {
        var filteredNumericValue = $(this).val().replace(/\D/g,'');
        $(this).val(filteredNumericValue);
        var fieldValue = $(this).val();
        if (isNaN(fieldValue)) {
            return false;
        }
        renderOrderStatsForOffice();
    });
    // Bind decimal type input change in manage order popup for office view
    $(document).on("input", "table.order-items-list input[type='text'].decimal", function() {
        var filteredNumericValue = $(this).val().replace(/[^\d.-]/g,'');
        $(this).val(filteredNumericValue);
        var fieldValue = $(this).val();
        if (isNaN(fieldValue)) {
            return false;
        }
        renderOrderStatsForOffice();
    });

    // Bind btn-add-order-item click event in office view
    $(document).on("click", "table.order-items-list button.btn-add-order-item", function() {
        addNewOrderItemLineForOffice($(this).attr("data-ordertable"));
    });

    // Set new attributes of new item on change event
    $(document).on("change", "select.order-set-new-item", function() {
        var itemId = $(this).val();
        var elementKey = $(this).attr("data-for");
        var orderTableId = toNumeric($(this).attr("data-ordertable"));
        if (itemId != "") {
            itemId = toNumeric(itemId);
            var itemInfo = LocalDB.getItemInfo(itemId);
            var itemRowHolder = $(this).parent().parent();
            $(itemRowHolder).addClass("item-holder-for-" + itemId);
            let itemPrice = LocalDB.getItemPriceByArea(itemId, LocalDB.getTableInfo(orderTableId).area_category_id);
            $(itemRowHolder).find("input[name='orderItems[id]["+ elementKey +"]']").val(itemId);
            $(itemRowHolder).find("input[name='orderItems[price]["+ elementKey +"]']").val(itemPrice.toFixed(2));
            $(itemRowHolder).find("input[name='orderItems[qty]["+ elementKey +"]']").val(1);
            $(itemRowHolder).find("span#orderItemNameAt"+elementKey).html(itemInfo.name);
        }
        renderOrderStatsForOffice();
    });

    // Bind btn-delete-order-item click event in office view
    $(document).on("click", "table.order-items-list button.btn-delete-order-item", function() {
        var itemId = $(this).attr("data-itemid");
        if (!isNaN(itemId) && itemId > 0) {
            $("table.order-items-list tr.item-holder-for-" + itemId).remove();
        } else {
            $(this).parent().parent().remove();
        }
        renderOrderStatsForOffice();
    });

    // Bind save office order button click event to update order to cloud
    $(document).on("click", "table.order-items-list button.btn-save-office-order", function() {
        if (confirm("Are you sure want to save these changes?")) {
            var orderId = $(this).attr("data-orderid");
            var orderFrom = ($(this).attr("data-islive") == 'true') ? 'live' : 'local';
            updateOrderStatsToCloud(orderId, orderFrom);
        }
        return false;
    });

    // Bind save profile button click event to save profile
    $(document).on("click", "button.btn-office-save-profile", function() {
        var restaurantName = $("input[name='profile_restaurant_name']").val();
        var restaurantContactNumber = $("input[name='profile_restaurant_contact_number']").val();
        var currency = $("select[name='profile_restaurant_currency']").val();
        var restaurantAddress = $("textarea[name='profile_restaurant_address']").val();
        var orderBookType = $("select[name='profile_restaurant_ob_type']").val();
        var restaurantGSTNumber = $("input[name='profile_restaurant_gst_number']").val();
        var restaurantInvoiceBottomLine = $("textarea[name='profile_restaurant_invoice_bottom_line']").val();
        var restaurantFoodTypes = [];
        $("input[type='checkbox'].restaurant-food-type").each(function() {
            if ($(this).prop("checked")) {
                restaurantFoodTypes.push($(this).val());
            }
        });
        var isError = false;
        if (restaurantName.toString().trim() == "") {
            showNotification("Please fill restaurant name", "error");
            isError = true;
        }
        if (currency == "") {
            showNotification("Please select currency", "error");
            isError = true;
        }
        if (orderBookType == "" || $.inArray(orderBookType, ['FNCLY', 'CLNDRY']) == -1) {
            showNotification("Please select valid order book type name", "error");
            isError = true;
        }
        if (restaurantFoodTypes.length == 0) {
            showNotification("Please select restaurant food type", "error");
            isError = true;   
        }
        if (restaurantInvoiceBottomLine.toString().trim() != '' && restaurantInvoiceBottomLine.toString().length > 80) {
            showNotification("Maximum 80 characters are allowed for invoice bottom line", "error");
            isError = true;   
        }
        if (isError) {
            return false;
        }
        var restaurant = {
            'name' : restaurantName.toString().trim(),
            'currency_id' : currency,
            'contact_number' : restaurantContactNumber.toString().trim(),
            'address' : restaurantAddress.toString().trim(),
            'order_book_type' : orderBookType.toString().trim(),
            'restaurant_food_types' : restaurantFoodTypes.join(","),
            'gst_number' : restaurantGSTNumber.toString().trim(),
            'invoice_bottom_line' : restaurantInvoiceBottomLine.toString().trim()
        };
        saveRestaurantProfile(restaurant);
    });

    $(document).on("change", "#modal-manage-item input.field_item_picture[type='file']", function(event) {
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            showNotification("Restaurant logo : Only these formats are allowed : "+fileExtension.join(', '), "error");
            return false;
        }
    });

    // Bind refresh page button click event
    $(document).on("click", "button.btn-refresh-today-orders", function() {
        syncTodayOrders();
    });

    // Bind search button submit/click event
    $(document).on("click", "button.btn-office-search-for-report", function() {
        var searchModule = $(this).attr("data-module");
        $("table.pagination").remove();
        switch(searchModule) {
            case 'orders':
                submitOrderSearchFormForCustomReport();
                break;
            default:
                break;
        }
    });

    // Bind print order report button click event
    $(document).on("click", "button.btn-print-order-report", function() {
        var printReportFor = $(this).attr("data-report").toString().trim();
        if (printReportFor == "") {
            showNotification("No report or data to print", "error");
            return false;
        }

        if (printReportFor == "custom") {
            var order_no = $("input[name='field_search_order_orderno_for_report']").val();
            var table_id = $("select[name='field_search_order_tableno_for_report']").val();
            var amount_from = $("input[name='field_search_order_amountfrom_for_report']").val();
            var amount_to = $("input[name='field_search_order_amountto_for_report']").val();
            var date_from = $("input[name='field_search_order_datefrom_for_report']").val();
            var date_to = $("input[name='field_search_order_dateto_for_report']").val();
            var payment_method = $("select[name='field_search_order_payment_method_for_report']").val();
            if (order_no == '' && table_id == '' && amount_from == '' && amount_to == '' && date_from == '' && date_to == '' && payment_method == '') {
                return false;
            }
            var orderParams = { 
                'order_no' : order_no,
                'table_id' : table_id,
                'table' : (!isNaN(table_id) && table_id > 0) ? LocalDB.getTableInfo(toNumeric(table_id)).table_no : '',
                'amount_from' : amount_from,
                'amount_to' : amount_to,
                'date_from' : date_from,
                'date_to' : date_to,
                'payment_method' : (payment_method != '') ? LocalDB.getPaymentMethodInfo(toNumeric(payment_method)).payment_method : '',
            };
            printOrders(orderParams);
        } else if (printReportFor == "month") {
            var month = $("select[name='report-search-options-month']").val();
            var year = $("select[name='report-search-options-month-year']").val();
            if (month.toString().trim() == "") {
                showNotification("Please select month to print report", "error");
                return false;
            }
            if (month.toString().trim() == "") {
                showNotification("Please select year to print report", "error");
                return false;
            }
            printOrders({
                'monthReport' : true,
                'monthReportFor' : toNumeric(month),
                'yearReportFor' : toNumeric(year)
            });
        } else if (printReportFor == "year") {
            var year = $("select[name='report-search-options-year']").val();
            if (year.toString().trim() == "") {
                showNotification("Please select year to print report", "error");
                return false;
            }
            printOrders({
                'yearReport' : true,
                'yearReportFor' : toNumeric(year)
            });
        }

    });

    // Bind datepickers to datepicker fields
    new Lightpick({ 
        field: document.getElementById('field_search_order_datefrom_for_report'),
        format: 'YYYY-MM-DD'
    });
    new Lightpick({
        field: document.getElementById('field_search_order_dateto_for_report'),
        format: 'YYYY-MM-DD'
    });
    new Lightpick({
        field: document.getElementById('search_fc_field_fcECardCreatedOn_from'),
        format: 'YYYY-MM-DD'
    });
    new Lightpick({
        field: document.getElementById('search_fc_field_fcECardCreatedOn_to'),
        format: 'YYYY-MM-DD'
    });

    // Bind on change event for report type checkbox
    $(document).on("change", "input[type='checkbox'][name='report_type']", function() {
        var isChecked = $(this).prop("checked");
        $("input[type='checkbox'][name='report_type']").each(function() {
            $(this).prop("checked", false);
        });
        $(this).prop("checked", isChecked);
        $("table.report-search-options").hide();
        if (isChecked) {
            var reportType = $(this).val();
            switch(reportType) {
                case 'month':
                    $("table.report-search-options.month").show();
                    break;
                case 'year':
                    $("table.report-search-options.year").show();
                    break;
                case 'custom':
                    $("table.report-search-options.custom-search").show();
                    break;
                default:
                    break;
            }
        }
    });

    // Bind generate month report button click event
    $(document).on("click", "button.btn-office-month-report", function() {
        var month = $("select[name='report-search-options-month']").val();
        var year = $("select[name='report-search-options-month-year']").val();
        if (month.toString().trim() == "") {
            showNotification("Please select month for the report", "error");
            return false;
        }
        if (year.toString().trim() == "") {
            showNotification("Please select year for the report", "error");
            return false;
        }
        submitOrderSearchFormForMonthReport(month, year);
    });

    // Bind generate year report button click event
    $(document).on("click", "button.btn-office-year-report", function() {
        var year = $("select[name='report-search-options-year']").val();
        if (year.toString().trim() == "") {
            showNotification("Please select year for the report", "error");
            return false;
        }
        submitOrderSearchFormForYearReport(year);
    });

    // Bind button manage system accounts event
    $(document).on("click", "button.btn-manage-system-accounts", function() {
        if (!withAccess("office")) {
            return false;
        }
        popupModal("#modal-manage-system-accounts");
        getSystemAccountsInfo();
    });

    // Bind button change system account password click event
    $(document).on("click", "button.btn-save-account-password", function() {
        if (!withAccess("office")) {
            return false;
        }
        let sysAccountHash = $(this).attr("data-accounthash");
        let newPassword = $(this).parent().parent().find("td input.account-password").val().toString().trim();
        let isError = false;
        if (newPassword == "") {
            showNotification("Password can not be empty", "error");
            isError = true;
        }
        if (newPassword.length < 8) {
            showNotification("Password must have minimum 8 characters", "error");
            isError = true;   
        }
        if (isError) {
            return false;
        } else {
            if (!confirm("Are you sure want to change password for this account?")) {
                return false;
            }
            changeSystemAccountPassword(sysAccountHash, newPassword);
        }
    });

    // Bind button delete system account click event
    $(document).on("click", "button.btn-delete-system-account", function() {
        if (!withAccess("office")) {
            return false;
        }
        if (!confirm("Are you sure want to delete this account?")) {
            return false;
        }
        let sysAccountHash = $(this).attr("data-accounthash");
        if (typeof sysAccountHash != 'undefined' && sysAccountHash != '') {
            deleteSystemAccount(sysAccountHash, $(this).parent().parent());
        } else {
            showNotification(OOPS_MSG, "error");
            return false;
        }
    });

    // Bind set no logo for restaurant button click event
    $(document).on("click", "button.btn-office-set-no-logo", function() {
        if (confirm('Are you sure want to remove logo from your restaurant?')) {
            removeLogoForRestaurant();
        }
    });

}

function showLoginLayout(showLoginRequiredMsg = false, customMsg = '') {
    $(".footer .actions").hide();
    $(".section").hide();
    $(".login-layout").show();
    scrollToTop();
    if (showLoginRequiredMsg) {
        var msg = (customMsg.length > 0) ? customMsg : "You can't perform this action without login";
        showNotification(msg, "error");
    }
}

function showTableLayout() {
    if ($(".section.table-layout").length == 0) {
        window.location.reload();
        return false;
    }
    LocalDB.setActiveTable(0);
    hideModal();
    if (checkLogin()) {
        $(".footer .actions").hide();
        $(".section").hide();
        $(".table-layout").show();
        if ($(".section.table-layout").length == 0) {
            scrollToTop();
        }
        $("button.btn-table-type.active:first").trigger("click");
    } else {
        showLoginLayout(true);
    }
}

function showProductsLayout() {
    if ($(".section.checkout-layout").length == 0) {
        window.location.reload();
        return false;
    }
    if (checkLogin()) {
        if (LocalDB.getActiveTable() > 0) {
            // adjustTableStatus();
            $(".footer .actions").show();
            $(".section").hide();
            $(".checkout-layout").show();
            scrollToTop();
        } else {
            showNotification("Please select table to add items in the cart", "error");
        }
    } else {
        showLoginLayout(true);
    }
}

function showSettingsLayout() {
    if ($(".section.settings-layout").length == 0) {
        window.location.reload();
        return false;
    }
    LocalDB.setActiveTable(0);
    hideModal();
    if (checkLogin()) {
        $(".footer .actions").hide();
        $(".section").hide();
        $(".settings-layout").show();
        scrollToTop();
    } else {
        showLoginLayout(true);
    }
}

function showKitchenLayout() {
    if ($(".section.kitchen-layout").length == 0) {
        window.location.reload();
        return false;
    }
    if (checkLogin()) {
        $(".section:not(.kitchen-layout):not(.settings-layout):not(.authentication-holder)").remove();
        // $("table.setting-block-right").parent().remove();
        $("button.btn-sync[data-module='taxes']").parent().remove();
        $("tr.list-shortcuts-container").remove();
        $(".section").hide();
        $(".section.kitchen-layout").show();
        $(".btn-app-configuration").show();
        scrollToTop();
    } else {
        showLoginLayout(true);
    }   
}

function showOfficeLayout() {
    if ($(".section.office-layout").length == 0) {
        window.location.reload();
        return false;
    }
    if (checkLogin()) {
        $(".section:not(.office-layout):not(.settings-layout):not(.authentication-holder)").remove();
        $("li:not(.for-office-only)").remove();
        $(".section").hide();
        $(".section.office-layout").show();
        $(".btn-app-configuration").show();
        scrollToTop();
    } else {
        showLoginLayout(true);
    }   
}

function submitItemToCart(itemId, quantity, fromCart = false) {
    if (checkLogin()) {
        var specifications = getItemSpecifications(quantity, itemId);
        if (fromCart) {
            addItemToCart(itemId, quantity, specifications);
            if (LocalDB.cartItems().length == 0) {
                LocalDB.clearTableCart(LocalDB.getActiveTable());
            }
        } else {
            if (quantity > 0) {
                addItemToCart(itemId, quantity, specifications);
            }
        }
        resetAskForItemQtyModal();
    } else {
        showLoginLayout(true);
    }
}

function getItemSpecifications(quantity = 0, itemId = 0) {
    var servedQty = 0;
    var hasSpecification = $("input[type='checkbox'][name='has_specifications']").prop("checked") ? true : false;
    var specifications = [];
    if (hasSpecification) {
        if ($("input[type='checkbox'][name='spefications_taste']:checked").length > 0) {
            var tasteSpecific = $("input[type='checkbox'][name='spefications_taste']:checked").val();
        } else {
            var tasteSpecific = 'regular';
        }
        var extraSpecification = $("textarea[name='specification_text']").val();
    } else {
        var tasteSpecific = 'regular';
        var extraSpecification = '';
    }
    if (itemId > 0) {
        var cartItems = LocalDB.cartItems(false, LocalDB.getActiveTable());
        if (cartItems.length > 0) {
            var itemInfoForitchen = [];
            $(cartItems).each(function(k, item) {
                if (servedQty == 0 && itemId == item.itemId) {
                    var itemSpecifications = JSON.parse(item.specifications);
                    $(itemSpecifications).each(function(isk, isv) {
                        if (isv.taste == tasteSpecific && isv.extraSpecification == extraSpecification) {
                            servedQty = isv.servedQty;
                        }
                    })
                }
            });
        }
    }
    specifications = {
        'hasSpecification' : hasSpecification,
        'taste' : tasteSpecific,
        'extraSpecification' : extraSpecification,
        'qty' : toNumeric(quantity),
        'servedQty' : (servedQty > 0) ? (quantity - servedQty) : 0,
    };
    return specifications;
}

function hideCart() {
    if (!isMobile()) {
        return false;
    }
    $(".panel-cart").removeClass('show').addClass('show');
    $(".btn-shopping-cart").trigger('click');
}

function showNotification(msg, type = 'success', ttd = 3000) {
    var notification = $("<div class='notification "+ type +"'> " + msg + " </div>");
    $('.notification-container').append(notification);
    $(notification).hide();
    $(notification).slideDown('fast');
    setTimeout(function() {
        $(notification).remove();
    }, ttd);
}

function syncAreaCategories(restaurant_id) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-area-categories',
                restaurant_id : restaurant_id,
            },
            function(areaCategories) {
                if (areaCategories.length > 0) {
                    $(areaCategories).each(function(k, area) {
                        LocalDB.saveAreaCategory(area);
                        if (k == (areaCategories.length - 1)) {
                            LocalDB.set(LocalDB.tableInfo, JSON.stringify([]));
                            showNotification(areaCategories.length + ' areas have been synced with cloud', 'success');
                            syncTables(toNumeric(LocalDB.restaurant().id));
                        }
                    });
                    if (withTableAccess()) {
                        loadAreaCategories(areaCategories);
                    }
                    if (withAccess("office")) {
                        if ($("button.btn-office-action[data-action='areacategories']").hasClass("active")) {
                            $("button.btn-office-action[data-action='areacategories']").trigger("click");
                        }
                    }
                } else {
                    LocalDB.set(LocalDB.tableInfo, JSON.stringify([]));
                    syncTables(toNumeric(LocalDB.restaurant().id));
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncTables(restaurant_id) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-tables',
                restaurant_id : restaurant_id,
            },
            function(tables) {
                $(".btn-sync[data-module='tables']").find(".sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync.png");
                showNotification(tables.length + ' tables have been synced with cloud', 'success');
                loadTables(tables);
                if (withAccess("office")) {
                    if ($("button.btn-office-action[data-action='tables']").hasClass("active")) {
                        $("button.btn-office-action[data-action='tables']").trigger("click");
                    }
                    if ($("button.btn-office-action[data-action='areacategories']").hasClass("active")) {
                        $("button.btn-office-action[data-action='areacategories']").trigger("click");
                    }
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncCategories(restaurant_id) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-categories',
                restaurant_id : restaurant_id,
            },
            function(categories) {
                $(".btn-sync[data-module='categories']").find(".sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync.png");
                showNotification(categories.length + ' categories and their items have been synced with cloud', 'success');
                LocalDB.set(LocalDB.categoriesInfo, JSON.stringify([]));
                $(categories).each(function(key, category) {
                    LocalDB.saveCategory(category);
                    if ((categories.length - 1) == key) {
                        syncItems(true);
                    }
                });
                if (withAccess("office")) {
                    if ($("button.btn-office-action[data-action='categories']").hasClass("active")) {
                        $("button.btn-office-action[data-action='categories']").trigger("click");
                    }
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncItems(reloadCategories = false) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-items',
                restaurant_id : RESTAURANT.ID
            },
            function(items) {
                LocalDB.set(LocalDB.itemsInfo, JSON.stringify([]));
                $(items).each(function(key, item) {
                    LocalDB.saveItem(item, item.category_id, true);
                    if (key == (items.length) - 1) {
                        if (withTableAccess()) {
                            renderCart();
                            loadCategories(LocalDB.categories());
                        }
                    }
                });
                if (reloadCategories) {
                    if (withAccess("office")) {

                    } else if(withTableAccess()) {
                        loadCategories(LocalDB.categories());
                    }
                }
                if (withAccess("office")) {
                    if ($("button.btn-office-action[data-action='items']").hasClass("active")) {
                        $("button.btn-office-action[data-action='items']").trigger("click");
                    }
                }
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncTaxes(restaurant_id) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-taxes',
                restaurant_id : restaurant_id,
            },
            function(taxes) {
                if (typeof taxes != 'undefined' && taxes.length > 0) {
                    $(taxes).each(function(index, tax) {
                        LocalDB.saveTax(tax);
                    });
                }
                $(".btn-sync[data-module='taxes']").find(".sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync.png");
                showNotification('Tax info has been synced with cloud', 'success');
                if (withTableAccess()) {
                    renderCart();
                }
                if (withAccess("office")) {
                    if ($("button.btn-office-action[data-action='taxes']").hasClass("active")) {
                        $("button.btn-office-action[data-action='taxes']").trigger("click");
                    }
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncDiscounts(restaurant_id) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-discounts',
                restaurant_id : restaurant_id,
            },
            function(discounts) {
                if (typeof discounts != 'undefined' && discounts.length > 0) {
                    $(discounts).each(function(index, discount) {
                        LocalDB.saveDiscount(discount);
                    });
                }
                $(".btn-sync[data-module='taxes']").find(".sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync.png");
                showNotification('Discount info has been synced with cloud', 'success');
                if (withTableAccess()) {
                    renderCart();
                }
                if (withAccess("office")) {
                    if ($("button.btn-office-action[data-action='discounts']").hasClass("active")) {
                        $("button.btn-office-action[data-action='discounts']").trigger("click");
                    }
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncRestaurant(restaurant_id, inBackground = false) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
            {
                action : 'get-restaurant-info',
                restaurant_id : restaurant_id
            },
            function(restaurant) {
                LocalDB.set(LocalDB.restaurantInfo, JSON.stringify([]));
                LocalDB.saveRestaurant(restaurant);
                if (!inBackground) {
                    $(".btn-sync[data-module='restaurant']").find(".sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync.png");
                    showNotification('Restaurant has been synced with cloud', 'success');
                    if (withTableAccess() && !withAccess("office")) {
                        // Initialize table view process
                        initTableViewProcess(restaurant);
                    } else if (withAccess("kitchen")) {
                        // Initialize kitchen view process
                        initKitchenViewProcess(restaurant);
                    } else if (withAccess("office")) {
                        // Initialize back office view
                        initOfficeViewProcess(restaurant);
                    }
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function syncTodayOrders(restaurant_id) {
    if (checkLogin()) {
        if (withAccess("office")) {
            callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'get-orders',
                restaurant_id : RESTAURANT.ID,
                options : JSON.stringify({
                    'todayOnly' : true,
                    'overall' : false
                }),
            },
            function(orders) {
                LocalDB.set(LocalDB.ordersInfo, JSON.stringify(orders));
                if (withAccess("office")) {
                    if ($("button.btn-office-action[data-action='todayorders']").hasClass("active")) {
                        $("button.btn-office-action[data-action='todayorders']").trigger("click");
                        $("button.btn-refresh-today-orders").removeClass("blink");
                    }
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            });
        }
    }
}

function syncSystem(restaurant_id) {
    syncTables(restaurant_id);
    syncTaxes(restaurant_id);
    syncDiscounts(restaurant_id);
    syncCategories(restaurant_id);
    syncRestaurant(restaurant_id);
}

function unsetAuthenticSections(role = '') {
    if (role != "") {
        if (role != "owner" && role != "counter" && role != "office") {
            $("li.for-owner-only").remove();
            $("#modal-add-bone").remove();
        }
        if (role != "owner" && role != "manager" && role != "office") {
            $("li.for-owner-manager-only").remove();
            $("#modal-statistics").remove();
        }
        if (role == "office") {
            $(".for-table-view-only").remove();
            $(".for-kitchen-view-only").remove();
        }
        if (role == "owner" || role == "manager" || role == "counter" || role == "waiter") {
            $(".for-kitchen-view-only").remove();
            $(".for-office-view-only").remove();
        }
        if (role == "kitchen") {
            $(".for-table-view-only").remove();
            $(".for-office-view-only").remove();
        }
        if (role == "counter") {
            $(".sensitive-block").remove();   
        }
        if (role == "manager" || role == "waiter") {
            $(".checkout-actions").remove();
            $(".financials").remove();
            $(".counter-needed").remove();
        }
        if (role != "office") {
            $(".for-strict-office-only").remove();
        }
    }
    if (isMobile()) {
        if (role == "owner") {
            $(".footer-btn.btn-app-help-tips").remove();
        }
    }
}

function hideSearchSuggestions() {
    showSearchSuggestions('');
}

function showSearchSuggestions(searchPhase) {
    $("tr.search-result-holder").hide();
    if (searchPhase.length == 0) {
        return false;
    }
    if (withAccess("office")) {
        showSearchSuggestionsForOfficeAccess(searchPhase);
    } else if(withTableAccess()) {
        showSearchSuggestionsForTableAccess(searchPhase);
    }
}

function showSearchSuggestionsForOfficeAccess(searchPhase) {
    $("table.list-search-result").empty();
    $("tr.search-result-holder").show();
    if ($("table.table-orders-office").css("display") != "none") {
        var ordersBySearchPhase = LocalDB.searchOrders(searchPhase);
        if(ordersBySearchPhase.length == 0) {
            showNoSearchResultFoundToSearchForOffice("order");
            return false;
        } else {
            // Show orders found
            $(ordersBySearchPhase).each(function(k, order) {
                var areaCategoryBlock = '<tr class="search-result-line search-result-line-order" data-searchorderid="' + order.id + '">';
                areaCategoryBlock+= '<td class="search-picture bg-theme"><img src="assets/img/icons/icon-restaurant.png" class="icon-small" /></td>';
                areaCategoryBlock+= '<td class="search-name">Order #' + order.order_no + ' <span class="d-block text-grey-dark text-x-sm">Table '+ LocalDB.getTableInfo(order.table_id).table_no + ' : ' + order.order_amount + ' ' + RESTAURANT.CURRENCY + ' (Paid by '+ order.payment_method +')</span></td>';
                areaCategoryBlock+= '</tr>';
                $("table.list-search-result").append(areaCategoryBlock);
            });
        }
    } else if ($("table.table-tables-office").css("display") != "none") {
        var tablesBySearchPhase = LocalDB.searchTables(searchPhase);
        if(tablesBySearchPhase.length == 0) {
            showNoSearchResultFoundToSearchForOffice("table");
            return false;
        } else {
            // Show tables found
            $(tablesBySearchPhase).each(function(k, table) {
                generateTableSearchBlock(table, 'office-table');
            });
        }
    } else if ($("table.table-items-office").css("display") != "none") {
        var itemsBySearchPhase = LocalDB.searchItems(searchPhase);
        if(itemsBySearchPhase.length == 0) {
            showNoSearchResultFoundToSearchForOffice("item");
            return false;
        } else {
            // Show items found
            $(itemsBySearchPhase).each(function(k, item) {
                generateItemSearchBlock(item, 'office-item');
            });
        }
    } else if ($("table.table-categories-office").css("display") != "none") {
        var categoriesBySearchPhase = LocalDB.searchCategories(searchPhase);
        if(categoriesBySearchPhase.length == 0) {
            showNoSearchResultFoundToSearchForOffice("category");
            return false;
        } else {
            // Show items found
            $(categoriesBySearchPhase).each(function(k, category) {
                generateCategorySearchBlock(category, 'office-category');
            });
        }
    } else if ($("table.table-area-categories-office").css("display") != "none") {
        var areaCategoriesBySearchPhase = LocalDB.areaCategories(searchPhase);
        if(areaCategoriesBySearchPhase.length == 0) {
            showNoSearchResultFoundToSearchForOffice("category");
            return false;
        } else {
            // Show items found
            $(areaCategoriesBySearchPhase).each(function(k, areaCategory) {
                generateAreaCategorySearchBlock(areaCategory, 'office-area-category');
            });
        }
    }
}

function showSearchSuggestionsForTableAccess(searchPhase) {
    var areaCategoriesBySearchPhase = LocalDB.searchAreaCategories(searchPhase);
    var tablesBySearchPhase = LocalDB.searchTables(searchPhase);
    var needToSearchCategoryAndItem = false;
    if (LocalDB.getActiveTable() > 0) {
        needToSearchCategoryAndItem = true;
        var categoriesBySearchPhase = LocalDB.searchCategories(searchPhase);
        var itemsBySearchPhase = LocalDB.searchItems(searchPhase);
    }
    $("table.list-search-result").empty();
    $("tr.search-result-holder").show();

    if(areaCategoriesBySearchPhase.length == 0 && tablesBySearchPhase.length == 0 && !needToSearchCategoryAndItem) {
        showNoSearchResultFoundToSearch();
        return false;
    }

    // Show area categories found
    $(areaCategoriesBySearchPhase).each(function(k, areaCategory) {
        generateAreaCategorySearchBlock(areaCategory, 'area-category');
    });

    // Show tables found
    $(tablesBySearchPhase).each(function(k, table) {
        generateTableSearchBlock(table, 'table');
    });

    if (!needToSearchCategoryAndItem) { // Do not show item suggestions if control is not on nontable area
        // Set first result as active
        $(".search-result-line:first").addClass("active");
        return false;
    }

    if(areaCategoriesBySearchPhase.length == 0 && tablesBySearchPhase.length == 0 && categoriesBySearchPhase.length == 0 && itemsBySearchPhase.length == 0) {
        showNoSearchResultFoundToSearch();
        return false;
    }

    // Show categories found
    $(categoriesBySearchPhase).each(function(k, category) {
        generateCategorySearchBlock(category, 'category');
    });

    // Show items found
    $(itemsBySearchPhase).each(function(k, item) {
        generateItemSearchBlock(item, 'item')
    });

    // Set first result as active
    $(".search-result-line:first").addClass("active");
}

function navigateSearchResult(direction = 'down') {
    if ($(".search-result-line.active").length == 0) {
        if (direction == 'down') {
            $(".search-result-line:first").addClass("active");
        } else {
            $(".search-result-line:last").addClass("active");
        }
    } else {
        var currentActiveResultLine = $(".search-result-line.active:first");
        if (direction == 'down') {
            if ($(".search-result-line:last").hasClass("active")) {
                $(".search-result-line:first").addClass("active");
            } else {
                $(currentActiveResultLine).next().addClass("active");
            }
        } else {
            if ($(".search-result-line:first").hasClass("active")) {
                $(".search-result-line:last").addClass("active");
            } else {
                $(currentActiveResultLine).prev().addClass("active");
            }
        }
        $(currentActiveResultLine).removeClass("active");
    }
}

function generateAreaCategorySearchBlock(areaCategory, as = '') {
    var areaCategoryBlock = '<tr class="search-result-line search-result-line-' + as + '" data-searchareacategoryid="' + areaCategory.id + '">';
    areaCategoryBlock+= '<td class="search-picture bg-theme"><img src="assets/img/icons/icon-restaurant.png" class="icon-small" /></td>';
    areaCategoryBlock+= '<td class="search-name">' + areaCategory.name + ' <span class="d-block text-grey-dark text-x-sm">'+ LocalDB.tables(areaCategory.id).length + ' Tables</span></td>';
    areaCategoryBlock+= '</tr>';
    $("table.list-search-result").append(areaCategoryBlock);
}

function generateTableSearchBlock(table, as = '') {
    var tableAreaCategory = LocalDB.getAreaCategoryInfo(table.area_category_id);
    var tableBlock = '<tr class="search-result-line search-result-line-' + as + '" data-searchtableid="' + table.id + '">';
    tableBlock+= '<td class="search-picture bg-theme"><img src="assets/img/icons/icon-table-small.png" class="icon-small" /></td>';
    tableBlock+= '<td class="search-name">Table ' + table.table_no + ' <span class="d-block text-grey-dark text-x-sm">'+ tableAreaCategory.name + '</span></td>';
    tableBlock+= '</tr>';
    $("table.list-search-result").append(tableBlock);
}

function generateItemSearchBlock(item, as = '') {
    var itemCategory = LocalDB.getCategoryInfo(item.category_id);
    var itemBlock = '<tr class="search-result-line search-result-line-' + as + '" data-searchitemid="' + item.id + '" data-searchitemcategoryid="' + item.category_id + '">';
    if (item.picture == null || item.picture == 'null' || item.picture == '') {
        var itemPicture = $(".restaurant-logo").attr("src");  
    } else {
        var itemPicture = 'assets/img/uploads/' + RESTAURANT.ID + '/categories/items/' + item.picture;
    }
    itemBlock+= '<td class="search-picture"><img src="' + itemPicture + '" class="item-img icon-small"></td>';
    var itemInfo = item.name;
    if (typeof item.item_code != 'undefined' && item.item_code != null && item.item_code != '') {
        itemInfo+= " - " + item.item_code;
    }
    if (withTableAccess()) {
        let tableId = LocalDB.getActiveTable();
        let itemPrice = LocalDB.getItemPriceByArea(item.id, LocalDB.getTableInfo(tableId).area_category_id);
        var itemPriceInfo = itemPrice + ' ' + RESTAURANT.CURRENCY + ' - ';
    } else {
        var itemPriceInfo = '';
    }
    itemBlock+= '<td class="search-name">' + itemInfo + ' <span class="d-block text-grey-dark text-x-sm">'+ itemPriceInfo + itemCategory.name + '</span></td>';
    itemBlock+= '</tr>';
    $("table.list-search-result").append(itemBlock);
}

function generateCategorySearchBlock(category, as = '') {
    var categoryItems = LocalDB.items(category.id);
    var categoryBlock = '<tr class="search-result-line search-result-line-' + as + '" data-searchcategoryid="' + category.id + '">';
    if (category.picture == null || category.picture == 'null' || category.picture == '') {
        var categoryPicture = $(".restaurant-logo").attr("src");  
    } else {
        var categoryPicture = 'assets/img/uploads/' + RESTAURANT.ID + '/categories/' + category.picture;
    }
    categoryBlock+= '<td class="search-picture"><img src="' + categoryPicture + '" class="item-img icon-small"></td>';
    categoryBlock+= '<td class="search-name">' + category.name + ' <span class="d-block text-grey-dark text-x-sm">' + categoryItems.length + ' items</span></td>';
    categoryBlock+= '</tr>';
    $("table.list-search-result").append(categoryBlock);
}

function checkLogin() {
    return LocalDB.isSet(LocalDB.loginAs);
}

function setTableStatus(tableInfo) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'set-table-status',
                status : tableInfo['status'],
                restaurant_id : toNumeric(LocalDB.restaurant().id),
                table_id : (typeof tableInfo['id'] != 'undefined' && !isNaN(tableInfo.id) && tableInfo.id > 0) ? tableInfo.id : LocalDB.getActiveTable(),
            },
            function(tableInfo) {
                LocalDB.setTableStatus(tableInfo.id, tableInfo.status);
                loadTables(LocalDB.tables());
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function saveOrder(orderInfo, options = []) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'save-order',
                orderInfo : {
                    cart_info : orderInfo.cart_info,
                    tax_info : orderInfo.tax_info,
                    discount_info : orderInfo.discount_info,
                    order_amount : toNumeric(orderInfo.order_amount),
                    order_amount_currency : RESTAURANT.CURRENCY_CODE,
                    order_no : orderInfo.order_no,
                    order_status : orderInfo.order_status,
                    payment_method : orderInfo.payment_method,
                    payment_method_info : orderInfo.payment_method_info,
                    restaurant_id : orderInfo.restaurant_id,
                    table_id : orderInfo.table_id,
                    restaurant_name : RESTAURANT.NAME,
                    restaurant_ob_type : LocalDB.restaurant().order_book_type,
                    customer : (typeof options.customer != 'undefined') ? options.customer : [],
                    isBillGiven : (isBillGivenToCustomer()) ? 1 : 0,
                    extra_information : JSON.stringify(getBillExtraInformation())
                }
            },
            function(response) {
                if(response.status == true) {
                    if(!isNaN(response.order.table_id) && response.order.table_id > 0) {
                        if (options.onlinePayment == true) {
                            initPayment(response.order.rzpOrder_id, orderInfo);
                            hideModalByForce();
                        } else if (options.sendPaymentLink == true) {
                            toggleWaitingLoader('show', true);
                            checkOrderStatusByRZPPaymentLinkId(response.order.rzpPaymentLink_Id, orderInfo);
                            hideModal();
                            CHECK_PAYMENT_LINK_STATUS_INTERVAL_ID = setInterval(function() {
                                checkOrderStatusByRZPPaymentLinkId(response.order.rzpPaymentLink_Id, orderInfo);
                            }, 2000);
                            hideModalByForce();
                        } else if (options.FCECard == true) {
                            toggleWaitingLoader('show', true);
                            initPaymentWithFCECard(options.FCECardNumber, orderInfo, response.order);
                        } else if (options.upi == true) {
                            showNotification("Order saved successfully", "success");
                            generateInvoice(orderInfo, 'upi', response.order.order_no, response.order.created_at);
                            resetPaymentForm();
                            LocalDB.clearTableCart(orderInfo['table_id']);
                            renderCart();
                        } else {
                            showNotification("Order saved successfully", "success");
                            generateInvoice(orderInfo, 'cash', response.order.order_no, response.order.created_at);
                            resetPaymentForm();
                            LocalDB.clearTableCart(orderInfo['table_id']);
                            renderCart();
                        }
                        $("input[type='checkbox'][name='have_customer_info']").prop("checked", false);
                        $("input[type='checkbox'][name='is_bill_given_to_customer']").prop("checked", false);
                    } else {
                        showNotification(OOPS_MSG, "error");    
                    }
                } else {
                    showNotification(response.message, "error");
                }
                PAYMENT_IN_PROCESS = false;
                resetProceedPaymentBtn();
            },
            function(error) {
                showNotification(error.responseText, "error");
                resetProceedPaymentBtn();
                PAYMENT_IN_PROCESS = false;
                $("button.btn-process-payment").html("Proceed");
                $("button.btn-process-payment").removeAttr("disabled");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function initPayment(rzpOrder_id, orderInfo) {
    if (checkLogin()) {
        loadRZPayCheckoutScript();
        var options = {
            "key": RZ_PAY_API_KEY_ID,
            "order_id": rzpOrder_id,
            "image": "assets/img/logo/logo.png",
            "description": "Bill for table #" + LocalDB.getActiveTable() + " " + LocalDB.restaurant().name,
            "handler": function (response) {
                toggleWaitingLoader('show', true);
                checkOrderStatusByRZPOrderId(rzpOrder_id, orderInfo);
                hideModal();
                CHECK_ORDER_STATUS_INTERVAL_ID = setInterval(function() {
                    checkOrderStatusByRZPOrderId(rzpOrder_id, orderInfo);
                }, 2000);
            },
            "modal": {
                "ondismiss": function(){
                    removeCloudOrder(rzpOrder_id);
                 }
            },
            "theme": {
                "color": "#38323e"
            }
        };
        setTimeout(function() {
            var rzpDialog = new Razorpay(options);
            rzpDialog.on('payment.failed', function (response) {
                showNotification(response.error.description, "error");
            });
            rzpDialog.open();
        }, 1000);
    } else {
        showLoginLayout(true);
    }
}

function checkOrderStatusByRZPOrderId(rzpOrder_id, orderInfo) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'order-status',
                rzpOrder_id : rzpOrder_id
            },
            function(response) {
                if (response.order_status == 'approved') {
                    LocalDB.clearTableCart(LocalDB.getActiveTable());
                    renderCart();
                    triggerCartUpdated(LocalDB.getActiveTable());
                    showNotification("Payment successfully captured", "success");
                    clearInterval(CHECK_ORDER_STATUS_INTERVAL_ID);
                    toggleWaitingLoader('hide');
                    resetPaymentForm();
                    generateInvoice(orderInfo, 'online_payment', response.order_no, response.order.created_at);
                } else if (response.order_status != 'pending') {
                    showNotification("Payment has been failed. Try again!", "error");
                    toggleWaitingLoader('hide');
                    clearInterval(CHECK_ORDER_STATUS_INTERVAL_ID);
                }
            },
            function(error) {
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function checkOrderStatusByRZPPaymentLinkId(rzpPaymentLink_Id, orderInfo) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'order-status',
                rzpPaymentLink_Id : rzpPaymentLink_Id
            },
            function(response) {
                if (response.order_status == 'approved') {
                    LocalDB.clearTableCart(LocalDB.getActiveTable());
                    triggerCartUpdated(LocalDB.getActiveTable());
                    renderCart();
                    showNotification("Payment successfully captured", "success");
                    clearInterval(CHECK_PAYMENT_LINK_STATUS_INTERVAL_ID);
                    toggleWaitingLoader('hide');
                    resetPaymentForm();
                    generateInvoice(orderInfo, 'payment_link', response.order_no, response.order.created_at);
                } else if (response.order_status != 'pending') {
                    showNotification("Payment has been failed. Try again!", "error");
                    toggleWaitingLoader('hide');
                    clearInterval(CHECK_PAYMENT_LINK_STATUS_INTERVAL_ID);
                }
            },
            function(error) {
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function checkOrderStatusByFCCardPaymentLink(order_id, orderInfo) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'order-status',
                restaurant_id : RESTAURANT.ID,
                order_id : order_id
            },
            function(response) {
                if (response.order_status == 'approved') {
                    LocalDB.clearTableCart(LocalDB.getActiveTable());
                    triggerCartUpdated(LocalDB.getActiveTable());
                    renderCart();
                    showNotification("Payment successfully captured", "success");
                    clearInterval(CHECK_FC_PAYMENT_LINK_STATUS_INTERVAL_ID);
                    toggleWaitingLoader('hide');
                    resetPaymentForm();
                    generateInvoice(orderInfo, 'FC-eCard', response.order_no, response.created_at);
                } else if (response.order_status != 'pending') {
                    showNotification("Payment has been failed. Try again!", "error");
                    toggleWaitingLoader('hide');
                    clearInterval(CHECK_FC_PAYMENT_LINK_STATUS_INTERVAL_ID);
                }
            },
            function(error) {
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function removeCloudOrder(rzpOrder_id) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'remove-order',
                orderId : rzpOrder_id
            },
            function(response) {
            },
            function(error) {
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function validateCustInfoAndSendPaymentLink(orderInfo) {
    if (checkLogin()) {
        var isError = false;
        if (checkElementEmpty($("input[type='text'][name='customer_email']")) && checkElementEmpty($("input[type='text'][name='customer_phone']"))) {
            resetProceedPaymentBtn();
            showNotification("Please provide either customer email or phone number to send payment link", "error");
            isError = true;
        }
        if(!isError) {
            saveOrder(orderInfo, {
                'sendPaymentLink' : true,
                'customer' : {
                    'email' : $("input[type='text'][name='customer_email']").val(),
                    'phone' : $("input[type='text'][name='customer_phone']").val(),
                }
            });
        }
    } else {
        showLoginLayout(true);
    }
}

function initPaymentWithFCECard(FCECardNumber, orderInfo, order = []) {
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'pay-with-fc-ecard', 
                restaurant_id : toNumeric(LocalDB.restaurant().id),
                FCECardNumber : FCECardNumber,
                orderAmount : toNumeric(orderInfo.order_amount),
                currency : LocalDB.restaurant().currency_symbol,
                order_id : order.id
            },
            function(response) {
                toggleWaitingLoader('hide');
                if (response.status == true) {
                    hideModal();
                    LocalDB.clearTableCart(LocalDB.getActiveTable());
                    triggerCartUpdated(LocalDB.getActiveTable());
                    renderCart();
                    showNotification(response.message, "success");
                    generateInvoice(orderInfo, 'FC-eCard', (typeof order.order_no != 'undefined') ? order.order_no : '', (typeof order.created_at != 'undefined') ? order.created_at : '');
                    resetPaymentForm();
                    EventHandler.notifyCustomerForFCCardTransaction(response);
                } else {
                    if (typeof response.WFR != 'undefined' && response.WFR == true) {
                        toggleWaitingLoader('show', true);
                        if (typeof order == 'undefined' || typeof order.id == 'undefined' || order.id == '' || isNaN(order.id)) {
                            toggleWaitingLoader("hide");
                        } else {
                            checkOrderStatusByFCCardPaymentLink(order.id, orderInfo);
                            hideModal();
                            CHECK_FC_PAYMENT_LINK_STATUS_INTERVAL_ID = setInterval(function() {
                                checkOrderStatusByFCCardPaymentLink(order.id, orderInfo);
                            }, 3000);
                            hideModalByForce();
                            showNotification(response.message, "info", 5000);
                        }
                    } else {
                        showNotification(response.message, "error");
                    }
                }
            },
            function(error) {
                toggleWaitingLoader('hide');
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function generateInvoice(orderInfo, paymentMethod = 'cash', invoiceOrderNo = '', invoiceDate = '') {
    if (checkLogin()) {
        var cartInfo = JSON.parse(orderInfo.cart_info);
        var subTotal = 0;
        var grandTotal = 0;
        var inclTaxLabel = '';
        var cartTotalAfterDiscount = 0;
        $("tr.invoice-line").each(function() {
            $(this).remove();
        });
        $("tr.invoice-header-line").each(function() {
            if ($(this).hasClass("bill-extra-field")) {
                $(this).remove();
            }
        });
        var billExtraInformation = getBillExtraInformation();
        if (typeof billExtraInformation != 'undefined') {
            if (billExtraInformation.customer_name != '') {
                var invoiceExtraInfoLine = '<tr class="invoice-header-line bill-extra-field">';
                invoiceExtraInfoLine+= '<td class="text-left text-sm p-2-f dotted-border-dark" colspan="5">';
                invoiceExtraInfoLine+= '<span class="text-bold">CUSTOMER : ';
                invoiceExtraInfoLine+= billExtraInformation.customer_name;
                invoiceExtraInfoLine+= '</span>';
                invoiceExtraInfoLine+= '</td>';
                invoiceExtraInfoLine+= '</tr>';
                $(invoiceExtraInfoLine).insertBefore($("tr.invoice-header-line:first"));
            }
            if (billExtraInformation.extra_fields.length > 0) {
                $(billExtraInformation.extra_fields).each(function(befiKey, billExtraFieldInfo) {
                    var invoiceExtraInfoLine = '<tr class="invoice-header-line bill-extra-field">';
                    invoiceExtraInfoLine+= '<td class="text-left text-sm p-2-f dotted-border-dark" colspan="5">';
                    invoiceExtraInfoLine+= '<span class="text-bold">' + billExtraFieldInfo.field + ' : ';
                    invoiceExtraInfoLine+= billExtraFieldInfo.value;
                    invoiceExtraInfoLine+= '</span>';
                    invoiceExtraInfoLine+= '</td>';
                    invoiceExtraInfoLine+= '</tr>';
                    if ($("tr.invoice-header-line.bill-extra-field:last").length == 0) {
                        $(invoiceExtraInfoLine).insertBefore($("tr.invoice-header-line:first"));
                    } else {
                        $(invoiceExtraInfoLine).insertAfter($("tr.invoice-header-line.bill-extra-field:last"));
                    }
                });
            }
        }
        $(cartInfo).each(function(k, v) {
            var invoiceItemRowClone = $("tr.item-line:first").clone();
            var itemInfo = LocalDB.getItemInfo(v.itemId);
            let itemPrice = LocalDB.getItemPriceByArea(v.itemId, LocalDB.getTableInfo(LocalDB.getActiveTable()).area_category_id);
            var total = toNumeric(v.qty * itemPrice);
            $(invoiceItemRowClone).addClass('item-line-info').addClass('invoice-line');
            $(invoiceItemRowClone).find('td:first').html(v.name);
            $(invoiceItemRowClone).find('td:nth-child(2)').html(v.qty);
            $(invoiceItemRowClone).find('td:nth-child(3)').html(toNumeric(itemPrice).toFixed(2));
            $(invoiceItemRowClone).find('td:nth-child(4)').html(total.toFixed(2));
            $(invoiceItemRowClone).insertAfter($("tr.item-line:last"));
            subTotal+= total;
        });
        grandTotal+= subTotal;

        if (LocalDB.taxes().length > 0 || LocalDB.discounts().length > 0) {
            var subTotalRow = '<tr class="invoice-line invoice-subtotal">';
            subTotalRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Subtotal</td>';
            subTotalRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + subTotal.toFixed(2) + '</td>';
            subTotalRow+= '<td class="text-sm p-2-f"></td>';
            subTotalRow+= '</tr>';
            $(subTotalRow).insertAfter($("tr.item-line:last"));
        }

        var cartTotalWithoutDiscount = grandTotal;
        var hasAtleastOneDiscount = false;
        if (LocalDB.discounts().length > 0) {
            var discounts = LocalDB.sequencedDiscounts();
            $(discounts).each(function(k, v) {
                if (v.tableId == null || v.tableId == orderInfo.table_id) {
                    if (!isNaN(v.amount) && v.amount > 0) {
                        var hasDiscount = false;
                        if (v.minimum_amount != null && cartTotalWithoutDiscount >= toNumeric(v.minimum_amount)) {
                            if (v.type == "percentage") {
                                var percentage = v.amount;
                                var discountAmount = ((grandTotal * percentage) / 100).toRound();
                            } else {
                                var discountAmount = v.amount.toRound();
                            }
                            grandTotal-= toNumeric(discountAmount);
                            hasDiscount = true;
                            if (!hasAtleastOneDiscount) {
                                hasAtleastOneDiscount = true;
                            }
                        } else if (v.minimum_amount == null) {
                            if (v.type == "percentage") {
                                var percentage = v.amount;
                                var discountAmount = ((grandTotal * percentage) / 100).toRound();
                            } else {
                                var discountAmount = v.amount.toRound();
                            }
                            grandTotal-= toNumeric(discountAmount);
                            hasDiscount = true;
                            if (!hasAtleastOneDiscount) {
                                hasAtleastOneDiscount = true;
                            }
                        }
                        if (hasDiscount) {
                            var discountRow = '<tr class="invoice-line invoice-discount-line">';
                            var discountDesc = '';
                            if (v.description != null && v.description != '') {
                                discountDesc = v.description + ' ';
                            }
                            if (v.type == "percentage") {
                                var discountBase = v.amount + "%";
                            } else {
                                var discountBase = "Flat";
                            }
                            discountRow+= '<td colspan="3" class="text-sm p-2-f dotted-border-dark text-right">' + discountDesc + '[' + discountBase + '] </td>';
                            discountRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + toNumeric(discountAmount).toFixed(2) + '</td>';
                            subTotalRow+= '<td class="text-sm p-2-f"></td>';
                            discountRow+= '</tr>';
                            $(discountRow).insertAfter($("tr.invoice-line:last"));
                        }
                    }
                }
            });
        }
        // Remove table custom discounts
        LocalDB.clearCustomDiscounts(orderInfo.table_id);

        var cartTotalAfterDiscount = grandTotal;
        if (LocalDB.taxes().length > 0 && hasAtleastOneDiscount) {
            var subTotalRow = '<tr class="invoice-line invoice-total-after-discount">';
            subTotalRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Total (Excl. Tax)</td>';
            subTotalRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + cartTotalAfterDiscount.toFixed(2) + '</td>';
            subTotalRow+= '<td class="text-sm p-2-f"></td>';
            subTotalRow+= '</tr>';
            $(subTotalRow).insertAfter($("tr.invoice-line:last"));
        }

        if (LocalDB.taxes().length > 0) {
            inclTaxLabel = ' (Incl. Tax)';
            var taxes = LocalDB.taxes();
            $(taxes).each(function(k, v) {
                if (v.tableId == null || v.tableId == LocalDB.getActiveTable()) {
                    if (!isNaN(v.amount) && v.amount > 0) {
                        if (v.type == "percentage") {
                            var percentage = v.amount;
                            var taxAmount = ((cartTotalAfterDiscount * percentage) / 100).toRound();
                        } else {
                            var taxAmount = v.amount.toRound();
                        }
                        grandTotal+= toNumeric(taxAmount);
                        var taxBase = "";
                        if (v.type != null && v.type == "percentage") {
                            taxBase = ' [' + v.amount + '%]';
                        }
                        var taxRow = '<tr class="invoice-line invoice-tax-line">';
                        taxRow+= '<td colspan="3" class="text-sm p-2-f dotted-border-dark text-right"> ' + v.name + taxBase + ' </td>';
                        taxRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + toNumeric(taxAmount).toFixed(2) + '</td>';
                        subTotalRow+= '<td class="text-sm p-2-f"></td>';
                        taxRow+= '</tr>';
                        $(taxRow).insertAfter($("tr.invoice-line:last"));
                    }
                }
            });
        }
        // Remove table custom taxes
        LocalDB.clearCustomTaxes(orderInfo.table_id);

        let totalRoundedBy = Math.round(toNumeric(grandTotal)) - toNumeric(grandTotal);
        if (totalRoundedBy != 0) {
            var grandTotalRoundRow = '<tr class="invoice-line invoice-grand-total-round">';
            grandTotalRoundRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Rounded By</td>';
            grandTotalRoundRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + totalRoundedBy.toFixed(2) + '</td>';
            grandTotalRoundRow+= '<td class="text-sm p-2-f"></td>';
            grandTotalRoundRow+= '</tr>';
            $(grandTotalRoundRow).insertAfter($("tr.invoice-line:last"));
        }

        var grandTotalRow = '<tr class="invoice-line invoice-grand-total">';
        grandTotalRow+= '<td colspan="3" class="text-sm p-2-f text-right dotted-border-dark">Grand Total</td>';
        grandTotalRow+= '<td class="text-sm p-2-f dotted-border-dark text-right">' + Math.round(toNumeric(grandTotal)).toFixed(2) + '</td>';
        grandTotalRow+= '<td class="text-sm p-2-f"></td>';
        grandTotalRow+= '</tr>';
        $(grandTotalRow).insertAfter($("tr.invoice-line:last"));

        var invoiceTable = LocalDB.getTableInfo(orderInfo.table_id);
        var tableInfo = "Table #"+invoiceTable.table_no;
        if (invoiceTable.area_category_id != null && invoiceTable.area_category_id > 0) {
            var invoiceTableAreaCategory = LocalDB.getAreaCategoryInfo(invoiceTable.area_category_id);
            tableInfo+= " [" + invoiceTableAreaCategory.name + "]";
        }
        invoiceOrderNo = (invoiceOrderNo != "") ? invoiceOrderNo : orderInfo.order_no;
        $("span.invoice-payment-method").html(paymentMethod.toUpperCase());
        $("span.invoice-restaurant-name").html(RESTAURANT.NAME);
        if (getRestaurantFoodTypesBadge() != '') {
            $(".invoice-restaurant-food-types-badge").html(getRestaurantFoodTypesBadge());
        }
        $("span.invoice-table-no").html(tableInfo);
        $("span.invoice-ref-no").html("Order #" + invoiceOrderNo);
        invoiceDate = (invoiceDate != "") ? invoiceDate : getCurrentDate();
        $("span.invoice-date").html(invoiceDate);
        $(".btn-print-invoice").show();
        $("img.invoice-logo").show();
        showInvoice();
    } else {
        showLoginLayout(true);
    }
}

// Customer extra information for bill/invoice
function getBillExtraInformation() {
    var billExtraInformation = {
        'customer_name' : '',
        'extra_fields' : []
    };
    var isSetCustExtraInfo = $("input[type='checkbox'][name='have_customer_info']").prop("checked");
    if (isSetCustExtraInfo) {
        // Set customer extra information for bill/invoice
        var customerName = $("input[type='text'][name='customer_name']").val().toString().trim();
        if (customerName != "") {
            billExtraInformation.customer_name = customerName;
        }
        var extraFields = [];
        $("input[type='text'].customer_info_field_name").each(function(cifk, cifv) {
            var extraFieldName = $(this).val().toString().trim();
            var extraFieldValue = $(this).parent().next().find("input[type='text'].customer_info_field_value").val().toString().trim();
            if (extraFieldName != "" && extraFieldValue != "") {
                var extraField = {
                    'field' : extraFieldName.toUpperCase(),
                    'value' : extraFieldValue
                };
                extraFields.push(extraField);
            }
        });
        billExtraInformation.extra_fields = extraFields;
    }
    return billExtraInformation;
}

// Check bill given to customer
function isBillGivenToCustomer() {
    var isBillGiven = false;
    if ($("input[type='checkbox'][name='is_bill_given_to_customer']").prop("checked")) {
        isBillGiven = true;
    }
    return isBillGiven;
}

function askFieldsForPrintOrder() {
    if (checkLogin()) {
        $("table.table-print-any-order-fields select[name='field_order_book_year']").html(getYearsSelectOptions(true, getCurrentDate('year')));
        popupModal("#modal-print-any-bill");
    } else {
        showLoginLayout(true);
    }
}

function askFCECardFields() {
    if (checkLogin()) {
        $(".table-fcECard-form-fields").show();
        $(".table-fcECard-info").hide();
        popupModal("#modal-fc-eCard");
    } else {
        showLoginLayout(true);
    }
}

function askFCECardFieldsForRecharge() {
    if (checkLogin()) {
        $("#modal-recharge-fc-eCard input[name='field_fcECardNumber']").val("");
        $("#modal-recharge-fc-eCard input[name='field_fcECardRechargeAmount']").val("");
        popupModal("#modal-recharge-fc-eCard");
    } else {
        showLoginLayout(true);
    }
}

function askFCECardFieldsForView() {
    if (checkLogin()) {
        popupModal("#modal-view-fc-eCard");
    } else {
        showLoginLayout(true);
    }
}

function generateAndshowFCeCard(field_fcECardDisplayName, field_fcECardPhoneNumber, field_fcECardEmail, field_fcECardInitRecharge, field_fcECardEnablePaymentVerification) {
    if (checkLogin()) {
        field_fcECardDisplayName = field_fcECardDisplayName.toString().trim();
        field_fcECardPhoneNumber = field_fcECardPhoneNumber.toString().trim();
        field_fcECardEmail = field_fcECardEmail.toString().trim();
        if (field_fcECardDisplayName.toString().trim() != '' && field_fcECardDisplayName.toString().trim().length > 8) {
            showNotification("Display name on card can not be longer than 8 characters", "error");
            isError = true;
        }
        if (field_fcECardPhoneNumber == '' && field_fcECardEmail == '') {
            showNotification("Please add either phone number or email for FC eCard", "error");
            return false;
        }
        if (field_fcECardEmail.toString().trim() != '' && !isValidEmail(field_fcECardEmail)) {
            showNotification("Please enter valid email for FC eCard", "error");
            return false;
        }
        if (field_fcECardInitRecharge.toString() == '' || isNaN(field_fcECardInitRecharge) || field_fcECardInitRecharge <= 0) {
            showNotification("Please fill initial recharge for the card", "error");
            return false;
        }
        toggleWaitingLoader('show');
        $(".table-fcECard-form-fields").hide();
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'generate-fc-ecard',
                restaurant_id : toNumeric(LocalDB.restaurant().id),
                display_name : field_fcECardDisplayName,
                linked_phone_number : field_fcECardPhoneNumber,
                linked_email : field_fcECardEmail,
                initial_recharge : field_fcECardInitRecharge,
                valid_for : 3,
                has_payment_verification : field_fcECardEnablePaymentVerification
            },
            function(response) {
                toggleWaitingLoader('hide');
                if (response.status == true) {
                    $("button.btn-print-fc-eCard").show();
                    $("td.fc-eCard-no").html(response.FCECard.ecard_no);
                    $("span.fc-eCard-valid_for").html(response.FCECard.valid_for);
                    $("td.fc-eCard-holder-display-name").html(response.FCECard.display_name);
                    $("span.fc-eCard-date").html(response.FCECard.created_at);
                    $('#block-fc-eCard').empty();
                    $('#block-fc-eCard').qrcode({
                        render: "table",
                        text: response.FCECard.ecard_no,
                        width: 120,
                        height: 120,
                        correctLevel: QRErrorCorrectLevel.H
                    });
                    $(".table-fcECard-info").show();
                    showFCeCardOption();
                    if ($("table.table-fc-ecard-report-office").css("display") != "none") {
                        loadFCECardReportForOffice({
                            'inBg' : true
                        });
                    }
                    EventHandler.notifyCustomerForFCCardInvoke(response.FCECard.ecard_no);
                } else {
                    showNotification(response.message, "error");
                }
            },
            function(error) {
                toggleWaitingLoader('hide');
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);   
    }
}

function generateMenuQR(options) {
    if (checkLogin()) {
        toggleWaitingLoader('show');
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'generate-restaurant-menu-qr',
                restaurant_id : toNumeric(LocalDB.restaurant().id),
                options : JSON.stringify(options)
            },
            function(response) {
                let fieldRestMenuQRSize = options.fieldRestMenuQRSize;
                let qrSize = {
                    width : 250,
                    height : 250
                };
                if (fieldRestMenuQRSize == 'medium') {
                    qrSize.width = 400;
                    qrSize.height = 400;
                } else if (fieldRestMenuQRSize == 'large') {
                    qrSize.width = 550;
                    qrSize.height = 550;
                } else if (fieldRestMenuQRSize == 'xsmall') {
                    qrSize.width = 180;
                    qrSize.height = 180;
                }
                toggleWaitingLoader('hide');
                if (response.status == true) {
                    // Generate QR
                    $("tr.restMenuQRCodeFieldBlock").hide();
                    if (options.fieldRestMenuQRText != '') {
                        $("span.restMenuQRCodeCustomText").html(options.fieldRestMenuQRText.toUpperCase());
                    } else {
                        $("span.restMenuQRCodeCustomText").html("VIEW MENU");
                    }
                    $("span.restMenuQRCodeCustomText").css("width", qrSize.width + "px");
                    $("span.restMenuQRCodeCustomText").css("font-size", parseInt((qrSize.width / 15)) + "px");
                    $("#modal-print-rest-menu-qr").css("min-width", (qrSize.width + 10) + "px");
                    $("div.restMenuQRCodeImg").empty();
                    $('div.restMenuQRCodeImg').qrcode({
                        render: "table",
                        text: response.menu_url,
                        width: qrSize.width,
                        height: qrSize.height,
                        correctLevel: QRErrorCorrectLevel.H
                    });
                    $("tr.restMenuQRCodeImgBlock").show();
                } else {
                    $("#modal-print-rest-menu-qr").css("min-width", "300px");
                    $("tr.restMenuQRCodeFieldBlock").show();
                    $("tr.restMenuQRCodeImgBlock").hide();
                    showNotification(response.message, "error");
                }
            },
            function(error) {
                toggleWaitingLoader('hide');
                $("#modal-print-rest-menu-qr").css("min-width", "300px");
                $("tr.restMenuQRCodeFieldBlock").show();
                $("tr.restMenuQRCodeImgBlock").hide();
                showNotification(error.responseText, "error");
            }
        );
    } else {
        showLoginLayout(true);
    }
}

function printInvoice() {
    if (checkLogin()) {
        if ($("#modal-invoice").css("display") == "none") {
            showNotification("No bill/invoice to print", "error");
            return false;
        }
        $("#modal-invoice button").hide();
        // $("img.invoice-logo").hide();
        $('#modal-invoice').css('border', 'none');
        $('#modal-invoice').printThis({
            importCSS: true,
            loadCSS: "assets/css/print.css",
            base: window.location.href,
            afterPrint: function() {
                hideModal();
            }
        });
    } else {
        showLoginLayout(true);
    }
}

function printFCeCard() {
    if (checkLogin()) {
        if ($("#modal-fc-eCard").css('display') == 'none') {
            return false;
        }
        $(".btn-print-fc-eCard").hide();
        $("img.invoice-logo").hide();
        $('#modal-fc-eCard').css('border', 'none');
        $('#modal-fc-eCard').printThis({
            importCSS: true,
            loadCSS: "assets/css/printfcecard.css",
            base: window.location.href,
            afterPrint: function() {
                hideModal();
            }
        });
    } else {
        showLoginLayout(true);
    }
}

function printRestQR() {
    if (checkLogin()) {
        if ($("#modal-print-rest-menu-qr").css('display') == 'none') {
            return false;
        }
        $('div.restMenuQRCodeHolder').css('border', 'none');
        $('div.restMenuQRCodeHolder').printThis({
            importCSS: true,
            loadCSS: "assets/css/printrestmenuqr.css",
            base: window.location.href,
            afterPrint: function() {
                hideModal();
            }
        });
    } else {
        showLoginLayout(true);
    }
}

// Save category to cloud
function saveCategoryToCloud(category) {
    if (LocalDB.loggedInAs().loginAs != "owner" && LocalDB.loggedInAs().loginAs != "counter" && LocalDB.loggedInAs().loginAs != "office") {
        showNotification(NO_ACCESS_MSG, "error");
        return false;
    }
    toggleWaitingLoader("show");
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'save-category',
                restaurant_id : toNumeric(RESTAURANT.ID),
                category : JSON.stringify(category)
            },
            function(response) {
                if (response.status == true) {
                    toggleWaitingLoader("hide");
                    if (response.category.id > 0) {
                        LocalDB.saveCategory(response.category);
                        if (withAccess("office")) {
                            $(".btn-office-action[data-action='categories']").trigger("click");
                        } else {
                            loadCategories(LocalDB.categories());
                        }
                        showNotification(response.message, "success");
                        hideModal();
                        clearBoneForms();
                    } else {
                        showNotification(OOPS_MSG, "error");    
                    }
                } else {
                    showNotification(response.message, "error");
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

// Save item to cloud
function saveItemToCloud(item) {
    if (LocalDB.loggedInAs().loginAs != "owner" && LocalDB.loggedInAs().loginAs != "counter" && LocalDB.loggedInAs().loginAs != "office") {
        showNotification(NO_ACCESS_MSG, "error");
        return false;
    }
    toggleWaitingLoader("show");
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'save-item',
                restaurant_id : toNumeric(RESTAURANT.ID),
                item : JSON.stringify(item)
            },
            function(response) {
                if (response.status == true) {
                    toggleWaitingLoader("hide");
                    if (response.item.id > 0) {
                        hideModal();
                        LocalDB.saveItem(response.item, response.item.category_id, true);
                        if (withAccess("office")) {
                            $(".btn-office-action[data-action='items']").trigger("click");
                        }
                        loadItems(response.item.category_id);
                        loadCategories(LocalDB.categories());
                        showNotification(response.message, "success");
                        clearBoneForms();
                    } else {
                        showNotification(OOPS_MSG, "error");
                    }
                } else {
                    toggleWaitingLoader("hide");
                    showNotification(response.message, "error");
                }
            },
            function(error) {
                // No need to showcase our own mistakes
            }
        );
    } else {
        showLoginLayout(true);
    }
}

// load table based discounts
function loadTableDiscounts() {
    var tableDiscounts = LocalDB.discounts();
    $("table.list-table-discounts").empty();
    if (tableDiscounts.length > 0) {
        $("table.list-table-discounts").parent().show();
        $(tableDiscounts).each(function(k, discount) {
            if (discount.tableId != null && discount.tableId == LocalDB.getActiveTable()) {
                var tableDiscountRow = "<tr>";
                tableDiscountRow+="<td class='dotted-border-sm text-sm'>";
                tableDiscountRow+= discount.description;
                tableDiscountRow+="</td>";
                tableDiscountRow+="<td class='dotted-border-sm text-sm'>";
                tableDiscountRow+= discount.amount + " (" + discount.type + ")";
                tableDiscountRow+="</td>";
                tableDiscountRow+="<td class='dotted-border-sm'>";
                tableDiscountRow+="<button class='btn-solid btn-remove-table-discount' data-id='" + discount.id + "' data-tableid='" + discount.tableId + "' title='Remove Discount'>X</button>";
                tableDiscountRow+="</td>";
                tableDiscountRow+= "</tr>";
                $("table.list-table-discounts").append(tableDiscountRow);
            }
        });
    } else {
        $("table.list-table-discounts").parent().hide();
    }
}

// Load table based taxes
function loadTableTaxes() {
    var tableTaxes = LocalDB.taxes();
    $("table.list-table-taxes").empty();
    if (tableTaxes.length > 0) {
        $("table.list-table-taxes").parent().show();
        $(tableTaxes).each(function(k, tax) {
            if (tax.tableId != null && tax.tableId == LocalDB.getActiveTable()) {
                var tableTaxRow = "<tr>";
                tableTaxRow+="<td class='dotted-border-sm text-sm'>";
                tableTaxRow+= tax.name;
                tableTaxRow+="</td>";
                tableTaxRow+="<td class='dotted-border-sm text-sm'>";
                tableTaxRow+= tax.amount + " (" + tax.type + ")";
                tableTaxRow+="</td>";
                tableTaxRow+="<td class='dotted-border-sm'>";
                tableTaxRow+="<button class='btn-solid btn-remove-table-tax' data-id='" + tax.id + "' data-tableid='" + tax.tableId + "' title='Remove Tax'>X</button>";
                tableTaxRow+="</td>";
                tableTaxRow+= "</tr>";
                $("table.list-table-taxes").append(tableTaxRow);
            }
        });
    } else {
        $("table.list-table-taxes").parent().hide();
    }
}

// Load restaurant statistics
function loadStatistics() {
    // if (LocalDB.loggedInAs().loginAs != "office") {
    //     showNotification(NO_ACCESS_MSG, "error");
    //     return false;
    // }
    $("table.table-stats").empty();
    toggleWaitingLoader("show");
    if (checkLogin()) {
        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
            {
                action : 'get-stats',
                restaurant_id : toNumeric(RESTAURANT.ID)
            },
            function(response) {
                toggleWaitingLoader("hide");
                showStatisticsOption();
                if (response.status == true) {
                    $("h4 span.report-date").html(response.stats.report_date);
                    var totalOrdersInfo = response.stats.total_orders_info;
                    var totalCashOrdersInfo = response.stats.total_cash_orders_info;
                    var totalCashRechargeInfo = response.stats.total_cash_recharge_info;
                    var totalCashNewCardsInfo = response.stats.total_cash_new_fc_ecards_info;
                    var totalPaymentMethodsInfo = response.stats.total_payment_methods_info;
                    var lastOrderInfo = response.stats.last_order_info;
                    var statLine = '<tr>';
                    statLine+= '<td class="dotted-border-sm text-left">Today orders amount('+ RESTAURANT.CURRENCY +')</td>';
                    statLine+= '<td class="dotted-border-sm">'+ totalOrdersInfo.order_amount + ' ' + RESTAURANT.CURRENCY +'</td>';
                    statLine+= '</tr>';
                    statLine+= '<tr>';
                    statLine+= '<td class="dotted-border-sm text-left">Today cash orders collection('+ RESTAURANT.CURRENCY +')</td>';
                    let totalCashOrdersAmount = (typeof totalCashOrdersInfo.order_amount != 'undefined' && totalCashOrdersInfo.order_amount != null && !isNaN(totalCashOrdersInfo.order_amount)) ? totalCashOrdersInfo.order_amount : 0;
                    statLine+= '<td class="dotted-border-sm">'+ totalCashOrdersAmount + ' ' + RESTAURANT.CURRENCY +'</td>';
                    statLine+= '</tr>';
                    statLine+= '<tr>';
                    statLine+= '<td class="dotted-border-sm text-left">Today cash fcECard recharge collection('+ RESTAURANT.CURRENCY +')</td>';
                    let totalCashRechargeAmount = (typeof totalCashRechargeInfo.total_cash_recharge_amount != 'undefined' && totalCashRechargeInfo.total_cash_recharge_amount != null && !isNaN(totalCashRechargeInfo.total_cash_recharge_amount)) ? totalCashRechargeInfo.total_cash_recharge_amount : 0;
                    statLine+= '<td class="dotted-border-sm">'+ totalCashRechargeAmount + ' ' + RESTAURANT.CURRENCY +'</td>';
                    statLine+= '</tr>';
                    statLine+= '<tr>';
                    statLine+= '<td class="dotted-border-sm text-left">Today (NEW) fcECard initial recharge collection('+ RESTAURANT.CURRENCY +')</td>';
                    let totalCashNewCardsAmount = (typeof totalCashNewCardsInfo.total_cash_init_recharge_amount != 'undefined' && totalCashNewCardsInfo.total_cash_init_recharge_amount != null && !isNaN(totalCashNewCardsInfo.total_cash_init_recharge_amount)) ? totalCashNewCardsInfo.total_cash_init_recharge_amount : 0;
                    statLine+= '<td class="dotted-border-sm">'+ totalCashNewCardsAmount + ' ' + RESTAURANT.CURRENCY +'</td>';
                    statLine+= '</tr>';
                    statLine+= '<tr>';
                    statLine+= '<td class="dotted-border-sm text-left">Today total orders</td>';
                    statLine+= '<td class="dotted-border-sm">'+ totalOrdersInfo.total_orders +'</td>';
                    statLine+= '</tr>';
                    statLine+= '<tr>';
                    if (totalPaymentMethodsInfo.length > 0) {
                        $(totalPaymentMethodsInfo).each(function(pmkey, paymentMethod) {
                            statLine+= '<tr>';
                            statLine+= '<td class="dotted-border-sm text-left">';
                            statLine+= 'By '+ paymentMethod.payment_method;
                            statLine+= '</td>';
                            statLine+= '<td class="dotted-border-sm">';
                            statLine+= paymentMethod.total_orders;
                            statLine+= '</td>';
                            statLine+= '</tr>';
                        });
                    }
                    if (typeof lastOrderInfo.order_no != 'undefined') {
                        statLine+= '<tr>';
                        statLine+= '<td class="dotted-border-sm text-left">';
                        statLine+= 'Last order # ' + lastOrderInfo.order_no;
                        statLine+= '</td>';
                        statLine+= '<td class="dotted-border-sm">';
                        statLine+= lastOrderInfo.order_amount + ' ' + RESTAURANT.CURRENCY + " by " + lastOrderInfo.payment_method;
                        statLine+= '</td>';
                        statLine+= '</tr>';
                    }
                    $("table.table-stats").append(statLine);
                } else {
                    showNotification(response.message, "error");
                    hideModal();
                }
            },
            function(error) {
                // No need to showcase our own mistakes
                hideModal();
            },
        );
    } else {
        showLoginLayout(true);
    }
}

// Check access level for table view
function withTableAccess() {
    if ($.inArray(LocalDB.loggedInAs().loginAs, ["owner", "manager", "waiter", "counter"]) >= 0) {
        return true;
    }
    return false;
}

// Check access is matching with given role
function withAccess(role = 'noaccess') {
    if ($.inArray(LocalDB.loggedInAs().loginAs, [role]) >= 0) {
        return true;
    }
    return false;
}

// Logout from the app
function logout() {
    if (!confirm('You will loose cart items after logout, Are you sure want to exit from the application?')) {
        return false;
    }
    callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
        {
            action : 'logout'
        },
        function(response) {
            // Behind the scenes
        },
        function(error) {
            // No need to showcase our own mistakes
        }
    );
    $(".footer").hide();
    LocalDB.clearAll();
    showLoginLayout(true, 'You have logged out from the application. You can login back anytime.');
}

// Validations to check for eligible FCECardNumber
function isFCECardNumberValid(FCECardNumber) {
    if (FCECardNumber.length <= 0 || !FCECardNumber.startsWith("FC")) {
        return false;
    }
    return true;
}

function checkElementEmpty(elem) {
    if ($(elem).val().toString().trim() == '') {
        return true;
    }
    return false;
}

function resetProceedPaymentBtn() {
    $(".btn-process-payment").html("Proceed");
    $("button.btn-process-payment").removeAttr("disabled");
}

function resetPaymentForm() {
    $("input[type='checkbox'].checkout_options").each(function(k, v) {
        $(this).prop('checked', false);
    });
    $("input[type='checkbox'][name='have_customer_info']").prop("checked", false);
    $("input[type='text'][name='customer_email']").val('');
    $("input[type='text'][name='customer_phone']").val('');
    $("input[type='text'][name='customer_name']").val('');
    $("input[type='text'].customer_info_field_name").val('');
    $("input[type='text'].customer_info_field_value").val('');
    $("tr.customer-info-holder").hide();
    $("tr.section_set_customer_info_for_invoice").hide();
    $("span.info-scan-fccard").removeClass('d-block-f');
    $(".btn-process-payment").show();
}

function resetAskForItemQtyModal() {
    $("input[type='checkbox'][name='has_specifications']").prop("checked", false);
    $("input[type='checkbox'][name='has_specifications']").trigger("change");
    $("input[type='checkbox'].spefications_taste").each(function() {
        $(this).prop("checked", false);
    });
    $("input[type='checkbox']#specification_taste_regular").prop("checked", true);
    $("textarea[name='specification_text']").val("");
}

function getCategoriesSelectOptions() {
    var categories = LocalDB.categories();
    var options = "";
    $(categories).each(function(ckey, category) {
        if (category.name != '') {
            options+= "<option value='" + category.id + "'>" + category.name + "</option>";
        }
    });
    return options;
}

function getAreaCategoriesSelectOptions() {
    var areaCategories = LocalDB.areaCategories();
    var options = "";
    $(areaCategories).each(function(ackey, areaCategory) {
        if (areaCategory.name != '') {
            options+= "<option value='" + areaCategory.id + "'>" + areaCategory.name + "</option>";
        }
    });
    return options;   
}

function getTableSelectOptions(placeholder = false, defaultValue = '') {
    var tables = LocalDB.tables();
    var options = "";
    if (placeholder) {
        options+= "<option value=''>Select</option>";
    }
    $(tables).each(function(tkey, table) {
        if (table.table_no != '') {
            let tableAreaCategory = LocalDB.getAreaCategoryInfo(table.area_category_id);
            let optionTitle = table.table_no;
            if (typeof tableAreaCategory != 'undefined' && tableAreaCategory.name != '') {
                optionTitle+= ' (' + tableAreaCategory.name + ')';
            }
            var selected = "";
            if (defaultValue != '' && defaultValue == table.id) {
                selected = "selected='selected'";
            }
            options+= "<option value='" + table.id + "' "+ selected +">" + optionTitle + "</option>";
        }
    });
    return options;
}

function getPaymentMethodsSelectOptions(placeholder = false, defaultValue = '') {
    var paymentMethods = LocalDB.paymentMethods();
    var options = "";
    if (placeholder) {
        options+= "<option value=''>Select</option>";
    }
    $(paymentMethods).each(function(tkey, paymentMethod) {
        if (paymentMethod.payment_method != '') {
            var selected = "";
            if (defaultValue != '' && defaultValue == paymentMethod.payment_method) {
                selected = "selected='selected'";
            }
            options+= "<option value='" + paymentMethod.id + "' "+ selected +">" + paymentMethod.payment_method + "</option>";
        }
    });
    return options;
}

function getCurrencySelectOptions(placeholder = false, defaultValue = '') {
    var currencies = LocalDB.currencies();
    var options = "";
    if (placeholder) {
        options+= "<option value=''>Select</option>";
    }
    $(currencies).each(function(curkey, currency) {
        if (currency.name != '') {
            var selected = "";
            if (defaultValue != '' && defaultValue == currency.id) {
                selected = "selected='selected'";
            }
            options+= "<option value='" + currency.id + "' "+ selected +">" + currency.code + " [" + currency.symbol + "]</option>";
        }
    });
    return options;
}

function getMonthsSelectOptions(placeholder = false, defaultValue = '') {
    var options = "";
    if (placeholder) {
        options+= "<option value=''>Select</option>";
    }
    $(CALENDER_MONTHS).each(function(mIndex, month) {
        let value = (mIndex + 1);
        var selected = "";
        if (defaultValue != '' && defaultValue == value) {
            selected = "selected='selected'";
        }
        options+= "<option value='" + (value) + "' " + selected + ">" + month + "</option>";
    });
    return options;
}

function getYearsSelectOptions(placeholder = false, defaultValue = '') {
    var options = "";
    if (placeholder) {
        options+= "<option value=''>Select</option>";
    }
    for (var year = 2021; year <= new Date().getFullYear(); year++) {
        var selected = "";
        if (defaultValue != '' && defaultValue == year) {
            selected = "selected='selected'";
        }
        options+= "<option value='" + year + "' " + selected + ">" + year + "</option>";
    }
    return options;
}

function popupModal(element) {
    $(".modal").hide();
    $(element).parent().removeClass('show').addClass('show');
    $(element).show();
    applyBounceEffect(element, 'tb');
    showModalCloseButton();
    // scrollToTop();
    if (!isMobileScreen()) {
        $(element + " input[type='text']:first").focusTextToEnd();
    }
}

function showModalCloseButton() {
    $("#modal-container .btn-close-modal").each(function() {
        $(this).remove();
    });
    $("#modal-container .modal").each(function() {
        if ($(this).css("display") == "block") {
            // $(this).prepend("<button class='btn-close-modal'>X</button>");
            $("#modal-container").prepend("<button class='btn-close-modal'>X</button>");
        }
    });
}

function hideModal() {
    if (!PAYMENT_IN_PROCESS) {
        $(".modal-container").removeClass("show");
    }
}

function hideModalByForce() {
    $(".modal-container").removeClass("show");
}

function generateNoDataFoundBlock(columns) {
    var noDataFound = '<tr class="no-data-holder">';
    noDataFound+= '<td colspan="'+ columns +'">';
    noDataFound+= '<div class="no-data-msg text-center text-white opacity-half">';
    noDataFound+= '<img src="assets/img/icons/icon-empty-box.png" />';
    noDataFound+= '<h5 class="mt-0 mb-p5-f">No data found</h5>';
    noDataFound+= '</div>';
    noDataFound+= '</td>';
    noDataFound+= '</tr>';
    return noDataFound;
}

function isMobile() {
    return (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|android|ipad|playbook|silk/i.test(navigator.userAgent || navigator.vendor || window.opera) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test((navigator.userAgent || navigator.vendor || window.opera).substr(0, 4)));
}

function isMobileScreen() {
    return window.matchMedia('(max-width: 600px)').matches;
}

function isiPadScreen() {
    return window.matchMedia('(device-width: 768px)').matches && window.matchMedia('(device-height: 1024px)').matches;
}

function scrollToTop() {
    $([document.documentElement, document.body]).animate({
        scrollTop: 0
    }, 50);
}

function toggleWaitingLoader(action = 'hide', isPaymentRelated = false) {
    $(".msg-do-not-refresh").hide();
    if (action == 'show') {
        if (isPaymentRelated) {
            $(".msg-do-not-refresh").show();
        }
        $(".overlay-loader").show();
    } else {
        $(".overlay-loader").hide();
    }
}

function getLoaderImg() {
    return '<img src="assets/img/icons/icon-cloud-sync-in-process.gif" />';
}

const checkOnlineStatus = async () => {
    return true;
};

function getCurrentDate(param = '') {
    var timeElapsed = Date.now();
    var today = new Date(timeElapsed);
    switch(param) {
        case 'month':
            return today.getMonth() + 1;
            break;
        case 'year':
            return today.getFullYear();
            break;
        default:
            return today.toDateFormat();
            break;
    }
}

function loadRZPayCheckoutScript() {
    if(typeof Razorpay != 'undefined') {
        // Razorpay already loaded 
    } else {
        let rzPayCheckoutScript = document.createElement("script");
        rzPayCheckoutScript.setAttribute("src", "assets/js/rzpay.checkout.js");
        document.body.appendChild(rzPayCheckoutScript);
    }
}

function isFocusOnField(elem) {
    if ($(elem).is("input") || $(elem).is("textarea")) {
        return true;
    }
    return false;
}

function showNoSearchResultFoundToSearch() {
    var noResultFoundBlock = '<tr class="search-result-line">';
    noResultFoundBlock+= '<td class="search-picture bg-theme"><img src="assets/img/icons/icon-no-food.png" class="icon-small opacity-half" /></td>';
    noResultFoundBlock+= '<td class="search-name">No search result found <span class="d-block text-grey-dark text-x-sm">Search table/item/category</span></td>';
    noResultFoundBlock+= '</tr>';
    $("table.list-search-result").append(noResultFoundBlock);
}

function showNoSearchResultFoundToSearchForOffice(moduleAs = 'results') {
    var noResultFoundBlock = '<tr class="search-result-line">';
    noResultFoundBlock+= '<td class="search-picture bg-theme"><img src="assets/img/icons/icon-no-food.png" class="icon-small opacity-half" /></td>';
    noResultFoundBlock+= '<td class="search-name">No ' + moduleAs + ' found <span class="d-block text-grey-dark text-x-sm restaurant-name"></span></td>';
    noResultFoundBlock+= '</tr>';
    $("table.list-search-result").append(noResultFoundBlock);
}

function clearBoneForms() {
    $("#modal-add-bone input,textarea,select").val("");
}

function setAssetsToInitState() {
    $("button.btn-process-payment").html("Proceed");
    $("button.btn-process-payment").removeAttr("disabled");
    $("img.sync-loader-icon").attr("src", "assets/img/icons/icon-cloud-sync.png");
}

function getRestaurantFoodTypesIcon() {
    var foodTypes = LocalDB.getRestaurantFoodTypes();
    var iconsHtml = '';
    $(foodTypes).each(function(fk, type) {
        if (type == 'V')
            iconsHtml+= "<img src='assets/img/icons/icon-veg.png' class='d-inline-block-f bg-white valign-middle' />";
        if (type == 'NV')
            iconsHtml+= "<img src='assets/img/icons/icon-non-veg.png' class='d-inline-block-f bg-white valign-middle' />";
    });
    return iconsHtml;
}

function getRestaurantFoodTypesBadge() {
    var foodTypes = LocalDB.getRestaurantFoodTypes();
    var typesBadge = [];
    $(foodTypes).each(function(fk, type) {
        if (type == 'V')
            typesBadge.push('Veg.');
        else if (type == 'NV')
            typesBadge.push('Non Veg.');
    });
    if (typesBadge.length > 0) {
        return typesBadge.join(", ");
    } else {
        return 'Restaurant';
    }
}

function toNumeric(number) {
    if (typeof number == "undefined") {
        return "";
    }
    var number = number.toString().replace(",", "");
    return number * 1;
}

function playNotification(type = 'general') {
    switch(type) {
        case 'general':
            break;
        case 'kitchenalert':
            // kitchenAlertSound.play();
            break;
        default:
            break;
    }
}

function generateDarkColorHex() {
    var lum = -0.25;
    var hex = String('#' + Math.random().toString(16).slice(2, 8).toUpperCase()).replace(/[^0-9a-f]/gi, '');
    if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    var rgb = "#",
        c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00" + c).substr(c.length);
    }
    return rgb;
}

function generateLightColorHex() {
  let color = "#";
  for (let i = 0; i < 3; i++)
    color += ("0" + Math.floor(((1 + Math.random()) * Math.pow(16, 2)) / 2).toString(16)).slice(-2);
  return color;
}

function applyBounceEffect(element, direction = 'lr') {
    animateBounce(element, 1, '10px', 200, direction);
}

function animateBounce(element, times, distance, speed, direction) {
    for(var i = 0; i < times; i++) {
        if (direction == 'tb') {
            $(element).animate({marginTop: '-='+distance}, speed)
                .animate({marginTop: '+='+distance}, speed);
        } else {
            $(element).animate({marginLeft: '-='+distance}, speed)
                .animate({marginLeft: '+='+distance}, speed);
        }
    }
}

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}

function isValidEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function isEmpty(element){
  return !$.trim(element.html());
}

String.prototype.limitedText = function(length = 9) {
    if (this.length > length) {
        return this.substring(0, length) + "...";
    } else {
        return this;
    }
}

$.fn.focusTextToEnd = function() {
    this.focus();
    var $thisVal = this.val();
    this.val('').val($thisVal);
    return this;
}

$.fn.digits = function() { 
    return this.each(function() { 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}

String.prototype.toRound = function() { 
    var number = toNumeric(this);
    return toNumeric((Math.round(number * 100) / 100).toFixed(2).toNumberFormat());
}

Number.prototype.toRound = function() { 
    var number = toNumeric(this);
    return (Math.round(number * 100) / 100).toFixed(2).toNumberFormat();
}

String.prototype.toNumberFormat = function() { 
    return this.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

Number.prototype.toNumberFormat = function() { 
    let number = this.toString();
    let formattedNumber = number.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return formattedNumber;
}

String.prototype.toDateFormat = function() {
    var date = this.toString();
    var parsedDate = new Date(date);
    let dateString = parsedDate.getDate() + " " + CALENDER_MONTHS[parsedDate.getMonth()] + " " + parsedDate.getFullYear();
    var timeString = ""; //parsedDate.getHours() + ":" + parsedDate.getMinutes();
    return dateString + " " + timeString;
}

Date.prototype.toDateFormat = function() {
    let date = this;
    var parsedDate = new Date(date);
    let dateString = parsedDate.getDate() + " " + CALENDER_MONTHS[parsedDate.getMonth()] + " " + parsedDate.getFullYear();
    var timeString = ""; //parsedDate.getHours() + ":" + parsedDate.getMinutes();
    return dateString + " " + timeString;
}

String.prototype.isJSON = function() {
    let str = this;
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}