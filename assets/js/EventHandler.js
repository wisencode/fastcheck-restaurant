class EventHandler {
    static channelPrefix = 'fastcheck-event-';
    static pusherObj = null;
    static fcEchoObj = null;
    static fcEchoLogToTerminal = false;
    static restaurantId;
    static channelTable = {
        name : this.channelPrefix + 'table',
        events : [
            'status-updated',
            'cart-updated',
            'push-to-kitchen',
            'new-kot',
            'mark-items-as-served',
            'cart-cleared',
        ]
    };
    static channelCommands = {
        name : this.channelPrefix + 'commands',
        events : [
            'sync-categories',
            'sync-items',
            'sync-restaurant',
            'sync-logo',
            'sync-discounts',
            'sync-taxes',
            'sync-tables',
            'sync-area-categories',
            'sync-order-received',
            'sync-order-updated',
            'sync-order-deleted',
        ]
    };

    constructor(restaurantId) {
        this.restaurantId = restaurantId;
        if (typeof LocalDB.get('pusher_api_key') != 'undefined' && LocalDB.get('pusher_api_key') != '' && LocalDB.get('pusher_api_key') != 'null') {
            Pusher.logToConsole = false;
            this.pusherObj = new Pusher(LocalDB.get('pusher_api_key'), {
                cluster: 'ap2'
            });
            this.subscribe(EventHandler.channelTable);
            this.subscribe(EventHandler.channelCommands);
        } else if (typeof LocalDB.get('fc_echo_secret') != 'undefined' && LocalDB.get('fc_echo_secret') != '' && LocalDB.get('fc_echo_secret') != 'null') {
            this.fcEchoLogToTerminal = false;
            this.fcEchoObj = new WebSocket(fcEchoSystemProtocol + '://'+ fcEchoSystemEndPoint + restaurantId + '/' + LocalDB.get('fc_echo_secret'));
            this.bindFCEcho();
        }
    }

    subscribe(channel) {
        var eventHandlerObj = this;
        var channelName = channel.name.toString().replace(EventHandler.channelPrefix, EventHandler.channelPrefix +
            "rest-" + this.restaurantId + "-");
        var eventChannel = this.pusherObj.subscribe(channelName);
        $(channel.events).each(function(eventKey, eventName) {
            eventHandlerObj.bindEvent(eventName, eventChannel);
        });
    }

    bindFCEcho() {
        this.fcEchoObj.onopen = () => {
          this.printFCEchoLog("You are connected");
        }
        this.fcEchoObj.onerror = (error) => {
          this.printFCEchoLog("Connection problem...");
        }
        this.fcEchoObj.onmessage = (event) => {
          if (event.data.isJSON()) {
            const receivedPayload = JSON.parse(event.data);
            if (typeof receivedPayload != 'undefined' && typeof receivedPayload.command != 'undefined' && receivedPayload.command != '') {
                this.printFCEchoLog(receivedPayload);
                this.perform(receivedPayload.command, receivedPayload.data);
            } else { 
                this.printFCEchoLog(receivedPayload.message);
            }
          } else {
            this.printFCEchoLog(event.data);
          }
        }
    }

    printFCEchoLog(log) {
        if (this.fcEchoLogToTerminal) {
            console.log("FCEcho: ", log);
        }
    }

    bindEvent(eventName, channel) {
        var eventHandlerObj = this;
        switch(eventName) {
            case 'status-updated':
                channel.bind(eventName, function(data) {
                  eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'cart-updated':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                    // let isDataInChunk = (typeof data.isChunk != 'undefined' && data.isChunk == true) ? true : false;
                    // let finalData = data;
                    // if (isDataInChunk) {
                    //     finalData = '';
                    //     if (typeof NOTIFICATION_CHUNKS[data.chunkId] == 'undefined' || NOTIFICATION_CHUNKS[data.chunkId].length == 0) {
                    //         NOTIFICATION_CHUNKS[data.chunkId] = [];
                    //     }
                    //     NOTIFICATION_CHUNKS[data.chunkId].push(data.chunkBlock);
                    //     if (data.isFinalChunk == true) {
                    //         $(NOTIFICATION_CHUNKS[data.chunkId]).each(function(k, v) {
                    //             finalData+= v;
                    //         });
                    //         finalData = JSON.parse(finalData);
                    //         NOTIFICATION_CHUNKS[data.chunkId] = [];
                    //     }
                    // }
                    
                    // if (!isDataInChunk || data.isFinalChunk == true) {
                    //     if (toNumeric(finalData.restaurant_id) == toNumeric(RESTAURANT.ID) && withTableAccess()) {
                    //         var cartItems = JSON.parse(finalData.cartInfo);
                    //         if (cartItems.length == 0 && toNumeric(finalData.tableId) > 0) {
                    //             LocalDB.set(LocalDB.getCartName(toNumeric(finalData.tableId)), finalData.cartInfo);
                    //             renderCart();
                    //         } else {
                    //             $(cartItems).each(function(k, item) {
                    //                 if (LocalDB.items(item.categoryId).length == 0) {
                    //                     loadItems(item.categoryId);
                    //                 }
                    //                 if (k == (cartItems.length - 1)) {
                    //                     LocalDB.set(LocalDB.getCartName(toNumeric(finalData.tableId)), finalData.cartInfo);
                    //                     renderCart();
                    //                 }
                    //             });
                    //         }
                    //     }
                    // }
                });
                break;
            case 'push-to-kitchen':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'new-kot':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'mark-items-as-served':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'cart-cleared':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-categories':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-items':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-restaurant':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-logo':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-discounts':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-taxes':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-tables':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-area-categories':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-order-received':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-order-updated':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            case 'sync-order-deleted':
                channel.bind(eventName, function(data) {
                    eventHandlerObj.perform(eventName, data);
                });
                break;
            default:
                break;
        }
    }

    perform(command, data) {
        switch(command) {
            case 'status-updated':
                if (data.table.restaurant_id == toNumeric(RESTAURANT.ID) && withTableAccess()) {
                    var table = LocalDB.getTableInfo(toNumeric(data.table.table_id));
                    if (typeof table != 'undefined' && table.status != data.table.table_status) {
                        LocalDB.setTableStatus(table.id, data.table.table_status);
                        var table = LocalDB.getTableInfo(toNumeric(table.id));
                        if (data.table.table_status == "A") {
                            showNotification("Table " + table.table_no + " is available now", "info");
                        } else if (data.table.table_status == "B") {
                            // showNotification("Table " + table.table_no + " is booked now", "info");
                        }
                    }
                  }
                  loadTables(LocalDB.tables());
                break;
            case 'cart-updated':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID) && withTableAccess() && toNumeric(data.tableId) > 0) {
                    setLiveTableCartItems(toNumeric(data.tableId));
                }
                break;
            case 'push-to-kitchen':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID) && withAccess("kitchen")) {
                    var items = JSON.parse(data.items);
                    if (items.length > 0) {
                      var itemIds = [];
                      $(items).each(function(k, item) {
                        var item = {
                            'itemId' : toNumeric(item.itemId),
                            'specifications' : item.specifications,
                            'qty' : item.qty,
                            'tableId' : toNumeric(data.tableId),
                            'logId' : LocalDB.kitchenItems().length + 1
                        };
                        LocalDB.saveItemForKitchen(item);
                        itemIds.push(item.itemId);
                      });
                      var kitchenItems = LocalDB.kitchenItems();
                      var availableKitchenItems = [];
                      $(kitchenItems).each(function(k, v) {
                         if (v.tableId == data.tableId) {
                            if ($.inArray(v.itemId, itemIds) >= 0) {
                                availableKitchenItems.push(v);
                            }
                         } else {
                            availableKitchenItems.push(v);
                         }
                      });
                      LocalDB.set(LocalDB.kitchenItemsInfo, JSON.stringify(availableKitchenItems));
                      renderKitchenLog();
                      playNotification("kitchenalert");
                    }
                }
                break;
            case 'new-kot':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID) && withAccess("kitchen")) {
                    if (toNumeric(data.table_id) > 0) {
                        $(".kot-log-" + data.table_id).removeClass("blink").addClass("blink");
                        $(".kot-log-" + data.table_id).removeClass("bg-theme").addClass("bg-kot-updated-block");
                        $("button.btn-refresh-kitchen-log").removeClass("blink").addClass("blink");
                        playNotification("kitchenalert");
                    }
                }
                break;
            case 'mark-items-as-served':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID) && withTableAccess()) {
                    var itemsInfo = JSON.parse(data.itemsInfo);
                    var taste = data.taste;
                    var extraSpecification = data.extraSpecification;
                    LocalDB.markItemAsServed(itemsInfo, taste, extraSpecification);
                }
                break;
            case 'cart-cleared':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                    if (withAccess("kitchen")) {
                        LocalDB.removeTableKitchenLog(toNumeric(data.tableId));
                        renderKitchenLog();
                    }
                    if (withTableAccess()) {
                        var tableCartName = LocalDB.cartName + '.table-' + toNumeric(data.tableId);
                        LocalDB.set(tableCartName, JSON.stringify([]));
                        renderCart();
                        LocalDB.setTableStatus(toNumeric(data.tableId), 'A');
                    }
                }
                break;
            case 'sync-categories':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                    if (withAccess("kitchen") || withTableAccess()) {
                        syncCategories(toNumeric(data.restaurant_id));
                    }
                }
                break;
            case 'sync-items':
                if (toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                    if (withAccess("kitchen") || withTableAccess()) {
                        syncItems(true);
                        loadItems(LocalDB.items());
                        renderCart();
                    }
                }
                break;
            case 'sync-restaurant':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        initRestaurantDetails(toNumeric(data.restaurant_id), LocalDB.loggedInAs().loginAs, true);
                    }
                }
                break;
            case 'sync-logo':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        var restaurant = LocalDB.restaurant();
                        restaurant.logo = data.logo;
                        LocalDB.set(LocalDB.restaurantInfo, JSON.stringify(restaurant));
                        setRestaurantInfo(restaurant);
                    }
                }
                break;
            case 'sync-discounts':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        LocalDB.set(LocalDB.discountInfo, JSON.stringify([]));
                        if (withTableAccess()) {
                            syncDiscounts(toNumeric(data.restaurant_id));
                        }
                    }
                }
                break;
            case 'sync-taxes':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        LocalDB.set(LocalDB.taxInfo, JSON.stringify([]));
                        if (withTableAccess()) {
                            syncTaxes(toNumeric(data.restaurant_id));
                        }
                    }
                }
                break;
            case 'sync-tables':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        LocalDB.set(LocalDB.areaCategoryInfo, JSON.stringify([]));
                        LocalDB.set(LocalDB.tableInfo, JSON.stringify([]));
                        if (withTableAccess() || withAccess("office")) {
                            syncAreaCategories(toNumeric(data.restaurant_id));
                        }
                        if (withAccess("office")) {
                            $(".btn-office-action").each(function() {
                                if ($(this).attr("data-action") == "tables" && $(this).hasClass("active")) {
                                    var button = $(this);
                                    setTimeout(function() {
                                        $(button).trigger("click");
                                    }, 100);
                                }
                            });
                        }
                    }
                }
                break;
            case 'sync-area-categories':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        LocalDB.set(LocalDB.areaCategoryInfo, JSON.stringify([]));
                        LocalDB.set(LocalDB.tableInfo, JSON.stringify([]));
                        if (withTableAccess() || withAccess("office")) {
                            syncAreaCategories(toNumeric(data.restaurant_id));
                        }
                        if (withAccess("office")) {
                            $(".btn-office-action").each(function() {
                                if ($(this).attr("data-action") == "areacategories" && $(this).hasClass("active")) {
                                    var button = $(this);
                                    setTimeout(function() {
                                        $(button).trigger("click");
                                    }, 100);
                                }
                            });
                        }
                    }
                }
                break;
            case 'sync-order-received':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        if (withAccess("office")) {
                            if (typeof data.order != 'undefined' && typeof data.order.id != 'undefined') {
                                var order = data.order;
                                // var orders = [];
                                // orders.push(order);
                                // var currentOrders = LocalDB.orders();
                                // $(currentOrders).each(function(okey, orderInfo) {
                                //     orders.push(orderInfo);
                                // });
                                // LocalDB.set(LocalDB.ordersInfo, JSON.stringify(orders));
                                var orderTable = LocalDB.getTableInfo(order.table_id);
                                var orderTableInfo = "";
                                if (typeof orderTable != "undefined") {
                                    orderTableInfo = " from Table #" + orderTable.table_no;
                                }
                                showNotification("Order received of " + order.order_amount + "" + RESTAURANT.CURRENCY + orderTableInfo, "info");
                                $("button.btn-refresh-today-orders").addClass("blink");
                            }
                        }
                    }
                }
                break;
            case 'sync-order-updated':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        if (withAccess("office")) {
                            if (typeof data.order != 'undefined' && typeof data.order.id != 'undefined') {
                                var order = data.order;
                                var currentOrders = LocalDB.orders();
                                if (currentOrders.length == 0) {
                                    return false;
                                }
                                var isOrderUpdated = false;
                                $(currentOrders).each(function(okey, orderInfo) {
                                    if (orderInfo.id == order.id) {
                                        if (!isOrderUpdated) {
                                            isOrderUpdated = true;
                                        }
                                        currentOrders[okey] = order;
                                    }
                                });
                                if (!isOrderUpdated) {
                                    currentOrders.push(order);
                                }
                                LocalDB.set(LocalDB.ordersInfo, JSON.stringify(currentOrders));
                                var orderTable = LocalDB.getTableInfo(order.table_id);
                                var orderTableInfo = "";
                                if (typeof orderTable != "undefined") {
                                    orderTableInfo = " from Table #" + orderTable.table_no;
                                }
                                showNotification("Order synced of " + order.order_amount + "" + RESTAURANT.CURRENCY + orderTableInfo, "info");
                                $("tr.today-order-data[data-id=" + order.id + "]").find("td.order-amount").html(toNumeric(order.order_amount).toFixed(2).toNumberFormat());
                                $("tr.report-data[data-id=" + order.id + "]").find("td.order-amount").html(toNumeric(order.order_amount).toFixed(2).toNumberFormat());
                                // $(".btn-office-action").each(function() {
                                //     if ($(this).attr("data-action") == "todayorders" && $(this).hasClass("active")) {
                                //         var button = $(this);
                                //         setTimeout(function() {
                                //             $(button).trigger("click");
                                //         }, 100);
                                //     }
                                // });
                            }
                        }
                    }
                }
                break;
            case 'sync-order-deleted':
                if (checkLogin()) {
                    if (typeof data.restaurant_id != 'undefined' && toNumeric(data.restaurant_id) > 0 && toNumeric(data.restaurant_id) == toNumeric(RESTAURANT.ID)) {
                        if (withAccess("office")) {
                            if (typeof data.order_id != 'undefined') {
                                // var currentOrders = LocalDB.orders();
                                // var updatedOrders = [];
                                // $(currentOrders).each(function(okey, orderInfo) {
                                //     updatedOrders.push(orderInfo);
                                //     if (orderInfo.id == data.order_id) {
                                //         updatedOrders[okey].is_empty = 1;
                                //     }
                                // });
                                // LocalDB.set(LocalDB.ordersInfo, JSON.stringify(updatedOrders));
                                showNotification("Order successfully marked as deleted", "error");
                                // $(".btn-office-action").each(function() {
                                //     if ($(this).attr("data-action") == "todayorders" && $(this).hasClass("active")) {
                                //         var button = $(this);
                                //         setTimeout(function() {
                                //             $(button).trigger("click");
                                //         }, 100);
                                //     }
                                // });
                            }
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    // Notify customer for new FC E-Card
    static notifyCustomerForFCCardInvoke(FCECardNumber = '') {
        if (FCECardNumber == '') {
            return false;
        }
        if (checkLogin()) {
            callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
                {
                    action : 'notify-customer-at-invoke-fc-ecard',
                    restaurant_id : toNumeric(RESTAURANT.ID),
                    FCECardNumber : FCECardNumber
                },
                function(response) {
                    // Happy hanging alone
                },
                function(error) {
                    // Happy hanging alone
                }
            );
        } else {
            showLoginLayout(true);
        }
    }

    // Notify customer for payment transaction with FC E-Card
    static notifyCustomerForFCCardTransaction(data) {
        if (typeof data == 'undefined' || typeof data.fcECardTransId == 'undefined' || isNaN(data.fcECardTransId) || data.fcECardTransId <= 0) {
            return false;
        }
        if (checkLogin()) {
            callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
                {
                    action : 'notify-customer-for-fc-ecard-transaction',
                    restaurant_id : toNumeric(RESTAURANT.ID),
                    fcECardTransId : toNumeric(data.fcECardTransId)
                },
                function(response) {
                    // Happy hanging alone
                },
                function(error) {
                    // Happy hanging alone
                }
            );
        } else {
            showLoginLayout(true);
        }
    }

}