window.APP_TITLE = 'Fastcheck';
window.APP_POWERED_BY = {
  'NAME': 'Wisencode Infotech',
  'EMAIL': 'info@wisencode.com',
  'WEBSITE': 'https://wisencode.com',
  'MOBILE_NUMBER': '+91 8238136154'
}
window.APP_DATASTATION = '../datastation/';
window.APP_API_ENDPOINT = APP_DATASTATION + 'api/';
window.RESTAURANT_CURRENCY = 'INR';
window.pageFlip = null;

$(document).ready(function() {
    
    // Setup app info
    $("span.app-title").html(APP_TITLE);
    $("span.app-powered-by").html(APP_POWERED_BY.NAME);
    $("a.app-powered-by-site").attr("href", APP_POWERED_BY.WEBSITE);

    $(document).on("scrollstart", ".page", function(e) {
        if (pageFlip != null)
            pageFlip.disable(true);
    })

    $(document).on("click", ".page", function() {
        if (pageFlip != null)
            pageFlip.disable(false);
    });

    $(document).on("click", ".suggest-baloon-holder button.btn-close", function() {
        $("div.suggest-baloon-holder").remove();
    });

    $(document).on("click", ".navigators button.btn-prev-page", function() {
        pageFlip.disable(false);
        pageFlip.flipPrev();
    });
    $(document).on("click", ".navigators button.btn-next-page", function() {
        pageFlip.disable(false);
        pageFlip.flipNext();
    });

    // Fetch restaurant & menu info
    $.ajax({
      url : APP_API_ENDPOINT + 'api.php',
      type : 'POST',
      data : {
        'action' : 'get-restaurant-menu',
        'hash' : $.urlParam('hash'),
        'area' : $.urlParam('area')
      },
      success : function(response) {
        $("div#menu-loader").remove();
        if (response.status == false) {
          $("div.menu-book").remove();
          $("div.suggest-baloon-holder").remove();
          alert(response.message);
        } else if (response.status == true) {
          let hasLogo = false;
          if (response.restaurant.logo != null || response.restaurant.logo != "") {
            hasLogo = true;
          }
          RESTAURANT_CURRENCY = response.restaurant.currency_symbol;
          setPages(response);
          setRestaurantLogo(hasLogo, "../assets/img/uploads/" + response.restaurant.id + "/logo/" + response.restaurant.logo);
          $(".rest-name").html(response.restaurant.name);
          $(document).prop('title', $(document).prop('title') + ' - ' + response.restaurant.name);
          $(".rest-address").html(nl2br(response.restaurant.address));
          $(".bottom-line").html(response.restaurant.invoice_bottom_line);
          $(".rest-contact-number").html(response.restaurant.contact_number);
          $(".rest-type").html(getRestaurantFoodTypesIcon(response.restaurant.food_type));
          if (typeof response.restaurant.contact_number == 'undefined' || response.restaurant.contact_number == null || response.restaurant.contact_number == "") {
            $(".rest-contact-number").remove();
          }
        }
      },
      error : function(error) {
        $("div#menu-loader").remove();
        $("div.menu-book").remove();
        $("div.suggest-baloon-holder").remove();
        alert("No menu is accessible for this url");
      }
    });
});

// Set pages as per the area of restaurant
function setPages(menuComponents = []) {
    var areas = menuComponents.areas;
    var categories = menuComponents.categories;
    var items = menuComponents.items;
    var menuPageContent = '';
    $(areas).each(function(ark, area) {
        var pageClone = $(".page:first").clone();
        $(pageClone).empty();
        $(pageClone).removeClass("cover").removeClass("cover-start").removeClass("cover-end");
        $(pageClone).attr("data-density", "hard");
        let areaHeading = "<p class='subheading text-bold text-right'><span class='bg-theme p-p5'>"+ area.name.toString().toUpperCase() +"</span></p>";
        $(pageClone).append(areaHeading);
        var menuItemsTable = "<table class='menu-items w-100' border='0'>";
        var categoryBlock = '';
        $(categories).each(function(ckey, category) {
            categoryBlock = '<tr data-catid="'+ category.id +'">';
            categoryBlock+= '<td class="category-row text-left text-bold" width="100%" colspan="2">';
            categoryBlock+= '<span class="category-title">' + category.name + '</span>';
            categoryBlock+= '</td>';
            categoryBlock+= '</tr>';
            let categoryItems = 0;
            $(items).each(function(ikey, item) {
                if (item.category_id == category.id) {
                    categoryBlock+= '<tr>';
                    categoryBlock+= '<td class="item-row text-left text-bold">';
                    categoryBlock+= '<span class="item-title">' + item.name + '</span>';
                    categoryBlock+= '</td>';
                    categoryBlock+= '<td class="item-row text-left text-bold">';
                    let itemPrice = 0;
                    let itemPriceInfo = JSON.parse(item.item_price_info);
                    $(itemPriceInfo).each(function(ipk, ipv) {
                        if (ipv.acatId == area.id) {
                            itemPrice = ipv.price;
                        }
                    })
                    categoryBlock+= '<span class="item-title">' + itemPrice.toFixed(2) + ' </span>';
                    categoryBlock+= '<span class="res-currency">' + RESTAURANT_CURRENCY + '</span>';
                    categoryBlock+= '</td>';
                    categoryBlock+= '</tr>';
                    categoryItems++;
                }
            });
            if (categoryItems > 0) {
                menuItemsTable+= categoryBlock;
            }
        });
        menuItemsTable+= "</table>";
        $(pageClone).append(menuItemsTable);
        $(pageClone).removeClass("show").removeClass("hide").addClass("hide");
        $(pageClone).insertBefore(".page.last");
    });
    pageFlip = new St.PageFlip(
        document.getElementById("pages"),
        {
            width: (isMobileScreen()) ? window.innerWidth : 500, // base page width
            height: window.innerHeight, // base page height
            autoCenter: true,
            size: "stretch",
            // set threshold values:
            maxWidth: window.innerWidth,
            maxHeight: window.innerHeight,
            maxShadowOpacity: 0.5, // Half shadow intensity
            showCover: true,
            mobileScrollSupport: true // disable content scrolling on mobile devices
        }
    );
    pageFlip.loadFromHTML(document.querySelectorAll(".page"));
}

function isMobileScreen() {
    return window.matchMedia('(max-width: 600px)').matches;
}

// Get current active page on menu book
function getCurrentActivePage() {
    var page = 1;
    var reachToPage = false;
    $(".page").each(function(k, v) {
        if (!reachToPage && $(this).hasClass("show")) {
            page = k + 1;
            reachToPage = true;
        }
    });
    return page;
}

// Auto handle navigator buttons on menu book while navigating through pages
function handleNavigators(page = 1) {
    if (page == 1) {
        $("button.btn-previous").hide();
    } else {
        $("button.btn-previous").show();
    }
    if ($(".page").length == page) {
        $("button.btn-next").hide();
    } else {
        $("button.btn-next").show();
    }
}

// Set restaurant logo if restaurant has set
function setRestaurantLogo(hasLogo = true, src = '') {
    if (hasLogo) {
        $("img.logo").attr("src", src);
        $("img.logo").removeClass("hide");
        $("img.logo").css("display", "block");
        $(".horizontalcenter").attr("src", src);
    } else {
        $("img.logo").remove();
    }
}


// Get restaurant type icons
function getRestaurantFoodTypesIcon(types = '') {
    var foodTypes = types.split(',');
    var iconsHtml = '';
    $(foodTypes).each(function(fk, type) {
        if (type == 'V')
            iconsHtml+= "<img src='../assets/img/icons/icon-veg.png' class='d-inline-block-f bg-white valign-middle' />";
        if (type == 'NV')
            iconsHtml+= "<img src='../assets/img/icons/icon-non-veg.png' class='d-inline-block-f bg-white valign-middle' />";
    });
    return iconsHtml;
}

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}

// Return text after converting new line to br tag
function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}