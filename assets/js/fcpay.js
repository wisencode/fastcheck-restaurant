window.APP_DATASTATION = '../../datastation/';
window.APP_API_ENDPOINT = APP_DATASTATION + 'api/';
function callAJAX(url, type = 'GET', data, onSuccess = null, onError = null) {
    $.ajax({
        url : '../../assets/img/defaults/pixel.png?v=' + Math.random(),
        type : 'GET',
        success : function(success) {
            $.ajax({
                url: url,
                type: type.toString().toUpperCase(),
                data: data,
                success: (onSuccess) ? onSuccess : function() {},
                error: (onError) ? onError : function(error) {
                    showNotification(error.responseText, "error");
                },
            });
        },
        error : function(error) {
            showNotification('CONNECTION PROBLEM...', "error");
        }
    });
}

function showNotification(msg, type = 'success') {
    var notification = $("<div class='notification "+ type +"'> " + msg + " </div>");
    $('.notification-container').append(notification);
    $(notification).hide();
    $(notification).slideDown('fast');
    setTimeout(function() {
        $(notification).remove();
    }, 3000);
}
