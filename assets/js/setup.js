$(document).ready(function() {
	$(".info-powered-by").html("Powered by <a href='" + APP_POWERED_BY.WEBSITE + "' class='no-link-style' target='_blank'>" + APP_POWERED_BY.NAME + "</a>");
	$(".app-name").html(APP_TITLE);
	$("span.info-powered-by-contact-number").html(APP_POWERED_BY.MOBILE_NUMBER);
	$("span.info-powered-by-contact-email").html(APP_POWERED_BY.EMAIL);
	// Check login and process login if not logged in
	if (!LocalDB.isSet(LocalDB.loginAs)) {
		$(".restaurant-logo-holder").hide();
		$(".btn-app-configuration").css('visibility', 'hidden');
		showLoginLayout();
	} else {
		dispatchProcessesIfValid();
	}
	
	initEvents();

});

function processLogin() {
	$(".login-layout").hide();
	validateAndLogin();
}

function dispatchProcessesIfValid() {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
		{
			action : 'check-login'
		},
		function(response) {
			if (response.status == false) {
				showLoginLayout(true);
			} else {
				var restaurant = LocalDB.restaurant();
				unsetAuthenticSections(LocalDB.loggedInAs().loginAs);
				if (withTableAccess() && !withAccess("office")) {
					// Initialize table view process
					initTableViewProcess(restaurant);
				} else if (withAccess("kitchen")) {
					// Initialize kitchen view process
					initKitchenViewProcess(restaurant);
				} else if (withAccess("office")) {
					// Initialize back office view
					initOfficeViewProcess(restaurant);
				}
			}
		},
		function(error) {
			showLoginLayout(true);
		}
	);
}

function validateAndLogin() {
	toggleWaitingLoader('show');
	var username = $("input[name='login-username']").val();
	var password = $("input[name='login-password']").val();

	if(username.length == '' || password.length == '') {
		$(".login-layout").show();
		toggleWaitingLoader('hide');
		showNotification("Username or password can not be blank", "error");
		return false;
	}

	callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
		{
			action : 'process-login',
			username : username,
			password : password,
		},
		function(response) {
			toggleWaitingLoader('hide');
			if (response.status == true) {
				LocalDB.setLoginAs({
					restaurant_id : response.restaurant_id,
					loginAs : response.role,
				});
				// Set restaurant info
				if (typeof response.pusher_api_key != 'undefined' && response.pusher_api_key != '') {
					LocalDB.set('pusher_api_key', response.pusher_api_key);
				}
				if (typeof response.fc_echo_secret != 'undefined' && response.fc_echo_secret != '') {
					LocalDB.set('fc_echo_secret', response.fc_echo_secret);
				}
				initRestaurantDetails(response.restaurant_id, response.role);
				if (withAccess("office")) {
					syncCategories(toNumeric(response.restaurant_id));
					syncItems();
					syncAreaCategories(toNumeric(response.restaurant_id));
					syncTables(toNumeric(response.restaurant_id));
					syncTaxes(toNumeric(response.restaurant_id));
					syncDiscounts(toNumeric(response.restaurant_id));
				} else if (withAccess("kitchen")) {
					syncCategories(toNumeric(response.restaurant_id));
					syncItems();
				}
			} else {
				showNotification(response.message, "error");
				$(".login-layout").show();
			}
		},
		function(error) {
			$(".login-layout").show();
			toggleWaitingLoader('hide');
			showNotification(error.responseText, "error");
		}
	);
}

function setRestaurantInfo(restaurant) {
	RESTAURANT.ID = toNumeric(restaurant.id);
	RESTAURANT.CURRENCY = restaurant.currency_symbol;
	RESTAURANT.CURRENCY_CODE = restaurant.currency_code;
	RESTAURANT.LOGO = restaurant.logo;
	RESTAURANT.NAME = restaurant.name;
	RESTAURANT.PAYMENT_METHODS = restaurant.payment_methods;
	$("span.currency").html(RESTAURANT.CURRENCY_CODE);
	$("span.currency-symbol").html(RESTAURANT.CURRENCY);
	$("span.restaurant-address").html(nl2br(restaurant.address));
	$("span.restaurant-contact-number").html(restaurant.contact_number);
	$(".restaurant-food-type-icons").html(getRestaurantFoodTypesIcon());
	$(".footer").show();
	if (withAccess("office")) {
		$(".no-preview-logo").remove();
	}
	if (typeof RESTAURANT.LOGO != 'undefined' && RESTAURANT.LOGO != null && RESTAURANT.LOGO != 'null' && RESTAURANT.LOGO != '') {
		$(".restaurant-logo").attr("src", "assets/img/uploads/" + RESTAURANT.ID + "/logo/" + RESTAURANT.LOGO);
		if (withAccess("office")) {
			$("#profile_restaurant_logo_preview").show();
		}
	} else {
		$(".restaurant-logo").attr("src", "assets/img/logo/logo.png");
		if (withAccess("office")) {
			$("#profile_restaurant_logo_preview").hide();
			$("<label class='no-preview-logo'>NO LOGO</label>").insertAfter($("#profile_restaurant_logo_preview"));
		}
	}
	$(".info-restaurant").html(RESTAURANT.NAME + " - " + APP_TITLE);
	$(".info-restaurant").append("<br>");
	$("span.restaurant-name").html(RESTAURANT.NAME);
	$("span.loggedin-as").html(LocalDB.loggedInAs().loginAs);
	$("span.app-name").html(APP_TITLE);
	if (restaurant.invoice_bottom_line != '' && restaurant.invoice_bottom_line.toString().trim() != '') {
		$("span.invoice-bottom-line").html(restaurant.invoice_bottom_line);
	} else {
		$("p.invoice-bottom-line-holder").hide();
	}
	$(".restaurant-logo-holder").show();
	$(".btn-app-configuration").css('visibility', 'visible');
	if ($(".section.checkout-layout").css("display") == "none") {
		// Hide table no. button and cart button and home button initially
		$(".footer .actions").hide();
	}
	$(".restaurant-gst-number").html(restaurant.gst_number);
	if (restaurant.currency_code == "INR") {
		$(".specific-for-gst").show();
		if (typeof restaurant.gst_number != "undefined" && restaurant.gst_number != null && restaurant.gst_number != "") {
			$("p.specific-for-gst").show();
		} else {
			$("p.specific-for-gst").hide();
		}
	} else {
		$(".specific-for-gst").hide();
	}
	// Bind event handler channel events
	if (typeof eventHandler != 'undefined' || eventHandler == null) {
		eventHandler = new EventHandler(RESTAURANT.ID);
	}
}

// Initialize table view process
function initTableViewProcess(restaurant) {
	setRestaurantInfo(restaurant);
	if (withAccess("waiter") || withAccess("manager")) {
		$("button.btn-proceed-fc-eCard").parent().hide();
	} else if (withAccess("counter") || withAccess("owner")) {
		$("button.btn-proceed-fc-eCard").parent().show();
	}
	adjustPaymentMethods(restaurant);

	// Retrieve area categories
	if (!LocalDB.isSet(LocalDB.areaCategoryInfo)) {
		initAreaCategories(RESTAURANT.ID);
	} else {
		var areaCategories = LocalDB.areaCategories();
		loadAreaCategories(areaCategories);
	}

	// Retrieve items
	if (!LocalDB.isSet(LocalDB.itemsInfo)) {
		initAllItems(RESTAURANT.ID);
	}

	// Retrieve categories
	if (!LocalDB.isSet(LocalDB.categoriesInfo)) {
		initCategories(RESTAURANT.ID);
	} else {
		var categories = LocalDB.categories();
		loadCategories(categories);
	}

	// Retrieve tables
	if (!LocalDB.isSet(LocalDB.tableInfo)) {
		initTables(RESTAURANT.ID);
	} else {
		var tables = LocalDB.tables();
		loadTables(tables);
	}

	// Retrieve tax info
    if (!LocalDB.isSet(LocalDB.taxInfo)) {
        initTaxes(RESTAURANT.ID);
    }

    // Retrieve discounts info
    if (!LocalDB.isSet(LocalDB.discountInfo)) {
        syncDiscounts(RESTAURANT.ID);
    }

    // Show table layout if device is logged in
    showTableLayout();
    $(".restaurant-logo-holder").show();
	$(".btn-app-configuration").css('visibility', 'visible');

    // Render cart
	renderCart();
}

function adjustPaymentMethods(restaurant) {
	ALLOWED_PAYMENT_METHODS = [];
	$(restaurant.payment_methods).each(function(k, v) {
		ALLOWED_PAYMENT_METHODS.push(v.payment_method);
	});
	$(".checkout_options").each(function() {
		if ($.inArray($(this).val(), ALLOWED_PAYMENT_METHODS) == -1) {
			$("tr.section_payment_" + $(this).val()).hide();
		} else {
			var isPaymentMethodEligible = true;
			if (isMobile() && $(this).val() == "fcECard") {
				isPaymentMethodEligible = false;
			}
			if ((withAccess("waiter") || withAccess("manager")) && $(this).val() == "fcECard") {
				isPaymentMethodEligible = false;
			}
			if (isPaymentMethodEligible) {
				$("tr.section_payment_" + $(this).val() + ":not(.child-section)").show();
			} else {
				$("tr.section_payment_" + $(this).val()).hide();
			}
		}
	});
}

// Initialize kitchen view process
function initKitchenViewProcess(restaurant) {
	setRestaurantInfo(restaurant);
	// $(".footer-btn").remove();
	// Retrieve tables
	if (!LocalDB.isSet(LocalDB.tableInfo)) {
		initTables(RESTAURANT.ID, true);
	}
	// Retrieve categories
	if (!LocalDB.isSet(LocalDB.categoriesInfo) || !LocalDB.isSet(LocalDB.itemsInfo)) {
		initCategories(RESTAURANT.ID, true);
	}
	showKitchenLayout();
	renderKitchenLog();
}

// Initialize office view process
function initOfficeViewProcess(restaurant) {
	setRestaurantInfo(restaurant);
	if (!LocalDB.isSet(LocalDB.paymentMethodsInfo)) {
		storePaymentMethods();
	}
	if (!LocalDB.isSet(LocalDB.currencyInfo)) {
		storeAllCurrencies();
	}
	syncTodayOrders(restaurant.id);
	showOfficeLayout();
	setAssetsForOfficeView();
}

function initRestaurantDetails(restaurant_id, role = "owner", inBackground = false) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'get-restaurant-info',
			restaurant_id : restaurant_id
		},
		function(restaurant) {
			LocalDB.saveRestaurant(restaurant);
			if (withTableAccess()) {
				adjustPaymentMethods(restaurant);
			}
			unsetAuthenticSections(role);
			if (!inBackground) {
				if (withAccess("office")) {
					initOfficeViewProcess(restaurant);
				} else if (withTableAccess()) {
					initTableViewProcess(restaurant);
				} else if (withAccess("kitchen")) {
					initKitchenViewProcess(restaurant);
				}
			} else {
				setRestaurantInfo(restaurant);
			}
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

function initAreaCategories(restaurant_id) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'get-area-categories',
			restaurant_id : restaurant_id,
		},
		function(areaCategories) {
			loadAreaCategories(areaCategories);
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

function initCategories(restaurant_id, saveOnly = false) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'get-categories',
			restaurant_id : restaurant_id,
		},
		function(categories) {
			if (!saveOnly) {
				$(categories).each(function(k, category) {
					if (k == (categories.length -1)) {
						loadCategories(categories);
					}
				});
			} else {
				if (categories.length > 0) {
					$(categories).each(function(k, category) {
						LocalDB.saveCategory(category);
						if (LocalDB.items(category.id).length == 0) {
							// loadItems(category.id, true);
						}
					});
				}
			}
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

function initAllItems(restaurant_id, saveOnly = false) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'get-items',
			restaurant_id : restaurant_id,
		},
		function(items) {
			if (items.length > 0) {
				$(items).each(function(k, item) {
					if (k == (items.length - 1)) {
						if (!LocalDB.isSet(LocalDB.categoriesInfo)) {
							initCategories(restaurant_id);
						} else {
							loadCategories(LocalDB.categories());
						}
					}
					LocalDB.saveItem(item, item.category_id);
				});
			}
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

function initTables(restaurant_id, saveOnly) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'get-tables',
			restaurant_id : restaurant_id,
		},
		function(tables) {
			if (!saveOnly) {
				loadTables(tables);
			} else {
				if (tables.length > 0) {
					$(tables).each(function(k, table) {
						LocalDB.saveTable(table);
					});
				}
			}
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

// Set area categories on floor
function loadAreaCategories(areaCategories, activeAreaCategoryId = '') {
	$("table.table-types").empty();
	var areaCategoryPerRow = 8;
	var activeClassForFirst = '';
	if (activeAreaCategoryId == '') {
		activeClassForFirst = ' active';
	}
	if (areaCategories.length > 0) {
		var areaCategory = '<tr>';
		areaCategory+= '<td>';
		areaCategory+= '<button class="btn-solid btn-table-type' + activeClassForFirst + '" data-areacategory="">All ('+ LocalDB.tables().length +')</button>';
		$(areaCategories).each(function(k, v) {
			LocalDB.saveAreaCategory(v);
			var totalTablesInAreaCategory = LocalDB.tables(v.id).length;
			var activeClass = '';
			if (activeAreaCategoryId == v.id) {
				activeClass = ' active';
			}
			areaCategory+= '<button class="btn-solid btn-table-type' + activeClass + '" data-areacategory="' + v.id + '">' + v.name + ' ('+ totalTablesInAreaCategory +')</button>';
			if ((k+1) % areaCategoryPerRow == 0) {
				areaCategory+= '</td></tr><tr><td>';
			}
		});
		if (areaCategories.length % areaCategoryPerRow != 0) {
			areaCategory+= '</td></tr>';
		}
		$("table.table-types").append(areaCategory);
	}
}

// Set tables on floor
function loadTables(tables, activeAreaCategoryId = '') {
	$(".table-list").empty();
	var tablePerRow = 10;
	if (isMobileScreen()) {
		tablePerRow = 3;
	}
	if (isiPadScreen()) {
		tablePerRow = 6;
	}
	if (tables.length > 0) {
		var table = '<tr>';
		$(tables).each(function(k, v) {
			LocalDB.saveTable(v);
			var tableAreaCategory = LocalDB.getAreaCategoryInfo(v.area_category_id);
			var tableClass = '';
			if (v.status == 'B') {
				tableClass = 'booked';
			}
			table+= '<td data-tableid="' + v.id + '" data-tablestatus="' + v.status + '" class="table-block text-white ' + tableClass + '">';
			table+= '<span class="ribbon-table-no">' + v.table_no + '</span>';
			if (v.status == 'B') {
				table+= '<img src="assets/img/icons/icon-restaurant-small.png" class="icon-table-booked" />';
				table+= '<img src="assets/img/icons/icon-table-booked.png" />';
			} else {
				table+= '<img src="assets/img/icons/icon-table.png" />';		
			}
			if (v.table_alias != null && v.tables_alias != '') {
				table+= '<span class="d-block text-sm text-grey-dark">' + v.table_alias + '</span>';
			}
			if (tableAreaCategory != null && typeof tableAreaCategory.name != 'undefined' && tableAreaCategory.name != '') {
				table+= '<span class="d-block text-sm text-grey">' + tableAreaCategory.name + '</span>';	
			}
			table+= '</td>';
			if ((k+1) % tablePerRow == 0) {
				table+= '</tr><tr>';
			}
			if (k == (tables.length - 1)) {
				loadAreaCategories(LocalDB.areaCategories(), activeAreaCategoryId);
			}
		});
		if (tables.length % tablePerRow != 0) {
			table+= '</tr>';
		}
		$(".table-list").append(table);
	} else {
		var table = '<tr>';
		table+= '<td class="text-white bg-theme p-5">';
		table+= 'No table found';
		table+= '</td>';
		table+= '</tr>';
		$(".table-list").append(table);
	}
}

// Set categories on floor
function loadCategories(categories) {
	$("ol.list-categories").empty();
	$(categories).each(function(k, v) {
		LocalDB.saveCategory(v);
		var category = "<li class='category' data-id='" + v.id + "'>";
		category+= "<table class='category-line' width='100%'>";
		category+= "<tr>";
		category+= "<td>";
		if (v.picture == null) {
			category+= '<span class="no-avatar">'+v.name.toString().substr(0, 1)+'</span>';	
		} else {
			var categoryPicture = 'assets/img/uploads/' + RESTAURANT.ID + '/' + 'categories/' + v.picture;
			category+= '<span class="avatar"><img src="' + categoryPicture + '" class="cat-picture" /></span>';
		}
		category+= "</td>";
		category+= "<td>";
		category+= '<span class="category-name">' + v.name + ' (' + LocalDB.items(v.id).length + ')</span>';
		category+= "</td>";
		category+= "</tr>";
		category+= "</li>";
		$("ol.list-categories").append(category);
		if (k == 1 && (LocalDB.getLastVisitedCategory() == null || isNaN(LocalDB.getLastVisitedCategory()))) {
			setTimeout(function() {
				$("li.category:first").trigger('click'); 
			}, 50);
		} else {
			setTimeout(function() {
				$("li.category[data-id='" + LocalDB.getLastVisitedCategory() + "']").trigger('click');
			}, 50);
		}
	});
}

// Set items on floor
function loadItems(category_id, saveOnly = false) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'category-items',
			category_id : category_id
		},
		function(items) {
			if (!saveOnly) {
				setItems(items, category_id)
			} else {
				if (items.length > 0) {
					$(items).each(function(k, item) {
						LocalDB.saveItem(item, category_id);
					});
				}
			}
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

function setItems(items, category_id) {
	$(".list-items").empty();
	if (LocalDB.getActiveTable() > 0) {
		$(items).each(function(ik, item) {
			LocalDB.saveItem(item, category_id);
			let tableId = LocalDB.getActiveTable();
			let itemPrice = LocalDB.getItemPriceByArea(item.id, LocalDB.getTableInfo(tableId).area_category_id);

			var itemBlock = "<div class='item' data-id='" + item.id + "' data-name='" + item.name + "' data-price='" + itemPrice + "'>";
		
			itemBlock+= "<div class='picture'>";
			if (typeof item.item_code != 'undefined' && item.item_code != null) {
				itemBlock+= "<span class='itemcode'>" + item.item_code + "</span>";
			}
			
			var itemPicture = 'assets/img/defaults/icon-food.png';
			var itemPictureClass = '';
			if (item.picture != null && item.picture != '') {
				var itemPicture = 'assets/img/uploads/' + RESTAURANT.ID + '/' + 'categories/items/' + item.picture;
				itemPictureClass = '';
			}
			itemBlock+= "<img src='"+itemPicture+"' class='"+itemPictureClass+" is-item-picture' />";
			itemBlock+= "</div>";

			itemBlock+= "<div class='info'>";
			
			itemBlock+= "<span class='name'>";
			itemBlock+= item.name;
			itemBlock+= "</span>";
			itemBlock+= "<span class='price'>";
			itemBlock+= itemPrice + " " + RESTAURANT.CURRENCY;
			itemBlock+= "</span>";
			itemBlock+= "</div>";

			itemBlock+= "</div>";
			$(".list-items").append(itemBlock);
		});

		if(LocalDB.items(category_id).length == 0) {
			var emptyItemsContainer = "<div class='text-center text-white opacity-half mt-25per'>";
			emptyItemsContainer+= '<img src="assets/img/icons/icon-no-food.png" />';
			emptyItemsContainer+= '<h3>No Items</h3>';
			emptyItemsContainer+= '</div>';
			$(".list-items").html(emptyItemsContainer);
		}
	}
}

// Ask quantity for an item
function askQuantityForItem(itemId, quantity = 1, doQtyReplace = false) {
	$(".modal").hide();
	var item = LocalDB.getItemInfo(toNumeric(itemId));
	var cartItemInfo = LocalDB.cartItemInfo(LocalDB.getActiveTable(), itemId);
	if (typeof cartItemInfo != 'undefined' && typeof cartItemInfo.itemId != 'undefined' && cartItemInfo.itemId > 0) {
		if (cartItemInfo.specifications != '') {
			var specifications = JSON.parse(cartItemInfo.specifications);
			// loadItemSpecifications(specifications);
			if (specifications.hasSpecification) {
				$("input[type='checkbox'][name='has_specifications']").prop("checked", true);
				$("input[type='checkbox'][name='has_specifications']").trigger("change");
				$("input[type='checkbox'].spefications_taste").each(function() {
			        $(this).prop("checked", false);
			    });
				$("input[type='checkbox'][name='spefications_taste'][value='" + specifications.taste + "']").prop("checked", true);
				$("textarea[name='specification_text']").val(specifications.extraSpecification);
			} else {
				resetAskForItemQtyModal();	
			}
		} else {
			resetAskForItemQtyModal();
		}
	}
	$("#modal-ask-quantity").parent().removeClass('show').addClass('show');
	let tableId = LocalDB.getActiveTable();
	let itemPrice = LocalDB.getItemPriceByArea(item.id, LocalDB.getTableInfo(tableId).area_category_id);
	var info = item.name + " (" + itemPrice + " " + RESTAURANT.CURRENCY + ")";
	$("#modal-ask-quantity").find(".quantity-for").html(info);
	$("#modal-ask-quantity").find("input[name='item_id']").val(itemId);
	if(doQtyReplace) {
		$("#modal-ask-quantity").find("input[name='do_qty_replace']").val("1");
		$("input#has_specifications[type='checkbox']").prop("checked", false).trigger("change").parent().hide();
	} else {
		$("#modal-ask-quantity").find("input[name='do_qty_replace']").val("0");
		$("input#has_specifications[type='checkbox']").prop("checked", true).trigger("change").parent().show();
	}
	$("#modal-ask-quantity").show();
	$("#modal-ask-quantity").find("input[name='item_qty']").val(quantity).trigger('focus');
	showModalCloseButton();
}

// Show specicif item's specifications
function loadItemSpecifications(specifications) {
	$("tr.line-item-specification").remove();
	if (specifications.length > 0) {
		$(specifications).each(function(spkey, specification) {
			if (specification.qty == -1) {
				$("tr.line-item-specification:last").remove();
			}
			var specificationLine = "<tr class='line-item-specification'>";
			specificationLine+= "<td class='dotted-border-sm text-sm'>";
			specificationLine+= "<select name='itemSpecificationsTaste[]'>";
			specificationLine+= "<option value='regular' " + showTasteSelected(specification.taste, 'regular') + ">Regular</option>";
			specificationLine+= "<option value='spicy' " + showTasteSelected(specification.taste, 'spicy') + ">Spicy</option>";
			specificationLine+= "<option value='medium' " + showTasteSelected(specification.taste, 'medium') + ">Medium</option>";
			specificationLine+= "</select>";
			specificationLine+= "</td>";
			specificationLine+= "<td class='dotted-border-sm text-sm text-left'>";
			specificationLine+= "<input type='text' class='sm-box' value="+ specification.qty + " name='itemSpecificationsQty[]' />";
			specificationLine+= "</td>";
			specificationLine+= "</tr>";
			if (specification.qty > 0) {
				$(specificationLine).insertAfter($("tr.specification_options:last"));
			}
		});
	}
}

// Show item taste option selected
function showTasteSelected(itemTaste, tasteToCheck = 'regular') {
	if (tasteToCheck == itemTaste) {
		return "selected = 'selected'";
	}
	return "";
}

// Checkout process options
function askCheckoutOptions() {
	let noCheckoutOptionSelected = true;
	$("input.checkout_options[type='checkbox']").each(function() {
		if (noCheckoutOptionSelected && $(this).prop("checked")) {
			noCheckoutOptionSelected = false;
		}
	});
	if (noCheckoutOptionSelected) {
		$("input#checkout_option_cash[type='checkbox'][value='cash']").prop("checked", "true");
	}
	popupModal("#modal-checkout");
}

// Invoice for checkout
function showInvoice() {
	popupModal("#modal-invoice");
}

// Search options
function showSearchOption() {
	hideSearchSuggestions();
	popupModal("#modal-search");
	$("input[name='field-search']").val('');
	$("input[name='field-search']").trigger('focus');
}

// Cart Adjustments
function showCartAdjustments() {
	popupModal("#modal-cart-adjustment");
}

// Generate FC eCard
function showFCeCardOption() {
	popupModal("#modal-fc-eCard");
}

// Add new bone
function showAddNewBoneOption() {
	popupModal("#modal-add-bone");	
}

// Show statistics
function showStatisticsOption() {
	popupModal("#modal-statistics");
}

// Show help tips
function showHelpTipsOption() {
	popupModal("#modal-help-tips");
	$("table.help-list-shortcuts").empty();
	var shortcutCounter = 0;
	var helpShortcutLine = '';
	$("ul.list-shortcuts li").each(function() {
		if (shortcutCounter == 0) {
			helpShortcutLine = '<tr>';
		}
		helpShortcutLine+= '<td class="dotted-border-sm">';
		helpShortcutLine+= $(this).html();
		helpShortcutLine+= '</td>';
		shortcutCounter++;
		if (shortcutCounter % 2 == 0) {
			helpShortcutLine+= '</tr><tr>';
		}
	});
	$("table.help-list-shortcuts").append(helpShortcutLine);
}

// Add item to the cart
function addItemToCart(itemId, quantity, specifications = '') {
	var itemObj = $(".item[data-id='" + itemId + "']");
	var isReplace = (toNumeric($("input[name='do_qty_replace']").val()) == "1") ? true : false;
	if (!isReplace) {
		LocalDB.addItemToCart(itemId, quantity, itemObj, 0, specifications);
	} else {
		LocalDB.replaceItemQtyToCart(itemId, quantity, specifications);
	}
	$("input[name='do_qty_replace']").val("0");
	renderCart();
}

// Show cart items on cart panel
function renderCart() {
	var cartItems = LocalDB.cartItems();
	emptyCartItems();
	var cartTotal = toNumeric(LocalDB.cartTotal());
	$(".tax-info table tr:not(.total)").remove();
	if (cartItems.length > 0) {
		var discountData = '';
		var hasAtleastOneDiscount = false;
		if (LocalDB.discounts().length > 0) {
			$(".tax-info table tr.discount").remove();
			var discounts = LocalDB.sequencedDiscounts();
			var cartTotalWithTax = cartTotal;
			var cartTotalWithoutDiscount = cartTotal;
			$(discounts).each(function(k, v) {
				if (v.tableId == null || v.tableId == LocalDB.getActiveTable()) {
					if (!isNaN(v.amount) && v.amount > 0) {
						var hasDiscount = false;
						if (v.minimum_amount != null && cartTotal >= toNumeric(v.minimum_amount)) {
							if (v.type == "percentage") {
								var percentage = v.amount;
								var discountAmount = ((cartTotal * percentage) / 100).toRound();
							} else {
								var discountAmount = v.amount.toRound();
							}
							cartTotal-= toNumeric(discountAmount);
							hasDiscount = true;
							if (!hasAtleastOneDiscount) {
								hasAtleastOneDiscount = true;
							}
						} else if (v.minimum_amount == null && toNumeric(v.amount) > 0) {
							if (v.type == "percentage") {
								var percentage = v.amount;
								var discountAmount = ((cartTotal * percentage) / 100).toRound();
							} else {
								var discountAmount = v.amount.toRound();
							}
							cartTotal-= toNumeric(discountAmount);
							hasDiscount = true;
							if (!hasAtleastOneDiscount) {
								hasAtleastOneDiscount = true;
							}
						}
						if (hasDiscount) {
							var discountLine = '<tr class="discount">';
							var discountDesc = '';
							if (v.description != null && v.description != '') {
								discountDesc = v.description + ' ';
							}
							if (v.type == "percentage") {
								var discountBase = v.amount + "%";
							} else {
								var discountBase = "Flat";
							}
							discountLine+= '<td>' + discountDesc + '(' + discountBase + ')</td>';
							discountLine+= '<td>' + toNumeric(discountAmount).toFixed(2) + ' ' + RESTAURANT.CURRENCY + '</td>';
							discountLine+= '</tr>';
							discountData+= discountLine;
						}
					}
				}
			});
		}

		var cartTotalAfterDiscount = cartTotal;
		var inclTaxLabel = '';
		if (LocalDB.taxes().length > 0) {
			inclTaxLabel = ' (Incl. Tax)';
			$(".tax-info .tax").remove();
			var taxes = LocalDB.taxes();
			$(taxes).each(function(k, v) {
				if (v.tableId == null || v.tableId == LocalDB.getActiveTable()) {
					if (!isNaN(v.amount) && v.amount > 0) {
						if (v.type == "percentage") {
							var percentage = v.amount;
							var taxAmount = ((cartTotalAfterDiscount * percentage) / 100).toRound();
						} else {
							var taxAmount = v.amount.toRound();
						}
						cartTotal+= toNumeric(taxAmount);
						var taxBase = "";
						if (v.type != null && v.type == "percentage") {
							taxBase = '(' + v.amount + '%)';
						}
						var taxLine = '<tr class="tax">';
						taxLine+= '<td>' + v.name + taxBase + '</td>';
						taxLine+= '<td>' + toNumeric(taxAmount).toFixed(2) + ' ' + RESTAURANT.CURRENCY + '</td>';
						taxLine+= '</tr>';
						$(".tax-info table").prepend(taxLine);
					}
				}
			});
			if (taxes.length > 0 && hasAtleastOneDiscount) {
				var taxLine = '<tr>';
				taxLine+= '<td>Total (Excl. Tax)</td>';
				taxLine+= '<td>' + toNumeric(cartTotalAfterDiscount).toFixed(2) + ' ' + RESTAURANT.CURRENCY + '</td>';
				taxLine+= '</tr>';
				$(".tax-info table").prepend(taxLine);
			}
		}

		$(".tax-info table").prepend(discountData);
		if (hasAtleastOneDiscount || LocalDB.taxes().length > 0) {
			var subTotalLine = '<tr>';
			subTotalLine+= '<td>Sub Total</td>';
			subTotalLine+= '<td>' + toNumeric(LocalDB.cartTotal()).toFixed(2) + ' ' + RESTAURANT.CURRENCY + '</td>';
			subTotalLine+= '</tr>';
			$(".tax-info table").prepend(subTotalLine);
		}
		
		$(".tax-info table tr.total-including-tax").remove();
		
		$(cartItems).each(function(k, v) {
			if (v.qty > 0) {
				item = getCartItemRow(v);
				$("ul.list-cart-items").append(item);
			} else {
				LocalDB.removeItemFromCart(v.itemId);
				if (LocalDB.cartItems().length == 0) {
					showCartEmpty();
					$(".cart-items-badge").hide();
				}
			}
		});
		if (LocalDB.cartItems().length > 0) {
			$(".cart-summary").show();
			$(".btn-empty-cart").show();
			$(".cart-items-badge").show();
			$(".cart-checkout-options").show();
		}
	} else {
		showCartEmpty();
	}
	$(".cart-items-badge").html(toNumeric(LocalDB.cartItems().length));
	let totalRoundedBy = (Math.round(cartTotal) - cartTotal);
	$("span.cart-total-round").html(totalRoundedBy.toFixed(2) + ' ' + RESTAURANT.CURRENCY);
	if (totalRoundedBy == 0) {
		$("tr.total.round").hide();
	} else {
		$("tr.total.round").show();
	}
	$("span.cart-total").html(Math.round(cartTotal).toFixed(2) + ' ' + RESTAURANT.CURRENCY);
	$("h4.checkout-amount").html(Math.round(cartTotal).toFixed(2) + ' ' + RESTAURANT.CURRENCY);
	if (LocalDB.cartItems().length == 0) {
		$(".cart-items-badge").hide();
	}
	if (LocalDB.getActiveTable() > 0) {
		// adjustTableStatus();
	}
	applyBounceEffect($(".total span.cart-total"));
}

function emptyCartItems() {
	$("ul.list-cart-items").empty();
}

function showCartEmpty() {
	var noItem = '<li>';
	noItem+= '<table width="100%" class="empty">';
	noItem+= '<tr>';
	noItem+= '<td>';
	noItem+= '<img class="no-cart-image" src="assets/img/icons/icon-clear-cart.png" />';
	noItem+= '<span class="d-block">Cart is empty</span>';
	noItem+= '</td>';
	noItem+= '</tr>';
	noItem+= '</table>';
	noItem+= '</li>';
	$("ul.list-cart-items").append(noItem);
	$(".cart-summary").hide();
	$(".btn-empty-cart").hide();
	$(".cart-checkout-options").hide();
}

function transferTableItems(transferFrom, transferTo) {
	if (isNaN(transferFrom) || isNaN(transferTo)) {
		showNotification("Tables can not be transferred", "error");
		return false;
	}
	var transferFromTableItems = LocalDB.cartItems(false, toNumeric(transferFrom));
	if (transferFromTableItems.length <= 0) {
		showNotification("Nothing to transfer: Empty table", "error");
		return false;
	}
	$(transferFromTableItems).each(function(cik, citem) {
		if (citem.tableId == transferFrom) {
			transferFromTableItems[cik].tableId = toNumeric(transferTo);
		}
	});
	if ($("td.table-block[data-tableid="+ transferTo +"]").length > 0) {
		var transferToTableItems = LocalDB.cartItems(false, toNumeric(transferTo));
		var updatedTransferToTableItems = [];
		var processedItemIds = [];
		$(transferFromTableItems).each(function(tfck, tfitem) {
			var itemQty = toNumeric(tfitem.qty);
			$(transferToTableItems).each(function(ttck, ttitem) {
				if (tfitem.itemId == ttitem.itemId) {
					itemQty+= toNumeric(ttitem.qty);
					processedItemIds.push(tfitem.itemId);
				}
			});
			transferFromTableItems[tfck].qty = itemQty;
			updatedTransferToTableItems.push(transferFromTableItems[tfck]);
		});
		$(transferToTableItems).each(function(ttck, ttitem) {
			if ($.inArray(ttitem.itemId, processedItemIds) == -1) {
				updatedTransferToTableItems.push(ttitem);
			}
		});
		LocalDB.set(LocalDB.getCartName(transferFromTableItems), JSON.stringify([]));
		LocalDB.set(LocalDB.getCartName(transferTo), JSON.stringify(updatedTransferToTableItems));
		$(".table-block[data-tableid="+ transferFrom +"]").attr("data-tablestatus", "A");
		$(".table-block[data-tableid="+ transferTo +"]").attr("data-tablestatus", "B");
		hideModal();
		$(".table-block[data-tableid='"+ transferTo +"']").trigger("click");
		// setTableStatus({
		// 	'status' : 'A',
		// 	'id' : toNumeric(transferFrom)
		// });
		// setTableStatus({
		// 	'status' : 'B',
		// 	'id' : toNumeric(transferTo)
		// });
		let transferFromTableInfo = LocalDB.getTableInfo(toNumeric(transferFrom));
		let transferToTableInfo = LocalDB.getTableInfo(toNumeric(transferTo));
		showNotification("Table transferred from table " + transferFromTableInfo.table_no + " to table " + transferToTableInfo.table_no, "info");
		triggerCartEmpty(toNumeric(transferFrom));
		triggerCartUpdated(toNumeric(transferTo));
	} else {
		showNotification("Target table does not exists in the system", "error");
		return false;	
	}
}

// Prepare cart item row design
function getCartItemRow(cartItemInfo) {
	itemInfo = LocalDB.getItemInfo(cartItemInfo.itemId);

	var item = '<li data-itemid="' + itemInfo.id + '" data-qty="' + cartItemInfo.qty + '">';
	item+= '<table width="100%">';
	item+= '<tr>';
	
	item+= '<td>';
	let itemPicture = $(".restaurant-logo:first").attr("src");
	if (typeof cartItemInfo.picture != 'undefined' && cartItemInfo.picture != null && cartItemInfo.picture != '') {
		itemPicture = cartItemInfo.picture;
	}
	item+= '<img src="' + itemPicture + '" />';	
	item+= '</td>';

	item+= '<td>';
	item+= '<span class="item-qty">' + cartItemInfo.qty + 'x </span>';
	item+= '<span class="item-name">' + itemInfo.name + '</span>';

	let tableId = LocalDB.getActiveTable();
    let itemPrice = LocalDB.getItemPriceByArea(itemInfo.id, LocalDB.getTableInfo(tableId).area_category_id);
	var itemTotal = itemPrice * cartItemInfo.qty;
	item+= '<div class="price-info">';
	item+= '<span class="item-total">' + itemTotal + '' + RESTAURANT.CURRENCY + '</span>';
	item+= '<span class="item-qty"> [ ' + cartItemInfo.qty + ' x </span>';
	item+= '<span class="item-price">' + itemPrice + '</span><span>' + RESTAURANT.CURRENCY + ' ]</span>';
	item+= '</div>';

	item+= '<div class="action-block">';
	item+= '<button type="button" class="btn-solid btn-cart add-qty" title="Add Quantity">+</button>';
	item+= '<button type="button" class="btn-solid btn-cart remove-qty" title="Remove Quantity">-</button>';
	item+= '<button type="button" class="btn-solid btn-cart edit-qty" title="Edit Quantity"><img src="assets/img/icons/icon-edit.png" class="icon-action" /></button>';
	item+= '<button type="button" class="btn-solid btn-cart remove-item" title="Remove Item">X</button>';
	item+= '</div>';

	item+= '</td>';

	item+= '</tr>';
	item+= '</table>';
	item+= '</li>';

	return item;
}

// Show kitchen log to kitchen view
function renderKitchenLog(filterBy = []) {
	var kitchenItems = LocalDB.kitchenItems();
	$("table.list-kitchen-log-holder").empty();
	if (kitchenItems.length > 0) {
		var tableShown = [];
		var tableWithRunningItems = [];
		var hasFilter = false;
		var totalRegularQty = 0;
		var totalSpicyQty = 0;
		var totalMediumQty = 0;
		var totalRegularServedQty = 0;
		var totalSpicyServedQty = 0;
		var totalMediumServedQty = 0;
		var totalItemsQty = 0;
		var totalItemsServedQty = 0;
		if (filterBy.itemId > 0) {
			hasFilter = true;
		}
		var kitchenItemBlock = '<tr class="list-kitchen-log-row">';
		var kotCount = 0;
		$(kitchenItems).each(function(k, kitchenLog) {
			if ($.inArray(kitchenLog.tableId, tableShown) === -1) {
				var kitchenLogTableInfo = LocalDB.kitchenItemsForTable(kitchenLog.tableId);
				var tableInfo = LocalDB.getTableInfo(kitchenLog.tableId);
				var totalTableQty = 0;
				var totalTableServedQty = 0;
				if (kitchenLogTableInfo.length > 0) {
					kitchenItemBlock+= '<td class="dotted-border-sm list-kitchen-log-block valign-top">';
					kitchenItemBlock+= '<table class="list-kitchen-log p-5" width="100%">';
					kitchenItemBlock+= '<tr>';
					kitchenItemBlock+= '<td class="text-left bg-theme p5 kot-log-title kot-log-'+ kitchenLog.tableId +'" colspan="2">';
					kitchenItemBlock+= '<h5 class="p-2-f m-0-f">TABLE ' + tableInfo.table_no + '</h5>';
					kitchenItemBlock+= '</td>';
					kitchenItemBlock+= '<tr>';
					kitchenItemBlock+= '<td class="pb-1-f">';
					kitchenItemBlock+= '</td>';
					kitchenItemBlock+= '</tr>';
					$(kitchenLogTableInfo).each(function(k, itemInfo) {
						var showKitchenItemBlock = true;
						if (hasFilter && filterBy.itemId != toNumeric(itemInfo.itemId)) {
							showKitchenItemBlock = false;
						}
						var itemBio = LocalDB.getItemInfo(itemInfo.itemId);
						if (typeof itemBio == 'undefined' || typeof itemBio.colorCode == 'undefined') {
							showKitchenItemBlock = false;	
						}
						// if (itemInfo.qty > 0 && showKitchenItemBlock) {
						if (showKitchenItemBlock) {
							var itemBio = LocalDB.getItemInfo(itemInfo.itemId);
							var itemSpecifications = JSON.parse(itemInfo.specifications);
							kitchenItemBlock+= '<tr>';
							kitchenItemBlock+= '<td class="text-sm text-grey-dark dotted-border-sm">';
							var regularQty = 0;
							var regularServedQty = 0;
							var spicyQty = 0;
							var spicyServedQty = 0;
							var mediumQty = 0;
							var mediumServedQty = 0;
							var totalTableBasedItemsQty = 0;
							var totalTableItemsServedQty = 0;
							var hasItemsWithExtraInformation = false;
							if (itemSpecifications.length > 0) {
								$(itemSpecifications).each(function(k, itemSpecification) {
									totalTableQty+= itemSpecification.qty;
									totalTableServedQty+= itemSpecification.servedQty;
									if (itemSpecification.extraSpecification == "" && (itemSpecification.qty > itemSpecification.servedQty || 1 == 1)) {
										if (itemSpecification.taste == "regular") {
											regularQty+= toNumeric(itemSpecification.qty);
											regularServedQty+= toNumeric(itemSpecification.servedQty);
										}
										if (itemSpecification.taste == "spicy") {
											spicyQty+= toNumeric(itemSpecification.qty);
											spicyServedQty+= toNumeric(itemSpecification.servedQty);
										}
										if (itemSpecification.taste == "medium") {
											mediumQty+= toNumeric(itemSpecification.qty);
											mediumServedQty+= toNumeric(itemSpecification.servedQty);
										}
									} else {
										if (!hasItemsWithExtraInformation)
											hasItemsWithExtraInformation = true;
									}
								});
							}
							
							totalRegularQty+= regularQty;
							totalSpicyQty+= spicyQty;
							totalMediumQty+= mediumQty;
							totalRegularServedQty+= regularServedQty;
							totalSpicyServedQty+= spicyServedQty;
							totalMediumServedQty+= mediumServedQty;

							var itemSpecificationsBlock = "";
							if (regularQty > 0 || spicyQty > 0 || mediumQty > 0) {
								itemSpecificationsBlock = "<table>";
								if (regularQty > 0) {
									itemSpecificationsBlock+= getItemSpecificationRowForKitchen(itemInfo, "regular", regularQty, "", regularServedQty);
								}
								if (spicyQty > 0) {
									itemSpecificationsBlock+= getItemSpecificationRowForKitchen(itemInfo, "spicy", spicyQty, "", spicyServedQty);
								}
								if (mediumQty > 0) {
									itemSpecificationsBlock+= getItemSpecificationRowForKitchen(itemInfo, "medium", mediumQty, "", mediumServedQty);
								}
								itemSpecificationsBlock+= "</table>";
							}
							var itemSpecificationsWithSpecificationBlock = "";
							if (hasItemsWithExtraInformation) {
								itemSpecificationsWithSpecificationBlock+= "<table>";
								$(itemSpecifications).each(function(k, itemSpecification) {
									if (itemSpecification.extraSpecification != "" && (toNumeric(itemSpecification.qty) > toNumeric(itemSpecification.servedQty) || 1 == 1)) {
										if (itemSpecification.taste == "regular") {
											totalRegularQty+= toNumeric(itemSpecification.qty);
											totalRegularServedQty+= toNumeric(itemSpecification.servedQty);
											regularQty+= toNumeric(itemSpecification.qty);
											regularServedQty+= toNumeric(itemSpecification.servedQty);
										}
										if (itemSpecification.taste == "spicy") {
											totalSpicyQty+= toNumeric(itemSpecification.qty);
											totalSpicyServedQty+= toNumeric(itemSpecification.servedQty);
											spicyQty+= toNumeric(itemSpecification.qty);
											spicyServedQty+= toNumeric(itemSpecification.servedQty);
										}
										if (itemSpecification.taste == "medium") {
											totalMediumQty+= toNumeric(itemSpecification.qty);
											totalMediumServedQty+= toNumeric(itemSpecification.servedQty);
											mediumQty+= toNumeric(itemSpecification.qty);
											mediumServedQty+= toNumeric(itemSpecification.servedQty);
										}
										itemSpecificationsWithSpecificationBlock+= getItemSpecificationRowForKitchen(itemInfo, itemSpecification.taste.toString().toLowerCase(), itemSpecification.qty, itemSpecification.extraSpecification, itemSpecification.servedQty);
									} else {
										// Do further for already served qty
									}
								});
								itemSpecificationsWithSpecificationBlock+= "</table>";
							}

							totalItemsQty = totalRegularQty + totalSpicyQty + totalMediumQty;
							totalItemsServedQty = totalRegularServedQty + totalSpicyServedQty + totalMediumServedQty;
							totalTableBasedItemsQty = regularQty + spicyQty + mediumQty;
							totalTableItemsServedQty = regularServedQty + spicyServedQty + mediumServedQty;

							kitchenItemBlock+= itemSpecificationsBlock;
							kitchenItemBlock+= itemSpecificationsWithSpecificationBlock;

							var allItemsServedClass = "";
							if (totalTableBasedItemsQty == totalTableItemsServedQty) {
								allItemsServedClass = "badge-all-items-served";
							}
							kitchenItemBlock+= '<td class="d-flex-f valign-top font-14 text-grey text-bold dotted-border-sm block-kitchen-log-item '+ allItemsServedClass +'" style="background-color: '+ itemBio.colorCode +';" data-itemid="' + itemBio.id + '" data-qtytoserve="'+ (totalTableBasedItemsQty - totalTableItemsServedQty) +'" data-tableid="'+ kitchenLog.tableId +'">';
							kitchenItemBlock+= (totalTableBasedItemsQty - totalTableItemsServedQty) + 'x ' + itemBio.name;
							kitchenItemBlock+= '</td>';
							kitchenItemBlock+= '</tr>';
						}
					});
					kitchenItemBlock+= '</tr>';
					kitchenItemBlock+= '</table>';
					kitchenItemBlock+= '</td>';
					kotCount++;
					if (kotCount % 5 == 0) {
						kitchenItemBlock+= '</tr><tr class="list-kitchen-log-row">';
					}
				}
				if (totalTableQty > 0 && totalTableServedQty > 0) {
					if ($.inArray(kitchenLog.tableId, tableWithRunningItems) === -1) {
						tableWithRunningItems.push(kitchenLog.tableId);
					}
				}
			}
			tableShown.push(kitchenLog.tableId);
		});
		if (hasFilter && filterBy.itemId > 0) {
			var totalRegularQtyToServe = (totalRegularQty - totalRegularServedQty);
			var totalSpicyQtyToServe = (totalSpicyQty - totalSpicyServedQty);
			var totalMediumQtyToServe = (totalMediumQty - totalMediumServedQty);

			var filterResultLine = "<tr class='block-kitchen-log-filter'>";
			filterResultLine+= "<td>";
			filterResultLine+= "<button class='btn-solid btn-reset-kitchen-filters'>Show All</button>";
			filterResultLine+= "<div class='d-linline-block-f ml-2 font-14 text-bold'>" + (totalItemsQty - totalItemsServedQty) + "x</span> ";
			filterResultLine+= LocalDB.getItemInfo(filterBy.itemId).name;
			filterResultLine+= "</div>";

			filterResultLine+= "<div class='d-linline-block-f ml-2 font-14 text-bold summary'>";
			if (totalRegularQtyToServe > 0) {
				filterResultLine+= "<span class='d-linline-block text-sm badge-regular-taste-item mr-1'>" + totalRegularQtyToServe + "x REGULAR</span>";
			}
			if (totalSpicyQtyToServe > 0) {
				filterResultLine+= "<span class='d-linline-block text-sm badge-spicy-taste-item mr-1'>" + totalSpicyQtyToServe + "x SPICY</span>";
			}
			if (totalMediumQtyToServe > 0) {
				filterResultLine+= "<span class='d-linline-block text-sm badge-medium-taste-item mr-1'>" + totalMediumQtyToServe + "x MEDIUM</span>";
			}
			if (totalRegularServedQty > 0) {
				filterResultLine+= "<span class='d-linline-block text-sm badge-item-served mr-1'>" + totalRegularServedQty + "x REGULAR SERVED</span>";
			}
			if (totalSpicyServedQty > 0) {
				filterResultLine+= "<span class='d-linline-block text-sm badge-item-served mr-1'>" + totalSpicyServedQty + "x SPICY SERVED</span>";	
			}
			if (totalMediumServedQty > 0) {
				filterResultLine+= "<span class='d-linline-block text-sm badge-item-served mr-1'>" + totalMediumServedQty + "x MEDIUM SERVED</span>";	
			}
			filterResultLine+= "</div>";
			filterResultLine+= "</td>";
			filterResultLine+= "</tr>";
			$("table.list-kitchen-log-holder").append(filterResultLine);
		}
		$("table.list-kitchen-log-holder").append(kitchenItemBlock);
		$("table.list-kitchen-log").each(function() {
			if($(this).find("tr").length == 2 && $(this).find("tr:nth-child(2)").find("td:first").is(":empty")) {
				$(this).parent().remove();
			}
		});
		var itemQtyToServeLog = [];
		$("table.list-kitchen-log td.block-kitchen-log-item").each(function() {
			if (typeof itemQtyToServeLog[$(this).attr("data-itemid")] == 'undefined') {
				itemQtyToServeLog[$(this).attr("data-itemid")] = 0;
			}
			itemQtyToServeLog[$(this).attr("data-itemid")]+= toNumeric($(this).attr("data-qtytoserve"));
		});
		if (itemQtyToServeLog.length > 0) {
			var highestQtyItemId = Object.keys(itemQtyToServeLog).reduce(function(a, b){ return itemQtyToServeLog[a] > itemQtyToServeLog[b] ? a : b });
			$("table.list-kitchen-log td.block-kitchen-log-item").each(function() {
				if ($(this).attr("data-itemid") == highestQtyItemId && itemQtyToServeLog[highestQtyItemId] > 1 && !$(this).hasClass("badge-all-items-served")) {
					$(this).attr("title", "HIGH DEMAND");
					$(this).find(".highlighted-dot").remove();
					$(this).prepend('<div class="d-flex-f highlighted-dot demanded-item"></div>&nbsp;');
				}

			});
		}
		if (tableWithRunningItems.length > 0) {
			$("table.list-kitchen-log td.block-kitchen-log-item").each(function() {
				if ($.inArray(toNumeric($(this).attr("data-tableid")), tableWithRunningItems) >= 0 && !$(this).hasClass("badge-all-items-served")) {
					var currentTitle = $(this).attr("title");
					if (typeof currentTitle == "undefined") {
						currentTitle = "";
					}
					$(this).attr("title", currentTitle + " RUNNING ITEM");
					$(this).prepend('<div class="d-flex-f highlighted-dot running-item"></div>&nbsp;');
				}

			});		
		}
	} else {
		var kitchenItemBlock = '<tr class="list-kitchen-log-row text-center">';
		kitchenItemBlock+= '<td class="dotted-border-sm">';
		kitchenItemBlock+= '<h6>NO KOT FOUND</h6>';
		kitchenItemBlock+= '</td>';
		kitchenItemBlock+= '</tr>';
		$("table.list-kitchen-log-holder").append(kitchenItemBlock);
	}
}

function getItemSpecificationRowForKitchen(itemInfo, taste = "regular", quantity = 0, extraSpecification = "", servedQty = 0) {
	var isServed = false;
	var isServedClass = "";
	if (quantity <= servedQty) {
		isServed = true;
		isServedClass = "bg-disabled";
	}
	var qtyToServe = toNumeric(quantity) - toNumeric(servedQty);
	var itemSpecificationsBlock = "<tr>";
	itemSpecificationsBlock+= "<td class='valign-top text-left dotted-border-sm'>";
	var quantityInfo = qtyToServe + "x ";
	if (itemInfo.qty == quantity) {
		quantityInfo = "";
	}
	if (qtyToServe > 0) {
		itemSpecificationsBlock+= "<span class='d-block text-sm badge-"+ taste +"-taste-item "+ isServedClass +"'>" + quantityInfo + taste.toString().toUpperCase() + "</span>";
		if (extraSpecification != "") {
			itemSpecificationsBlock+= "<span class='d-block'>" + extraSpecification + "</span>";
		}
	}
	if (servedQty > 0 && !isServed) {
		itemSpecificationsBlock+= "<span class='d-block badge-item-served'>"+ servedQty +"x "+ taste.toString().toUpperCase() +" SERVED</span>";
		if (extraSpecification != "") {
			itemSpecificationsBlock+= "<span class='d-block'>" + extraSpecification + "</span>";
		}
	} else if(isServed) {
		itemSpecificationsBlock+= "<span class='d-block badge-item-served'>"+ quantity +"x "+ taste.toString().toUpperCase() +" SERVED</span>";
		if (extraSpecification != "") {
			itemSpecificationsBlock+= "<span class='d-block'>" + extraSpecification + "</span>";
		}
	}
	itemSpecificationsBlock+= '</td>';
	if (!isServed) {
		itemSpecificationsBlock+= "<td class='valign-top dotted-border-sm text-center'>";
		itemSpecificationsBlock+= '<button class="btn-solid btn-kitchen-log-action remove-item" data-logid="' + itemInfo.logId + '" data-itemid="' + itemInfo.itemId + '" data-tableid="' + itemInfo.tableId + '" data-qty="' + qtyToServe + '" data-taste="' + taste + '" data-extraspecification = "' + extraSpecification + '" title="Mark as served"><img src="assets/img/icons/icon-serve.png" class="icon-xsmall" /></button>';
		itemSpecificationsBlock+= "</td>";
	}
	itemSpecificationsBlock+= "</tr>";
	return itemSpecificationsBlock;
}

// Fetch taxes info
function initTaxes(restaurant_id) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'GET', 
		{
			action : 'get-taxes',
			restaurant_id : restaurant_id,
		},
		function(taxes) {
			if (typeof taxes != 'undefined' && taxes.length > 0) {
				$(taxes).each(function(index, tax) {
					LocalDB.saveTax(tax);
				});
				renderCart();
			}
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

// Adjust table status as per the cart items
function adjustTableStatus() {
	if (LocalDB.cartItems().length > 0) {
		setTableStatus({
			'status' : 'B'
		});
	} else {
		setTableStatus({
			'status' : 'A'
		});
	}
}

// Trigger item added to cart for event handler
function triggerCartUpdated(tableId) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
		{
			action : 'trigger-cart-updated',
			restaurant_id : RESTAURANT.ID,
			cartInfo : JSON.stringify(LocalDB.cartItems(false, tableId)),
			tableId : tableId,
		},
		function(response) {
			adjustTableStatus();
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

// Trigger cart is empty for event handler
function triggerCartEmpty(tableId) {
	callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
		{
			action : 'trigger-cart-empty',
			restaurant_id : RESTAURANT.ID,
			tableId : tableId,
		},
		function(response) {
			LocalDB.setTableStatus(tableId, 'A');
            loadTables(LocalDB.tables());
		},
		function(error) {
			// No need to showcase our own mistakes
		}
	);
}

// Push items to kitchen with event handler
function pushCartItemsToKitchen(tableId = 0) {
	tableId = (typeof tableId != 'undefined' && !isNaN(tableId) && tableId > 0) ? tableId : LocalDB.getActiveTable();
	var cartItems = LocalDB.cartItems(false, tableId);
	// console.log(cartItems);
	if (cartItems.length > 0) {
		var itemInfoForitchen = [];
		$(cartItems).each(function(k, item) {
			item.qty = toNumeric(item.qty);
			item.servedQty = toNumeric(item.servedQty);
			var itemSpecifications = JSON.parse(item.specifications);
			var updatedItemSpecifications = [];
			var finalItemSpecifications = [];
			var avoidSpecificaionKeys = [];
			// console.log(itemSpecifications);
			$(itemSpecifications).each(function(spkey, specification) {
				if (toNumeric(specification.qty) == -1 && typeof itemSpecifications[spkey-1] != "undefined" && itemSpecifications[spkey-1].qty == 1 && itemSpecifications[spkey-1].taste == specification.taste && itemSpecifications[spkey-1].extraSpecification == specification.extraSpecification) {
					avoidSpecificaionKeys.push(spkey);
					avoidSpecificaionKeys.push(spkey-1);
				}
				if (toNumeric(specification.qty) <= toNumeric(specification.servedQty)) {
					// avoidSpecificaionKeys.push(spkey);
				}
				updatedItemSpecifications.push(specification);
			});
			// console.log(updatedItemSpecifications);
			var totalQtyToBeServed = 0;
			$(updatedItemSpecifications).each(function(uspkey, specification) {
				if ($.inArray(uspkey, avoidSpecificaionKeys) === -1) {
					totalQtyToBeServed+= specification.qty;
					finalItemSpecifications.push(specification);
				}
			});
			// console.log(finalItemSpecifications);
			var itemSpecificationsData = JSON.stringify(finalItemSpecifications);
			if ((item.qty - item.servedQty) >= 1) {
				var itemInfo = {
					'itemId' : item.itemId,
					'specifications' : itemSpecificationsData,
					'qty' : toNumeric(totalQtyToBeServed),
				};
				itemInfoForitchen.push(itemInfo);
			}
		});
		callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
			{
				action : 'trigger-kitchen-for-items',
				restaurant_id : RESTAURANT.ID,
				items : JSON.stringify(itemInfoForitchen),
				tableId : tableId,
			},
			function(response) {
				// Hanging alone
			},
			function(error) {
				// No need to showcase our own mistakes
			}
		);
	}
}

// Mark table items as served
function markItemsAsServed(itemsInfo, taste = 'no', extraSpecification = 'N/A', tableId = 0) {
	// console.log(itemsInfo)
	if (itemsInfo != null) {
		callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
			{
				action : 'trigger-mark-items-as-served',
				restaurant_id : RESTAURANT.ID,
				itemsInfo : JSON.stringify(itemsInfo),
				taste : taste,
				extraSpecification : extraSpecification,
				tableId : tableId
			},
			function(response) {
				// Hanging alone
			},
			function(error) {
				// No need to showcase our own mistakes
			}
		);
	}
}

function setLiveTableCartItems(tableId) {
	if (checkLogin()) {
		if (withTableAccess()) {
			if (LocalDB.getActiveTable() == tableId) {
				toggleWaitingLoader("show");
			}
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'get-table-cart-items',
					restaurant_id : RESTAURANT.ID,
					table_id : tableId
				},
				function(cartItems) {
					if (LocalDB.getActiveTable() == tableId) {
						toggleWaitingLoader("hide");
					}
					let tableStatus = 'A';
					if (typeof cartItems != 'undefined' && cartItems != null && typeof cartItems.items_log != 'undefined' && cartItems.items_log != '') {
						var cartItems = JSON.parse(cartItems.items_log);
						$(cartItems).each(function(cik, item) {
							cartItems[cik].specifications = JSON.stringify(cartItems[cik].specifications);
						});
						tableStatus = 'B';
					} else {
						var cartItems = [];
						tableStatus = 'A';				
					}
					LocalDB.set(LocalDB.getCartName(toNumeric(tableId)), JSON.stringify(cartItems));
					renderCart();
					LocalDB.setTableStatus(toNumeric(tableId), tableStatus);
                	loadTables(LocalDB.tables());
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    }
	} else {
		showLoginLayout(true);
	}
}

// Render live kitchen log
function renderLiveKitchenLog() {
	if (checkLogin()) {
		if (withAccess("kitchen")) {
			toggleWaitingLoader("show");
	        callAJAX(APP_API_ENDPOINT + 'api.php', 'POST', 
				{
					action : 'get-kitchen-log',
					restaurant_id : RESTAURANT.ID,
				},
				function(itemsLog) {
					$("button.btn-refresh-kitchen-log").removeClass("blink");
					$(".kot-log-title").removeClass("blink");
					$(".kot-log-title").removeClass("bg-kot-updated-block");
					toggleWaitingLoader("hide");
					if (itemsLog.length > 0) {
						$(itemsLog).each(function(lkey, log) {
							var items = JSON.parse(log.items_log);
							if (items.length > 0) {
	                          var itemIds = [];
	                          $(items).each(function(k, item) {
	                            var item = {
	                                'itemId' : toNumeric(item.itemId),
	                                'specifications' : JSON.stringify(item.specifications),
	                                'qty' : item.qty,
	                                'tableId' : toNumeric(log.table_id),
	                                'logId' : LocalDB.kitchenItems().length + 1
	                            };
	                            LocalDB.saveItemForKitchen(item);
	                            itemIds.push(item.itemId);
	                          });
	                          var kitchenItems = LocalDB.kitchenItems();
	                          var availableKitchenItems = [];
	                          $(kitchenItems).each(function(k, v) {
	                             if (v.tableId == log.table_id) {
	                                if ($.inArray(v.itemId, itemIds) >= 0) {
	                                    availableKitchenItems.push(v);
	                                }
	                             } else {
	                                availableKitchenItems.push(v);
	                             }
	                          });
	                          LocalDB.set(LocalDB.kitchenItemsInfo, JSON.stringify(availableKitchenItems));
	                          renderKitchenLog();
	                        }
						});
					} else {
						LocalDB.set(LocalDB.kitchenItemsInfo, JSON.stringify([]));
						renderKitchenLog();
					}
				},
				function(error) {
					// No need to showcase our own mistakes
				}
			);
	    } else {
	    	showNotification(NO_ACCESS_MSG, "error");
	    }
    } else {
        showLoginLayout(true);
    }
}