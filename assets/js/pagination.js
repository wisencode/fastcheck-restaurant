class Pagination {
	
	totalItems;
	itemsPerPage = (!isNaN(NUMBER_OF_ITEM_PER_PAGE) && NUMBER_OF_ITEM_PER_PAGE > 0) ? NUMBER_OF_ITEM_PER_PAGE : 10;
	currentPage;
	numberOfPages;
	items;

	constructor(items) {
		this.items = items;
		this.totalItems = items.length;
		this.numberOfPages = Math.ceil(this.totalItems / this.itemsPerPage);
	}

	inbounds(page) {
		var startOffset = (page - 1) * this.itemsPerPage;
		var endOffSet = startOffset + this.itemsPerPage;
		return {
			'start' : toNumeric(startOffset),
			'end' : toNumeric(endOffSet - 1)
		};
	}

	getPagesHtml(tableClass = 'table', callback = null) {
		var paginationLine = '<table class="pagination pagination-for-'+tableClass+'" cellspacing="5">';
						paginationLine+= '<tr>';
		for (var page = 1; page <= this.numberOfPages; page++) {
			var extraClass = '';
			if (page == 1) {
				extraClass = ' active';
			}
			var loadEntitiesFunc = callback.toString() + '('+ this.inbounds(page).start + ','+this.inbounds(page).end+','+page+')';
			paginationLine+= '<td class="dotted-border-sm text-center'+extraClass+'" data-page="'+ page +'" title="Page '+ page +'" onClick="'+loadEntitiesFunc+'";">';
			paginationLine+= page;
			paginationLine+= '</td>';
		}
		paginationLine+= '</tr>';
		paginationLine+= '</table>';
		return paginationLine;
	}

	static getPageSummary(start, end, page, length) {
		var pageSummary = '<span class="pagination-page-summary ml-px5 text-sm">';
		pageSummary+= 'Page ' + page;
		end = ((end + 1) <= length) ? (end + 1) : length;
		if ((start + 1) == end && (start+1) >= length) {
			pageSummary+= ' - showing last record out of ' + length + ' records';
		} else {
			pageSummary+= ' - showing ' + (start + 1) + ' to ' + end + ' records out of ' + length + ' records';
		}
		pageSummary+= '</span>';
		return pageSummary;
	}

	static managePagination(currentPage = 1, paginationTable = '') {
		if (isNaN(currentPage) || currentPage <= 0 || paginationTable == '') {
			return false;
		}
		var paginationTable = "table.pagination-for-" + paginationTable;
		var totalPages = $(paginationTable + " tr:first td:not(.navs)").length;
		$(paginationTable + " tr:first td.navs").remove();

		if (totalPages <= 5) {
			return false;
		}

		if (currentPage > 1) {
			$("<td class='navs navaction' data-navpage='"+(currentPage - 1)+"'><</td>").insertAfter($(paginationTable + " tr:first td:first"));
		}
		if (currentPage < totalPages) {
			$("<td class='navs navaction' data-navpage='"+(currentPage + 1)+"'>></td>").insertBefore($(paginationTable + " tr:first td:last"));
		}

		var pageStart = ((currentPage - 2) >= 1) ? (currentPage - 2) : 1;
		var pageEnd = ((currentPage + 2) <= totalPages) ? (currentPage + 2) : totalPages;

		if (currentPage < 5) {
			pageStart = 1;
			pageEnd = 5;
		}
		if ((currentPage + 4) >= totalPages) {
			pageStart = totalPages - 4;
			pageEnd = totalPages;
		}

		$(paginationTable + " tr td:not(.navs)").each(function() {
			let pageNo = $(this).attr("data-page");
			$(this).hide();
			if (pageNo >= pageStart && pageNo <= pageEnd) {
				$(this).show();
			}
		});

		if (currentPage == 1) {
			$(paginationTable + " tr td[data-page=1]:not(.navs)").removeClass("navaction").html(1).show();
		} else {
			$(paginationTable + " tr td[data-page=1]:not(.navs)").html("<<").addClass("navaction").show();
		}
		if (currentPage == totalPages) {
			$(paginationTable + " tr td[data-page="+totalPages+"]:not(.navs)").removeClass("navaction").html(totalPages).show();
		} else {
			$(paginationTable + " tr td[data-page="+totalPages+"]:not(.navs)").addClass("navaction").html(">>").show();
		}
	}

}