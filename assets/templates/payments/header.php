<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo APP_NAME; ?> - <?php echo $pageTitle; ?></title>
	<link rel="shortcut icon" href="../../assets/img/logo/favicon.png">
	<link rel="apple-touch-icon" href="../../assets/img/logo/apple-touch-icon.png">
	<style type="text/css">
		@import url('../../assets/css/font.css');
		@import url('../../assets/css/custom.css');
		body {
			background-color: #1d2026;
			color: #ffffff;
			font-family: 'Quicksand-Medium';
		}
		.container {
			margin-top: 5%;
			word-break: break-all;
		}
		.text-white {
			color: #ffffff;
		}
		.bg-theme {
	      background-color: #38323e;
	      color: #fff;
	      border-radius: 4px;
	      padding: 2px;
	    }
	    .container .page-note {
	    	margin-top: 5em;
	    	color: #dedede;
	    }
	    .d-block {
	    	display: block;
	    }
	    .action-box {
	    	margin-top: 2em;
	    	margin-bottom: 1em;
	    }
	    .btn {
	    	padding: 5px;
	    	border-radius: 5px;
	    	border-style: solid;
	    	border-color: transparent;
	    	font-family: 'Quicksand-Bold';
	    	cursor: pointer;
	    }
	    .btn.btn-green {
	    	background-color: #33a237;
	    	color: #fff;
	    }
		footer {
			position: fixed;
		    right: 0px;
		    text-align: left;
		    bottom: 10px;
		    background-color: #38323e;
		    padding-top: 2px;
		    padding-left: 2px;
		    padding-right: 2px;
		    padding-bottom: 4px;
		    border-radius: 3px;
		    bottom: 0px;
		}
		footer a {
			color: #c4def2;
			text-decoration: none;
		}
		img.logo {
			height: 80px;
			width: 80px;
		}
		@media only screen 
		  and (min-device-width: 320px) 
		  and (max-device-width: 480px)
		  and (-webkit-min-device-pixel-ratio: 2) {
		  	.container div.request-box {
				font-size: 22px;
			}
			.container .page-note {
				font-size: 12px;
				margin-top: 5em;
			}
			.btn {
				border-radius: 10px;
				font-size: 50px;
			}
			img.logo {
				height: 150px;
				width: 150px;
			}
		}
	</style>
</head>
<body>
	<center>
		<img src="../../assets/img/logo/logo.png" class="logo" />
	</center>