<style type="text/css">
	div.request-box {
		background-color: #f3fbff;
		border-radius: 4px;
		padding: 10px;
		color: #000000;
		word-break: break-all;
		width: fit-content;
	}
</style>
<div class="container">
	<center>
		<div class="request-box">
			<strong><?php echo $restaurant['name']; ?></strong> has requested <strong><?php echo number_format($order['order_amount'], 2).' '.$restaurant['currency_code']; ?></strong> from your FC ECard <strong>#<?php echo $fc_eCard_no; ?></strong>. Kindly click on below <strong>"Proceed to pay"</strong> button to accept this request
			<div class="d-block action-box">
				<button class="btn btn-green proceed-to-pay">Proceed to pay</button>
			</div>
		</div>
		<div class="page-note">
			<i>Note: Kindly contact us on <?php echo (!empty($restaurant['contact_number'])) ? $restaurant['contact_number'] : ' our restaurant counter'; ?> if you did not initiate this request</i>
		</div>
	</center>
</div>

<script type="text/javascript" src="../../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../../assets/js/fcpay.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("button.proceed-to-pay").on("click", function(e) {
			var paymentBtn = $(this);
			var prevHtml = $(paymentBtn).html();
			$(paymentBtn).html('Please wait...');
			callAJAX(APP_API_ENDPOINT + 'api.php', 'POST',
				{
					action : 'customer-pay-with-fc-ecard',
	                restaurant_id : '<?php echo Helper::encrypt($restaurant['id']); ?>',
	                FCECardNumber : '<?php echo Helper::encrypt($fc_eCard_no); ?>',
	                orderAmount : '<?php echo Helper::encrypt($order['order_amount']); ?>',
	                currency : '<?php echo Helper::encrypt($restaurant['currency_code']); ?>',
	                order_id : '<?php echo Helper::encrypt($order['id']); ?>',
	                pay_link : '<?php echo Helper::encrypt('true'); ?>',
				},
				function(response) {
					if (response.status == true) {
						$(paymentBtn).remove();
						$("div.request-box").html(response.message);
						showNotification(response.message, "info");
					} else {
						$(paymentBtn).html(prevHtml);
						showNotification(response.message, "error");
					}
				},
				function(error) {
					$(paymentBtn).html(prevHtml);
					showNotification("<?php echo MESSAGES['OOPS']; ?>", "error");
				}
			)
		});
	});
</script>