<div class="container">
	<center>
		<img src="../../assets/img/icons/icon-oops.png" />
		<h3 class="text-white"><?php echo $error; ?></h3>
		<p><small><?php echo $description ?? ''; ?></small></p>
	</center>
</div>