<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style type="text/css">
		body {
			font-family: 'Quicksand', sans-serif;
		}
		table.list, table.heading {
			border-collapse: collapse;
		}
		table.heading tr td {
			padding: 3px;
			border-color: silver;
			border-style: solid;
			font-size: 13px;
			text-align: left;
			border-width: thin;
		}
		table.list th {
			background-color: #2b756e;
			color: #fff;
			text-align: center;
			padding: 3px;
			border-color: #fff;
			border-style: solid;
			font-size: 14px;
		}
		table.list tr td {
			padding: 3px;
			text-align: center;
			border-color: #fff;
			border-style: solid;
			font-size: 13px;
			font-weight: normal;
		}
		table.list tr:nth-child(even) td {
			background-color: #dedede;
		}
		table.heading td:nth-child(even) {
			text-align: center !important;
		}
		.clear {
			clear: both;
			margin-top: 1em;
		}
		td.logo {
			border-color: transparent !important;
			border-style: solid !important;
			text-align: center !important;
			background-color: transparent !important;
			border-width: thin !important;
		}
		img.logo {
			display: block;
			height: 50px;
			width: 50px;
		}
		.d-block {
			display: block;
		}
		.restaurant-address, .restaurant-address {
			font-size: 12px;
			opacity: 0.7;
		}
		header {
			font-size: 12px;
			text-align: right;
			font-style: italic;
			opacity: 0.7;
		}
		footer {
			font-size: 12px;
			text-align: left;
			font-style: italic;
			margin-top: 1em;
			opacity: 0.7;
		}
		header a, footer a {
			text-decoration: none;
			color: #2b756e;
		}
		td.summary {
			background-color: #F1DFE1;
			color: #000;
			padding: 3px;
			border-radius: 2px;
			font-size: 13px;
		}
		.float-left {
			float: left !important;
			text-align: left !important;
		}
		.float-right {
			float: right !important;
			text-align: right !important;
		}
		tr.footer-summary td {
			background-color: #cee2f1 !important;
			color: #000000 !important;
			border: 1px dotted #ffffff !important;
		}
	</style>
</head>
<body>
	<header>
        <?php echo TAG_LINE_FOR_REPORT; ?>
    </header>
	<table class="heading" border="1" width="100%">
		<tr>
			<td class="text-center logo" colspan="3">
				<?php 
					$logoUrl = Helper::protocol().Helper::host().'/assets/img/uploads/'.$restaurant['id'].'/logo/'.$restaurant['logo'];
					$reportName = "CUSTOM REPORT";
					if (!empty($options)) {
						if (!empty($options->todayOnly) && $options->todayOnly) {
							$reportName = "REPORT OF ".date('d F Y');
						} else {
							$reportName = "FILTERED REPORT WITH PARAMS";
							$reportByParams = array_keys((array) $options);
							$reportByValues = array_values((array) $options);
							$reportSearchPhaseTable = "<table style='width: 100%'>";
							$reportSearchPhaseTable.= "<tr>";
							foreach ($reportByValues as $key => $searchValue) {
								if (!empty($searchValue) && $reportByParams[$key] != 'table_id') {
									$reportSearchPhaseTable.= "<td class='summary'>";
									$reportSearchPhaseTable.= strtoupper(str_replace("_", " ", $reportByParams[$key]));
									$reportSearchPhaseTable.= "</td>";
									$reportSearchPhaseTable.= "<td>";
									$reportSearchPhaseTable.= $searchValue;
									$reportSearchPhaseTable.= "</td>";
								}
							}
							$reportSearchPhaseTable.= "</tr>";
							$reportSearchPhaseTable.= "</table>";
						}
					}
				?>
				<span class="restaurant-name d-block"><?php echo $restaurant['name']; ?></span>
				<span class="restaurant-address d-block"><?php echo nl2br($restaurant['address']); ?></span>
				<h4 class="d-block"><?php echo $reportName; ?></h4>
				<?php 
					if (!empty($reportSearchPhaseTable)) {
						echo $reportSearchPhaseTable;
					}
				?>
			</td>
		</tr>
		<tr>
			<td class="summary">Total Orders : <?php echo $stats['total_orders_info']['total_orders']; ?></td>
			<td class="summary">Total Payment : <?php echo number_format($stats['total_orders_info']['order_amount'], 2).' '.$restaurant['currency_code']; ?></td>
			<?php 
				if (!empty($stats['report_date'])) {
					?>
						<td class="summary float-right">Generated Date : <?php echo date('d F Y', strtotime($stats['report_date'])); ?></td>
					<?php
				}
			?>
		</tr>
	</table>
	<div class="clear"></div>
	<table class="list" border="1" width="100%">
		<tr>
			<th>#</th>
			<th>Order No</th>
			<th>Amount (<?php echo $restaurant['currency_code']; ?>)</th>
			<th>Payment Method</th>
			<th>Table</th>
			<th>Date</th>
		</tr>
		<?php 
			if (!empty($orders)) {
				foreach ($orders as $key => $order) {
					?>
						<tr>
							<td><?php echo ($key + 1); ?></td>
							<td><?php echo $order['order_no']; ?></td>
							<td><?php echo number_format($order['order_amount'], 2); ?></td>
							<td><?php echo $order['payment_method']; ?></td>
							<td><?php echo $order['table_no']; ?></td>
							<td><?php echo date('d F Y h:i a', strtotime($order['created_at'])); ?></td>
						</tr>
					<?php
				}
			} else {
				?>
					<tr>
						<td colspan="6">No Orders</td>
					</tr>
				<?php
			}
		?>
	</table>
	<footer>
        <?php echo TAG_LINE_FOR_REPORT; ?>
    </footer>
</body>
</html>