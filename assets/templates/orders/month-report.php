<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style type="text/css">
		body {
			font-family: 'Quicksand', sans-serif;
		}
		table.list, table.heading {
			border-collapse: collapse;
		}
		table.heading tr td {
			padding: 3px;
			border-color: silver;
			border-style: solid;
			font-size: 13px;
			text-align: left;
			border-width: thin;
		}
		table.list th {
			background-color: #2b756e;
			color: #fff;
			text-align: left;
			padding: 3px;
			border-color: #fff;
			border-style: solid;
			font-size: 12px;
			padding-left: 10px;
		}
		table.list tr td {
			padding: 3px;
			text-align: left;
			border-color: #fff;
			border-style: solid;
			font-size: 13px;
			font-weight: normal;
			padding-left: 10px;
		}
		table.list tr:nth-child(even) td {
			background-color: #dedede;
		}
		table.heading td:nth-child(odd) {
			background-color: #dedede;
		}
		table.heading td:nth-child(even) {
			text-align: center !important;
		}
		.clear {
			clear: both;
			margin-top: 1em;
		}
		td.logo {
			border-color: transparent !important;
			border-style: solid !important;
			text-align: center !important;
			background-color: transparent !important;
			border-width: thin !important;
		}
		img.logo {
			display: block;
			height: 50px;
			width: 50px;
		}
		.d-block {
			display: block;
		}
		.restaurant-address, .restaurant-address {
			font-size: 12px;
			opacity: 0.7;
		}
		header {
			font-size: 12px;
			text-align: right;
			font-style: italic;
			opacity: 0.7;
		}
		footer {
			font-size: 12px;
			text-align: left;
			font-style: italic;
			opacity: 0.7;
			margin-top: 1em;
		}
		header a, footer a {
			text-decoration: none;
			color: #2b756e;
		}
		td.summary {
			background-color: #F1DFE1;
			color: #000;
			padding: 3px;
			border-radius: 2px;
			font-size: 13px;
		}
		tr.footer-summary td {
			background-color: #cee2f1 !important;
			color: #000000 !important;
			border: 1px dotted #ffffff !important;
		}
		td.text-center {
			text-align: center !important;
		}
		td.text-sm {
			font-size: small;
		}
		td.text-green {
			color: green;
		}
		td.text-red {
			color: red;
		}
		td.text-bold {
			font-weight: bold;
		}
	</style>
</head>
<body>
	<header>
        <?php echo TAG_LINE_FOR_REPORT; ?>
    </header>
	<table class="heading" border="1" width="100%">
		<tr>
			<td class="text-center logo" colspan="6">
				<?php 
					$logoUrl = Helper::protocol().Helper::host().'/assets/img/uploads/'.$restaurant['id'].'/logo/'.$restaurant['logo'];
				?>
				<span class="restaurant-name d-block"><?php echo $restaurant['name']; ?></span>
				<span class="restaurant-address d-block"><?php echo nl2br($restaurant['address']); ?></span>
				<h4 class="d-block">REPORT FOR <?php echo strtoupper($reportName); ?></h4>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
	<?php 
		$orderData = (new Order())->getOrderData($restaurant['id'], [
			'month' => $reportMonth,
			'year' => $reportYear,
		]);
		$taxStats = [];
		$taxNames = [];
		$subTotalStats = [];
		$isTaxable = false;
		foreach ($orderData as $order) {
			if (!empty($order)) {
				$date = $order['o_date'];
				$order_amount = 0;
				$order_info = json_decode($order['order_info']);
				if (!empty($order_info)) {
					foreach ($order_info as $item) {
						$order_amount+= $item->price * $item->qty;
					}
				}
				if (empty($subTotalStats[$date])) {
					$subTotalStats[$date] = 0;
				}
				$subTotalStats[$date]+= $order_amount;
				$discount_info = json_decode($order['discount_info']);
				if (!empty($discount_info)) {
					foreach ($discount_info as $discount) {
						if ($discount->type == "percentage") {
							if (!empty($discount->minimum_amount) && $discount->minimum_amount <= $order_amount) 
							{
								$discount->amount = (($order_amount * $discount->amount) / 100);
								$subTotalStats[$date]-= $discount->amount;
								$order_amount-= $discount->amount;
							} else if (empty($discount->minimum_amount)) {
								$discount->amount = (($order_amount * $discount->amount) / 100);
								$subTotalStats[$date]-= $discount->amount;
								$order_amount-= $discount->amount;
							}
						} else {
							if (!empty($discount->minimum_amount) && $discount->minimum_amount <= $order_amount)
							{
								$subTotalStats[$date]-= $discount->amount;
								$order_amount-= $discount->amount;
							} else if (empty($discount->minimum_amount)) {
								$subTotalStats[$date]-= $discount->amount;
								$order_amount-= $discount->amount;
							}
						}
					}
				}
				$taxes = json_decode($order['tax_info']);
				foreach ($taxes as $tax) {
					if (empty($tax->tax_type) || $tax->tax_type == "GOVRNMNT") {
						if (!$isTaxable) {
							$isTaxable = true;
						}
						$tax->name = preg_replace('/\s+/', '-', $tax->name);
						if (!in_array($tax->name, $taxNames)) {
							$taxNames[] = $tax->name;
						}
						if (empty($taxStats[$date][$tax->name])) {
							$taxStats[$date][$tax->name] = 0;
						}
						if ($tax->type == "percentage") {
							$tax->amount = (($order_amount * $tax->amount) / 100);
							$taxStats[$date][$tax->name]+= $tax->amount;
						} else {
							$taxStats[$date][$tax->name]+= $tax->amount;
						}
					}
				}
			}
		}
		$totalPayment = 0;
		$taxablePayment = 0;
		$taxStatsTotal = [];
		$totalTaxableAmount = 0;
		$totalOrderPayment = 0;
		if (!empty($reportLines) && $isTaxable) {
			$reportTable = '<table class="list" border="1" width="100%">';
			foreach ($taxNames as $taxName) {
				$taxStatsTotal[$taxName] = 0;
			}
			foreach ($reportLines as $key => $reportLine) {
				$dayTotalInclAmount = 0;
				$row = '';
				if ($key == 0) {
					$row = '<tr>';
					$row.= '<th width="5%">#</th>';
					$row.= '<th width="5%">Order/Bill Nos.</th>';
					$row.= '<th width="15%">Date</th>';
					$row.= '<th width="12%">Taxable Amount ('.$restaurant['currency_code'].')</th>';
					foreach ($taxNames as $taxName) {
						$row.= '<th>'.$taxName.' ('.$restaurant['currency_code'].')</th>';
					}
					$row.= '<th width="12%">Amount Incl. Tax ('.$restaurant['currency_code'].')</th>';
					$row.= '</tr>';
				}
				$row.= '<tr>';
				$row.= '<td>';
				$row.= ($key + 1);
				$row.= '</td>';
				$row.= '<td class="text-center">';
				if ($reportLine['bill_no_from'] != $reportLine['bill_no_to']) {
					$row.= $reportLine['bill_no_from']."-".$reportLine['bill_no_to'];
				} else {
					$row.= $reportLine['bill_no_from'];
				}
				$row.= '</td>';
				$row.= '<td>';
				$row.= $reportLine['date'];
				$row.= '</td>';
				$row.= '<td>';
				if (!empty($subTotalStats[$reportLine['o_date']])) {
					$row.= number_format($subTotalStats[$reportLine['o_date']], 2);
					$dayTotalInclAmount+= $subTotalStats[$reportLine['o_date']];
				}
				$row.= '</td>';
				foreach ($taxNames as $taxName) {
					if (empty($taxStats[$reportLine['o_date']][$taxName])) {
						$taxStats[$reportLine['o_date']][$taxName] = 0;
					}
					$row.= '<td>'.number_format($taxStats[$reportLine['o_date']][$taxName], 2).'</td>';
					$taxStatsTotal[$taxName]+= $taxStats[$reportLine['o_date']][$taxName];
					$dayTotalInclAmount+= $taxStats[$reportLine['o_date']][$taxName];
				}
				$row.= '<td>';
				$row.= number_format($dayTotalInclAmount, 2);
				$row.= '</td>';
				$row.= '</tr>';
				$reportTable.= $row;
				$totalPayment+= $dayTotalInclAmount;
				if (!empty($subTotalStats[$reportLine['o_date']])) {
					$totalTaxableAmount+= $subTotalStats[$reportLine['o_date']];
				}
			}
			$reportTable.= '<tr class="empty-row">';
			$reportTable.= '<td colspan="'.(4 + count($taxNames)).'"></td>';
			$reportTable.= '</tr>';
			$reportTable.= '<tr class="footer-summary">';
			$reportTable.= '<td colspan="3"></td>';
			$reportTable.= '<td>';
			$reportTable.= number_format($totalTaxableAmount, 2);
			$reportTable.= '</td>';
			foreach ($taxNames as $taxName) {
				$reportTable.= '<td>'.number_format($taxStatsTotal[$taxName], 2).'</td>';
			}
			$reportTable.= '<td>';
			$reportTable.= number_format($totalPayment, 2);
			$reportTable.= '</td>';
			$reportTable.= '</tr>';
			$reportTable.= '</table>';
		} else {
			foreach ($reportLines as $key => $reportLine) {
				$totalOrderPayment+= $reportLine['total_payment'];
			}
			$reportTable = '<table class="list" border="1" width="100%">';
			if ($totalOrderPayment > 0) {
				$reportTable.= '<tr>';
				$reportTable.= '<td class="text-center text-red">';
				$reportTable.= '<i>THIS MONTH REPORT IS NOT TAXABLE</i>';
				$reportTable.= '</td>';
				$reportTable.= '</tr>';
				$reportTable.= '<tr>';
				$reportTable.= '<td class="text-center text-sm text-green text-bold">';
				$reportTable.= '<i>All '.number_format($totalOrderPayment, 2).' ('.$restaurant['currency_code'].') is not taxable</i>';
				$reportTable.= '</td>';
				$reportTable.= '</tr>';
				$reportTable.= '</table>';
				$reportTable.= '<table class="list" border="1" width="100%">';
				$row = '<tr>';
				$row.= '<th width="5%">#</th>';
				$row.= '<th width="5%">Order/Bill Nos.</th>';
				$row.= '<th width="15%">Date</th>';
				$row.= '<th width="12%">Amount ('.$restaurant['currency_code'].')</th>';
				$row.= '</tr>';
				$reportTable.= $row;
				foreach ($reportLines as $key => $reportLine) {
					$row = '<tr>';
					$row.= '<td>';
					$row.= ($key + 1);
					$row.= '</td>';
					$row.= '<td class="text-center">';
					if ($reportLine['bill_no_from'] != $reportLine['bill_no_to']) {
						$row.= $reportLine['bill_no_from']."-".$reportLine['bill_no_to'];
					} else {
						$row.= $reportLine['bill_no_from'];
					}
					$row.= '</td>';
					$row.= '<td>';
					$row.= $reportLine['date'];
					$row.= '</td>';
					$row.= '<td>';
					$row.= $reportLine['total_payment'];
					$row.= '</td>';
					$reportTable.= $row;
				}
			} else {
				$row = '<tr>';
				$row.= '<td class="text-center text-red text-bold">';
				$row.= 'No Data available';
				$row.= '</td>';
				$row.= '</tr>';
				$reportTable.= $row;
			}
			$reportTable.= '</table>';
		}
	?>
	<?php if ($isTaxable) { ?>
		<table width="100%">
			<td class="summary">Total Payment (Incl. Tax) : <?php echo number_format($totalPayment, 2).' ('.$restaurant['currency_code'].')'; ?></td>
		</table>
	<?php } ?>
	<?php 
		if (!empty($reportTable)) {
			echo $reportTable;
		} else {
			?>
				<table class="list" border="1" width="100%">
					<tr>
						<td class="text-center text-red text-bold">No Data available</td>
					</tr>
				</table>
			<?php
		}
	?>
	<footer>
        <?php echo TAG_LINE_FOR_REPORT; ?>
    </footer>
</body>
</html>