<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<style type="text/css">
		body {
			font-family: 'Quicksand', sans-serif;
		}
		table.list, table.heading {
			border-collapse: collapse;
		}
		table.heading tr td {
			padding: 3px;
			border-color: silver;
			border-style: solid;
			font-size: 13px;
			text-align: left;
			border-width: thin;
		}
		table.list th {
			background-color: #2b756e;
			color: #fff;
			text-align: left;
			padding: 3px;
			border-color: #fff;
			border-style: solid;
			font-size: 14px;
			padding-left: 10px;
		}
		table.list tr td {
			padding: 3px;
			text-align: left;
			border-color: #fff;
			border-style: solid;
			font-size: 13px;
			font-weight: normal;
			padding-left: 10px;
		}
		table.list tr:nth-child(even) td {
			background-color: #dedede;
		}
		table.heading td:nth-child(odd) {
			background-color: #dedede;
		}
		table.heading td:nth-child(even) {
			text-align: center !important;
		}
		.clear {
			clear: both;
			margin-top: 1em;
		}
		td.logo {
			border-color: transparent !important;
			border-style: solid !important;
			text-align: center !important;
			background-color: transparent !important;
			border-width: thin !important;
		}
		img.logo {
			display: block;
			height: 50px;
			width: 50px;
		}
		.d-block {
			display: block;
		}
		.restaurant-address, .restaurant-address {
			font-size: 12px;
			opacity: 0.7;
		}
		header {
			font-size: 12px;
			text-align: right;
			font-style: italic;
			opacity: 0.7;
		}
		footer {
			font-size: 12px;
			text-align: left;
			font-style: italic;
			opacity: 0.7;
			margin-top: 1em;
		}
		header a, footer a {
			text-decoration: none;
			color: #2b756e;
		}
		td.summary {
			background-color: #F1DFE1;
			color: #000;
			padding: 3px;
			border-radius: 2px;
			font-size: 13px;
		}
		tr.footer-summary td {
			background-color: #cee2f1 !important;
			color: #000000 !important;
			border: 1px dotted #ffffff !important;
		}
	</style>
</head>
<body>
	<header>
        <?php echo TAG_LINE_FOR_REPORT; ?>
    </header>
	<table class="heading" border="1" width="100%">
		<tr>
			<td class="text-center logo" colspan="6">
				<?php 
					$logoUrl = Helper::protocol().Helper::host().'/assets/img/uploads/'.$restaurant['id'].'/logo/'.$restaurant['logo'];
				?>
				<span class="restaurant-name d-block"><?php echo $restaurant['name']; ?></span>
				<span class="restaurant-address d-block"><?php echo nl2br($restaurant['address']); ?></span>
				<h4 class="d-block">REPORT FOR <?php echo strtoupper($reportYear); ?></h4>
			</td>
		</tr>
	</table>
	<div class="clear"></div>
	<?php 
		if (!empty($reportLines)) {
			$reportTable = '<table class="list" border="1" width="100%">';
			$totalPayment = 0;
			foreach ($reportLines as $key => $reportLine) {
				$row = '';
				if ($key == 0) {
					$row = '<tr>';
					$row.= '<th width="5%">#</th>';
					$row.= '<th width="25%">Month</th>';
					$row.= '<th width="25%">Amount ('.$restaurant['currency_code'].')</th>';
					$row.= '</tr>';
				}
				$row.= '<tr>';
				$row.= '<td>';
				$row.= ($key + 1);
				$row.= '</td>';
				$row.= '<td>';
				$row.= $reportLine['month'];
				$row.= '</td>';
				$row.= '<td>';
				$row.= number_format($reportLine['total_payment'], 2);
				$row.= '</td>';
				$row.= '</tr>';
				$reportTable.= $row;
				$totalPayment+= $reportLine['total_payment'];
			}
			$reportTable.= '</table>';
		}
	?>
	<table width="100%">
		<td class="summary">Total Payment : <?php echo number_format($totalPayment, 2).' ('.$restaurant['currency_code'].')'; ?></td>
	</table>
	<?php 
		echo $reportTable;
	?>
	<footer>
        <?php echo TAG_LINE_FOR_REPORT; ?>
    </footer>
</body>
</html>