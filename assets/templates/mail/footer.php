 <?php
  $fastchecklogoUrl = Helper::protocol().Helper::host().'/assets/img/logo/logo.png';
 ?>
 <tr>
  <td style="padding:30px;text-align:center;font-size:12px;background-color:#4b464f;color:#f5f5f5;">
    <p style="margin:0 0 0 0;">
      <a href="https://fastcheck.io/" style="text-decoration:none;color: #ffffff;">
        <img src="<?php echo $fastchecklogoUrl ?>" width="50" height="50" alt="<?php echo APP_NAME; ?>" title="<?php echo APP_NAME; ?>" style="display:inline-block;color:#f5f5f5;text-align: center;" />
        <p style="margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;font-weight:normal;letter-spacing:-0.02em;text-align: center;">Thank you for using <?php echo ucfirst(strtolower(APP_NAME)); ?> services</p>
      </a> 
    </p>
    <p style="margin:0;font-size:14px;line-height:20px;color: #f5f5f5;">Powered By <a class="unsub" href="<?php echo APP_POWERED_BY['WEBSITE'] ?>" style="color:#f5f5f5;text-decoration:underline;"><?php echo APP_POWERED_BY['NAME'] ?></a></p>
  </td>
</tr>