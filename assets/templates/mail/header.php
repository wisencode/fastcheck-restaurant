<?php 
  if (!empty($restaurant['logo'])) {
    $logoUrl = Helper::protocol().Helper::host().'/assets/img/uploads/'.$restaurant['id'].'/logo/'.$restaurant['logo'];
  }
?>
<tr>
  <td style="padding:40px 30px 30px 30px;text-align:center;font-size:24px;font-weight:normal;color: #ffffff;">
    <?php 
      if (!empty($restaurant['logo'])) {
        ?>
          <img src="<?php echo $logoUrl ?>" width="80" alt="Logo" style="width:80px;max-width:80%;height:auto;border:none;">
        <?php
      }
    ?>
    <span style="display: block;"><?php echo $restaurant['name']; ?></span>
    <span style="font-size: small;"><?php echo $restaurant['address']; ?></span>
  </td>
</tr>