<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <title>FC Card Transaction Log</title>
  <style>
    table.transactionLog {
      width: 100%;
      margin:0px;
      padding-left:30px;
      padding-right: 30px;
      background-color:#ffffff;
      font-size: 12px;
      border:none;
    }
    table.transactionLog td, table.transactionLog th {
      padding: 2px;
    }
    .text-center {
      text-align: center;
    }
    .text-left {
      text-align: left;
    }
    .text-right {
      text-align: right;
    }
    .text-bold {
      font-weight: bold;
    }
    .bg-red {
      background-color: #CD5C5C;
      color: #ffffff;
      border-radius: 4px;
      padding: 2px;
    }
    .bg-green {
      background-color: #3CB371;
      color: #ffffff;
      border-radius: 4px;
      padding: 2px;
    }
    .bg-theme {
      background-color: #38323e;
      color: #fff;
      border-radius: 4px;
      padding: 2px;
    }
    .dotted-border {
      border:  1px dotted #dedede;
    }
    @media screen and (max-width: 530px) {
      .unsub {
        display: block;
        padding: 8px;
        margin-top: 14px;
        border-radius: 6px;
        background-color: #555555;
        text-decoration: none !important;
        font-weight: bold;
      }
      .col-lge {
        max-width: 100% !important;
      }
      td.qr div, td.qr div img {
        height: 100px !important;
        width: 100px !important;
      }
    }
    @media screen and (min-width: 531px) {
      .col-sml {
        max-width: 27% !important;
      }
      .col-lge {
        max-width: 73% !important;
      }
    }
  </style>
</head>
<body style="margin:0;padding:0;word-spacing:normal;background-color:#939297;">
  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#4b464f;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <table role="presentation" style="width:100%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <?php include_once 'header.php'; ?>
            <tr>
              <td style="padding-top:0px;background-color:#ffffff;">
                <h6 style="padding:30px;margin-top:0;margin-bottom:0px;font-size:12px;font-weight:bold;letter-spacing:-0.02em;">
                  Hey Customer! <br><br>
                  Thank you for using our services. You've new transaction in your FC-ECard #<?php echo $fcECardTransaction['ecard_no']; ?>. You can see details regarding to this transaction at below:
                </h6>
              </td>
            </tr>
            <tr>
              <td style="padding-top:0px;background-color:#ffffff;">
                <table class="transactionLog" width="100%" cellpadding="5">
                  <tr>
                    <th class="dotted-border text-center">Amount <?php echo "(".$restaurant['currency_code'].")"; ?></th>
                    <th class="dotted-border text-center">Trans. Type</th>
                    <th class="dotted-border text-center">Prev. Balance <?php echo "(".$restaurant['currency_code'].")"; ?></th>
                    <th class="dotted-border text-center">Current Balance <?php echo "(".$restaurant['currency_code'].")"; ?></th>
                    <th class="dotted-border text-center">Date</th>
                  </tr>
                  <?php 
                    $newBalance = 0;
                    $transType = '-';
                    if ($fcECardTransaction['transaction_type'] == 'RCHRG') {
                      $transType = '<span class="text-bold bg-green">Recharge(+)</span>';
                      $newBalance = $fcECardTransaction['prev_balance'] + $fcECardTransaction['transaction_amount'];
                    } else if ($fcECardTransaction['transaction_type'] == 'INTRCHRG') {
                      $transType = '<span class="text-bold bg-green">Initial Recharge(+)</span>';
                      $newBalance = $fcECardTransaction['prev_balance'] + $fcECardTransaction['transaction_amount'];
                    } else if ($fcECardTransaction['transaction_type'] == 'ORDER') {
                      $transType = '<span class="text-bold bg-red">ORDER(-)</span>';
                      $newBalance = $fcECardTransaction['prev_balance'] - $fcECardTransaction['transaction_amount'];
                    }
                  ?>
                  <tr>
                    <td class="dotted-border text-center text-bold"><?php echo $fcECardTransaction['transaction_amount']; ?></td>
                    <td class="dotted-border text-center" style="width: 30%;"><?php echo $transType; ?></td>
                    <td class="dotted-border text-center"><?php echo number_format($fcECardTransaction['prev_balance'], 2); ?></td>
                    <td class="dotted-border text-center"><span class="text-bold bg-theme"><?php echo number_format($newBalance, 2); ?></span></td>
                    <td class="dotted-border text-left"><?php echo date('d F Y h:i:s a', strtotime($fcECardTransaction['trans_date'])); ?></td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding-left:30px;padding-right:30px;background-color:#ffffff;">
                <p style="margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;letter-spacing:-0.02em;text-align: center;color: #795548a6;font-weight: bold;"><i>Note: This card (<?php echo $fcECardTransaction['ecard_no']; ?>) is valid for <?php echo $restaurant['name']; ?> only</i></p>
              </td>
            </tr>
            <?php if ($fcECardTransaction['transaction_type'] == 'ORDER' && !empty($fcECardTransaction['order_id'])) {
              $orderInfo = (new Order())->orderBaseInfo(
                [
                  'order_id' => $fcECardTransaction['order_id'],
                  'restaurant_id' => $restaurant['id'],
                ]
              );
              if (!empty($orderInfo)) {
                $orderTable = (new Table())->baseInfo($restaurant['id'], $orderInfo['table_id']);
                ?>
                  <tr>
                    <td style="padding-top: 15px;padding-left:30px;padding-right:30px;background-color:#ffffff;">
                      <p style="margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;letter-spacing:-0.02em;text-align: left;color: grey;font-weight: bold;"><i>Order Summary</i></p>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-top: 0px;background-color: #ffffff;">
                      <table class="transactionLog" width="100%" cellpadding="0">
                        <tr>
                          <th class="dotted-border text-left">Order #<?php echo $orderInfo['order_no']; ?></th>
                          <th class="dotted-border text-left">Table #<?php echo $orderTable['table_no']." [".$orderTable['area']."]"; ?></th>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <?php 
                    $orderItems = json_decode($orderInfo['order_info']);
                    $orderTotal = 0;
                    if (!empty($orderItems)) {
                      ?>
                        <tr>
                          <td style="padding-top: 0px;background-color: #ffffff;">
                            <table class="transactionLog" cellpadding="0">
                              <tr>
                                <th class="dotted-border text-center">Item</th>
                                <th class="dotted-border text-center">Qty</th>
                                <th class="dotted-border text-center">Rate (<?php echo "(".$restaurant['currency_code'].")"; ?>)</th>
                                <th class="dotted-border text-center">Total (<?php echo "(".$restaurant['currency_code'].")"; ?>)</th>
                              </tr>
                              <?php
                              foreach ($orderItems as $item) {
                                ?>
                                      <tr>
                                        <td class="dotted-border text-center"><?php echo $item->name; ?></td>
                                        <td class="dotted-border text-center"><?php echo $item->qty; ?></td>
                                        <td class="dotted-border text-center"><?php echo number_format($item->price, 2); ?></td>
                                        <td class="dotted-border text-center text-bold"><?php echo number_format(($item->qty * $item->price), 2); ?></td>
                                      </tr>
                                <?php
                                $orderTotal+= $item->qty * $item->price;
                              }
                              ?>
                              <tr>
                                <td colspan="3" class="dotted-border text-right text-bold">Subtotal</td>
                                <td class="dotted-border text-center text-bold"><?php echo number_format($orderTotal, 2); ?></td>
                              </tr>
                      <?php
                    }
                  ?>
                <?php
                    $orderDiscounts = json_decode($orderInfo['discount_info']);
                    $orderTotalBeforeDiscount = $orderTotal;
                    if (!empty($orderDiscounts)) {
                              foreach ($orderDiscounts as $discount) {
                                $dicountAmount = 0;
                                $discountTypeLabel = '';
                                if ($discount->type == 'percentage') {
                                  $dicountAmount = (($orderTotalBeforeDiscount * $discount->amount)) / 100;
                                  $discountTypeLabel = '['.$discount->amount.'%]';
                                }
                                if ($discount->type == 'flat') {
                                  $dicountAmount = $discount->amount;
                                }
                                $orderTotalBeforeDiscount-= number_format($dicountAmount, 2);
                                $orderTotal-= number_format($dicountAmount, 2);
                                ?>
                                  <tr>
                                    <td colspan="3" class="dotted-border text-right text-bold"><?php echo $discount->description.$discountTypeLabel; ?></td>
                                    <td class="dotted-border text-center text-bold"><?php echo number_format($dicountAmount, 2); ?></td>
                                  </tr>
                                <?php
                              }
                              ?>
                            <tr>
                              <td colspan="3" class="dotted-border text-right text-bold">Total (Excl. Tax)</td>
                              <td class="dotted-border text-center text-bold"><?php echo number_format($orderTotal, 2); ?></td>
                            </tr>
                      <?php
                    }
                    $orderTaxes = json_decode($orderInfo['tax_info']);
                    $orderTotalAfterDiscount = $orderTotal;
                    if (!empty($orderTaxes)) {
                              foreach ($orderTaxes as $tax) {
                                $taxAmount = 0;
                                $taxTypeLabel = '';
                                if ($tax->type == 'percentage') {
                                  $taxAmount = (($orderTotalAfterDiscount * $tax->amount)) / 100;
                                  $taxTypeLabel = '['.$tax->amount.'%]';
                                }
                                if ($tax->type == 'flat') {
                                  $taxAmount = $tax->amount;
                                }
                                $orderTotal+= number_format($taxAmount, 2);
                                ?>
                                  <tr>
                                    <td colspan="3" class="dotted-border text-right text-bold"><?php echo $tax->name.$taxTypeLabel; ?></td>
                                    <td class="dotted-border text-center text-bold"><?php echo number_format($taxAmount, 2); ?></td>
                                  </tr>
                                <?php
                              }
                              ?>
                            <?php 
                              $rounded = round($orderTotal);
                              $roundedBy = $rounded - $orderTotal;
                            ?>
                            <tr>
                              <td colspan="3" class="dotted-border text-right text-bold">Rounded By </td>
                              <td class="dotted-border text-center text-bold"><?php echo number_format($roundedBy, 2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="3" class="dotted-border text-right text-bold">Grand Total</td>
                              <td class="dotted-border text-center text-bold"><?php echo number_format($rounded, 2); ?></td>
                            </tr>
                      <?php
                    }
                    ?>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td style="padding: 30px;background-color:#ffffff;">
                              <p style="margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;letter-spacing:-0.02em;text-align: center;color: grey;"><i>You are getting this email because your email is linked with this card. For any concern contact us on <strong><?php echo $restaurant['contact_number']; ?></strong></i></p>
                            </td>
                          </tr>
                    <?php
              }
            } ?>
           <?php include_once 'footer.php' ?>
          </table>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>