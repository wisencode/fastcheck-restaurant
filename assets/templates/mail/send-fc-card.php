<?php
  $qrimageUrl = 'https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl='.$FCECard['ecard_no'].'&choe=UTF-8';
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <title>FC Card</title>
  <style>
    .text-green {
      background-color: #38323e;
      color: #7effb8;
      border-radius: 4px;
      padding: 2px;
    }
    .bg-theme {
      background-color: #38323e;
      color: #fff;
      border-radius: 4px;
      padding: 2px;
    }
    @media screen and (max-width: 530px) {
      .unsub {
        display: block;
        padding: 8px;
        margin-top: 14px;
        border-radius: 6px;
        background-color: #555555;
        text-decoration: none !important;
        font-weight: bold;
      }
      .col-lge {
        max-width: 100% !important;
      }
      td.qr div, td.qr div img {
        height: 100px !important;
        width: 100px !important;
      }
    }
    @media screen and (min-width: 531px) {
      .col-sml {
        max-width: 27% !important;
      }
      .col-lge {
        max-width: 73% !important;
      }
    }
  </style>
</head>
<body style="margin:0;padding:0;word-spacing:normal;background-color:#939297;">
  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#4b464f;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <?php include_once 'header.php'; ?>
            <tr>
              <td style="padding-top:0px;background-color:#ffffff;">

                <h6 style="padding:30px;margin-top:0;margin-bottom:0px;font-size:12px;font-weight:bold;letter-spacing:-0.02em;">
                  Hey Customer! <br><br>
                  Your Fastcheck e-card <span class="bg-theme"><?php echo $FCECard['ecard_no']; ?></span> for <?php echo $restaurant['name'] ?> has been successfully generated with initial balance <span class="text-green"><?php echo number_format($FCECard['current_balance'], 2)." ".$restaurant['currency_code']; ?></span>. Please keep the below details at some secure place in case if you loose your card or want to do any transaction with it. Your card details are as below:
                </h6>
              </td>
            </tr>
            <tr>
              <td style="padding:10px;font-size:15px;line-height:20px;font-weight:bold;background-color:#ffffff;border-bottom:1px solid #f0f0f5;border-color:rgba(201,201,207,.35);">
                  
                  <table width="100%" class="table-fcECard-info" style="background-color:#46413f;color:white;padding-bottom:2px;">
                    <tbody><tr>
                      <td class="block-fc-eCard-container valign-top m-0-f p-0-f qr" style="padding: 10px;" width="30%" valign="top">
                        <div id="block-fc-eCard" style="width: 100px;">
                          <img src="<?php echo $qrimageUrl ; ?>" height="120px" width="120px">
                        </div>
                      </td>
                      <td class="valign-top" style="text-align: left;" valign="top">
                        <table style="color: #fff;">
                          <tbody><tr>
                            <td class="text-left valign-top" style="padding-top: 10px;">
                              <span class="restaurant-name"><?php echo $restaurant['name'] ?></span>
                            </td>
                          </tr>
                          <tr>
                            <td class="text-left fc-eCard-no"><?php echo $FCECard['ecard_no'] ?></td>
                          </tr>
                          <tr>
                            <td class="text-left fc-eCard-holder-display-name" style="letter-spacing: 5px;"><?php echo $FCECard['display_name'] ?></td>
                          </tr>
                          <tr><td class="text-x-sm text-left text-grey"><i>Generated on <span class="fc-eCard-date"><?php echo date('d F Y', strtotime($FCECard['created_at'])); ?></span></i></td></tr>
                          <tr><td class="text-x-sm text-left text-grey-dark"><i>Powered by <span class="app-name">Fastcheck</span></i></td></tr>
                        </tbody></table>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                <p style="margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;letter-spacing:-0.02em;text-align: center;color: #38323e;font-weight: bold;"><i>Note: This card will be valid for <?php echo $restaurant['name']; ?> only. You are getting this email because your email is linked with this card.</i></p>
              </td>
            </tr>
           <?php include_once 'footer.php' ?>
          </table>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>