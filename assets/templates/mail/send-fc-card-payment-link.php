<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta name="x-apple-disable-message-reformatting">
  <title>FC Card Payment Link</title>
  <style>
    .text-green {
      background-color: #38323e;
      color: #7effb8;
      border-radius: 4px;
      padding: 2px;
    }
    .bg-theme {
      background-color: #38323e;
      color: #fff;
      border-radius: 4px;
      padding: 2px;
    }
    .btn {
      padding: 5px;
      border-radius: 5px;
      border-style: solid;
      border-color: transparent;
      cursor: pointer;
    }
    .btn.btn-green {
      background-color: #33a237;
      color: #fff;
    }
    a.btn {
      text-decoration: none;
    }
    @media screen and (max-width: 530px) {
      .unsub {
        display: block;
        padding: 8px;
        margin-top: 14px;
        border-radius: 6px;
        background-color: #555555;
        text-decoration: none !important;
        font-weight: bold;
      }
      .col-lge {
        max-width: 100% !important;
      }
      td.qr div, td.qr div img {
        height: 100px !important;
        width: 100px !important;
      }
    }
    @media screen and (min-width: 531px) {
      .col-sml {
        max-width: 27% !important;
      }
      .col-lge {
        max-width: 73% !important;
      }
    }
  </style>
</head>
<body style="margin:0;padding:0;word-spacing:normal;background-color:#939297;">
  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#4b464f;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <?php include_once 'header.php'; ?>
            <tr>
              <td style="padding-top:0px;background-color:#ffffff;">
                <h6 style="padding:30px;padding-bottom: 10px;margin-top:0;margin-bottom:0px;font-size:12px;font-weight:bold;letter-spacing:-0.02em;">
                  Hey Customer! <br><br>
                  <strong><?php echo $restaurant['name']; ?></strong> has requested <strong><?php echo number_format($orderAmount, 2).' '.$restaurant['currency_code']; ?></strong> from your FC ECard <strong>#<?php echo $FCECardNumber; ?></strong>. Kindly click on below link to verify your payment
                </h6>
                <p style="text-align: center;">
                  <a class="btn btn-green proceed-to-pay" href="<?php echo $pay_link; ?>">Verify Payment</a>
                </p>
                <p style="padding-left: 30px;padding-right: 30px;margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;letter-spacing:-0.02em;text-align: center;font-weight: bold;">
                  In any case if above button is not working then use this link
                  <a href="<?php echo $pay_link; ?>" style="color: blue;text-decoration: none;">
                    <?php echo $pay_link; ?>
                  </a> to verify your payment
                </p>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                <p style="margin-top:0;margin-bottom:0px;font-size:12px;line-height:20px;letter-spacing:-0.02em;text-align: center;color: #38323e;font-weight: bold;"><i>Note: You are getting this email because your email is linked with this card.</i></p>
              </td>
            </tr>
           <?php include_once 'footer.php' ?>
          </table>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>